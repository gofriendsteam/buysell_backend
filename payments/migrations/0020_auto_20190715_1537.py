# Generated by Django 2.1.7 on 2019-07-15 15:37

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('payments', '0019_rozetkainvoices_rozetkapayments_rozetkareportfiletmpurl'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='RozetkaPayments',
            new_name='RozetkaTransactionHistory',
        ),
    ]
