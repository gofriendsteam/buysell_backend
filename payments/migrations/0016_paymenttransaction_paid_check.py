# Generated by Django 2.1.7 on 2019-07-09 11:22

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('payments', '0015_auto_20190708_1741'),
    ]

    operations = [
        migrations.AddField(
            model_name='paymenttransaction',
            name='paid_check',
            field=models.FileField(blank=True, null=True, upload_to='payments/paid_check', verbose_name='Файл с подтверждение оплаты счет фактуры'),
        ),
    ]
