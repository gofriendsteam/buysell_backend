# Generated by Django 2.1.7 on 2020-02-13 16:20

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('payments', '0057_auto_20200213_1615'),
    ]

    operations = [
        migrations.AddField(
            model_name='paymenttransaction',
            name='invoice_id',
            field=models.CharField(blank=True, max_length=50, null=True, verbose_name='Invoice id'),
        ),
    ]
