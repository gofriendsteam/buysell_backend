# Generated by Django 2.1.7 on 2019-07-24 12:01

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('payments', '0028_auto_20190722_1757'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='paymenttransactionpocketliqpayproxy',
            options={'verbose_name': 'Покупка пакета', 'verbose_name_plural': 'Покупка пакетов'},
        ),
    ]
