# Generated by Django 2.1.7 on 2019-07-03 14:47

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('payments', '0003_applicationtounbalance_paymentsettings'),
    ]

    operations = [
        migrations.AddField(
            model_name='paymenttransaction',
            name='commission_credit',
            field=models.CharField(blank=True, max_length=500, null=True, verbose_name='Комиссия с получателя в валюте'),
        ),
        migrations.AddField(
            model_name='paymenttransaction',
            name='commission_debit',
            field=models.CharField(blank=True, max_length=500, null=True, verbose_name='Комиссия с отправителя в валюте'),
        ),
        migrations.AlterField(
            model_name='paymenttransaction',
            name='card_token',
            field=models.CharField(blank=True, max_length=500, null=True, verbose_name='Token карты оправителя'),
        ),
    ]
