# Generated by Django 2.1.7 on 2019-07-09 11:32

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('payments', '0016_paymenttransaction_paid_check'),
    ]

    operations = [
        migrations.AddField(
            model_name='paymenttransaction',
            name='is_approved',
            field=models.BooleanField(default=False, verbose_name='Подтверждение оплаты'),
        ),
    ]
