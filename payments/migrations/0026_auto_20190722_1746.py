# Generated by Django 2.1.7 on 2019-07-22 17:46

from django.conf import settings
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('payments', '0025_rozetkatransactionhistory_log_id'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='rozetkainvoices',
            unique_together={('user', 'id')},
        ),
        migrations.AlterUniqueTogether(
            name='rozetkareportfiletmpurl',
            unique_together={('user', 'id')},
        ),
        migrations.AlterUniqueTogether(
            name='rozetkatransactionhistory',
            unique_together={('user', 'id')},
        ),
    ]
