# Generated by Django 2.1.7 on 2020-02-27 11:57

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('payments', '0062_auto_20200219_1446'),
    ]

    operations = [
        migrations.AlterField(
            model_name='balance_status',
            name='block_balance',
            field=models.DecimalField(blank=True, decimal_places=2, default=0, max_digits=10, null=True, verbose_name='Заблокированный баланс'),
        ),
        migrations.AlterField(
            model_name='balance_status',
            name='written_off',
            field=models.DecimalField(blank=True, decimal_places=2, default=0, max_digits=10, null=True, verbose_name='Cписанный баланс'),
        ),
    ]
