# Generated by Django 2.1.7 on 2020-02-06 16:14

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('payments', '0044_auto_20200206_1538'),
    ]

    operations = [
        migrations.CreateModel(
            name='Balance_status',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('block_balance', models.DecimalField(decimal_places=2, max_digits=10, verbose_name='Заблокированный баланс')),
                ('written_off', models.DecimalField(decimal_places=2, max_digits=10, verbose_name='Cписанный баланс')),
                ('user_contactor', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='user_contractor', to=settings.AUTH_USER_MODEL, verbose_name='Пользователь')),
            ],
        ),
    ]
