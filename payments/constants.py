from django.utils.translation import ugettext as _


class TransactionTypes:
    LIQPAY = 1
    INVOICE = 2

    TRANSACTION_TYPES = (
        (LIQPAY, _('Liq Pay')),
        (INVOICE, _('Счет фактура'))
    )


class TransactionSources:
    RECHARGE_BALANCE = 1
    POCKET = 2
    UNBALANCE = 3
    SUCCESSFUL_ORDER = 4
    RETURN_PRODUCT = 5
    MODULE = 6

    TRANSACTION_SOURCES = (
        (RECHARGE_BALANCE, _('Пополнение баланса')),
        # (POCKET, _('Покупка пакета')),
        (POCKET, _('Коммисия')),
        (UNBALANCE, _('Вывод средств')),
        (SUCCESSFUL_ORDER, _('Успешный заказ')),
        (RETURN_PRODUCT, _('Возврат товара')),
        (MODULE, _('Покупка модуля'))
    )

class StatusConstants:
    BLOCK = 1
    WRITTEN_OFF = 2
    REVERSED = 3

    STATUS_CONSTANTS = (
        (BLOCK, _('Заблокировано')),
        (WRITTEN_OFF, _('Списано')),
        (REVERSED, _('Платеж возвращен')),
    )

class LiqPayConstant:
    ERROR = 'ERROR'
    FAILURE = 'FAILURE'
    REVERSED = 'REVERSED'
    SUBSCRIBED = 'SUBSCRIBED'
    SUCCESS = 'SUCCESS'
    UNSUBSCRIBED = 'UNSUBSCRIBED'
    SANDBOX = 'SANDBOX'

    LIQ_PAY_STATUSES = (
        (ERROR, _('Неуспешный платеж. Некорректно заполнены данные')),
        (FAILURE, _('Неуспешный платеж')),
        (REVERSED, _('Платеж возвращен')),
        (SUBSCRIBED, _('Подписка успешно оформлена')),
        (SUCCESS, _('Успешный платеж')),
        (UNSUBSCRIBED, _('Подписка успешно деактивирована')),
        (SANDBOX, _('Песочница'))
    )

class UaPayConstant:
    FINISHED = 'FINISHED'
    HOLDED = 'HOLDED'
    CANCELED = 'CANCELED'
    REVERSED = 'REVERSED'
    REJECTED = 'REJECTED'
    NEEDS_CONFIRMATION = 'NEEDS_CONFIRMATION'
    PENDING = 'PENDING'

    UA_PAY_STATUSES = (
        (FINISHED, _('	Платіж завершено успішно, гроші відправлено одержувачу')),
        (HOLDED, _('Необхідно підтвердження. Для завершення списання коштів потрібно виконати підтвердження.')),
        (CANCELED, _('Процес оплати не завершений та платіж був відхилений (обірвалося з\'єднання, платіж зупинений на проміжному етапі з вини платника).')),
        (REVERSED, _('Платіж повернуто, кошти повернулися відправнику.')),
        (REJECTED, _('Платіж не відбувся з технічних причин.')),
        (NEEDS_CONFIRMATION, _('Платіж очікує підтвердження (лукап або 3ds)')),
        (PENDING, _('	Платіж знаходиться в стані оплати (проміжний статус)'))
    )

class WayforPayConstant:
    EXPIRED = 'EXPIRED'
    APPROVED = 'APPROVED'
    DECLINED = 'DECLINED'
    REFUNDED = 'REFUNDED'


    UA_PAY_STATUSES = (
        (EXPIRED, _('Expired')),
        (APPROVED, _('Approved')),
        (DECLINED, _('Declined')),
        (REFUNDED, _('Refunded'))
    )



class ApplicationConstant:

    PROCESSED = 'PROCESSED'
    CANCELED = 'CANCELED'
    ACCEPTED = 'ACCEPTED'

    APPLICATION_STATUSES = (
        (PROCESSED, _('В обработке')),
        (ACCEPTED, _('Принято')),
        (CANCELED, _('Отменено'))
    )


class RozetkaOperationsTypes:
    RESERVATION_AMOUNT = 1
    SALES_COMMISSION = 2
    REMOVAL_OF_THE_RESERVE_FOR_THE_OUTSTANDING_ORDER = 3
    REFILL = 4
    PAYMENT_FOR_ACCESS_TO_THE_PLATFORM = 5
    BALANCE_ADJUSTMENT = 6
    ORDER_ADJUSTMENT = 7
    QUANTITY_CHANGE_IN_ORDER = 8
    REMOVE_ITEM_FROM_ORDER = 9
    RESERVATION_AMOUNT_FOR_THE_ADDED_PRODUCT = 10
    ADJUSTMENT_OF_THE_MONTHLY_FEE_INVOICE_MANUALLY = 11
    TOP_UP_ACCOUNT_FOR_ACCESS_TO_THE_PLATFORM = 12
    ADJUSTMENT_OF_THE_ACCOUNT = 13
    RETURN = 14
    RETURN_OF_COMMISSION_ON_ORDER = 15
    ADJUSTMENT_OF_THE_ROYALTY_ACCOUNT_IS_BETWEEN_THE_MONTHLY_FEE_AND_THE_ROYALTY = 16

    OPERATIONS_TYPES = (
        (RESERVATION_AMOUNT, _('Резервирование сумы по сделаному заказу')),
        (SALES_COMMISSION, _('Комиссия за продажу')),
        (REMOVAL_OF_THE_RESERVE_FOR_THE_OUTSTANDING_ORDER, _('Снятие резерва за невыполненый заказ')),
        (REFILL, _('Пополнение счета')),
        (PAYMENT_FOR_ACCESS_TO_THE_PLATFORM, _('Оплата доступа к платформе')),
        (BALANCE_ADJUSTMENT, _('Корретировка баланса')),
        (ORDER_ADJUSTMENT, _('Корректировка заказа')),
        (QUANTITY_CHANGE_IN_ORDER, _('Изменение количества в заказе')),
        (REMOVE_ITEM_FROM_ORDER, _('Удаление товара из заказа')),
        (RESERVATION_AMOUNT_FOR_THE_ADDED_PRODUCT, _('Резервирование суммы по добавленному товару')),
        (ADJUSTMENT_OF_THE_MONTHLY_FEE_INVOICE_MANUALLY, _('Корретировка счета абонплаты (проведена вручную)')),
        (TOP_UP_ACCOUNT_FOR_ACCESS_TO_THE_PLATFORM, _('Пополнение счета за доступ к платформе')),
        (ADJUSTMENT_OF_THE_ACCOUNT, _('Корретировка счета, сделанная программно')),
        (RETURN, _('Возврат')),
        (RETURN_OF_COMMISSION_ON_ORDER, _('Корректировке роялти по заказу - возврат комиссии по заказу')),
        (ADJUSTMENT_OF_THE_ROYALTY_ACCOUNT_IS_BETWEEN_THE_MONTHLY_FEE_AND_THE_ROYALTY,
         _('Корректировке счета роялти - между абонплатой и роялти')),
    )
