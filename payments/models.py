from django.db import models

from abc_profiles.models import MainContent
from catalog.models import TimeStampedModel
from django.contrib.auth import get_user_model

from marketplace.models import PocketPlan
from .constants import TransactionTypes, TransactionSources, LiqPayConstant, ApplicationConstant, \
    RozetkaOperationsTypes, StatusConstants, UaPayConstant
from django.utils.translation import ugettext as _

User = get_user_model()


class PaymentSettings(models.Model):
    frozen_time = models.SmallIntegerField(verbose_name=_('Время на заморозку средств'), default=14)

    class Meta:
        verbose_name = _('Настройки оплаты')
        verbose_name_plural = _('Настройки оплаты')


class PaymentTransaction(TimeStampedModel):
    # main
    user = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        verbose_name=_('Пользователь'),
        related_name='payment_transactions',
    )
    amount = models.DecimalField(max_digits=19, decimal_places=10)
    trans_type = models.PositiveSmallIntegerField(
        null=True, blank=True,
        choices=TransactionTypes.TRANSACTION_TYPES
    )
    source = models.PositiveSmallIntegerField(
        null=True, blank=True,
        choices=TransactionSources.TRANSACTION_SOURCES
    )

    transaction_status = models.PositiveIntegerField(
        choices=StatusConstants.STATUS_CONSTANTS,
        null=True, blank=True,
        verbose_name=_('Статус транзакции')
    )

    date_status = models.BooleanField(default=False,
                                      verbose_name='Заблокировано/Списано(14 дней)',
                                      null=True,
                                      blank=True)
    # if buy pocket

    pocket = models.ForeignKey(
        PocketPlan,
        on_delete=models.SET_NULL,
        null=True, blank=True,
        verbose_name=_('Пакет')
    )

    # if buy module
    module = models.ForeignKey(
        MainContent,
        on_delete=models.SET_NULL,
        null=True, blank=True,
        verbose_name=_('Модуль')
    )
    invoice_id = models.CharField(
        max_length=50,
        null=True, blank=True,
        verbose_name=_('Invoice id')
    )

    # liq pay params

    status = models.CharField(
        choices=UaPayConstant.UA_PAY_STATUSES,
        max_length=12,
        null=True, blank=True,
        verbose_name=_('Статус платежа')
    )

    agent_commission = models.DecimalField(
        max_digits=10, decimal_places=5,
        verbose_name=_('Комиссия агента в валюте платежа'),
        null=True, blank=True
    )

    is_valid_signature = models.BooleanField(
        default=True,
        verbose_name=_('Проверка сигнатуры')
    )

    card_token = models.CharField(
        max_length=500,
        null=True, blank=True,
        verbose_name=_('Token карты оправителя')
    )

    commission_credit = models.CharField(
        max_length=500,
        null=True, blank=True,
        verbose_name=_('Комиссия с получателя в валюте')
    )

    commission_debit = models.CharField(
        max_length=500,
        null=True, blank=True,
        verbose_name=_('Комиссия с отправителя в валюте')
    )

    currency = models.CharField(
        max_length=256,
        null=True, blank=True,
        verbose_name=_('Валюта платежа')
    )

    description = models.TextField(
        null=True, blank=True,
        verbose_name=_('Комментарий к платежу')
    )

    err_description = models.TextField(
        null=True, blank=True,
        verbose_name=_('Описание ошибок')
    )

    info = models.CharField(
        max_length=256,
        null=True, blank=True,
        verbose_name=_('Дополнительная информация о платеже')
    )

    ip = models.CharField(
        max_length=256,
        null=True, blank=True,
        verbose_name=_('IP адрес отправителя')
    )

    way_for_pay_order_id = models.CharField(
        max_length=256,
        null=True, blank=True,
        verbose_name=_('Order_id платежа в системе LiqPay')
    )

    order_pay_id = models.CharField(
        max_length=256,
        null=True, blank=True,
        verbose_name=_('Order_id платежа')
    )

    payment_id = models.CharField(
        max_length=256,
        null=True, blank=True,
        verbose_name=_('Id платежа в системе LiqPay')
    )

    sender_card_bank = models.CharField(
        max_length=256,
        null=True, blank=True,
        verbose_name=_('Банк отправителя')
    )

    sender_card_country = models.CharField(
        max_length=256,
        null=True, blank=True,
        verbose_name=_('Страна карты отправителя. Цифровой ISO 3166-1 код')
    )

    sender_card_mask2 = models.CharField(
        max_length=256,
        null=True, blank=True,
        verbose_name=_('Карта отправителя')
    )

    sender_card_type = models.CharField(
        max_length=256,
        null=True, blank=True,
        verbose_name=_('Тип карты отправителя MC/Visa')
    )

    sender_first_name = models.CharField(
        max_length=256,
        null=True, blank=True,
        verbose_name=_('Имя отправителя')
    )

    sender_last_name = models.CharField(
        max_length=256,
        null=True, blank=True,
        verbose_name=_('Фамилия отправителя')
    )

    sender_phone = models.CharField(
        max_length=256,
        null=True, blank=True,
        verbose_name=_('Телефон отправителя')
    )

    # Orders
    system_order_partner = models.ForeignKey(
        'orders.Order',
        null=True, blank=True,
        verbose_name=_('Заказ'),
        on_delete=models.SET_NULL,
    )

    # contractor orders
    system_order = models.ForeignKey(
        'orders.ContractorOrder',
        null=True, blank=True,
        verbose_name=_('Заказ поставщика'),
        on_delete=models.SET_NULL,
    )

    system_order_self = models.ForeignKey(
        'orders.OrderSelf',
        null=True, blank=True,
        verbose_name=_('Заказ поставщика'),
        on_delete=models.SET_NULL,
    )
    contractor_and_marketplace_part = models.IntegerField(default=0, null=True, blank=True,
                                                          verbose_name=_('Часть поставщика с заказа'))
    partner_and_marketplace_part = models.IntegerField(default=0, null=True, blank=True,
                                                       verbose_name=_('Часть продавца с заказа'))
    total_sum = models.IntegerField(default=0, null=True, blank=True, verbose_name=_('Полная сумма с заказа'))

    # unbalance though application

    application = models.ForeignKey(
        'payments.ApplicationToUnbalance',
        on_delete=models.SET_NULL,
        null=True, blank=True,
        verbose_name=_('Заявка на вывод средств')
    )

    # buy pocket by invoice

    invoice_file = models.FileField(
        upload_to='payments/invoices',
        null=True, blank=True,
        verbose_name=_('Файл с счет фактурой')
    )

    # invoice paid confirmation

    paid_check = models.FileField(
        upload_to='payments/paid_check',
        null=True, blank=True,
        verbose_name=_('Файл с подтверждение оплаты счет фактуры')
    )

    is_approved = models.BooleanField(
        default=None,
        verbose_name=_('Подтверждение оплаты'),
        blank=True,
        null=True
    )
    commision_partner_sum = models.DecimalField(
        verbose_name='Комиссия для продавца',
        blank=True, null=True,
        max_digits=10, decimal_places=5
    )
    system_balance_sum = models.DecimalField(
        verbose_name='Комисия платформы',
        blank=True, null=True,
        max_digits=10, decimal_places=5
    )

    class Meta:
        verbose_name = _('Транзакция оплаты')
        verbose_name_plural = _('Транзакции оплат')
        ordering = ('-created',)

    def __str__(self):
        return self.user.get_full_name()


class ApplicationToUnbalance(TimeStampedModel):
    user = models.ForeignKey(User, on_delete=models.PROTECT, related_name='unbalance_applications')
    amount = models.DecimalField(max_digits=10, decimal_places=5, verbose_name=_('Сумма на вывод'))
    status = models.CharField(
        max_length=9,
        choices=ApplicationConstant.APPLICATION_STATUSES,
        default=ApplicationConstant.PROCESSED,
        verbose_name=_('Статус')
    )
    approve_screen_check = models.ImageField(
        upload_to='payments/screen_checks',
        null=True, blank=True,
        verbose_name=_('Скан чека для подтверждения')
    )
    credit_card = models.CharField(
        max_length=16,
        null=True, blank=True,
        verbose_name=_('Номер кредитной карты')
    )

    class Meta:
        verbose_name = _('Заявка на вывод средств')
        verbose_name_plural = _('Заявки на вывод средств')
        ordering = ('-created',)

    def save(self, *args, **kwargs):
        return_data = super().save(*args, **kwargs)
        if self.status == ApplicationConstant.ACCEPTED:
            trans, _ = PaymentTransaction.objects.update_or_create(
                user=self.user,
                source=TransactionSources.UNBALANCE,
                application_id=self.id,
                defaults={
                    'amount': self.amount,
                }
            )
        return return_data


class RozetkaTransactionHistory(models.Model):
    user = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        verbose_name=_('Пользователь'),
        related_name='rozetka_trans_histories',
    )
    rozetka_id = models.PositiveIntegerField(null=True, blank=True)
    log_id = models.PositiveIntegerField(null=True, blank=True, )
    order_id = models.PositiveIntegerField(verbose_name=_('ID заказа'), null=True, blank=True, )
    product_id = models.PositiveIntegerField(verbose_name=_('ID товара'), null=True, blank=True, )
    purchase_id = models.PositiveIntegerField(verbose_name=_('ID покупки'), null=True, blank=True, )
    operation_type = models.PositiveSmallIntegerField(
        choices=RozetkaOperationsTypes.OPERATIONS_TYPES,
        verbose_name=_('Тип операции'),
    )
    price = models.DecimalField(
        null=True, blank=True,
        max_digits=10, decimal_places=5,
        verbose_name=_('Цена за единицу')
    )
    quantity = models.PositiveSmallIntegerField(
        null=True, blank=True,
        verbose_name=_('Количество')
    )
    cost = models.DecimalField(
        null=True, blank=True,
        max_digits=10, decimal_places=5,
        verbose_name=_('Цена (price*quantity)')
    )
    sum_in_gray_changed = models.DecimalField(
        max_digits=10, decimal_places=5,
        verbose_name=_('Изменение серой зоны'),
        null=True, blank=True
    )
    sum_in_gray = models.DecimalField(
        max_digits=10, decimal_places=5,
        verbose_name=_('Серая зона'),
        null=True, blank=True
    )
    current_balance = models.DecimalField(
        max_digits=10, decimal_places=5,
        verbose_name=_('Настоящий баланс'),
        null=True, blank=True
    )
    balance_changed = models.DecimalField(
        max_digits=10, decimal_places=5,
        verbose_name=_('Изменение баланса'),
        null=True, blank=True
    )
    subscription_balance = models.DecimalField(
        max_digits=10, decimal_places=5,
        verbose_name=_('Subscription_balance'),
        null=True, blank=True
    )
    subscription_balance_changed = models.DecimalField(
        max_digits=10, decimal_places=5,
        verbose_name=_('Subscription_balance_changed'),
        null=True, blank=True
    )
    correction_number = models.PositiveSmallIntegerField(
        verbose_name=_('Номер корректировки'),
        null=True, blank=True
    )
    created_at = models.DateTimeField(
        null=True, blank=True,
        verbose_name=_('Дата создания')
    )
    transaction_ts = models.DateTimeField(
        null=True, blank=True,
        verbose_name=_('Дата транзакции')
    )

    class Meta:
        verbose_name = _('Розетка. Транзакия магазина')
        verbose_name_plural = _('Розетка. Список транзакций магазина')
        unique_together = (('user', 'rozetka_id',),)
        ordering = ('-created_at',)


class RozetkaInvoices(models.Model):
    rozetka_id = models.PositiveIntegerField(null=True, blank=True)
    user = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        verbose_name=_('Пользователь'),
        related_name='rozetka_invoices',
    )
    market_owox_id = models.PositiveIntegerField(
        verbose_name=_('Номер корректировки'),
        null=True, blank=True
    )
    market_id = models.PositiveIntegerField(
        verbose_name=_('ID маркета'),
        null=True, blank=True
    )
    amount = models.DecimalField(
        max_digits=10, decimal_places=5,
        verbose_name=_('Сумма счета'),
        null=True, blank=True
    )
    contract_number = models.CharField(
        max_length=256,
        null=True, blank=True,
        verbose_name=_('Номер контракта с продавцом')
    )
    number = models.CharField(
        max_length=256,
        null=True, blank=True,
        verbose_name=_('Номер счета')
    )
    file_invoice = models.URLField(
        null=True, blank=True,
        verbose_name=_('Название файла счета')
    )
    type = models.CharField(
        max_length=256,
        null=True, blank=True,
        verbose_name=_('Тип счета')
    )
    date_of_invoice = models.DateField(
        null=True, blank=True,
        verbose_name=_('Дата выставления счета')
    )

    class Meta:
        verbose_name = _('Счет на оплату')
        verbose_name_plural = _('Счета на оплату')
        unique_together = (('user', 'rozetka_id',),)
        ordering = ('-date_of_invoice',)


class RozetkaReportFileTMPURL(models.Model):
    rozetka_id = models.PositiveIntegerField(null=True, blank=True)
    user = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        verbose_name=_('Пользователь'),
        related_name='rozetka_report_files',
    )
    market_id = models.PositiveIntegerField(
        verbose_name=_('ID маркета'),
        null=True, blank=True
    )
    created_at = models.DateTimeField(
        null=True, blank=True,
        verbose_name=_('Дата создания')
    )
    report_period = models.DateTimeField(
        null=True, blank=True,
        verbose_name=_('Дата создания')
    )
    report_file = models.URLField(
        null=True, blank=True,
        verbose_name=_('Файл отчета')
    )

    class Meta:
        verbose_name = _('Файл отчета о проданных товарах')
        verbose_name_plural = _('Файл отчета о проданных товарах')
        unique_together = (('user', 'rozetka_id',),)
        ordering = ('-created_at',)


class Balance_status(models.Model):  # contractor

    user_contactor = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        verbose_name=_('Пользователь'),
        related_name='user_contractor',
    )
    block_balance = models.DecimalField(
        max_digits=10,
        decimal_places=2,
        blank=True,
        null=True,
        verbose_name=_('Заблокированный баланс'),
        default=0
    )
    written_off = models.DecimalField(
        max_digits=10,
        decimal_places=2,
        blank=True,
        null=True,
        verbose_name=_('Cписанный баланс'),
        default=0
    )
    return_money = models.DecimalField(
        max_digits=10,
        decimal_places=2,
        blank=True,
        null=True,
        verbose_name=_('Повернений баланс'),
        default=0
    )
    total = models.DecimalField(
        max_digits=10,
        decimal_places=2,
        blank=True,
        null=True,
        verbose_name=_('баланс'),
        default=0
    )

    def __str__(self):
        return 'Поставщик {} ' \
               'Заблокировано {}' \
               ' Списано {} ' \
               'Возвращено {}'.format(
            self.user_contactor,
            self.block_balance,
            self.written_off,
            self.return_money
        )


class BalancePartner(models.Model):  # partner
    user = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        verbose_name=_('Пользователь'),
        related_name='user_partner',
    )
    frozen = models.DecimalField(
        max_digits=10,
        decimal_places=2,
        verbose_name=_('В резерве'),
    )
    available = models.DecimalField(
        max_digits=10,
        decimal_places=2,
        verbose_name=_('Доступно к снятию'),
    )
    written = models.DecimalField(
        max_digits=10,
        decimal_places=2,
        verbose_name=_('Выведено'),
    )
    return_money = models.DecimalField(
        max_digits=10,
        decimal_places=2,
        verbose_name=_('Возврат'),
    )


from solo.models import SingletonModel


class SystemBalance(SingletonModel):
    balance = models.DecimalField(max_digits=20,
                                  decimal_places=5,
                                  default=0,
                                  verbose_name=_('Баланс'), )


class PaymentInvoiceInfo(models.Model):
    session_id = models.CharField(
        max_length=50,
        null=True, blank=True,
        verbose_name=_('Session id')
    )
    invoice_id = models.CharField(
        max_length=50,
        null=True, blank=True,
        verbose_name=_('Invoice id')
    )
    user = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        verbose_name=_('Пользователь')
    )
    external_id = models.CharField(
        max_length=50,
        null=True, blank=True,
        verbose_name=_('External id')
    )


class PaymentInvoice(TimeStampedModel):
    user = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        verbose_name=_('Пользователь')
    )
    invoice_id = models.CharField(
        max_length=50,
        null=True, blank=True,
        verbose_name=_('Invoice id')
    )
    amount = models.DecimalField(
        max_digits=19,
        decimal_places=10,
        null=True,
        blank=True,
    )
    status = models.CharField(
        max_length=50,
        null=True, blank=True,
        verbose_name=_('Status')
    )

class DataFromWidget(models.Model):
    merchant_account = models.CharField(max_length=256, null=True, blank=True, verbose_name=_('Идентификатор продавца.'))
    merchant_signature = models.CharField(max_length=256, null=True, blank=True, verbose_name=_('Подпись запроса'))
    order_reference = models.CharField(max_length=256, verbose_name=_('Уникальный номер заказа'))
    amount = models.DecimalField(max_digits=10, decimal_places=2, blank=True, null=True, verbose_name=_('Сумма заказа'))
    currency = models.CharField(max_length=256, null=True, blank=True, verbose_name=_('Валюта заказа'))
    auth_code = models.CharField(max_length=256, null=True, blank=True, verbose_name=_('Код авторизации'))
    email = models.CharField(max_length=256, null=True, blank=True, verbose_name=_('Email клиента'))
    phone = models.CharField(max_length=256, null=True, blank=True, verbose_name=_('Номер телефона клиента'))

    created_date = models.IntegerField(null=True, blank=True, verbose_name=_('Дата создания запроса'))
    processing_date = models.IntegerField(null=True, blank=True, verbose_name=_('Дата процессирования транзакции'))
    card_pan = models.CharField(max_length=256, null=True, blank=True, verbose_name=_('Маскированный номер карты'))
    card_type = models.CharField(max_length=256, null=True, blank=True, verbose_name=_('Тип карты'))
    issuer_bank_country = models.CharField(max_length=256, null=True, blank=True, verbose_name=_('Страна карты'))
    issuer_bank_name = models.CharField(max_length=256, null=True, blank=True, verbose_name=_('Имя Банка карты'))
    transaction_status = models.CharField(max_length=256, null=True, blank=True, verbose_name=_('Статус транзакции'))
    reason = models.CharField(max_length=256, null=True, blank=True, verbose_name=_('Причина отказа'))
    reason_code = models.IntegerField(null=True, blank=True, verbose_name=_('Код отказа'))
    fee = models.IntegerField(null=True, blank=True, verbose_name=_('Комиссия psp'))
    payment_system = models.CharField(max_length=256, null=True, blank=True, verbose_name=_('Платежная система'))
    user = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name=_('Пользователь'),null=True, blank=True)
    # client_start_time = models.IntegerField(null=True, blank=True, verbose_name=_('client_start_time'))
    # user