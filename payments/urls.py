from django.urls import path
from rest_framework.routers import DefaultRouter

from payments import views as pay_views

app_name = 'payments'

router = DefaultRouter()
router.register(r'pocket_by_invoice', pay_views.ConfirmInvoicePocketPaidViewSet, base_name='pocket_by_invoice_and_confirm')
router.register(r'recharge_by_invoice', pay_views.ConfirmInvoiceRechargeViewSet, base_name='recharge_by_invoice')

urlpatterns = [
    path('pocket-pay/', pay_views.LiqPayPocketBuyView.as_view(), name='pocket_pay_view'),
    path('pocket-pay-callback/', pay_views.LiqPayPocketCallBackView.as_view(), name='pocket_pay_callback'),
    path('recharge-balance-pay/', pay_views.LiqPayRechargeBalanceView.as_view(), name='recharge_balance_pay'),
    path('recharge-balance-pay-callback/', pay_views.LiqPayRechargeBalanceCallBackView.as_view(), name='recharge_balance_pay_callback'),
    path('application_to_unbalances/', pay_views.ApplicationToUnbalanceView.as_view(), name='unbalances_list_create'),
    path('buy_pocket_by_invoice/', pay_views.InvoiceBuyPocketView.as_view(), name='buy_pocket_by_invoice'),
    path('recharge_balance_by_invoice/', pay_views.RechargeBalanceInvoiceView.as_view(), name='recharge_balance_by_invoice'),
    path('transactions/liq_pay/', pay_views.UserPaymentTransactionLiqPayView.as_view(), name='transactions_liq_pay'),
    path('transactions/unbalance/', pay_views.UserPaymentUnbalanceView.as_view(), name='transactions_unbalance'),
    path('transactions/order/', pay_views.PaymentTransactionOrderView.as_view(), name='transactions_order'),
    path('transactions/order/total/', pay_views.PaymentTransactionOrderAnnotatedView.as_view(), name='transactions_order_total'),
    path('transactions/rozetka/trans_history/', pay_views.RozetkaTransactionHistoryListView.as_view(), name='rozetka_trans_history'),
    path('transactions/rozetka/invoices/', pay_views.RozetkaInvoicesListView.as_view(), name='rozetka_invoices'),
    path('transactions/rozetka/reports_files/', pay_views.RozetkaReportFileTMPURLListView.as_view(), name='rozetka_reports_files'),
    path('transactions/full/', pay_views.FullTransactionPayments.as_view(), name='full-transactions'),
    path('transactions/recharge_by_liqpay/', pay_views.RechargeByLiqPay.as_view(), name='recharge_by_liqpay_list'),
    path('transactions/balance/<str:role>/', pay_views.BalanceStatusView.as_view(), name='balance_contractor'),
    path('transactions/wayforpay/get_data_for_widget/', pay_views.WayForPayGetDataForWidget.as_view(), name='get_data_for_widget'),
    path('transactions/wayforpay/set_data_for_widget/', pay_views.SetDataWayForPayGetDataForWidget.as_view(), name='set_data_for_widget'),
] + router.urls

