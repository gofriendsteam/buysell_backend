from django.contrib import admin
from django.utils.translation import ugettext as _
from solo.admin import SingletonModelAdmin
from payments.constants import TransactionSources, TransactionTypes
from payments.models import PaymentTransaction, ApplicationToUnbalance, RozetkaTransactionHistory, RozetkaInvoices, \
    RozetkaReportFileTMPURL , Balance_status  , SystemBalance


admin.site.register(PaymentTransaction)
admin.site.register(Balance_status)


# proxy models

admin.site.register(SystemBalance, SingletonModelAdmin)

class PaymentTransactionPocketLiqPayProxy(PaymentTransaction):
    class Meta:
        verbose_name = _('Покупка пакета')
        verbose_name_plural = _('Покупка пакетов')
        proxy = True


class PaymentTransactionPocketInvoiceProxy(PaymentTransaction):
    class Meta:
        verbose_name = _('Покупка пакета через счет фактуру')
        verbose_name_plural = _('Покупка пакетов через счет фактуру')
        proxy = True


class PaymentTransactionUnbalance(PaymentTransaction):
    class Meta:
        verbose_name = _('Вывод средств (транзакция)')
        verbose_name_plural = _('Выводы средств (транзакция)')
        proxy = True


class PaymentTransactionOrderSuccess(PaymentTransaction):
    class Meta:
        verbose_name = _('Перевод по успешним заказам')
        verbose_name_plural = _('Переводы по успешним заказам')
        proxy = True


class PaymentTransactionOrderReturn(PaymentTransaction):
    class Meta:
        verbose_name = _('Перевод по возвратам товара')
        verbose_name_plural = _('Переводы по возвратам товара')
        proxy = True


class PaymentTransactionRechargeBalanceLiqPay(PaymentTransaction):
    class Meta:
        verbose_name = _('Пополнение баланса (liqpay)')
        verbose_name_plural = _('Пополнение баланса (liqpay)')
        proxy = True


class PaymentTransactionRechargeBalanceInvoice(PaymentTransaction):
    class Meta:
        verbose_name = _('Пополнение баланса (счет фактура)')
        verbose_name_plural = _('Пополнение баланса (счет фактура)')
        proxy = True


@admin.register(ApplicationToUnbalance)
class ApplicationToUnbalanceAdmin(admin.ModelAdmin):
    fields = (
        'user',
        'amount',
        'status',
        'approve_screen_check',
        'credit_card',
        'created',
    )

    readonly_fields = ('created',)

    list_display = fields


@admin.register(PaymentTransactionPocketLiqPayProxy)
class PaymentTransactionPocketAdmin(admin.ModelAdmin):
    fields = (
        'user',
        'amount',
        'pocket',
        'created',
        'status',
        'agent_commission',
        'is_valid_signature',
        'card_token',
        'commission_credit',
        'commission_debit',
        'currency',
        'description',
        'err_description',
        'info',
        'ip',
        'way_for_pay_order_id',
        'order_pay_id',
        'payment_id',
        'sender_card_bank',
        'sender_card_country',
        'sender_card_mask2',
        'sender_card_type',
        'sender_first_name',
        'sender_last_name',
        'sender_phone',
        'invoice_file',
    )

    list_display = (
        'user',
        'amount',
        'trans_type',
        'pocket',
    )

    readonly_fields = ('created',)

    list_filter = ('trans_type', )

    def get_queryset(self, request):
        return super().get_queryset(request).filter(
            source=TransactionSources.POCKET,
        )

    def save_model(self, request, obj, form, change):
        obj.source = TransactionSources.POCKET
        return super().save_model(request, obj, form, change)


@admin.register(PaymentTransactionUnbalance)
class PaymentTransactionUnbalanceAdmin(admin.ModelAdmin):
    fields = (
        'user',
        'amount',
        'created',
        'source',
    )

    readonly_fields = ('created', )

    list_display = fields

    def get_queryset(self, request):
        query = super().get_queryset(request).filter(source=TransactionSources.UNBALANCE)
        return query

    def save_model(self, request, obj, form, change):
        obj.source = TransactionSources.UNBALANCE
        return super().save_model(request, obj, form, change)


@admin.register(PaymentTransactionOrderReturn)
class PaymentTransactionOrderReturnAdmin(admin.ModelAdmin):
    fields = (
        'user',
        'amount',
        'system_order',
        'created',
    )

    readonly_fields = ('created', )
    list_display = fields

    def save_model(self, request, obj, form, change):
        obj.source = TransactionSources.RETURN_PRODUCT
        return super().save_model(request, obj, form, change)

    def get_queryset(self, request):
        return super().get_queryset(request).filter(source=TransactionSources.RETURN_PRODUCT)


@admin.register(PaymentTransactionOrderSuccess)
class PaymentTransactionOrderReturnSuccess(admin.ModelAdmin):
    fields = (
        'user',
        'amount',
        'system_order',
        'created',
    )

    readonly_fields = ('created',)

    list_display = fields

    def save_model(self, request, obj, form, change):
        obj.source = TransactionSources.SUCCESSFUL_ORDER
        return super().save_model(request, obj, form, change)

    def get_queryset(self, request):
        return super().get_queryset(request).filter(source=TransactionSources.SUCCESSFUL_ORDER)


@admin.register(PaymentTransactionRechargeBalanceLiqPay)
class PaymentTransactionRechargeBalanceLiqPayAdmin(admin.ModelAdmin):
    fields = (
        'user',
        'amount',
        'trans_type',
        'created',
        'status',
        'agent_commission',
        'is_valid_signature',
        'card_token',
        'commission_credit',
        'commission_debit',
        'currency',
        'description',
        'err_description',
        'info',
        'ip',
        'way_for_pay_order_id',
        'order_pay_id',
        'payment_id',
        'sender_card_bank',
        'sender_card_country',
        'sender_card_mask2',
        'sender_card_type',
        'sender_first_name',
        'sender_last_name',
        'sender_phone',
        'system_order',
    )

    list_display = (
        'user',
        'amount',
        'trans_type',
        'created'
    )

    readonly_fields = ('created',)

    def get_queryset(self, request):
        return super().get_queryset(request).filter(
            source=TransactionSources.RECHARGE_BALANCE,
            trans_type=TransactionTypes.LIQPAY
        )

    def save_model(self, request, obj, form, change):
        obj.source = TransactionSources.RECHARGE_BALANCE
        obj.trans_type = TransactionTypes.LIQPAY
        return super().save_model(request, obj, form, change)


@admin.register(PaymentTransactionRechargeBalanceInvoice)
class PaymentTransactionRechargeBalanceInvoiceAdmin(admin.ModelAdmin):
    fields = (
        'user',
        'amount',
        'paid_check',
        'is_approved',
        'created',
    )

    list_display = (
        'user',
        'amount',
        'paid_check',
        'is_approved',
        'created'
    )

    readonly_fields = ('created',)

    def get_queryset(self, request):
        return super().get_queryset(request).filter(
            source=TransactionSources.RECHARGE_BALANCE,
            trans_type=TransactionTypes.INVOICE
        )

    def save_model(self, request, obj, form, change):
        obj.source = TransactionSources.RECHARGE_BALANCE
        obj.trans_type = TransactionTypes.INVOICE
        return super().save_model(request, obj, form, change)


@admin.register(RozetkaTransactionHistory)
class RozetkaTransactionHistoryAdmin(admin.ModelAdmin):
    fields = (
        'user',
        'id',
        'rozetka_id',
        'log_id',
        'order_id',
        'product_id',
        'purchase_id',
        'price',
        'quantity',
        'cost',
        'sum_in_gray_changed',
        'sum_in_gray',
        'current_balance',
        'balance_changed',
        'subscription_balance',
        'subscription_balance_changed',
        'correction_number',
        'created_at',
        'transaction_ts',
    )

    readonly_fields = fields

    list_display = (
        'id',
        'rozetka_id',
        'user',
        'log_id',
        'created_at',
    )


@admin.register(RozetkaInvoices)
class RozetkaInvoicesAdmin(admin.ModelAdmin):
    fields = (
        'id',
        'rozetka_id',
        'user',
        'market_owox_id',
        'market_id',
        'amount',
        'contract_number',
        'number',
        'file_invoice',
        'type',
        'date_of_invoice',
    )

    readonly_fields = fields

    list_display = (
        'id',
        'rozetka_id',
        'user',
        'date_of_invoice',
    )


@admin.register(RozetkaReportFileTMPURL)
class RozetkaReportFileTMPURLAdmin(admin.ModelAdmin):
    fields = (
        'id',
        'rozetka_id',
        'user',
        'market_id',
        'created_at',
        'report_period',
        'report_file',
    )

    readonly_fields = fields

    list_display = (
        'id',
        'rozetka_id',
        'user',
        'created_at',
        'report_period',
    )


