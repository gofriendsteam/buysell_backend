import datetime
import time
import uuid

import requests
from django.conf import settings
from django.db.models import Sum, IntegerField
from django.shortcuts import get_object_or_404
from django.utils.decorators import method_decorator
from django.views import View
from django.views.decorators.csrf import csrf_exempt
from django_filters.rest_framework import DjangoFilterBackend
from djangorestframework_camel_case.parser import CamelCaseJSONParser

from liqpay.liqpay3 import LiqPay

from django.http import HttpResponse
from rest_framework.generics import CreateAPIView, ListCreateAPIView, ListAPIView
from rest_framework.parsers import MultiPartParser
from rest_framework.permissions import AllowAny
from rest_framework.response import Response

from rest_framework.views import APIView
from rest_framework import permissions, status, viewsets, filters
from django.contrib.auth import get_user_model

from catalog.utils import get_host_name
from marketplace.models import PocketPlan
from orders.models import ContractorOrder, OrderItem
from payments.constants import TransactionTypes, TransactionSources
from payments.filters import ApplicationToUnbalanceFilter, PaymentTransactionFilter, PaymentLiqPayFilter, \
    PaymentTransactionOrderFilter, PaymentTransactionWithStatusFilter, RozetkaTransactionHistoryFilter, \
    RozetkaInvoicesFilter, RozetkaReportFileTMPURLFilter, TransactionFullFilter
from payments.models import PaymentTransaction, ApplicationToUnbalance, RozetkaTransactionHistory, RozetkaInvoices, \
    RozetkaReportFileTMPURL, PaymentInvoiceInfo, DataFromWidget
from payments.serializers import PocketIdSerializer, ApplicationToUnbalanceSerializer, \
    PocketInvoiceSerializer, RechargeBalanceInvoiceSerializer, ConfirmInvoicePocketPaidSerializer, \
    ConfirmInvoiceRechargeSerializer, PaymentTransactionInvoiceSerializer, PaymentTransactionUnbalanceSerializer, \
    PaymentTransactionLiqpaySerializer, PaymentTransactionOrderSerializer, RechargeBalanceLiqPaySerializer, \
    RozetkaTransactionHistorySerializer, RozetkaInvoicesSerializer, RozetkaReportFileTMPURLSerializer, \
    TransactionFullByUserSerializer, RechargeByLiqPaySerializer, PaymentTransactionOrderAnnotatedSerializer, \
    UaPayInvoiceSerializer, WayForPaySerializer
from payments.utils import get_liqpay_data_and_signature
from users.permissions import IsPartner
from django.db.models import F
from .models import Balance_status, BalancePartner
from django.db.models import Sum

User = get_user_model()


class LiqPayPocketBuyView(CreateAPIView):
    permission_classes = (permissions.AllowAny,)
    serializer_class = PocketIdSerializer

    def post(self, request, *args, **kwargs):
        pocket = get_object_or_404(PocketPlan, id=request.data.get('pocket_id'))
        params = {
            'action': 'pay',
            'amount': str(pocket.price),
            'currency': pocket.currency,
            'description': 'Payment for buying pocket',
            'order_id': "{}_{}_{}".format(request.user.id, pocket.id, uuid.uuid4()),
            'version': '3',
            'sandbox': 1,
            'result_url': get_host_name(),
            'server_url': get_host_name() + '/api/v1/payments/pocket-pay-callback/',  # url to callback view
            # 'server_url': 'http://d1441f04.ngrok.io' + '/api/v1/payments/pocket-pay-callback/', # url to callback view
        }

        response_data = get_liqpay_data_and_signature(**params)

        return Response(
            response_data,
            status=status.HTTP_200_OK,
        )


@method_decorator(csrf_exempt, name='dispatch')
class LiqPayPocketCallBackView(View):
    # permission_classes = (AllowAny, )

    def post(self, request, *args, **kwargs):
        liqpay = LiqPay(settings.LIQ_PAY_PUBLIC_KEY, settings.LIQ_PAY_PRIVATE_KEY)
        data = request.POST.get('data')
        signature = request.POST.get('signature')
        sign = liqpay.str_to_sign(settings.LIQ_PAY_PUBLIC_KEY + data + settings.LIQ_PAY_PRIVATE_KEY)

        response = liqpay.decode_data_from_str(data)
        if sign == signature:
            response['is_valid_signature'] = True
        user = User.objects.get(id=int(response['order_id'].split('_')[0]))
        PaymentTransaction.objects.create(
            user=user,
            amount=response.get('amount_credit'),
            trans_type=TransactionTypes.LIQPAY,
            source=TransactionSources.POCKET,
            pocket_id=int(response['order_id'].split('_')[1]),
            is_valid_signature=response.get('is_valid_signature', False),
            status=response.get('status', '').upper(),
            agent_commission=response.get('agent_commission', None),
            card_token=response.get('card_token', None),
            commission_credit=response.get('commission_credit', None),
            commission_debit=response.get('commission_debit', None),
            currency=response.get('currency', None),
            description=response.get('description', None),
            err_description=response.get('err_description', None),
            info=response.get('info', None),
            ip=response.get('ip', None),
            way_for_pay_order_id=response.get('way_for_pay_order_id', None),
            order_pay_id=response.get('order_id', None),
            payment_id=response.get('payment_id', None),
            sender_card_bank=response.get('sender_card_bank', None),
            sender_card_country=response.get('sender_card_country', None),
            sender_card_mask2=response.get('sender_card_mask2', None),
            sender_card_type=response.get('sender_card_type', None),
            sender_first_name=response.get('sender_first_name', None),
            sender_last_name=response.get('sender_last_name', None),
            sender_phone=response.get('sender_phone', None),
        )

        if response.get('status') == 'success':
            user.user_pocket_id = int(response['order_id'].split('_')[1])
            user.save()
        return HttpResponse()  # HttpResponseRedirect(redirect_to='https://topmarket.ua/')


class LiqPayRechargeBalanceView(CreateAPIView):
    permission_classes = (permissions.AllowAny,)
    serializer_class = RechargeBalanceLiqPaySerializer

    def post(self, request, *args, **kwargs):
        amount = request.data.get('amount')
        params = {
            'action': 'pay',
            'amount': str(amount),
            'currency': 'UAH',
            'description': 'Payment for clothes',
            'order_id': "{}_{}".format(request.user.id, uuid.uuid4()),
            'version': '3',
            'sandbox': 1,
            'result_url': get_host_name(),
            'server_url': get_host_name() + '/api/v1/payments/recharge-balance-pay-callback/',  # url to callback view
            # 'server_url': 'https://e3fb289d.ngrok.io' + '/api/v1/payments/recharge-balance-pay-callback/', # url to callback view
        }

        response_data = get_liqpay_data_and_signature(**params)

        return Response(
            response_data,
            status=status.HTTP_200_OK,
        )


@method_decorator(csrf_exempt, name='dispatch')
class LiqPayRechargeBalanceCallBackView(View):

    def post(self, request, *args, **kwargs):
        liqpay = LiqPay(settings.LIQ_PAY_PUBLIC_KEY, settings.LIQ_PAY_PRIVATE_KEY)
        data = request.POST.get('data')
        signature = request.POST.get('signature')
        sign = liqpay.str_to_sign(settings.LIQ_PAY_PUBLIC_KEY + data + settings.LIQ_PAY_PRIVATE_KEY)

        response = liqpay.decode_data_from_str(data)
        if sign == signature:
            response['is_valid_signature'] = True
        user = User.objects.get(id=int(response['order_id'].split('_')[0]))
        PaymentTransaction.objects.create(
            user=user,
            amount=response.get('amount_credit'),
            trans_type=TransactionTypes.LIQPAY,
            source=TransactionSources.RECHARGE_BALANCE,
            is_valid_signature=response.get('is_valid_signature', False),
            status=response.get('status', '').upper(),
            agent_commission=response.get('agent_commission', None),
            card_token=response.get('card_token', None),
            commission_credit=response.get('commission_credit', None),
            commission_debit=response.get('commission_debit', None),
            currency=response.get('currency', None),
            description=response.get('description', None),
            err_description=response.get('err_description', None),
            info=response.get('info', None),
            ip=response.get('ip', None),
            way_for_pay_order_id=response.get('way_for_pay_order_id', None),
            order_pay_id=response.get('order_id', None),
            payment_id=response.get('payment_id', None),
            sender_card_bank=response.get('sender_card_bank', None),
            sender_card_country=response.get('sender_card_country', None),
            sender_card_mask2=response.get('sender_card_mask2', None),
            sender_card_type=response.get('sender_card_type', None),
            sender_first_name=response.get('sender_first_name', None),
            sender_last_name=response.get('sender_last_name', None),
            sender_phone=response.get('sender_phone', None),
        )

        return HttpResponse()  # HttpResponseRedirect(redirect_to='https://topmarket.ua/')


class InvoiceBuyPocketView(CreateAPIView):
    serializer_class = PocketInvoiceSerializer
    model = PaymentTransaction
    permission_classes = (permissions.IsAuthenticated,)


class RechargeBalanceInvoiceView(CreateAPIView):
    serializer_class = RechargeBalanceInvoiceSerializer
    model = PaymentTransaction


class ApplicationToUnbalanceView(ListCreateAPIView):
    serializer_class = ApplicationToUnbalanceSerializer
    permission_classes = (permissions.IsAuthenticated,)  # (IsPartner, )
    filter_backends = [filters.SearchFilter, DjangoFilterBackend, filters.OrderingFilter]
    filterset_class = ApplicationToUnbalanceFilter

    search_fields = (
        'id',
    )

    ordering_fields = (
        'id',
        'amount',
        'created'
    )

    def get_queryset(self):
        return ApplicationToUnbalance.objects.filter(user=self.request.user)


class ConfirmInvoicePocketPaidViewSet(viewsets.ModelViewSet):
    serializer_class = ConfirmInvoicePocketPaidSerializer
    http_method_names = ['get', 'put']
    permission_classes = (permissions.IsAuthenticated,)

    def get_queryset(self):
        return PaymentTransaction.objects.filter(
            user=self.request.user,
            trans_type=TransactionTypes.INVOICE,
            source=TransactionSources.POCKET,
        )


class ConfirmInvoiceRechargeViewSet(viewsets.ModelViewSet):
    serializer_class = ConfirmInvoiceRechargeSerializer
    http_method_names = ['get', 'put']
    parser_classes = (MultiPartParser, CamelCaseJSONParser,)
    permission_classes = (permissions.IsAuthenticated,)
    filterset_class = PaymentTransactionFilter
    filter_backends = [filters.SearchFilter, DjangoFilterBackend, filters.OrderingFilter]

    search_fields = (
        'id',
    )

    ordering_fields = (
        'id',
        'amount',
        'created'
    )

    def get_queryset(self):
        return PaymentTransaction.objects.filter(
            user=self.request.user,
            trans_type=TransactionTypes.INVOICE,
            source=TransactionSources.RECHARGE_BALANCE,
            is_approved=False,
        )


class RechargeByLiqPay(ListAPIView):
    serializer_class = RechargeByLiqPaySerializer
    filterset_class = PaymentLiqPayFilter
    filter_backends = [filters.SearchFilter, DjangoFilterBackend, filters.OrderingFilter]

    search_fields = (
        'id',
    )

    ordering_fields = (
        'id',
        'amount',
        'created'
    )

    def get_queryset(self):
        return PaymentTransaction.objects.filter(
            user=self.request.user,
            trans_type=TransactionTypes.LIQPAY,
            source=TransactionSources.RECHARGE_BALANCE
        )


class UserPaymentTransactionInvoiceView(ListAPIView):
    serializer_class = PaymentTransactionInvoiceSerializer
    permission_classes = (permissions.IsAuthenticated,)
    filterset_class = PaymentTransactionFilter
    filter_backends = [filters.SearchFilter, DjangoFilterBackend, filters.OrderingFilter]

    search_fields = (
        'id',
    )

    ordering_fields = (
        'id',
        'amount',
        'created'
    )

    def get_queryset(self):
        return PaymentTransaction.objects.filter(
            user=self.request.user,
            trans_type=TransactionTypes.INVOICE,
            is_approved=True,
        )


class UserPaymentTransactionLiqPayView(ListAPIView):
    serializer_class = PaymentTransactionLiqpaySerializer
    permission_classes = (permissions.IsAuthenticated,)
    filterset_class = PaymentLiqPayFilter
    filter_backends = [filters.SearchFilter, DjangoFilterBackend, filters.OrderingFilter]

    search_fields = (
        'id',
    )

    ordering_fields = (
        'id',
        'amount',
        'created'
    )

    def get_queryset(self):
        return PaymentTransaction.objects.filter(
            user=self.request.user,
            trans_type=TransactionTypes.LIQPAY,
        )


class UserPaymentUnbalanceView(ListAPIView):
    serializer_class = PaymentTransactionUnbalanceSerializer
    permission_classes = (permissions.IsAuthenticated,)
    filterset_class = PaymentTransactionWithStatusFilter
    filter_backends = [filters.SearchFilter, DjangoFilterBackend, filters.OrderingFilter]
    filterset_fields = (
        'id',
        'amount',
        'created',
        'status',
    )

    search_fields = (
        'id',
    )

    ordering_fields = (
        'id',
        'amount',
        'created'
    )

    def get_queryset(self):
        return PaymentTransaction.objects.filter(
            user=self.request.user,
            trans_type__isnull=True,
            source=TransactionSources.UNBALANCE
        )


class PaymentTransactionOrderView(ListAPIView):
    serializer_class = PaymentTransactionOrderSerializer
    permission_classes = (permissions.IsAuthenticated,)
    filterset_class = PaymentTransactionOrderFilter
    filter_backends = [filters.SearchFilter, DjangoFilterBackend, filters.OrderingFilter]
    search_fields = (
        'id',
    )

    ordering_fields = (
        'id',
        'amount',
        'created'
    )

    def get_queryset(self):
        return PaymentTransaction.objects.filter(
            user=self.request.user,
            source__in=[
                TransactionSources.RETURN_PRODUCT,
                TransactionSources.SUCCESSFUL_ORDER,
            ],
            system_order__isnull=False
        )


class PaymentTransactionOrderAnnotatedView(ListAPIView):
    serializer_class = PaymentTransactionOrderAnnotatedSerializer
    permission_classes = (permissions.IsAuthenticated,)

    def get_queryset(self):
        return PaymentTransaction.objects.filter(
            user=self.request.user,
            source__in=[
                TransactionSources.RETURN_PRODUCT,
                TransactionSources.SUCCESSFUL_ORDER,
            ],
            system_order__isnull=False
        )

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())
        full_amount = 0
        difference = 0
        balance_of_the_total = 0
        success_qs = queryset.filter(source=TransactionSources.SUCCESSFUL_ORDER)
        returned_qs = queryset.filter(source=TransactionSources.RETURN_PRODUCT)

        if self.request.user.role == 'CONTRACTOR':
            full_amount = int(returned_qs.aggregate(total=Sum('total_sum'))['total'] or 0) - int(
                success_qs.aggregate(total=Sum('total_sum'))['total'] or 0)
            difference = int(success_qs.aggregate(part=Sum('partner_and_marketplace_part'))['part'] or 0)
            balance_of_the_total = int(returned_qs.aggregate(Sum('amount'))['amount__sum'] or 0) - int(
                success_qs.aggregate(Sum('amount'))['amount__sum'] or 0)
        elif self.request.user.role == 'PARTNER':
            full_amount = int(success_qs.aggregate(total=Sum('total_sum'))['total'] or 0) - int(
                returned_qs.aggregate(total=Sum('total_sum'))['total'] or 0)
            difference = int(returned_qs.aggregate(part=Sum('contractor_and_marketplace_part'))['part'] or 0)
            balance_of_the_total = int(success_qs.aggregate(Sum('amount'))['amount__sum'] or 0) - int(
                returned_qs.aggregate(total=Sum('amount'))['amount__sum'] or 0)

        data = {
            'full_amount': full_amount,
            'difference': difference,
            'balance_of_the_total': balance_of_the_total,
        }
        return Response(data, status=status.HTTP_200_OK)


class RozetkaTransactionHistoryListView(ListAPIView):
    serializer_class = RozetkaTransactionHistorySerializer
    permission_classes = (IsPartner,)
    filterset_class = RozetkaTransactionHistoryFilter
    filter_backends = [filters.SearchFilter, DjangoFilterBackend, filters.OrderingFilter]
    search_fields = (
        'rozetka_id',
    )

    ordering_fields = (
        'rozetka_id',
        'log_id',
        'order_id',
        'product_id',
        'purchase_id',
        'quantity',
        'cost',
        'sum_in_gray_changed',
        'sum_in_gray',
        'balance_changed',
        'correction_number',
        'created_at',
    )

    def get_queryset(self):
        return RozetkaTransactionHistory.objects.filter(user=self.request.user)


class RozetkaInvoicesListView(ListAPIView):
    serializer_class = RozetkaInvoicesSerializer
    permission_classes = (IsPartner,)
    filterset_class = RozetkaInvoicesFilter
    filter_backends = [filters.SearchFilter, DjangoFilterBackend, filters.OrderingFilter]

    search_fields = (
        'rozetka_id',
    )

    ordering_fields = (
        'rozetka_id',
        'market_owox_id',
        'market_id',
        'amount',
        'contract_number',
        'number',
        'date_of_invoice',
    )

    def get_queryset(self):
        return RozetkaInvoices.objects.filter(user=self.request.user)


class RozetkaReportFileTMPURLListView(ListAPIView):
    serializer_class = RozetkaReportFileTMPURLSerializer
    permission_classes = (IsPartner,)
    filterset_class = RozetkaReportFileTMPURLFilter
    filter_backends = [filters.SearchFilter, DjangoFilterBackend, filters.OrderingFilter]
    search_fields = (
        'rozetka_id',
    )

    ordering_fields = (
        'rozetka_id',
        'market_id',
        'created_at',
    )

    def get_queryset(self):
        return RozetkaReportFileTMPURL.objects.filter(user=self.request.user)


class DeleteAllRozetkaTrans(APIView):
    permission_classes = (AllowAny,)

    def get(self, *args, **kwargs):
        RozetkaTransactionHistory.objects.all().delete()
        RozetkaInvoices.objects.all().delete()
        RozetkaReportFileTMPURL.objects.all().delete()
        return HttpResponse(status=200)


class FullTransactionPayments(ListAPIView):
    permission_classes = (AllowAny,)
    serializer_class = TransactionFullByUserSerializer
    filterset_class = TransactionFullFilter
    filter_backends = [filters.SearchFilter, DjangoFilterBackend, filters.OrderingFilter]
    ordering_fields = (
        'id',
        'amount',
        'created',
        'updated',
        'agent_commission',
        'payment_id',
    )

    def get_queryset(self):
        return PaymentTransaction.objects.filter(user_id=self.request.user)  # self.request.user


class BalanceStatusView(APIView):
    PARTNER = 'partner'
    CONTRACTOR = 'contractor'

    def get_for_partner(self):
        current_user = self.request.user
        rezerv = PaymentTransaction.objects.filter(user_id=current_user, date_status=False,
                                                   is_approved=None)  # TODO: add is_approved=None
        # print(PaymentTransaction.objects.filter(user_id=current_user , date_status=False , is_approved=None))
        available = PaymentTransaction.objects.filter(user_id=current_user, date_status=True,
                                                      is_approved=False)  # TODO: add is_approved=False
        # available.update(is_approved=True)
        written = PaymentTransaction.objects.filter(user_id=current_user, date_status=True,
                                                    is_approved=True)  # TODO: add is_approved=True
        return_money = PaymentTransaction.objects.filter(user_id=current_user, date_status=None,
                                                         is_approved=None)  # TODO: add is_approved=None
        try:
            instance, created = BalancePartner.objects.update_or_create(user=current_user, defaults={
                'frozen': rezerv.aggregate(Sum('agent_commission'))['agent_commission__sum'] or 0,
                'available': available.aggregate(Sum('agent_commission'))['agent_commission__sum'] or 0,
                'return_money': return_money.aggregate(Sum('agent_commission'))['agent_commission__sum'] or 0,
                'written': written.aggregate(Sum('agent_commission'))['agent_commission__sum'] or 0

            })
            print(' instance.available', instance.available)
            print(' instance.frozen', instance.frozen)
            print(' instance.written', instance.written)
            print(' instance.return_money', instance.return_money)

            return Response(status=status.HTTP_200_OK, data={
                'available': instance.available,
                'frozen': instance.frozen,
                'written': instance.written,
                'return_money': instance.return_money,
            })
        except Exception as e:
            print(e)
            return Response(status=status.HTTP_200_OK)

    def get_for_contractor(self):
        # False - Заблоковано
        '''
            source field :
                2 - комисия
        '''
        print('get_for_contractor')
        print(self.request.user)
        current_user = self.request.user
        all_transtactions_for_written_comminision = PaymentTransaction.objects \
            .filter(date_status=True,
                    user=current_user,
                    source=2).aggregate(Sum('agent_commission'))['agent_commission__sum']  # списано комиссии

        all_transtactions_for_blocked_comminision = PaymentTransaction.objects \
            .filter(date_status=False,
                    user=current_user,
                    source=2).aggregate(Sum('agent_commission'))['agent_commission__sum']  # Заблоковано комиссии
        all_transtactions_for_return_comminision = PaymentTransaction.objects \
            .filter(date_status=None,
                    user=current_user,
                    source=2).aggregate(Sum('agent_commission'))['agent_commission__sum']  # Возврат средств комиссии

        instance, created = Balance_status.objects.update_or_create(user_contactor=current_user,
                                                                    defaults={
                                                                        'block_balance': all_transtactions_for_blocked_comminision,
                                                                        'written_off': all_transtactions_for_written_comminision,
                                                                        'return_money': all_transtactions_for_return_comminision,
                                                                    }
                                                                    )
        user = User.objects.get(id=current_user.id)

        return Response(status=status.HTTP_200_OK, data={
            'written': all_transtactions_for_written_comminision,
            'blocked': all_transtactions_for_blocked_comminision,
            'return': all_transtactions_for_return_comminision,
            'balance': instance.total
        })

    def get(self, *args, **kwargs):
        role_args = kwargs.pop('role')
        user_role = self.request.user.role.lower()
        if role_args == self.PARTNER and user_role == self.PARTNER:
            return self.get_for_partner()
        elif role_args == self.CONTRACTOR and user_role == self.CONTRACTOR:
            return self.get_for_contractor()
        '''
            field source : 2 - коммисия
        '''
        return Response(status=status.HTTP_403_FORBIDDEN)


class WayForPayGetDataForWidget(APIView):

    def get(self, request, *args, **kwargs):
        orderReference = str(uuid.uuid4())
        # print(int(time.time()))
        data = {
            'merchantAccount': 'buy_sell_com_ua',
            'merchantDomainName': 'www.buy-sell.com.ua',
            'orderReference': orderReference,
            'orderDate': int(time.time()),
            'currency': 'UAH',
            'productName': 'Процессор Intel Core i5-4670 3.4GHz',
            'productCount': '1',
            'productPrice': '1000',
            'merchantSecretKey': '8e1f9051f1b6e43fc1a3cb6f18718f4662db9c93',

        }
        return Response(
            data,
            status=status.HTTP_200_OK,
        )


class SetDataWayForPayGetDataForWidget(CreateAPIView):
    queryset = DataFromWidget.objects.all()
    model = DataFromWidget
    serializer_class = WayForPaySerializer

    def post(self, request, *args, **kwargs):

        # return Response(
        #     status=status.HTTP_200_OK,
        # )
        self.request.data['user'] = self.request.user.id
        # print( self.request.data)
        return super().create(self.request, *args, **kwargs)
