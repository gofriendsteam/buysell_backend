from datetime import datetime

import pdfkit
from django.conf import settings
from django.template.loader import get_template
from liqpay.liqpay3 import LiqPay


def get_liqpay_data_and_signature(**params):
    liqpay = LiqPay(settings.LIQ_PAY_PUBLIC_KEY, settings.LIQ_PAY_PRIVATE_KEY)

    params = {
        'action': 'pay',
        'amount': params.get('amount'),
        'currency': params.get('currency', 'UAH'),
        'description': params.get('description', ''),
        'order_id': params.get('order_id'),
        'version': '3',
        'sandbox': '1',  # sandbox mode, set to 1 to enable it
        'server_url': params.get('server_url'),  # url to callback view
        'language': 'ru'
    }

    data = {
        'signature': liqpay.cnb_signature(params),
        'data': liqpay.cnb_data(params)
    }

    return data


def send_invoice(*args, **kwargs):
    pass


def extra_name():
    return str(datetime.now().date()) + '-' + str(datetime.now().time())


def render_to_pdf(template_src, context_dict={}):
    template = get_template(template_src)
    html = template.render(context_dict)
    file = pdfkit.from_string(html, False)
    return file


def replace_subdomain_static_to_domain(url):
    return url.replace(settings.CUSTOM_API_ACCESS_ENDPOINT, "https://storage.googleapis.com")
