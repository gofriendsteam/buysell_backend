import base64
from datetime import datetime
from io import BytesIO
from urllib.request import urlopen, Request
from django.conf import settings
from django.template.loader import render_to_string
from rest_framework import serializers
from rest_framework.exceptions import ValidationError
from sendgrid import Mail, Attachment, SendGridAPIClient
from django.core.files import File
from marketplace.models import PocketPlan, GeneralData
from payments.constants import TransactionTypes, TransactionSources, LiqPayConstant, RozetkaOperationsTypes, \
    UaPayConstant, WayforPayConstant
from payments.models import PaymentTransaction, ApplicationToUnbalance, RozetkaTransactionHistory, RozetkaInvoices, \
    RozetkaReportFileTMPURL, DataFromWidget
from payments.utils import extra_name, render_to_pdf, replace_subdomain_static_to_domain
from django.utils.translation import ugettext as _


class PocketIdSerializer(serializers.Serializer):
    pocket_id = serializers.PrimaryKeyRelatedField(queryset=PocketPlan.objects.all())

    class Meta:
        fields = (
            'pocket_id',
        )


class PocketInvoiceSerializer(serializers.Serializer):

    pocket_id = serializers.PrimaryKeyRelatedField(queryset=PocketPlan.objects.all())

    class Meta:
        fields = (
            'pocket_id',
        )

    def create(self, validated_data):
        pocket = validated_data['pocket_id']
        user = self.context['request'].user

        # email message
        mail_subject = 'Покупка пакета'
        message = render_to_string('buy_by_invoice.html', {
            'user': user,
            'theme': 'пакета услуг',
        })
        data = {
            'to_emails': [user.email, ],
            'subject': mail_subject,
            'html_content': message
        }

        # pdf
        context = {
            'amount': pocket.price,
            'background_url': GeneralData.objects.first().pocket_background.url,
            'user': user,
            'current_date': datetime.now().date(),
            'type_of_invoice': 'Пакет услуг',
        }

        pdf = render_to_pdf(
            template_src='invoice.html',
            context_dict=context
        )

        file = File(BytesIO(pdf))

        payment = PaymentTransaction.objects.create(
            user=user,
            amount=pocket.price,
            trans_type=TransactionTypes.INVOICE,
            source=TransactionSources.POCKET,
            pocket=pocket,
            is_approved=False,
        )
        file_name = 'invoice' + '-' + str(user.id) + '-' + extra_name() + '.pdf'
        payment.invoice_file.save(file_name, file)

        from_email = settings.DEFAULT_FROM_EMAIL
        message = Mail(
            from_email=from_email,
            **data,
        )
        req = Request(payment.invoice_file.url, headers={'User-Agent': 'Mozilla/5.0'})
        attachment = Attachment()
        attachment.file_content = base64.b64encode(urlopen(req).read()).decode('utf-8')
        # with open(payment.invoice_file.url, 'rb') as f:
        #     attachment.file_content = base64.b64encode(f.read()).decode('utf-8')
        attachment.file_name = 'invoice-to-buy-pocket.pdf'
        message.add_attachment(attachment)

        sg = SendGridAPIClient(settings.SENDGRID_API_KEY)
        sg.send(message)

        return payment


class RechargeBalanceLiqPaySerializer(serializers.Serializer):
    amount = serializers.DecimalField(max_digits=10, decimal_places=2, required=True)

    class Meta:
        fields = (
            'amount',
        )
        
    def validate_amount(self, val):
        user = self.context['request'].user

        if user.user_balance < val or user.user_balance <= 1000:
            raise ValidationError(
                _('Выбранная сума недоступна!')
            )


class RechargeBalanceInvoiceSerializer(serializers.Serializer):
    amount = serializers.DecimalField(max_digits=10, decimal_places=2, required=True)

    class Meta:
        fields = (
            'amount',
        )

    def create(self, validated_data):
        amount = validated_data['amount']
        user = self.context['request'].user

        # email message
        mail_subject = 'Пополнения баланса'
        message = render_to_string('buy_by_invoice.html', {
            'user': user,
            'theme': 'пополнение баланса',
        })

        data = {
            'to_emails': [user.email, ],
            'subject': mail_subject,
            'html_content': message
        }

        # pdf
        context = {
            'amount': amount,
            'background_url': GeneralData.objects.first().pocket_background.url,
            'user': user,
            'current_date': datetime.now().date(),
            'type_of_invoice': 'Пополнение баланса',
            'number': PaymentTransaction.objects.last().id + 1,
        }

        pdf = render_to_pdf(
            template_src='invoice.html',
            context_dict=context
        )

        file = File(BytesIO(pdf))

        payment = PaymentTransaction.objects.create(
            user=user,
            amount=amount,
            trans_type=TransactionTypes.INVOICE,
            source=TransactionSources.RECHARGE_BALANCE,
            is_approved=False
        )

        file_name = 'invoice' + '-' + str(user.id) + '-' + extra_name() + '.pdf'
        payment.invoice_file.save(file_name, file)

        from_email = settings.DEFAULT_FROM_EMAIL
        message = Mail(
            from_email=from_email,
            **data,
        )

        req = Request(payment.invoice_file.url, headers={'User-Agent': 'Mozilla/5.0'})

        attachment = Attachment()
        attachment.file_content = base64.b64encode(urlopen(req).read()).decode('utf-8')
        # with open(payment.invoice_file.url, 'rb') as f:
        #     attachment.file_content = base64.b64encode(f.read()).decode('utf-8')
        attachment.file_name = 'invoice-to-recharge-balance.pdf'
        message.add_attachment(attachment)

        sg = SendGridAPIClient(settings.SENDGRID_API_KEY)
        sg.send(message)

        return payment


class ApplicationToUnbalanceSerializer(serializers.ModelSerializer):

    class Meta:
        model = ApplicationToUnbalance
        fields = (
            'id',
            'amount',
            'credit_card',
            'created',
            'status',
        )

        read_only_fields = (
            'created',
            'status',
        )

    def create(self, validated_data):
        validated_data['user'] = self.context['request'].user
        print('Вывод денег ')
        print(validated_data)
        return super().create(validated_data)


class ConfirmInvoicePocketPaidSerializer(serializers.ModelSerializer):

    paid_check = serializers.FileField(required=True)

    class Meta:
        model = PaymentTransaction
        fields = (
            'amount',
            'created',
            'pocket',
            'paid_check',
            'invoice_file'
        )

        read_only_fields = (
            'amount',
            'created',
            'pocket',
            'invoice_file',
        )


class ConfirmInvoiceRechargeSerializer(serializers.ModelSerializer):

    paid_check = serializers.FileField(required=True)

    class Meta:
        model = PaymentTransaction
        fields = (
            'amount',
            'created',
            'paid_check',
            'invoice_file',
            'is_approved',
        )

        read_only_fields = (
            'amount',
            'created',
            'invoice_file',
            'is_approved',
        )


class PaymentTransactionInvoiceSerializer(serializers.ModelSerializer):

    class Meta:
        model = PaymentTransaction
        fields = (
            'id',
            'amount',
            'source',
            'paid_check',
            'invoice_file',
            'created'
        )

        read_only_fields = fields


class PaymentTransactionLiqpaySerializer(serializers.ModelSerializer):

    class Meta:
        model = PaymentTransaction
        fields = (
            'id',
            'amount',
            'source',
            'status',
            'description',
            'err_description',
            'created',
        )

        read_only_fields = fields


class PaymentTransactionUnbalanceSerializer(serializers.ModelSerializer):
    class Meta:
        model = PaymentTransaction
        fields = (
            'id',
            'amount',
            'created',
            'status',
        )

        read_only_fields = fields


class PaymentTransactionOrderSerializer(serializers.ModelSerializer):

    class Meta:
        model = PaymentTransaction
        fields = (
            'id',
            'amount',
            'created',
            'source',
            'system_order',
            'total_sum'
        )

    def to_representation(self, instance):
        result_instance = super().to_representation(instance)
        if self.context['request'].user.role == 'CONTRACTOR':
            result_instance['system_order'] = instance.system_order.order.id
            result_instance['partner'] = instance.system_order.order.user_id
            result_instance['partner_and_marketplace_part'] = instance.partner_and_marketplace_part
        elif self.context['request'].user.role == 'PARTNER':
            result_instance['system_order'] = instance.system_order.order.id
            result_instance['contractor'] = instance.system_order.order.user_id
            result_instance['contractor_and_marketplace_part'] = instance.contractor_and_marketplace_part
        return result_instance


class PaymentTransactionOrderAnnotatedSerializer(serializers.ModelSerializer):
    full_amount = serializers.DecimalField(max_digits=5, decimal_places=5)
    difference = serializers.DecimalField(max_digits=5, decimal_places=5)
    balance_of_the_total = serializers.DecimalField(max_digits=5, decimal_places=5)

    class Meta:
        model = PaymentTransaction
        fields = (
            'full_amount',
            'difference',
            'balance_of_the_total',
        )


class RozetkaTransactionHistorySerializer(serializers.ModelSerializer):

    class Meta:
        model = RozetkaTransactionHistory
        fields = (
            'user',
            'id',
            'rozetka_id',
            'log_id',
            'order_id',
            'product_id',
            'purchase_id',
            'price',
            'quantity',
            'cost',
            'operation_type',
            'sum_in_gray_changed',
            'sum_in_gray',
            'current_balance',
            'balance_changed',
            'subscription_balance',
            'subscription_balance_changed',
            'correction_number',
            'created_at',
            'transaction_ts',
        )


class RozetkaInvoicesSerializer(serializers.ModelSerializer):

    class Meta:
        model = RozetkaInvoices
        fields = (
            'id',
            'rozetka_id',
            'user',
            'market_owox_id',
            'market_id',
            'amount',
            'contract_number',
            'number',
            'file_invoice',
            'type',
            'date_of_invoice',
        )


class RozetkaReportFileTMPURLSerializer(serializers.ModelSerializer):

    class Meta:
        model = RozetkaReportFileTMPURL
        fields = (
            'id',
            'rozetka_id',
            'user',
            'market_id',
            'created_at',
            'report_period',
            'report_file',
        )


class TransactionFullByUserSerializer(serializers.ModelSerializer):

    class Meta:
        model = PaymentTransaction
        fields = (
            'id',
            'amount',
            'trans_type',
            'source',
            'pocket',
            'status',
            'agent_commission',
            'is_valid_signature',
            'card_token',
            'commission_credit',
            'commission_debit',
            'currency',
            'description',
            'err_description',
            'info',
            'ip',
            'way_for_pay_order_id',
            'order_pay_id',
            'payment_id',
            'sender_card_bank',
            'sender_card_country',
            'sender_card_mask2',
            'sender_card_type',
            'sender_first_name',
            'sender_last_name',
            'sender_phone',
            'system_order',
            'application',
            'invoice_file',
            'paid_check',
            'is_approved',
            'created',
            'updated',
            'date_status',
            'transaction_status',
            'system_order_partner',
            'system_order',
            'system_order_self'
        )

    def to_representation(self, instance):
        ret = super().to_representation(instance)
        if ret['source']:
            sources = dict(TransactionSources.TRANSACTION_SOURCES)
            ret['source'] = sources[ret['source']]
        if ret['status']:
            statuses_dict = dict(WayforPayConstant.UA_PAY_STATUSES)
            ret['status'] = statuses_dict[ret['status'].upper()]
        if ret['trans_type']:
            trans_types = dict(TransactionTypes.TRANSACTION_TYPES)
            ret['trans_type'] = trans_types[ret['trans_type']]
        if ret['system_order_partner']:
            ret['order_id'] = instance.system_order_partner.id if  instance.system_order_partner else None
        if ret['system_order']:
            ret['order_id'] = instance.system_order.id if instance.system_order else None
        if ret['system_order_self']:
            ret['order_id'] = instance.system_order_self.id if instance.system_order_self  else None
        return ret


class RechargeByLiqPaySerializer(serializers.ModelSerializer):
    class Meta:
        model = PaymentTransaction
        fields = (
            'id',
            'amount',
            'status',
            'agent_commission',
            'is_valid_signature',
            'card_token',
            'commission_credit',
            'commission_debit',
            'currency',
            'description',
            'err_description',
            'info',
            'ip',
            'way_for_pay_order_id',
            'order_pay_id',
            'payment_id',
            'sender_card_bank',
            'sender_card_country',
            'sender_card_mask2',
            'sender_card_type',
            'sender_first_name',
            'sender_last_name',
            'sender_phone',
            'created',
            'updated'
        )


class UaPayInvoiceSerializer(serializers.Serializer):
    amount = serializers.DecimalField(max_digits=10, decimal_places=2, required=True)
    description = serializers.CharField(max_length=256, required=True)
    # amount = serializers.DecimalField(max_digits=10, decimal_places=2, required=True)

    class Meta:
        fields = (
            'amount',
            'description',
        )

class WayForPaySerializer(serializers.ModelSerializer):

    class Meta:
        model = DataFromWidget
        fields = (
            'merchant_account',
            'merchant_signature',
            'order_reference',
            'amount',
            'currency',
            'auth_code',
            'email',
            'phone',
            'created_date',
            'processing_date',
            'card_pan',
            'card_type',
            'issuer_bank_country',
            'issuer_bank_name',
            'reason',
            'reason_code',
            'fee',
            'payment_system',
            'user',
            # 'client_start_time',
        )