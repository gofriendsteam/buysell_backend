import datetime
import hmac
import time
from decimal import Decimal
from pprint import pprint

import requests
from celery.utils.log import get_task_logger
from django.contrib.auth import get_user_model
from django.utils.dateparse import parse_datetime

from catalog.models import Product
from catalog.utils import get_rozetka_auth_token
from buy_sell.celery import app
from orders.models import OrderSelf, Order, ContractorOrder

from payments.models import RozetkaTransactionHistory, Balance_status, \
    RozetkaInvoices, RozetkaReportFileTMPURL, \
    PaymentTransaction, SystemBalance, Balance_status, PaymentInvoiceInfo, PaymentInvoice, DataFromWidget
from users.models import CustomUser

User = get_user_model()
logger = get_task_logger(__name__)


def upload_rozetka_transaction_history(user, token_rozetka):
    next_page = 1
    processing = True
    while processing:
        url = 'https://api.seller.rozetka.com.ua/balances/search?page={}'.format(next_page)
        headers = {
            'Authorization': "Bearer {}".format(token_rozetka),
            'cache-control': "no-cache",
        }
        r = requests.Request("GET", url, headers=headers)
        prep = r.prepare()
        s = requests.Session()
        resp = s.send(prep)
        r.encoding = 'utf-8'
        data = resp.json()

        transaction_history_data = data['content']['billingLogUserBalances']
        if next_page == data['content']['_meta']['pageCount']:
            processing = False
        else:
            next_page += 1

        for item in transaction_history_data:
            RozetkaTransactionHistory.objects.update_or_create(
                rozetka_id=item['id'],
                user=user,
                defaults={
                    'log_id': item['logId'],
                    'order_id': item['orderId'],
                    'product_id': item['productId'],
                    'purchase_id': item['purchase_id'],
                    'operation_type': item['operationType'],
                    'price': item['price'],
                    'quantity': item['quantity'],
                    'cost': item['cost'],
                    'sum_in_gray_changed': item['sumInGrayChanged'],
                    'sum_in_gray': item['sum_in_gray'],
                    'current_balance': item['currentBalance'],
                    'balance_changed': item['balanceChanged'],
                    'subscription_balance': item['subscription_balance'],
                    'subscription_balance_changed': item['subscription_balance_changed'],
                    'correction_number': item['correctionNumber'],
                    'created_at': parse_datetime(item['createdAt']),
                    'transaction_ts': parse_datetime(item['transaction_ts']),
                }
            )


def upload_rozetka_invoices_files(user, token_rozetka, item_id):
    url = 'https://api.seller.rozetka.com.ua/invoices/invoice-file-tmp-url/{}'.format(item_id)
    headers = {
        'Authorization': "Bearer {}".format(token_rozetka),
        'cache-control': "no-cache",
    }
    r = requests.Request("GET", url, headers=headers)
    prep = r.prepare()
    s = requests.Session()
    resp = s.send(prep)
    r.encoding = 'utf-8'
    data = resp.json()

    if data['success']:
        invoice = RozetkaInvoices.objects.get(rozetka_id=item_id, user=user)
        invoice.file_invoice = data['content']['file']
        invoice.save()


def upload_rozetka_invoices(user, token_rozetka):
    next_page = 1
    processing = True
    while processing:
        url = 'https://api.seller.rozetka.com.ua/invoices/search?page={}'.format(next_page)
        headers = {
            'Authorization': "Bearer {}".format(token_rozetka),
            'cache-control': "no-cache",
        }
        r = requests.Request("GET", url, headers=headers)
        prep = r.prepare()
        s = requests.Session()
        resp = s.send(prep)
        r.encoding = 'utf-8'
        data = resp.json()

        invoices = data['content']['invoices']
        if next_page == data['content']['_meta']['pageCount']:
            processing = False
        else:
            next_page += 1

        for item in invoices:
            RozetkaInvoices.objects.update_or_create(
                rozetka_id=item['id'],
                user=user,
                defaults={
                    'market_owox_id': item['market_owox_id'],
                    'market_id': item['market_id'],
                    'amount': item['amount'],
                    'contract_number': item['contract_number'],
                    'number': item['number'],
                    'type': item['type'],
                    'date_of_invoice': parse_datetime(item['date_of_invoice']),
                }
            )
            upload_rozetka_invoices_files(user, token_rozetka, item['id'])


def upload_rozetka_report_tmp_file(user, token_rozetka, item_id):
    url = 'https://api.seller.rozetka.com.ua/reports/report-file-tmp-url/{}'.format(item_id)
    headers = {
        'Authorization': "Bearer {}".format(token_rozetka),
        'cache-control': "no-cache",
    }
    r = requests.Request("GET", url, headers=headers)
    prep = r.prepare()
    s = requests.Session()
    resp = s.send(prep)
    r.encoding = 'utf-8'
    data = resp.json()

    if data['success']:
        report = RozetkaReportFileTMPURL.objects.get(rozetka_id=item_id, user=user)
        report.report_file = data['content']['file']
        report.save()


def upload_rozetka_report_tmp(user, token_rozetka):
    next_page = 1
    processing = True
    while processing:
        url = 'https://api.seller.rozetka.com.ua/reports/search?page={}'.format(next_page)
        headers = {
            'Authorization': "Bearer {}".format(token_rozetka),
            'cache-control': "no-cache",
        }
        r = requests.Request("GET", url, headers=headers)
        prep = r.prepare()
        s = requests.Session()
        resp = s.send(prep)
        r.encoding = 'utf-8'
        data = resp.json()

        invoices = data['content']['reports']
        if next_page == data['content']['_meta']['pageCount']:
            processing = False
        else:
            next_page += 1

        for item in invoices:
            RozetkaReportFileTMPURL.objects.update_or_create(
                rozetka_id=item['id'],
                user=user,
                defaults={
                    'market_id': item['market_id'],
                    'created_at': parse_datetime(item['created_at']),
                    'report_period': parse_datetime(item['report_period']),
                }
            )
            upload_rozetka_report_tmp_file(user, token_rozetka, item['id'])


@app.task
def load_rozetka_trans():
    # print('gooooooooo')
    for user in User.objects.filter(role='PARTNER'):
        token_rozetka = get_rozetka_auth_token(user)
        # print('user:')
        # print(user.id)
        # print('token:')
        # print(token_rozetka)
        if token_rozetka:
            upload_rozetka_transaction_history(user, token_rozetka)
            upload_rozetka_invoices(user, token_rozetka)
            upload_rozetka_report_tmp(user, token_rozetka)


@app.task
def create_payment_transactions_self_order_contractor():
    print('goooo')
    orders = OrderSelf.objects.all()
    for order in orders:
        # print(order.novaposhtadeliveryhistoryitemorderself.status)
        print(order.order_self_status_history_item.all().last().status_id)
        if order.order_self_status_history_item.all().last().status_id == 6:
            try:
                commision = round(
                    (order.product.price - (order.product.price * (100 - order.user.markup) / 100)) * order.count, 2)
                print('comm=', commision)
            except Exception as e:
                print('except operat', e)
            try:
                transaction = PaymentTransaction.objects.get(
                    system_order_self=order
                )
                continue
            except Exception as e:
                print('pay!!!!!!!!!!!!', e)
                print('total_price ', order.total_price)
                print('commision ', commision)

                transaction = PaymentTransaction(user_id=order.contractor_user_id,
                                                 # amount=5,
                                                 amount=round(order.total_price, 2),
                                                 agent_commission=Decimal(round(commision, 2)),
                                                 system_order_self=order,
                                                 source=2,
                                                 transaction_status=1,
                                                 system_balance_sum = Decimal(round(commision, 2))
                                                 )
                transaction.save()
        elif order.order_self_status_history_item.all().last().status_id == 19:
            try:
                PaymentTransaction.objects.update_or_create(user_id=order.contractor_user_id,
                                                            system_order_self=order,
                                                            defaults={
                                                                'transaction_status': 3,
                                                                'date_status': None
                                                            })
            except Exception as e:
                print('Exception!!!!!!!!!!!!!!!!!', e)


@app.task
def create_payment_transactions_self_order_partner():
    print('goooo')
    orders = OrderSelf.objects.all()
    for order in orders:
        # print(order.novaposhtadeliveryhistoryitemorderself.status)
        print(order.order_self_status_history_item.all().last().status_id)
        if order.order_self_status_history_item.all().last().status_id == 6:
            try:
                commision = round(
                    (order.product.price - (order.product.price * (100 - order.user.markup) / 100)) * order.count, 2)
                print('&&&&&&&&&&&&&&&&&&', order.product.price)
                print('comm=', commision)
            except Exception as e:
                print('except operat', e)
            try:
                transaction = PaymentTransaction.objects.get(
                    system_order_self=order
                )
                continue
            except Exception as e:
                print('pay!!!!!!!!!!!!', e)
                print('total_price ', order.total_price)
                print('commision ', commision)

                transaction = PaymentTransaction(user_id=order.user.id,
                                                 # amount=5,
                                                 amount=round(order.total_price, 2),
                                                 # agent_commission=round(commision, 2),
                                                 system_order_self=order,
                                                 source=2,
                                                 transaction_status=1
                                                 )
                transaction.save()
        elif order.order_self_status_history_item.all().last().status_id == 19:
            try:
                PaymentTransaction.objects.update_or_create(user_id=order.user.id,
                                                            system_order_self=order,
                                                            defaults={
                                                                'transaction_status': 3,
                                                                'date_status': None
                                                            })
            except Exception as e:
                print('Exception!!!!!!!!!!!!!!!!!', e)


@app.task
def create_payment_transactions_orders_partner():
    # print(55555)
    orders = Order.objects.all()
    for order in orders:

        if order.status == 6:
            commision_partner_sum = 0
            system_balance_sum = 0
            price_sum = 0
            # if PaymentTransaction.objects.get(system_order_partner=order)
            for product in order.products.all():
                try:
                    if product.fixed_recommended_price:  # описати фіксовану ррц
                        price = product.contractor_product.recommended_price
                        contractor = User.objects.get(id=product.contractor_product.user.id)
                        # commision_partner = price * product.partner_percent / 100
                        system_balance = (price - product.contractor_product.price) * contractor.markup / 100
                        commision_partner = price - product.contractor_product.price - system_balance

                    else:

                        price = product.contractor_product.price + (
                                product.contractor_product.price * product.partner_percent) / 100
                        contractor = User.objects.get(id=product.contractor_product.user.id)
                        # print(contractor.markup)
                        commision_partner = price * product.partner_percent / 100
                        # print(commision_partner)

                        system_balance = (price - commision_partner) * contractor.markup / 100
                        # print(system_balance)

                    commision_partner_sum += commision_partner
                    system_balance_sum += system_balance  # якщо ти шукаєш системний баланс він тут(5%)
                    price_sum += price
                except Exception as e:
                    print('error ', e)
            try:
                transaction = PaymentTransaction.objects.get(
                    system_order_partner=order
                )
                continue
            except Exception as e:
                print('pay!!!!!!!!!!!!', e)
                print('round(price_sum, 2) ' , round(price_sum, 2))
                print('round(commision_partner_sum, 2)' , round(commision_partner_sum, 2))
                try:

                    transaction = PaymentTransaction(user_id=order.user.id,
                                                     # amount=5,
                                                     amount=round(price_sum, 2),
                                                     agent_commission=round(commision_partner_sum, 2),
                                                     system_order_partner=order,
                                                     source=2,
                                                     transaction_status=1
                                                     )
                    transaction.save()

                except Exception as e:
                    print('Error!!!')
                    # print(type(e) , e)
                    # return
        elif order.status == 19:
            try:
                PaymentTransaction.objects.update_or_create(user_id=order.user.id,
                                                            system_order_partner=order,
                                                            defaults={
                                                                'transaction_status': 3,
                                                                'date_status': None
                                                            })
            except Exception as e:
                print('Exception!!!!!!!!!!!!!!!!!', e)


@app.task
def create_payment_transactions_orders_contractor():
    orders = ContractorOrder.objects.all()
    for order in orders:
        commision_partner_sum = 0
        system_balance_sum = 0
        price_sum = 0
        if order.status == 6:
            for product in order.products.all():
                if product.fixed_recommended_price:  # описати фіксовану ррц
                    print('contractor')
                    price = product.contractor_product.recommended_price
                    contractor = User.objects.get(id=product.contractor_product.user.id)
                    # commision_partner = price * product.partner_percent / 100
                    system_balance = (price - product.contractor_product.price) * contractor.markup / 100
                    commision_partner = price - product.contractor_product.price - system_balance


                    pass
                else:
                    price = product.contractor_product.price + (
                            product.contractor_product.price * product.partner_percent) / 100
                    contractor = User.objects.get(id=order.contractor.id)
                    commision_partner = price * product.partner_percent / 100
                    system_balance = (price - commision_partner) * contractor.markup / 100
                    # print(system_balance)
                commision_partner_sum += commision_partner
                system_balance_sum += system_balance  # якщо ти шукаєш системний баланс він тут(5%)
                price_sum += price
            all_commision_contractor = system_balance_sum + commision_partner_sum
            try:
                transaction = PaymentTransaction.objects.get(
                    system_order=order
                )
                continue
            except Exception as e:
                print('pay!!!!!!!!!!!!', e)
                transaction = PaymentTransaction(user_id=order.contractor.id,
                                                 # amount=5,
                                                 amount=round(price_sum, 2),
                                                 agent_commission=round(all_commision_contractor, 2),
                                                 system_order=order,
                                                 source=2,
                                                 transaction_status=1,
                                                 commision_partner_sum=commision_partner_sum,
                                                 system_balance_sum=system_balance_sum
                                                 )
                transaction.save()
        elif order.status == 19:
            try:
                PaymentTransaction.objects.update_or_create(user_id=order.contractor.id,
                                                            system_order=order,
                                                            defaults={
                                                                'transaction_status': 3,
                                                                'date_status': None
                                                            })
            except Exception as e:
                print('Exception!!!!!!!!!!!!!!!!!', e)


@app.task
def update_date_status():
    transactions = PaymentTransaction.objects.all()
    CONTRACTOR = 'CONTRACTOR'
    for trans in transactions:
        contractor = trans.user
        delta = trans.created - datetime.datetime.now()
        # print(trans.id)
        if abs(delta.days) >= 14:
            print(True)
            if not trans.date_status and contractor.role == CONTRACTOR:
                system_balance = trans.system_balance_sum
                commision_partner_sum = None
                if not trans.system_order_self:
                    commision_partner_sum = trans.commision_partner_sum
                # if trans.system_order_self:
                #     system_balance = trans.system_balance_sum
                print('commision_partner_sum' , commision_partner_sum)
                balance = SystemBalance.get_solo() # Баланс системы БАЙСЕЛ
                balance.balance += system_balance or 0
                balance.save()
                try:
                    balance_contractor = Balance_status.objects.get(user_contactor=contractor)
                    balance_contractor.total -= commision_partner_sum or 0
                    balance_contractor.save()
                except Exception as e:
                    b = Balance_status(user_contactor=contractor , total=-(commision_partner_sum or 0)) # минусовый баланс
                    b.save()
                print('system_balance ', system_balance)
                print('commision_partner_sum ', commision_partner_sum)
                pass  # Create SystemBalance (model) with FK to transaction
            # тут потрібно викинути возврати(PaymentTransaction transaction_status StatusConstants.REVERSED)
            PaymentTransaction.objects.filter(pk=trans.id).update(date_status=True,
                                                                  transaction_status=2 ,
                                                                  is_approved=False)


@app.task
def update_transactionlist():
    print(44)
    # merchantAccount;dateBegin;dateEnd
    date_end = int(time.time())
    date_begin = date_end - 3600*3  # за 3 часа
    secret_key = b'8e1f9051f1b6e43fc1a3cb6f18718f4662db9c93'
    message = '{};{};{}'.format('buy_sell_com_ua', date_begin, date_end)
    merchant_signature = hmac.new(secret_key, bytes(message, 'utf-8')).hexdigest()
    # print(str)
    payload = \
        {
            "apiVersion": 1,
            "transactionType": "TRANSACTION_LIST",
            "merchantAccount": "buy_sell_com_ua",
            "merchantSignature": merchant_signature,
            "dateBegin": date_begin,
            "dateEnd": date_end
        }

    url = "https://api.wayforpay.com/api"
    headers = {'Content-type': 'application/json'}
    x = requests.post(url, json=payload, headers=headers)
        # print(x.text)
    response_data = x.json()
    # print(response_data)
    if response_data['reason'] == "Ok":
        for trans in response_data['transactionList']:
            widget = DataFromWidget.objects.filter(order_reference=trans['orderReference']).last()

            # print(widget.user.id)
            instance, created = PaymentTransaction.objects.update_or_create(user=CustomUser.objects.get(id=widget.user.id),
                                                        # amount=trans[''],
                                                        status=trans['transactionStatus'],
                                                        way_for_pay_order_id=trans['orderReference'],
                                                        source=1,
                                                        info=trans['reason'],
                                                        defaults={
                                                            'amount': trans['amount'],
                                                            'currency': trans['currency'],
                                                            'sender_phone': trans['phone'] if ('phone' in trans) else None,
                                                            'sender_card_bank': trans['issuerBankName']  if ('issuerBankName' in trans) else None,
                                                            'sender_card_country': trans['issuerBankCountry'] if ('issuerBankCountry' in trans) else None,
                                                            'sender_card_mask2': trans['cardPan']  if ('cardPan' in trans) else None,
                                                            'sender_card_type': trans['cardType'] if ('cardType' in trans) else None
                                                        }
                                                        )
            print('instance ',  instance)
            print('created', created)
            if created:
                if trans['transactionStatus'] == 'Approved':

                    balance = Balance_status(user_contactor=widget.user, total=trans['amount'])
                    balance.save()
            print(trans)