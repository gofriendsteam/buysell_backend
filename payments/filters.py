from django.contrib.auth import get_user_model
from django_filters import rest_framework as filters

from payments.models import ApplicationToUnbalance, RozetkaTransactionHistory, RozetkaInvoices, PaymentTransaction, \
    RozetkaReportFileTMPURL

User = get_user_model()

class ListFilter(filters.Filter):
    def filter(self, qs, value):
        value_list = value.split(u',')
        return super(ListFilter, self).filter(qs, filters.Lookup(value_list, 'in'))


class DateCreatedRangeBase(filters.FilterSet):
    start_date = filters.DateFilter(field_name="created", lookup_expr='gte')
    end_date = filters.DateFilter(field_name="created", lookup_expr='lte')


class ApplicationToUnbalanceFilter(DateCreatedRangeBase):

    class Meta:
        model = ApplicationToUnbalance
        fields = (
            'id',
            'amount',
            'credit_card',
            'status',
        )


class PaymentTransactionFilter(DateCreatedRangeBase):

    class Meta:
        fields = (
            'id',
            'amount',
            'created',
        )


class PaymentTransactionWithStatusFilter(DateCreatedRangeBase):

    class Meta:
        fields = (
            'id',
            'amount',
            'created',
            'status',
        )


class PaymentTransactionOrderFilter(DateCreatedRangeBase):
    source = filters.BaseInFilter(field_name='source', lookup_expr='in')

    class Meta:
        fields = (
            'id',
            'amount',
            'created',
            'source',
            'system_order',
        )


class PaymentLiqPayFilter(DateCreatedRangeBase):
    source = filters.BaseInFilter(field_name='source', lookup_expr='in')

    class Meta:
        fields = (
            'id',
            'amount',
            'status',
            'source',
            'created'
        )


class RozetkaTransactionHistoryFilter(filters.FilterSet):
    start_date = filters.DateFilter(field_name="created_at", lookup_expr='gte')
    end_date = filters.DateFilter(field_name="created_at", lookup_expr='lte')

    class Meta:
        model = RozetkaTransactionHistory
        fields = (
            'rozetka_id',
            'log_id',
            'order_id',
            'product_id',
            'purchase_id',
            'operation_type',
            'quantity',
            'cost',
            'sum_in_gray_changed',
            'sum_in_gray',
            'current_balance',
            'balance_changed',
            'subscription_balance',
            'subscription_balance_changed',
            'correction_number',
            'created_at',
            'transaction_ts',
        )


class RozetkaInvoicesFilter(filters.FilterSet):
    start_date = filters.DateFilter(field_name="date_of_invoice", lookup_expr='gte')
    end_date = filters.DateFilter(field_name="date_of_invoice", lookup_expr='lte')

    class Meta:
        model = RozetkaInvoices
        fields = (
            'rozetka_id',
            'market_owox_id',
            'market_id',
            'amount',
            'contract_number',
            'number',
            'type',
            'date_of_invoice',
        )


class RozetkaReportFileTMPURLFilter(filters.FilterSet):
    start_date = filters.DateFilter(field_name="created_at", lookup_expr='gte')
    end_date = filters.DateFilter(field_name="created_at", lookup_expr='lte')

    class Meta:
        model = RozetkaReportFileTMPURL
        fields = (
            'rozetka_id',
            'market_id',
            'created_at',
            'report_period',
        )


class TransactionFullFilter(filters.FilterSet):
    start_date = filters.DateFilter(field_name="created", lookup_expr='gte')
    end_date = filters.DateFilter(field_name="created", lookup_expr='lte')
    source = filters.BaseInFilter(field_name='source', lookup_expr='in')

    class Meta:
        model = PaymentTransaction
        fields = (
            'id',
            'amount',
            'trans_type',
            'source',
            'pocket',
            'status',
            'agent_commission',
            'is_valid_signature',
            'card_token',
            'commission_credit',
            'commission_debit',
            'currency',
            'description',
            'err_description',
            'info',
            'ip',
            'order_pay_id',
            'payment_id',
            'sender_card_bank',
            'sender_card_country',
            'sender_card_mask2',
            'sender_card_type',
            'sender_first_name',
            'sender_last_name',
            'sender_phone',
            'system_order',
            'application',
            'is_approved',
            'created',
            'updated',
        )
