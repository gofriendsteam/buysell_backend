## Deploy project

### Postgres
```
psql postgres
>> create database buy_dev; 
>> create user buy_user with password 'pass12pass';
>> grant all privileges on database buy_dev to buy_user;
```

### Python
```
apt install python3
apt install virtualenv
virtualenv -p python3 env
source env/bin/activate

```

### Redis

```
brew install redis-server
brew services start redis
```

### Django

```
. env.sh
pip install -r requirements.txt
python manage.py migrate

python manage.py runserver 
```

### Celery
```
celery -A buy_sell worker -D
```


### migrate conflicts
```
if command python manage.py migrate after git clone make conflict with names CategoryProm' and 'Category_prom_x' , 
you need reset to cfb586e id commit - "git reset --hard cfb586e" and than you can try run command 'python manage.py migrate',
if last command running without errors , than you can try put command 'git pull' and running 'python manage.py migrate'
```