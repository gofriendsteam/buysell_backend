import argparse
import asyncio
from nats.aio.client import Client as NATS


def generate_pub_args(subject, data):
    parser = argparse.ArgumentParser([])
    parser.set_defaults(subject=subject)

    parser.add_argument('--subject', default=subject)
    parser.add_argument('-d', '--data', default=data)
    parser.add_argument('-s', '--servers', default=[], action='append')
    parser.add_argument('--creds', default="")
    return parser.parse_args([])


def run(loop, args):

    nc = NATS()

    @asyncio.coroutine
    def error_cb(e):
        print("Error:", e)

    @asyncio.coroutine
    def closed_cb():
        print("Connection to NATS is closed.")

    @asyncio.coroutine
    def reconnected_cb():
        print("Connected to NATS at {}...".format(nc.connected_url.netloc))

    options = {
        "io_loop": loop,
        "error_cb": error_cb,
        "closed_cb": closed_cb,
        "reconnected_cb": reconnected_cb
    }

    if len(args.creds) > 0:
        options["user_credentials"] = args.creds

    try:
        if len(args.servers) > 0:
            options['servers'] = args.servers
        yield from nc.connect(**options)
    except Exception as e:
        print(e)

    print("Connected to NATS at {}...".format(nc.connected_url.netloc))
    yield from nc.publish(args.subject, args.data.encode())
    yield from nc.flush()
    yield from nc.close()


def send_nats_message(args):
    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)
    loop.set_debug(enabled=True)
    try:
        loop.run_until_complete(run(loop, args))
    finally:
        loop.close()
