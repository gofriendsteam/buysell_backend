class MessageSubjectType:
    UPDATE_CATEGORIES = 1
    UPDATE_PRODUCTS = 2
    UPDATE_MAIN_INFO = 3
    NEW_ORDER = 4
    HELP_INFO = 5
    CALL_BACK = 6
    EMAIL_SUBSCRIBER = 7


    SUBJECT_TYPES = (
        (UPDATE_CATEGORIES, 'Обновление категорий'),
        (UPDATE_PRODUCTS, 'Обновление продуктов'),
        (UPDATE_MAIN_INFO, 'Обновление общей информации'),
        (NEW_ORDER, 'Новый заказ'),
        (HELP_INFO, 'Заявка на помощь'),
        (CALL_BACK, 'Помощь'),
        (EMAIL_SUBSCRIBER, 'Подписка емейла')
    )


MAIN_SERVICE_NAME = 'topmarket'

