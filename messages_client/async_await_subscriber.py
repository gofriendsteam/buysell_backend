import argparse
import asyncio
import json
import signal

from nats.aio.client import Client as NATS

from messages_client.constants import MessageSubjectType
# from orders.tasks1 import new_order_from_shop


def generate_sub_args(subject):
    parser = argparse.ArgumentParser()
    parser.add_argument('subject', default=subject, nargs='?')
    parser.add_argument('-s', '--servers', default=[], action='append')
    parser.add_argument('-q', '--queue', default="")
    parser.add_argument('--creds', default="")
    return parser.parse_args([])


# handler_functions_dict = {
#     MessageSubjectType.NEW_ORDER: new_order_from_shop,
# }


async def run(loop, args):
    nc = NATS()

    await nc.connect("127.0.0.1:4222", loop=loop)

    async def error_cb(e):
        print("Error: ", e)

    async def closed_cb():
        print("Connection to NATS is closed.")
        await asyncio.sleep(0.1, loop=loop)

    async def reconnected_cb():
        print("Connected to NATS at {}...".format(nc.connected_url.netloc))

    async def subscribe_handler(msg):
        data = msg.data.decode().replace("\'", "\"").replace("None", "\"\"")
        print(data)
        data_json = json.loads(data)
        receive_type = data_json['type']
        receive_source = data_json['source']
        # handler_functions_dict[receive_type](data_json['content'], receive_source)

    options = {
        "io_loop": loop,
        "error_cb": error_cb,
        "closed_cb": closed_cb,
        "reconnected_cb": reconnected_cb
    }

    if len(args.creds) > 0:
        options["user_credentials"] = args.creds

    try:
        if len(args.servers) > 0:
            options['servers'] = args.servers

        await nc.connect(**options)
    except Exception as e:
        print(e)

    print("Connected to NATS at {}...".format(nc.connected_url.netloc))

    def signal_handler():
        if nc.is_closed:
            return
        print("Disconnecting...")
        loop.create_task(nc.close())

    for sig in ('SIGINT', 'SIGTERM'):
        loop.add_signal_handler(getattr(signal, sig), signal_handler)

    await nc.subscribe(args.subject, args.queue, subscribe_handler)


def receive_nats_message(args):
    loop = asyncio.get_event_loop()
    asyncio.ensure_future(run(loop, args))
    loop.run_until_complete(run(loop, args))
    loop.run_forever()
    # try:
    #     loop.run_forever()
    # finally:
    #     loop.close()


args = generate_sub_args('topmarket')

receive_nats_message(args)
