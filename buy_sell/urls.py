from django.contrib import admin
from django.core.files.base import ContentFile
from django.http import HttpResponse
from django.urls import path, include
from drf_yasg import openapi
from drf_yasg.inspectors import SwaggerAutoSchema
from drf_yasg.views import get_schema_view
from rest_framework.permissions import AllowAny
from django.conf import settings
from django.conf.urls.static import static
from catalog.models import YMLTemplate
from catalog.yml_handler_view import YmlHandler


class CategorizedAutoSchema(SwaggerAutoSchema):
    def get_tags(self, operation_keys):
        if len(operation_keys) >= 1:
            operation_keys = operation_keys[1:]
        return super().get_tags(operation_keys)


schema_view = get_schema_view(
    openapi.Info(
        title="Buy&Sell API",
        default_version='v1',
    ),
    permission_classes=(AllowAny,),
)


def errors_views(request):
    import datetime
    import os
    file_error = os.path.join('.', 'error.txt')
    with open(file_error) as f:
        errors = ''
        for err in f.readlines():
            errors += '<p>' + err + '</p>'
        return HttpResponse(errors)
    # return HttpResponse('asdasd')


from django.contrib.auth import get_user_model
import os

User = get_user_model()


def update_storage(request):
    path_to_yml_templates = os.path.join(settings.MEDIA_ROOT, 'yml_templates')
    ROZETKA_NAME = 'rozetka_products-'
    list_files = os.listdir(path_to_yml_templates)
    for filename in list_files:
        if ROZETKA_NAME in filename:
            user_id = filename.replace(ROZETKA_NAME, '').replace('.xml', '')
            id_ = None
            if user_id.rfind('_') != -1:
                index_underscore = user_id.rfind('_')
                id_ = user_id[:index_underscore]
            else:
                id_ = user_id
            if id_:
                try:
                    yml_template = YMLTemplate.objects.get(user__id=int(id_))
                    # if os.path.exists(os.path.join(settings.MEDIA_ROOT, 'yml_templates' , filename)):
                    #     continue
                    with open(os.path.join(settings.MEDIA_ROOT, yml_template.template.name), 'r') as file:
                        file_content = file.read()
                        new_file = ContentFile(file_content)
                        yml_template.delete()
                        new_yml_template = YMLTemplate(user_id=id_)
                        new_yml_template.template.save(ROZETKA_NAME + id_ + '.xml', new_file)
                        new_yml_template.save()
                        os.remove(os.path.join(settings.MEDIA_ROOT, 'yml_templates', filename))

                        # print(filename, 'filename')
                        # print(new_file)
                        # continue
                except YMLTemplate.DoesNotExist as e:
                    with open(os.path.join(settings.MEDIA_ROOT, 'yml_templates', filename), 'r') as file:
                        try:
                            User.objects.get(id=int(id_))
                            new_yml_template = YMLTemplate(user_id=int(id_))
                            file_content = file.read()
                            new_file = ContentFile(file_content)
                            new_yml_template.template.save(ROZETKA_NAME + id_ + '.xml', new_file)
                            new_yml_template.save()
                            os.remove(os.path.join(settings.MEDIA_ROOT, 'yml_templates', filename))

                            # continue
                        except Exception as e:
                            print(type(e), e)
                except FileNotFoundError as e:
                    with open(os.path.join(settings.MEDIA_ROOT, 'yml_templates', filename), 'r') as file:
                        content = file.read()
                        yml_template.delete()
                        new_file = ContentFile(content)
                        new_yml_template = YMLTemplate(user_id=id_)
                        new_yml_template.template.save(ROZETKA_NAME + id_ + '.xml', new_file)
                        new_yml_template.save()

                        os.remove(os.path.join(settings.MEDIA_ROOT, 'yml_templates', filename))
                        # continue
    return HttpResponse('ok!')


def create_info_price_yml(request):
    path_to_yml_templates = os.path.join(settings.MEDIA_ROOT, 'yml_templates')
    list_files = os.listdir(path_to_yml_templates)
    for file in list_files:
        yml  = YmlHandler(filepath=file)
        yml.update_price_info()
    return HttpResponse('create_info_price_yml!')


def delete_all_yml_templates(req):
    YMLTemplate.objects.all().delete()
    import os
    path_to_yml_templates = os.path.join(settings.MEDIA_ROOT, 'yml_templates')
    ROZETKA_NAME = 'rozetka_products-'
    list_files = os.listdir(path_to_yml_templates)
    for filename in list_files:
        os.remove(os.path.join(settings.MEDIA_ROOT, 'yml_templates', filename))

    return HttpResponse('delete all files!')


urlpatterns = [
    path('admin/', admin.site.urls),
    path('docs/', schema_view.with_ui(), name='docs'),
    path('api/<version>/', include('users.urls')),
    path('api/<version>/news/', include('news.urls')),
    path('api/<version>/marketplace/', include('marketplace.urls')),
    path('api/<version>/catalog/', include('catalog.urls', namespace='catalog')),
    path('api/<version>/', include('orders.urls')),
    path('api/<version>/payments/', include('payments.urls')),
    path('api/<version>/abc/', include('abc_profiles.urls')),
    path('api/<version>/academy/', include('academy.urls')),
    path('errors/', errors_views),
    path('create_info_price_yml/', create_info_price_yml),

    # path('update_storage/', update_storage),
    # path('delete_all_yml_templates/', delete_all_yml_templates),
]
urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
