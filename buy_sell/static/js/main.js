jQuery(document).ready(function($) {
	$('.owl-carousel').owlCarousel({
		items: 2,
		responsive:{
			0:{
				items: 1,
			},
			992:{
				items:2,
			}
		}
	})
});