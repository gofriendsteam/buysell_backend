from urllib.parse import quote

from storages.backends.gcloud import GoogleCloudStorage
from storages.utils import clean_name
from django.conf import settings


class CustomGoogleCloudStorage(GoogleCloudStorage):

    def url(self, name):
        """
        Return public url or a signed url for the Blob.
        This DOES NOT check for existance of Blob - that makes codes too slow
        for many use cases.
        """
        name = self._normalize_name(clean_name(name))
        blob = self.bucket.blob(self._encode_name(name))
        public_url = "{storage_base_url}/{bucket_name}/{quoted_name}".format(
            storage_base_url=settings.CUSTOM_API_ACCESS_ENDPOINT,
            bucket_name=blob.bucket.name,
            quoted_name=quote(blob.name.encode("utf-8")),
        )
        return public_url

