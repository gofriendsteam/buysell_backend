from django.conf import settings
from django.views.static import serve, Http404


class StaticServeMiddleware(object):
    """
    Middleware to serve media files on developer server
    You must put this at top of all middlewares for speedups
    (Whe dont need sessions, request.user or other stuff)
    """
    MEDIA_URL = getattr(settings, 'MEDIA_URL', None)
    MEDIA_ROOT = getattr(settings, 'MEDIA_ROOT', None)

    def process_request(self, request):
        if self.MEDIA_ROOT and self.MEDIA_URL and request.path_info.startswith(self.MEDIA_URL):
            path = request.path_info.replace(self.MEDIA_URL, '').lstrip('/')
            print(path)
        #     abs_uri = request.build_absolute_uri()
        #     try:
        #         response = serve(request, path=path, document_root=self.MEDIA_ROOT)
        #         return response
        #     except Http404:
        #         raise Http404('Requested file was not found on the file system')
        # return None