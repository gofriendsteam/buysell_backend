from __future__ import absolute_import, unicode_literals
import os
from celery import Celery
from celery.schedules import crontab

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'buy_sell.settings')

app = Celery('buy_sell')
app.config_from_object('django.conf:settings', namespace='CELERY')
app.autodiscover_tasks()

app.conf.CELERYBEAT_SCHEDULE = {
    "set_status": {
        "task": "orders.tasks.checkout_orders",
        "schedule": crontab(minute="*/30"),
    },
    "upload_orders": {
        "task": "orders.tasks.upload_orders",
        "schedule": crontab(minute="*/10"),
    },
    "upload_orders_prom": {
        "task": "orders.tasks.upload_orders_prom",
        "schedule": crontab(minute="*/16"),
    },
    "checkout_nova_poshta_delivery_status": {
        "task": "orders.tasks.checkout_nova_poshta_delivery_status",
        "schedule": crontab(minute="*/15"),
        # "schedule": crontab(minute="*/10"),
    },
    "checkout_nova_poshta_delivery_status_order_self": {
        "task": "orders.tasks.checkout_nova_poshta_delivery_status_order_self",
        "schedule": crontab(minute="*/35"),
    },
    # "load_categories": {
    #     "task": "catalog.tasks.load_categories",
    #     "schedule": crontab(day_of_week="3", hour=18)
    #     # "schedule": crontab(hour="*/1")
    #     # "schedule": crontab(minute="*/2")
    # },
    # # "load_categories": {
    # #     "task": "catalog.tasks.load_categories",
    # #     "schedule": crontab(day_of_week="4", hour=15)
    # #     # "schedule": crontab(hour="*/1")
    # #     # "schedule": crontab(minute="*/2")
    # # },
    # "load_categories_prom": {
    #     "task": "catalog.tasks.load_categories",
    #     "schedule": crontab(day_of_week="4", hour=15)
    #     # "schedule": crontab(hour="*/1")
    #     # "schedule": crontab(minute="*/2")
    # },
    "load_rozetka_trans": {
        "task": "payments.tasks.load_rozetka_trans",
        "schedule": crontab(minute="*/16"),
    },
    "create_payment_transactions_self_order_contractor": {
        "task": "payments.tasks.create_payment_transactions_self_order_contractor",
        "schedule": crontab(minute="*/10"),
    },
    "update_date_status": {
        "task": "payments.tasks.update_date_status",
        "schedule": crontab(minute="*/11"),
    },
    "create_payment_transactions_orders_partner": {
        "task": "payments.tasks.create_payment_transactions_orders_partner",
        "schedule": crontab(minute="*/12"),
    },
    "create_payment_transactions_orders_contractor": {
        "task": "payments.tasks.create_payment_transactions_orders_contractor",
        "schedule": crontab(minute="*/13"),
    },
    "create_payment_transactions_self_order_partner": {
        "task": "payments.tasks.create_payment_transactions_self_order_partner",
        "schedule": crontab(minute="*/14"),
    },
    "update_payment_invoice": {
        "task": "payments.tasks.update_transactionlist",
        "schedule": crontab(hour="*/20"),
    },
}
