from django.contrib import admin
from mptt.admin import MPTTModelAdmin

from .models import (
    MainContent,
    VideoMaterial,
    PDFMaterial,
    HomeWork,
    PinnedMaterial,
    Item
)


admin.site.register(HomeWork)


class VideoMaterialTabularInline(admin.TabularInline):
    model = VideoMaterial
    extra = 1


class PDFMaterialTabularInline(admin.TabularInline):
    model = PDFMaterial
    extra = 1


class PinnedMaterialTabularInline(admin.TabularInline):
    model = PinnedMaterial
    extra = 1


@admin.register(Item)
class ItemModelAdmin(admin.ModelAdmin):
    inlines = [
        VideoMaterialTabularInline,
        PDFMaterialTabularInline,
        PinnedMaterialTabularInline,
    ]


class MainContentAdmin(MPTTModelAdmin):
    pass


admin.site.register(MainContent, MainContentAdmin)
