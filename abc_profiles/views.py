import uuid

from django.views import View
from django.conf import settings
from django.http import HttpResponse
from django.shortcuts import get_object_or_404
from django.contrib.auth import get_user_model
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt

from rest_framework import status
from rest_framework import permissions
from rest_framework.response import Response
from rest_framework.generics import ListAPIView, RetrieveAPIView, CreateAPIView

from liqpay.liqpay3 import LiqPay

from payments.constants import TransactionTypes, TransactionSources
from payments.models import PaymentTransaction
from .serializers import (
    ContentDetailSerializer,
    ItemDetailSerializer,
    HomeWorkSerializer,
    MainContentIdSerializer
)

from .models import MainContent, Item, HomeWork
from .permissions import IsDetailPayPermission

from catalog.utils import get_host_name
from payments.utils import get_liqpay_data_and_signature

User = get_user_model()


class LiqPayModuleBuyView(CreateAPIView):
    permission_classes = (permissions.AllowAny, )
    serializer_class = MainContentIdSerializer

    def post(self, request, *args, **kwargs):
        module = get_object_or_404(MainContent, pk=request.data.get('module_id'))
        print(request.user)
        params = {
            'action': 'pay',
            'amount': str(module.price),
            'currency': module.currency,
            'description': 'Payment for buying pocket',
            'order_id': "{}_{}_{}".format(request.user.id, module.id, uuid.uuid4()),
            'version': '3',
            'sandbox': 1,
            'result_url': get_host_name(),
            'server_url': get_host_name() + '/api/v1/payments/pocket-pay-callback/', # url to callback view
            # 'server_url': 'https://a7f65ac9.ngrok.io' + '/api/v1/abc/abc-pay-callback/',    # url to callback view
        }
        response_data = get_liqpay_data_and_signature(**params)

        return Response(
            response_data,
            status=status.HTTP_200_OK,
        )


@method_decorator(csrf_exempt, name='dispatch')
class LiqPayModuleCallBackView(View):
    # permission_classes = (AllowAny, )

    def post(self, request, *args, **kwargs):
        liqpay = LiqPay(settings.LIQ_PAY_PUBLIC_KEY, settings.LIQ_PAY_PRIVATE_KEY)
        data = request.POST.get('data')
        signature = request.POST.get('signature')
        sign = liqpay.str_to_sign(settings.LIQ_PAY_PUBLIC_KEY + data + settings.LIQ_PAY_PRIVATE_KEY)

        response = liqpay.decode_data_from_str(data)
        if sign == signature:
            response['is_valid_signature'] = True
        user = User.objects.get(id=int(response['order_id'].split('_')[0]))
        module = MainContent.objects.get(id=int(response['order_id'].split('_')[1]))
        PaymentTransaction.objects.create(
            user=user,
            amount=response.get('amount_credit'),
            trans_type=TransactionTypes.LIQPAY,
            source=TransactionSources.POCKET,
            module_id=int(response['order_id'].split('_')[1]),
            is_valid_signature=response.get('is_valid_signature', False),
            status=response.get('status', '').upper(),
            agent_commission=response.get('agent_commission', None),
            card_token=response.get('card_token', None),
            commission_credit=response.get('commission_credit', None),
            commission_debit=response.get('commission_debit', None),
            currency=response.get('currency', None),
            description=response.get('description', None),
            err_description=response.get('err_description', None),
            info=response.get('info', None),
            ip=response.get('ip', None),
            way_for_pay_order_id=response.get('way_for_pay_order_id', None),
            order_pay_id=response.get('order_id', None),
            payment_id=response.get('payment_id', None),
            sender_card_bank=response.get('sender_card_bank', None),
            sender_card_country=response.get('sender_card_country', None),
            sender_card_mask2=response.get('sender_card_mask2', None),
            sender_card_type=response.get('sender_card_type', None),
            sender_first_name=response.get('sender_first_name', None),
            sender_last_name=response.get('sender_last_name', None),
            sender_phone=response.get('sender_phone', None),
        )
        if response.get('status') == 'success':
            user.buy_abc_modules.add(module)
            user.save()
        return HttpResponse()   # HttpResponseRedirect(redirect_to='https://topmarket.ua/')


class ContentListAPIView(ListAPIView):
    serializer_class = ContentDetailSerializer
    permission_classes = permissions.IsAuthenticated,
    queryset = MainContent.objects.root_nodes()
    pagination_class = None


class ItemRetrieveAPIView(RetrieveAPIView):
    serializer_class = ItemDetailSerializer
    permission_classes = permissions.IsAuthenticated, IsDetailPayPermission
    queryset = Item.objects.all()
    pagination_class = None


class HomeWorkCreateAPIView(CreateAPIView):
    serializer_class = HomeWorkSerializer
    permission_classes = permissions.IsAuthenticated,
    queryset = HomeWork.objects.all()
    pagination_class = None
