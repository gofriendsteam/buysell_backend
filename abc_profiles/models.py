from django.db import models
from django.core.validators import FileExtensionValidator

from mptt.models import MPTTModel, TreeForeignKey
from ckeditor.fields import RichTextField
from .constants import IMAGE_EXTENSION, PINNED_CONTENT_EXTENSION, PDF, VIDEO_FILE_EXTENSION


class MainContent(MPTTModel):
    price = models.DecimalField(
        max_digits=10, decimal_places=2,
        verbose_name='Цена пакета',
        blank=True,
        null=True
    )
    currency = models.CharField(
        max_length=10,
        default='UAH',
        verbose_name='Валюта',
    )
    parent = TreeForeignKey(
        'self',
        on_delete=models.CASCADE,
        verbose_name='Родитель',
        related_name='children',
        null=True,
        blank=True
    )
    name = models.CharField(
        max_length=255,
        verbose_name='Название'
    )

    class Meta:
        verbose_name = 'Контент'
        verbose_name_plural = 'Контент'

    class MPTTMeta:
        ordering = ('tree_id', 'lft')
        order_insertion_by = ['name', ]

    def __str__(self):
        return 'Контент: {}'.format(self.name)


class Item(models.Model):
    content = models.ForeignKey(
        MainContent,
        on_delete=models.CASCADE,
        related_name='item_main_content',
        verbose_name='Контент'
    )
    image = models.FileField(
        validators=[FileExtensionValidator(allowed_extensions=IMAGE_EXTENSION)],
        verbose_name='Превью картинка'
    )
    name = models.CharField(
        max_length=30
    )
    description = models.CharField(max_length=255)
    description_HTML = RichTextField(max_length=10000, blank=True, null=True, verbose_name='Описание')

    class Meta:
        verbose_name = 'Элемент страницы'
        verbose_name_plural = 'Элементы страницы'

    def __str__(self):
        return 'Блок страницы: {}'.format(self.content.name)


class VideoMaterial(models.Model):
    item = models.ForeignKey(
        Item,
        on_delete=models.CASCADE,
        verbose_name='Контент',
        related_name='item_video_material',
        null=True,
        blank=True
    )
    file = models.FileField(
        validators=[FileExtensionValidator(allowed_extensions=VIDEO_FILE_EXTENSION)],
        null=True,
        blank=True,
        verbose_name='Файл'
    )
    link = models.URLField(
        null=True,
        blank=True,
        verbose_name='URL ссылка'
    )
    code = models.CharField(
        max_length=50,
        blank=True,
        null=True,
        verbose_name='Код доступа'
    )
    preview_image_video = models.ImageField()

    class Meta:
        verbose_name = 'Видео Материал'
        verbose_name_plural = 'Видео Материалы'

    def __str__(self):
        return 'Видео контент: - {}'.format(self.id)


class PDFMaterial(models.Model):
    item = models.ForeignKey(
        Item,
        on_delete=models.CASCADE,
        verbose_name='Контент',
        related_name='item_image_material',
        null=True,
        blank=True
    )
    file = models.FileField(
        validators=[FileExtensionValidator(allowed_extensions=PDF)],
        null=True,
        blank=True,
        verbose_name='Файл'
    )

    class Meta:
        verbose_name = 'PDF материал'
        verbose_name_plural = 'PDF материалы'

    def __str__(self):
        return 'Медиа контент: - {}'.format(self.id)


class PinnedMaterial(models.Model):
    item = models.ForeignKey(
        Item,
        on_delete=models.CASCADE,
        verbose_name='Контент',
        related_name='item_pinned_material',
        null=True, blank=True
    )
    file = models.FileField(
        validators=[FileExtensionValidator(allowed_extensions=PINNED_CONTENT_EXTENSION)],
        null=True,
        blank=True,
        verbose_name='Файл'
    )

    class Meta:
        verbose_name = 'Прикрепленный Материал'
        verbose_name_plural = 'Прикрепленные Материалы'

    def __str__(self):
        return 'Прикрепленный материал: - {}'.format(self.id)


class HomeWork(models.Model):
    user = models.ForeignKey(
        'users.CustomUser',
        on_delete=models.CASCADE,
        related_name='user_homework'
    )
    item = models.ForeignKey(
        Item,
        on_delete=models.CASCADE,
        verbose_name='Контент',
        related_name='item_homework_material',
        null=True, blank=True
    )
    link = models.URLField(verbose_name='URL ссылка')

    class Meta:
        verbose_name = 'Домашнее задание'
        verbose_name_plural = 'Домашнее задание'

    def __str__(self):
        return 'Домашнее задание: - {}'.format(self.id)
