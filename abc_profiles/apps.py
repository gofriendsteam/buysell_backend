from django.apps import AppConfig


class AbcProfilesConfig(AppConfig):
    name = 'abc_profiles'
