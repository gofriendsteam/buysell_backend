from rest_framework import serializers
from rest_framework.validators import UniqueValidator

from .models import (
    HomeWork,
    PDFMaterial,
    PinnedMaterial,
    VideoMaterial,
    Item,
    MainContent,
)


class HomeWorkSerializer(serializers.ModelSerializer):
    class Meta:
        model = HomeWork
        fields = (
            'id',
            'item',
            'user',
            'link',
        )


class PinnedMaterialSerializer(serializers.ModelSerializer):
    class Meta:
        model = PinnedMaterial
        fields = (
            'id',
            'file',
        )


class PDFMaterialSerializer(serializers.ModelSerializer):
    class Meta:
        model = PDFMaterial
        fields = (
            'id',
            'file',
        )


class VideoMaterialSerializer(serializers.ModelSerializer):
    class Meta:
        model = VideoMaterial
        fields = (
            'id',
            'file',
            'link',
            'preview_image_video',
        )


class ItemDetailSerializer(serializers.ModelSerializer):
    # homework = HomeWorkSerializer(many=True, source='item_homework_material', required=False)
    pinned = PinnedMaterialSerializer(many=True, source='item_pinned_material', required=False)
    pdf = PDFMaterialSerializer(many=True, source='item_image_material', required=False)
    video = VideoMaterialSerializer(many=True, source='item_video_material', required=False)
    homework = serializers.SerializerMethodField()

    def get_homework(self, obj):
        user = self.context.get('request').user
        qs = HomeWork.objects.filter(item=obj, user=user)
        serializer = HomeWorkSerializer(instance=qs, many=True)
        return serializer.data

    class Meta:
        model = Item
        fields = (
            'id',
            'name',
            'description',
            'description_HTML',
            'video',
            'pinned',
            'pdf',
            'homework',
        )


class ItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = Item
        fields = (
            'id',
            'name',
            'description',
            'image',
        )


class RecursiveField(serializers.BaseSerializer):

    def to_representation(self, instance):
        ParentSerializer = self.parent.parent.__class__
        serializer = ParentSerializer(instance, context=self.context)
        return serializer.data


class ContentDetailSerializer(serializers.ModelSerializer):
    child = RecursiveField(many=True, source='children')
    item = ItemSerializer(many=True, source='item_main_content')

    class Meta:
        model = MainContent
        fields = (
            'id',
            'name',
            'child',
            'price',
            'item',
            'level',
        )

    def to_representation(self, instance):
        data = super(ContentDetailSerializer, self).to_representation(instance)
        user = self.context.get('request').user
        lvl = data.pop('level')
        if lvl == 0:
            data['is_have_perm'] = True if data['id'] in user.buy_abc_modules.all().values_list('id',
                                                                                                flat=True) else False
        return data


class MainContentIdSerializer(serializers.Serializer):
    module_id = serializers.IntegerField(
        validators=[UniqueValidator(queryset=MainContent.objects.filter(level=0))]
    )

    class Meta:
        fields = (
            'module_id',
        )
