from rest_framework.permissions import BasePermission

from .models import MainContent, Item


class IsDetailPayPermission(BasePermission):
    def has_permission(self, request, view):
        try:
            obj = Item.objects.get(pk=view.kwargs.get('pk'))

            if obj.content.parent in request.user.buy_abc_modules.all():
                return True
        except Item.DoesNotExist:
            return False
        return False
