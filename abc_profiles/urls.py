from django.urls import path

from rest_framework import routers

from .views import (
    ContentListAPIView,
    ItemRetrieveAPIView,
    HomeWorkCreateAPIView,
    LiqPayModuleBuyView,
    LiqPayModuleCallBackView,
)


router = routers.DefaultRouter()
router.register('homework', HomeWorkCreateAPIView, base_name='abc_homework_route')


urlpatterns = [
    path('content/list/', ContentListAPIView.as_view(), name='abc_list_url'),
    path('item/detail/<int:pk>/', ItemRetrieveAPIView.as_view(), name='abc_detail_url'),
    path('homework/', HomeWorkCreateAPIView.as_view(), name='abc_homework_route'),
    path('abc-pay/', LiqPayModuleBuyView.as_view(), name='abc_module_pay_view'),
    path('abc-pay-callback/', LiqPayModuleCallBackView.as_view(), name='abc_module_pay_callback'),
]
