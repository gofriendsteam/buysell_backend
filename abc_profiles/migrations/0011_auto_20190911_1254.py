# Generated by Django 2.1.7 on 2019-09-11 12:54

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('abc_profiles', '0010_auto_20190911_1253'),
    ]

    operations = [
        migrations.RenameField(
            model_name='item',
            old_name='name',
            new_name='title',
        ),
    ]
