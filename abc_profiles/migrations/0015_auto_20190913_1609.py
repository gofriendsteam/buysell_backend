# Generated by Django 2.1.7 on 2019-09-13 16:09

import ckeditor.fields
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('abc_profiles', '0014_auto_20190913_1600'),
    ]

    operations = [
        migrations.AlterField(
            model_name='imagematerial',
            name='description_HTML',
            field=ckeditor.fields.RichTextField(blank=True, choices=[('left', 'left'), ('right', 'right'), ('bottom', 'bottom'), ('up', 'up')], max_length=5000, null=True, verbose_name='Описание'),
        ),
    ]
