# Generated by Django 2.1.7 on 2019-09-13 17:28

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('abc_profiles', '0019_auto_20190913_1727'),
    ]

    operations = [
        migrations.AlterField(
            model_name='block',
            name='text_position',
            field=models.CharField(choices=[('Слева', 'Слева'), ('Справа', 'Справа'), ('Снизу', 'Снизу'), ('Сверху', 'Сверху')], max_length=50),
        ),
        migrations.AlterField(
            model_name='imagematerial',
            name='text_position',
            field=models.CharField(choices=[('Слева', 'Слева'), ('Справа', 'Справа'), ('Снизу', 'Снизу'), ('Сверху', 'Сверху')], max_length=50),
        ),
        migrations.AlterField(
            model_name='pinnedmaterial',
            name='text_position',
            field=models.CharField(choices=[('Слева', 'Слева'), ('Справа', 'Справа'), ('Снизу', 'Снизу'), ('Сверху', 'Сверху')], max_length=50),
        ),
        migrations.AlterField(
            model_name='videomaterial',
            name='text_position',
            field=models.CharField(choices=[('Слева', 'Слева'), ('Справа', 'Справа'), ('Снизу', 'Снизу'), ('Сверху', 'Сверху')], max_length=50),
        ),
    ]
