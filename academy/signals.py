from django.db.models.signals import post_save
from django.dispatch import receiver

from academy.models import HomeAssignment, Lesson
from academy.tasks import check_finish_lessons, check_finish_module


@receiver(post_save, sender=HomeAssignment)
def update_stock(sender, instance, created, **kwargs):

    if instance:
        check_finish_lessons.delay()
        check_finish_module.delay()