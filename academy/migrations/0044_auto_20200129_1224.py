# Generated by Django 2.1.7 on 2020-01-29 12:24

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('academy', '0043_auto_20191226_1058'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='file_lesson',
            options={'verbose_name': 'файл урока', 'verbose_name_plural': 'файлы уроков'},
        ),
        migrations.AlterModelOptions(
            name='homeassignment',
            options={'verbose_name': 'Выполненное домашнее задание', 'verbose_name_plural': 'Выполненные домашние задания'},
        ),
        migrations.AlterModelOptions(
            name='hometask',
            options={'verbose_name': 'домашние задание', 'verbose_name_plural': 'домашние задания'},
        ),
        migrations.AlterModelOptions(
            name='lesson',
            options={'verbose_name': 'урок', 'verbose_name_plural': 'уроки'},
        ),
        migrations.AlterModelOptions(
            name='material',
            options={'verbose_name': 'материал', 'verbose_name_plural': 'материалы'},
        ),
        migrations.AlterModelOptions(
            name='module',
            options={'verbose_name': 'модуль', 'verbose_name_plural': 'модули'},
        ),
        migrations.AlterModelOptions(
            name='package',
            options={'verbose_name': 'пакет', 'verbose_name_plural': 'пакеты'},
        ),
        migrations.AlterModelOptions(
            name='videolink',
            options={'verbose_name': 'ссылка на видео', 'verbose_name_plural': 'ссылки на видео'},
        ),
        migrations.AlterField(
            model_name='homeassignment',
            name='status',
            field=models.SmallIntegerField(choices=[(1, 'Сдано'), (2, 'Не выполнено'), (3, 'Ожидание проверки')], null=True, verbose_name='Статус'),
        ),
        migrations.AlterField(
            model_name='homeassignment',
            name='text',
            field=models.TextField(blank=True, verbose_name='Текст'),
        ),
        migrations.AlterField(
            model_name='homeassignment',
            name='user',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL, verbose_name='Пользователь'),
        ),
        migrations.AlterField(
            model_name='homeassignment',
            name='verifyed',
            field=models.BooleanField(default=False, verbose_name='Верифицирован'),
        ),
    ]
