# Generated by Django 2.1.7 on 2019-12-16 18:21

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('academy', '0035_hometask'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='hometask',
            name='number',
        ),
    ]
