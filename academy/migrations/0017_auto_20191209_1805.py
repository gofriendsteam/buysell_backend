# Generated by Django 2.1.7 on 2019-12-09 18:05

import django.core.validators
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('academy', '0016_auto_20191209_1756'),
    ]

    operations = [
        migrations.CreateModel(
            name='Material',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('file', models.FileField(blank=True, null=True, upload_to='', validators=[django.core.validators.FileExtensionValidator(allowed_extensions=['pdf', 'doc', 'docx', 'zip', 'rar', 'xlsx', 'xls', 'xml', 'yml'])], verbose_name='Файл')),
            ],
        ),
        migrations.AlterField(
            model_name='lesson',
            name='file',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='academy.Material', verbose_name='PDF файл'),
        ),
    ]
