# Generated by Django 2.1.7 on 2019-12-06 17:02

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('academy', '0009_auto_20191206_1658'),
    ]

    operations = [
        migrations.AlterField(
            model_name='package',
            name='name',
            field=models.CharField(choices=[('Stand', 'Standart'), ('Bus', 'Business'), ('Prem', 'Premium')], default='Stand', max_length=5),
        ),
    ]
