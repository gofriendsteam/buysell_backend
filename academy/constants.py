from django.utils.translation import ugettext as _

PINNED_CONTENT_EXTENSION = ['zip', 'xlsx']
Files = ['pdf', 'doc', 'docx', 'zip', 'rar', 'xlsx', 'xls', 'xml', 'yml', 'png']


class PackageTypes:
    START = 'Start'
    STANDART = 'Stand'
    BUSINESS = 'Bus'
    PREMIUM = 'Prem'
    NO = 'No'

    NAME_CHOICES = [
        (START, 'Start'),
        (STANDART, 'Standart'),
        (BUSINESS, 'Business'),
        (PREMIUM, 'Premium'),
        (NO, 'No'),
    ]


class HomeAssignmentReviewTypes:
    PASSED = 1
    NOT_DONE = 2
    AWAIT_VERIFICATION = 3

    REVIEW_TYPES = (
        (PASSED, _('Сдано')),
        (NOT_DONE, _('Не выполнено')),
        (AWAIT_VERIFICATION, _('Ожидание проверки'))
    )
