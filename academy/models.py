import uuid

from django.contrib.postgres.fields import ArrayField
from django.core.validators import FileExtensionValidator
from django.db import models
from ckeditor.fields import RichTextField
from mptt.fields import TreeForeignKey

from .constants import HomeAssignmentReviewTypes, PINNED_CONTENT_EXTENSION, Files, PackageTypes


class Material(models.Model):
    file = models.FileField(
        validators=[FileExtensionValidator(allowed_extensions=Files)],
        null=True,
        blank=True,
        verbose_name='Файли'
    )

    class Meta:
        verbose_name = 'материал'
        verbose_name_plural = 'материалы'

    def __str__(self):
        return self.file.name


class Package(models.Model):
    name = models.CharField(
        max_length=5,
        choices=PackageTypes.NAME_CHOICES,
        default=PackageTypes.NO, )
    price = models.DecimalField(
        max_digits=10, decimal_places=2,
        verbose_name='Цена пакета',
        blank=True,
        null=True
    )
    currency = models.CharField(
        max_length=10,
        default='UAH',
        verbose_name='Валюта',
    )
    description = RichTextField(max_length=10000, blank=True, null=True, verbose_name='Описание')

    # parent = TreeForeignKey(
    #     'self',
    #     null=True,
    #     blank=True,
    #     related_name='children',
    #     on_delete=models.SET_NULL,
    #     db_index=True,
    # )

    class Meta:
        verbose_name = 'пакет'
        verbose_name_plural = 'пакеты'


class Module(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    title = models.CharField(max_length=150, verbose_name='Название')
    number = models.IntegerField(verbose_name='Номер модуля')
    package = models.ForeignKey(Package, on_delete=models.CASCADE, verbose_name='Пакет', null=True)

    # finished = models.BooleanField(default=False)

    def __str__(self, ):
        return self.title + str(self.number)

    class Meta:
        verbose_name = 'модуль'
        verbose_name_plural = 'модули'


class Lesson(models.Model):
    title = models.IntegerField(verbose_name='Номер урока')
    module = models.ForeignKey(
        'academy.Module',
        on_delete=models.CASCADE,
        related_name='lessons',
        verbose_name='Модуль'
    )
    text = models.TextField(blank=True)

    def __str__(self):
        module_to_str  = self.module.title + ' ' +str(self.module.number)
        return '{} Номер урока {}'.format(module_to_str, self.title)

    class Meta:
        verbose_name = 'урок'
        verbose_name_plural = 'уроки'

class File_Lesson(models.Model):
    lesson = models.ForeignKey(
        Lesson,
        on_delete=models.CASCADE,
        verbose_name='Урок',
        related_name='files',
        null=True,
    )
    lesson_file = models.ForeignKey(Material, on_delete=models.CASCADE, verbose_name='Файл', null=True, blank=True)


    class Meta:
        verbose_name = 'файл урока'
        verbose_name_plural = 'файлы уроков'


class VideoLink(models.Model):
    lesson = models.ForeignKey(
        Lesson,
        on_delete=models.CASCADE,
        verbose_name='Урок',
        related_name='videos',
        null=True,
    )
    video_link = models.URLField(
        null=True,
        blank=True,
        verbose_name='URL ссылка на видео'
    )

    class Meta:
        verbose_name = 'ссылка на видео'
        verbose_name_plural = 'ссылки на видео'

    def __str__(self):
        return '{} {}'.format(self.lesson , self.video_link)


class HomeTask(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    # number = models.IntegerField(verbose_name='Номер модуля')
    text = models.TextField(blank=True)
    Lesson = models.ForeignKey(
        Lesson,
        on_delete=models.CASCADE,
        verbose_name='Урок',
        related_name='home_task',
        null=True,
    )

    def lesson(self):
        return self.Lesson.title

    def module(self):
        # self.title + str(self.number)
        module = self.Lesson.module
        print('module.title  + str(module.number) ' , module.title  + str(module.number))
        return "{} {}".format(module.title  + str(module.number), self.Lesson.module.number)

    class Meta:
        verbose_name = 'домашние задание'
        verbose_name_plural = 'домашние задания'

class HomeAssignment(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    Lesson = models.ForeignKey(
        Lesson,
        on_delete=models.CASCADE,
        verbose_name='Урок',
        related_name='home_assignment',
        null=True,
    )

    text = models.TextField(blank=True ,   verbose_name='Текст',)
    verifyed = models.BooleanField(default=False ,   verbose_name='Верифицирован',)
    file = models.ForeignKey(Material, on_delete=models.CASCADE, verbose_name='Файл', null=True, blank=True)
    user = models.ForeignKey('users.CustomUser', on_delete=models.CASCADE, null=True, verbose_name='Пользователь')
    status = models.SmallIntegerField(choices=HomeAssignmentReviewTypes.REVIEW_TYPES, null=True , verbose_name='Статус')

    class Meta:
        verbose_name = 'Выполненное домашнее задание'
        verbose_name_plural = 'Выполненные домашние задания'


# class HomeAssignment2(models.Model):
#     id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
#     home_task = models.ForeignKey(
#         HomeTask,
#         on_delete=models.CASCADE,
#         verbose_name='Урок',
#         related_name='home_assignment2',
#         null=True,
#     )
#
#     text = models.TextField(blank=True)
#     verifyed = models.BooleanField(default=False)
#     file = models.ForeignKey(Material, on_delete=models.CASCADE, verbose_name='Файл', null=True, blank=True)
#     user = models.ForeignKey('users.CustomUser', on_delete=models.CASCADE, null=True)
#     status = models.SmallIntegerField(choices=HomeAssignmentReviewTypes.REVIEW_TYPES, null=True)


class UserModelFinish(models.Model):
    user = models.ForeignKey('users.CustomUser', on_delete=models.CASCADE, null=True)
    module = models.ForeignKey(
        'academy.Module',
        on_delete=models.CASCADE,
        related_name='user_model_finish',
        verbose_name='Модуль'
    )
    finished = models.BooleanField(default=False)


    def __str__(self):
        return '{} {}'.format(self.user , self.module)


class UserLessonFinish(models.Model):
    user = models.ForeignKey('users.CustomUser', on_delete=models.CASCADE, null=True)
    lesson = models.ForeignKey(
        Lesson,
        on_delete=models.CASCADE,
        verbose_name='Урок',
        related_name='user_lesson_finish',
        null=True,
    )
    finished = models.BooleanField(default=False)


class Topic(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    title = models.CharField(max_length=150, verbose_name='Название')
    module = models.ForeignKey(
        'academy.Module',
        on_delete=models.CASCADE,
        verbose_name='Модуль'
    )

    def __str__(self, ):
        return self.title


class TopicFile(models.Model):
    file = models.FileField(verbose_name='Файл для темы')
    topic = models.ForeignKey(
        'academy.Topic',
        on_delete=models.CASCADE,
        verbose_name='Тема'
    )

    @property
    def is_video(self, ):
        if self.file.name.split('.')[-1] in ['doc', 'docx', 'pdf']:
            return False
        else:
            return True

    def __str__(self, ):
        return self.file.name


# class HomeAssignment(models.Model):
#     id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
#     module = models.OneToOneField(
#         'academy.Module',
#         on_delete=models.CASCADE,
#         verbose_name='Модуль'
#     )
#     description = models.TextField(verbose_name='Описание')
#
#     def __str__(self, ):
#         return self.module.title


class HomeAssignmentResponse(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    home_assignment = models.ForeignKey(
        'academy.HomeAssignment',
        on_delete=models.CASCADE,
        verbose_name='Домашнее задание',
    )
    user = models.ForeignKey('users.CustomUser', on_delete=models.CASCADE)
    status = models.SmallIntegerField(choices=HomeAssignmentReviewTypes.REVIEW_TYPES)
    response = models.TextField(verbose_name='Ответ клиента')
    uploaded_file = models.FileField()
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self, ):
        return self.user.email + ' - ' + self.home_assignment.module.title


class PinnedMaterial(models.Model):
    file = models.FileField(
        validators=[FileExtensionValidator(allowed_extensions=PINNED_CONTENT_EXTENSION)],
        null=True,
        blank=True,
        verbose_name='Файл'
    )

    class Meta:
        verbose_name = 'Прикрепленный Материал'
        verbose_name_plural = 'Прикрепленные Материалы'

    def __str__(self):
        return 'Прикрепленный материал: - {}'.format(self.id)
