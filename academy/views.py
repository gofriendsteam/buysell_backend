import base64
import uuid
from django.conf import settings
from django.contrib.auth import get_user_model
from django.core.files.base import ContentFile
from django.http import HttpResponse
from django.shortcuts import get_object_or_404
from django.utils.decorators import method_decorator
from django.views import View
from django.views.decorators.csrf import csrf_exempt
from liqpay.liqpay3 import LiqPay
from rest_framework import generics, permissions, response, status, viewsets
from rest_framework.decorators import action
from rest_framework.mixins import (CreateModelMixin, DestroyModelMixin,
                                   ListModelMixin, RetrieveModelMixin,
                                   UpdateModelMixin)
from rest_framework.response import Response

from catalog.utils import get_host_name
from payments.constants import TransactionSources, TransactionTypes
from payments.models import PaymentTransaction
from payments.utils import get_liqpay_data_and_signature
from users.permissions import IsPartner

from .models import HomeAssignment, HomeAssignmentResponse, Module, Topic, Material
from .serializers import (HomeAssignmentResponseSerializer,
                          HomeAssignmentSerializer, ModuleIdSerializer,
                          ModuleSerializer, TopicSerializer)

User = get_user_model()


class ModuleViewSet(ListModelMixin,
                    CreateModelMixin,
                    RetrieveModelMixin,
                    DestroyModelMixin,
                    viewsets.GenericViewSet):
    queryset = Module.objects.all()
    serializer_class = ModuleSerializer

    def list(self, request, *args, **kwargs):

        queryset = list(self.filter_queryset(self.get_queryset()).filter(package__in=[ elem.id for elem in self.request.user.user_pocket.all()]))
        print(self.request.user)
        # print(queryset[0].number)
        # queryset[0], queryset[1]=queryset[1], queryset[0]
        ##################
        # queryset[0].lessons.all().order_by('id'))
        # for q in queryset:
        #     print(q.lessons.all().order_by('id'))
        #
        #     # print(q.lessons)
        # print(self.request.user.user_pocket)
        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)


class TopicViewSet(ListModelMixin,
                   CreateModelMixin,
                   RetrieveModelMixin,
                   DestroyModelMixin,
                   viewsets.GenericViewSet):
    queryset = Topic.objects.all()
    serializer_class = TopicSerializer


class HomeAssignmentViewSet(ListModelMixin,
                            CreateModelMixin,
                            RetrieveModelMixin,
                            DestroyModelMixin,
                            viewsets.GenericViewSet):
    permission_classes = (IsPartner,)
    queryset = HomeAssignment.objects.all()
    serializer_class = HomeAssignmentSerializer


    def create(self, request, *args, **kwargs):
        try:
            # print(request.data)
            request.data['status'] = 3
            request.data['user'] = self.request.user.id

            # print(request.data)
            serializer = self.get_serializer(data=request.data)
            serializer.is_valid(raise_exception=True)
            self.perform_create(serializer)
            headers = self.get_success_headers(serializer.data)
            # print(self.request.user)
            # serializer.data['user'] = self.request.user
            # print(serializer.data['user'])
            return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)
        except Exception as e:
            # print(str(e))
            return Response('{"Exept_in_academy_view_HomeAssignmentViewSet_create":' + (str(e))+'}')


    def perform_create(self, serializer):
        try:
            format, imgstr = self.request.data['file'].split(';base64,')
            ext = format.split('/')[-1]
            if ext == 'vnd.openxmlformats-officedocument.spreadsheetml.sheet':
                ext = 'xlsx'
            elif ext == 'vnd.ms-excel':
                ext = 'xls'
            try:
                if ext.rindex('document'):
                    ext = 'docx'
            except ValueError as e:
                print(e)
            if ext == 'msword':
                ext = 'doc'
            data = ContentFile(base64.b64decode(imgstr), name='temp.' + ext)
            # print(data)
            file = Material(file=data)
            file.save()
            # serializer.save(user=self.request.user, Lesson_id=self.request.data['_lesson_id'], file=file, status=3)
            HomeAssignment.objects.update_or_create(user=self.request.user, Lesson_id=self.request.data['_lesson_id'],
                                                    defaults={'text': self.request.data['text'], 'file': file, 'status': 3})
        except:
            # serializer.save(user=self.request.user, Lesson_id=self.request.data['_lesson_id'], status=3)
            HomeAssignment.objects.update_or_create(user=self.request.user, Lesson_id=self.request.data['_lesson_id'],
                                                    defaults={'text': self.request.data['text'], 'status': 3})

class HomeAssignmentResponseViewSet(ListModelMixin,
                                    CreateModelMixin,
                                    DestroyModelMixin,
                                    UpdateModelMixin,
                                    viewsets.GenericViewSet):
    queryset = HomeAssignmentResponse.objects.all()
    serializer_class = HomeAssignmentResponseSerializer


class GetHomeAssigmentViewSet(viewsets.ViewSet):
    @action(detail=False, method=['GET', ])
    def get_home_assigment(self, request, *args, **kwargs):
        user_id = kwargs.get('user_id')

        result = dict()
        for assigment_response in HomeAssignmentResponse.objects.filter(user=user_id):
            module_id = str(assigment_response.home_assignment.module.id)
            result[module_id] = HomeAssignmentResponseSerializer().to_representation(assigment_response)

        return response.Response(
            data=result,
            status=status.HTTP_200_OK
        )


class LiqPayModuleBuyView(generics.CreateAPIView):
    permission_classes = (permissions.AllowAny, )
    serializer_class = ModuleIdSerializer

    def post(self, request, *args, **kwargs):
        module = get_object_or_404(Module, id=request.data.get('module_id'))
        print(request.user)
        params = {
            'action': 'pay',
            'amount': str(module.price),
            'currency': module.currency,
            'description': 'Payment for buying pocket',
            'order_id': "{}_{}_{}".format(request.user.id, module.id, uuid.uuid4()),
            'version': '3',
            'sandbox': 1,
            'result_url': get_host_name(),
            'server_url': get_host_name() + '/api/v1/payments/pocket-pay-callback/', # url to callback view
            # 'server_url': 'https://a7f65ac9.ngrok.io' + '/api/v1/abc/abc-pay-callback/',    # url to callback view
        }
        response_data = get_liqpay_data_and_signature(**params)

        return Response(
            response_data,
            status=status.HTTP_200_OK,
        )


@method_decorator(csrf_exempt, name='dispatch')
class LiqPayModuleCallBackView(View):
    # permission_classes = (AllowAny, )

    def post(self, request, *args, **kwargs):
        liqpay = LiqPay(settings.LIQ_PAY_PUBLIC_KEY, settings.LIQ_PAY_PRIVATE_KEY)
        data = request.POST.get('data')
        signature = request.POST.get('signature')
        sign = liqpay.str_to_sign(settings.LIQ_PAY_PUBLIC_KEY + data + settings.LIQ_PAY_PRIVATE_KEY)

        response = liqpay.decode_data_from_str(data)
        if sign == signature:
            response['is_valid_signature'] = True
        user = User.objects.get(id=int(response['order_id'].split('_')[0]))
        module = Module.objects.get(id=int(response['order_id'].split('_')[1]))
        PaymentTransaction.objects.create(
            user=user,
            amount=response.get('amount_credit'),
            trans_type=TransactionTypes.LIQPAY,
            source=TransactionSources.POCKET,
            module_id=str(response['order_id'].split('_')[1]),
            is_valid_signature=response.get('is_valid_signature', False),
            status=response.get('status', '').upper(),
            agent_commission=response.get('agent_commission', None),
            card_token=response.get('card_token', None),
            commission_credit=response.get('commission_credit', None),
            commission_debit=response.get('commission_debit', None),
            currency=response.get('currency', None),
            description=response.get('description', None),
            err_description=response.get('err_description', None),
            info=response.get('info', None),
            ip=response.get('ip', None),
            way_for_pay_order_id=response.get('way_for_pay_order_id', None),
            order_pay_id=response.get('order_id', None),
            payment_id=response.get('payment_id', None),
            sender_card_bank=response.get('sender_card_bank', None),
            sender_card_country=response.get('sender_card_country', None),
            sender_card_mask2=response.get('sender_card_mask2', None),
            sender_card_type=response.get('sender_card_type', None),
            sender_first_name=response.get('sender_first_name', None),
            sender_last_name=response.get('sender_last_name', None),
            sender_phone=response.get('sender_phone', None),
        )
        if response.get('status') == 'success':
            user.buy_abc_modules.add(module)
            user.save()
        return HttpResponse()   # HttpResponseRedirect(redirect_to='https://topmarket.ua/')
