from django.apps import AppConfig


class AcademyConfig(AppConfig):
    name = 'academy'
    verbose_name = "Школа"
    def ready(self):
        import academy.signals