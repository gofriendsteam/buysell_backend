from django.urls import path
from rest_framework.routers import DefaultRouter

from .views import (GetHomeAssigmentViewSet, HomeAssignmentResponseViewSet,
                    HomeAssignmentViewSet, ModuleViewSet, TopicViewSet,
                    LiqPayModuleBuyView, LiqPayModuleCallBackView)

app_name = 'academy'

router = DefaultRouter()
router.register(r'module', ModuleViewSet, base_name='module')
router.register(r'topic', TopicViewSet, base_name='topic')
router.register(r'home-assignment', HomeAssignmentViewSet, base_name='home_assignment')
router.register(r'home-assignment-response', HomeAssignmentResponseViewSet, base_name='home_assignment_response')
# router.register(r'user-model-finish', HomeAssignmentResponseViewSet, base_name='home_assignment_response')

urlpatterns = router.urls

urlpatterns += [
    path('home-assignment-response/<slug:user_id>/get', GetHomeAssigmentViewSet.as_view({'get':'get_home_assigment'})),
    path('academy-pay/', LiqPayModuleBuyView.as_view(), name='academy_module_pay_view'),
    path('academy-pay-callback/', LiqPayModuleCallBackView.as_view(), name='academy_module_pay_callback'),
]
