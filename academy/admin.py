from django.contrib import admin

from .models import (HomeAssignment, HomeAssignmentResponse, Module, Topic,
                     TopicFile, Package, Lesson, Material, UserModelFinish, HomeTask, UserLessonFinish, VideoLink,
                     File_Lesson)

admin.site.register(Package)
admin.site.register(Material)
admin.site.register(Module)
admin.site.register(UserModelFinish)
admin.site.register(UserLessonFinish)
admin.site.register(File_Lesson)

@admin.register(VideoLink)
class VideoLinkAdmin(admin.ModelAdmin):
    list_display = ['id' , 'lesson' , 'video_link']
    list_filter = ['lesson']

@admin.register(Lesson)
class LessonAdmin(admin.ModelAdmin):
    list_filter = ['module']
    list_display = ['module', 'title']


@admin.register(HomeTask)
class HomeTaskAdmin(admin.ModelAdmin):
    list_display = ['lesson', 'module']


class TopicFilesInLine(admin.TabularInline):
    model = TopicFile


class TopicAdmin(admin.ModelAdmin):
    model = Topic
    inlines = [TopicFilesInLine, ]


@admin.register(HomeAssignment)
class UserAdmin(admin.ModelAdmin):

    def texthome(self, obj):
        return obj.text[:50] + '...' if len(obj.text) > 50 else obj.text

    def lesson(self, obj):
        return obj.Lesson.title

    def module(self, obj):
        return '{} {}'.format(obj.Lesson.module.title, obj.Lesson.module.number)

    texthome.short_description = 'Текст'
    lesson.short_description = 'Урок'
    module.short_description = 'Модуль'

    list_display = [
        # 'id',
        'user',
        'verifyed',
        'status',
        'texthome',
        'file',
        'lesson',
        'module',
    ]
    list_editable = [
        'verifyed',
        'status',
    ]

    list_filter = ['Lesson', 'user']
