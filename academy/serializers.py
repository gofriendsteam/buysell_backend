import traceback

from rest_framework import serializers
from rest_framework.serializers import raise_errors_on_nested_writes
from rest_framework.utils import model_meta
from rest_framework.validators import UniqueValidator

from .models import (HomeAssignment, HomeAssignmentResponse, Module, Topic,
                     TopicFile, Lesson, Material, Package, UserModelFinish, HomeTask, UserLessonFinish, VideoLink,
                     File_Lesson)


class TopicFileSerializer(serializers.ModelSerializer):
    class Meta:
        model = TopicFile
        fields = (
            'file',
            'is_video',
        )


class TopicSerializer(serializers.ModelSerializer):
    files = TopicFileSerializer(source='topicfile_set', many=True)

    class Meta:
        model = Topic
        fields = (
            'title',
            'module',
            'files',
        )

class MaterialSerializer(serializers.ModelSerializer):
    class Meta:
        model = Material
        fields = (
            'file',
        )


class HomeTaskSerializer(serializers.ModelSerializer):
    class Meta:
        model = HomeTask
        fields = (
            'id',
            'text',
            'Lesson_id',
        )


class VideoLinkSerializer(serializers.ModelSerializer):
    class Meta:
        model = VideoLink
        fields = (
            'id',
            'video_link',
            'lesson_id',
        )


class File_LessonSerialiser(serializers.ModelSerializer):
    lesson_file = MaterialSerializer()
    class Meta:
        model = File_Lesson
        fields = (
            'id',
            'lesson_id',
            'lesson_file',
        )


class HomeAssignmentSerializer(serializers.ModelSerializer):
    file = MaterialSerializer(read_only=True)

    class Meta:
        model = HomeAssignment
        fields = (
            'id',
            'text',
            'verifyed',
            'file',
            'Lesson_id',
            'user',
            'status',
        )


# class HomeAssignment2Serializer(serializers.ModelSerializer):
#     file = MaterialSerializer(read_only=True)
#     home_assignment2 = HomeAssignmentSerializer(read_only=True)
#     class Meta:
#         model = HomeAssignment
#         fields = (
#             'id',
#             'text',
#             'verifyed',
#             'file',
#             'home_assignment2',
#             'user',
#             'status',
#         )

class HomeAssignmentResponseSerializer(serializers.ModelSerializer):
    class Meta:
        model = HomeAssignmentResponse
        fields = (
            'user',
            'status',
            'response',
            'uploaded_file',
            'created',
            'updated',
        )

class UserLessonFinishSerializer(serializers.ModelSerializer):
    # module = ModuleSerializer(many=False)
    class Meta:
        model = UserLessonFinish
        fields = (
            'id',
            'user',
            'lesson_id',
            'finished'
        )


class LessonSerializer(serializers.ModelSerializer):

    home_assignment = serializers.SerializerMethodField()
    home_task = HomeTaskSerializer(many=True)
    user_lesson_finish = serializers.SerializerMethodField()
    videos = VideoLinkSerializer(many=True)
    files = File_LessonSerialiser(many=True)

    def get_user_lesson_finish(self, obj):
        # print(obj)
        # print(1)
        queryset = UserLessonFinish.objects.filter(user=self.context['request'].user).filter(lesson=obj)
        serializer = UserLessonFinishSerializer(queryset, many=True)

        return serializer.data


    def get_home_assignment(self, obj):
        # print(obj)
        # print(2)
        queryset = HomeAssignment.objects.filter(user=self.context['request'].user).filter(Lesson=obj)
        serializer = HomeAssignmentSerializer(queryset, many=True)

        return serializer.data

    class Meta:
        model = Lesson
        fields = (
            'id',
            'title',
            'text',
            'files',
            'videos',
            'home_assignment',
            'home_task',
            'user_lesson_finish'
        )


class PackageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Package
        fields = (
            'id',
            'name',
            'price',
            'currency',
            'description',
        )


class UserModelFinishSerializer(serializers.ModelSerializer):
    # module = ModuleSerializer(many=False)
    class Meta:
        model = UserModelFinish
        fields = (
            'id',
            'user',
            'module_id',
            'finished'
        )


class ModuleSerializer(serializers.ModelSerializer):
    # package = TopicSerializer(source='topic_set', many=True)
    lessons = LessonSerializer(many=True)
    # user_model_finish = UserModelFinishSerializer(many=True)
    user_model_finish = serializers.SerializerMethodField()

    def get_user_model_finish(self, obj):
        # print(obj)
        # print(1)
        queryset = UserModelFinish.objects.filter(user=self.context['request'].user).filter(module=obj)
        serializer = UserModelFinishSerializer(queryset, many=True)

        return serializer.data
    class Meta:
        model = Module
        fields = (
            'id',
            'title',
            'number',
            'lessons',
            'user_model_finish',
            # 'package',
        )



# class ModuleSerializer(serializers.ModelSerializer):
#     topics = TopicSerializer(source='topic_set', many=True)
#     home_assigment = HomeAssignmentSerializer(source='homeassignment')
#
#     class Meta:
#         model = Module
#         fields = (
#             'id',
#             'number',
#             'title',
#             'description',
#             'topics',
#             'home_assigment',
#         )


class ModuleIdSerializer(serializers.Serializer):
    module_id = serializers.UUIDField(
        validators=[UniqueValidator(queryset=Module.objects.all())]
    )

    class Meta:
        fields = (
            'module_id',
        )
