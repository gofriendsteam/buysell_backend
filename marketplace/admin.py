from django.contrib import admin
from .models import AdditionalService, TrainingModule, VideoTraining, ContactUs, PocketVideo, PocketPlan, GeneralData

# admin.site.register(AdditionalService)
# admin.site.register(TrainingModule)
# admin.site.register(VideoTraining)
# admin.site.register(ContactUs)
# admin.site.register(PocketVideo)
admin.site.register(GeneralData)


@admin.register(PocketPlan)
class PocketPlanAdmin(admin.ModelAdmin):
    fields = (
        'name',
        'price',
        'currency',
        'description',
    )

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False
