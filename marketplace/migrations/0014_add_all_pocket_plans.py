from __future__ import unicode_literals

from django.db import migrations


def forwards_func(apps, schema_editor):
    PocketPlan = apps.get_model("marketplace", "PocketPlan")
    db_alias = schema_editor.connection.alias
    PocketPlan.objects.using(db_alias).delete()
    PocketPlan.objects.using(db_alias).bulk_create([
        PocketPlan(
            id=1,
            name='NO',
            price=0,
            currency='UAH',
            description='This is description about base pocket plan',
        ),
        PocketPlan(
            id=2,
            name='SILVER',
            price=27000,
            currency='UAH',
            description='This is description about silver pocket plan',
        ),
        PocketPlan(
            id=3,
            name='GOLD',
            price=45000,
            currency='UAH',
            description='This is description about gold pocket plan',
        ),
        PocketPlan(
            id=4,
            name='PLATINUM',
            price=59000,
            currency='UAH',
            description='This is description about silver pocket plan',
        ),

    ])


def reverse_func(apps, schema_editor):
    pass


class Migration(migrations.Migration):
    dependencies = [
        ('marketplace', '0013_auto_20190711_1004'),
    ]

    atomic = True

    operations = [
        migrations.RunPython(forwards_func, reverse_func),
    ]
