import logging
import re
import sys
import time
from datetime import datetime

import requests
from django.contrib.auth import get_user_model

from catalog.models import Product
from catalog.utils import get_rozetka_auth_token
from users.models import MyStore
from .constants import OrderStatusGroups, OrderStatuses, OrderCreateTypes
from .models import Order, OrderUser, OrderDelivery, OrderItem, OrderSellerComment, OrderStatusHistoryItem, \
    ContractorOrder, NovaPoshtaDeliveryHistoryItem, OrderSelf, NovaPoshtaDeliveryHistoryItemOrderSelf
from buy_sell.celery import app

User = get_user_model()


def upload_orders(user, token_rozetka, order_type=''):
    logging.basicConfig(stream=sys.stdout, level=logging.INFO)
    next_page = 1
    processing = True
    while processing:
        url = "https://api.seller.rozetka.com.ua/orders/search?expand=user,delivery,order_status_history,purchases&page={}".format(
            next_page)
        if order_type:
            url += '&type={}'.format(order_type)
        if user.rozetka_orders_last_update:
            url += '&changed_from={}'.format(user.rozetka_orders_last_update.date())
        headers = {
            'Authorization': "Bearer {}".format(token_rozetka),
            'cache-control': "no-cache",
        }
        r = requests.Request("GET", url, headers=headers)
        prep = r.prepare()
        s = requests.Session()
        resp = s.send(prep)
        r.encoding = 'utf-8'
        data = resp.json()

        orders = data['content']['orders']
        if next_page == data['content']['_meta']['pageCount'] or data['content']['_meta']['pageCount'] == 0:
            processing = False
        else:
            next_page += 1
        for order in orders:
            seller_comment_created = order.pop('seller_comment_created')
            order_instance, created = Order.objects.update_or_create(
                rozetka_id=order['id'],
                user=user,
                defaults={
                    'market_id': order['market_id'],
                    'created': order['created'],
                    'amount': order['amount'],
                    'amount_with_discount': order['amount_with_discount'],
                    'cost': order['cost'],
                    'cost_with_discount': order['cost_with_discount'],
                    'status': order['status'],
                    'status_group': order['status_group'],
                    'current_seller_comment': order['current_seller_comment'],
                    'comment': order['comment'],
                    'user_phone': order['user_phone'],
                    'from_warehouse': order['from_warehouse'],
                    'ttn': order['ttn'],
                    'total_quantity': order['total_quantity'],
                    'can_copy': order['can_copy'],
                    'created_type': order['created_type']
                }
            )
            if seller_comment_created:
                order_instance.seller_comment_created = seller_comment_created
            order_instance.save()

            user_id = order['user'].pop('id')
            OrderUser.objects.update_or_create(
                order=order_instance,
                rozetka_id=user_id,
                defaults=order['user']
            )

            delivery_dict = order['delivery']
            city = delivery_dict['city']['name'] if 'city' in delivery_dict.keys() and delivery_dict['city'] else None
            delivery_dict.pop('city')
            logging.info('Delivery obj = {}'.format(order['delivery']))
            OrderDelivery.objects.update_or_create(
                order=order_instance,
                city=city,
                defaults=order['delivery']
            )

            for purchase in order['purchases']:
                OrderItem.objects.update_or_create(
                    order=order_instance,
                    product_id=purchase['id'],
                    defaults={
                        'image_url': purchase['item']['photo_preview'],
                        'quantity': purchase['quantity'],
                        'name': purchase['item_name'],
                        'system_product': Product.objects.filter(id=purchase['id']).first(),
                        'price': purchase['price']
                    }
                )

            for seller_comment_dict in order['seller_comment']:
                OrderSellerComment.objects.update_or_create(
                    order=order_instance,
                    comment=seller_comment_dict['comment'],
                    created=seller_comment_dict['created']
                )

            for order_status_history_dict in order['order_status_history']:
                OrderStatusHistoryItem.objects.update_or_create(
                    order=order_instance,
                    status_id=order_status_history_dict['status_id'],
                    created=order_status_history_dict['created']
                )


@app.task
def upload_orders_prom():
    for user in User.objects.filter(role='PARTNER').filter(token_prom__isnull=False):
        token_prom = user.token_prom
        # print(user.id)
        # print(token_prom)
        if token_prom:
            url = "https://my.prom.ua/api/v1/orders/list"
            headers = {
                # 'Authorization': "Bearer {}".format("595447eaaab555bcdb57a86f513be0209831a1f9"),
                'Authorization': "Bearer {}".format(token_prom),
                'Content-Type': "application/json",
            }
            # r = requests.Request("GET", url, headers=headers)
            r = requests.get(url, headers=headers)
            if r.status_code == 200:
                # print(200)
                data = r.json()
                status_group = None
                for order in data['orders']:
                    if order['status'] == 'canceled':
                        status_group = 3
                    elif order['status'] == 'received' or order['status'] == 'pending':
                        status_group = 1
                    elif order['status'] == 'delivered' or order['status'] == 'paid':
                        status_group = 2
                    # order_instance, created = Order.objects.create(
                    try:
                        order_instance, created = Order.objects.update_or_create(
                            # prom_id=order['id'],
                            user=user,
                            prom_id=order['id'],
                            defaults={
                                'created': order['date_created'],
                                'status_group': status_group,
                                'status': status_group,
                                # 'amount': int(order['price']),
                                # 'amount': order['price'][:-4],
                                'amount': int(order['price'][:-4].replace('\xa0', '')),
                                # 'amount_with_discount': int(order['price_with_special_offer'][:-4].replace('\xa0', '')),
                                'status_prom': order['status'],
                                'cost': int(order['price'][:-4].replace('\xa0', '')),
                                # 'cost': order['price'][:-4],
                                'cost_with_discount': order['special_offer_discount'],
                                'comment': order['client_notes'],
                                'user_phone': order['phone'],
                                'can_copy': True,
                                # 'created_type': 1
                            }
                        )
                        order_instance.save()
                        OrderUser.objects.update_or_create(
                            order=order_instance,
                            prom_id=order['id'],
                            email=order['email'],
                            contact_fio=order['client_first_name'] + order['client_second_name'] + order[
                                'client_last_name']
                        )
                        for purchase in order['products']:
                            OrderItem.objects.update_or_create(
                                order=order_instance,
                                product_id=purchase['id'],
                                defaults={
                                    'image_url': purchase['image'],
                                    'quantity': purchase['quantity'],
                                    'name': purchase['name'],
                                    'system_product': Product.objects.filter(yml_id=purchase['id']).first(),
                                    'price': int(purchase['price'][:-4].replace('\xa0', '')),
                                }
                            )
                        pattern = re.compile(r'\w+')

                        OrderDelivery.objects.update_or_create(
                            order=order_instance,
                            city=pattern.findall(order['delivery_address'])[0],
                            defaults={
                                'delivery_service_id': 5,
                                'delivery_service_name': order['delivery_option']['name'],
                                'recipient_title': order['client_first_name'] + ' ' + order['client_second_name'],
                                'delivery_method_id': 1,  # 1- выддылення || 2 - на адрес
                                'name_logo': "nova-pochta",
                                # 'place_street':  'ул. Почтовая',
                                'address': order['delivery_address'],
                                # 'status': status_group
                            }
                            # defaults=order['delivery_option']
                        )
                        # print(33)
                    except Exception as e:
                        print(e)


@app.task
def checkout_orders():
    for user in User.objects.filter(role='PARTNER'):
        # upload_orders_prom(user, 1, '1')
        token_rozetka = get_rozetka_auth_token(user)
        print('token_rozetka token_rozetka token_rozetka')

        if token_rozetka:
            print(token_rozetka)
            for order_type in dict(OrderStatusGroups.STATUS_GROUPS).keys():
                upload_orders(user, token_rozetka, order_type=order_type)
                time.sleep(0.3)

            user.rozetka_orders_last_update = datetime.now()
            user.save()


@app.task
def checkout_nova_poshta_delivery_status():
    for user in User.objects.filter(role='PARTNER'):
        if user.nova_poshta_api_key:
            user_orders = ContractorOrder.objects.filter(contractor=user)
            print('user info')
            print(user)
            print(user.nova_poshta_api_key)
            print('* '* 15)
            request_body = {
                "apiKey": user.nova_poshta_api_key,
                "modelName": "TrackingDocument",
                "calledMethod": "getStatusDocuments",
                "methodProperties": {
                    "Documents": []
                }
            }
            for user_order in user_orders:
                if user_order.order.ttn:
                    request_body['methodProperties']['Documents'].append({
                        "DocumentNumber": user_order.order.ttn,
                        "Phone": user_order.order.user_phone
                    })

            url = 'https://api.novaposhta.ua/v2.0/json/'
            headers = {
                'Content-Type': 'application/json'
            }

            r = requests.Request("POST", url, headers=headers, json=request_body)
            prep = r.prepare()
            s = requests.Session()
            resp = s.send(prep)
            r.encoding = 'utf-8'

            res = resp.json()
            print(res)
            if res['success']:
                for data in res['data']:
                    user_order = ContractorOrder.objects.filter(order__ttn=data['Number'], contractor=user).first()
                    if user_order:
                        NovaPoshtaDeliveryHistoryItem.objects.update_or_create(
                            order=user_order,
                            status=data['Status'],
                            status_code=data['StatusCode']
                        )


@app.task
def checkout_nova_poshta_delivery_status_order_self():
    for user in User.objects.filter(role='PARTNER'):
        print('user ', user)
        if user.nova_poshta_api_key:
            user_orders = OrderSelf.objects.filter(user=user)
            request_body = {
                "apiKey": user.nova_poshta_api_key,
                "modelName": "TrackingDocument",
                "calledMethod": "getStatusDocuments",
                "methodProperties": {
                    "Documents": []
                }
            }
            for user_order in user_orders:
                if user_order.ttn:
                    request_body['methodProperties']['Documents'].append({
                        "DocumentNumber": user_order.ttn,
                        "Phone": user_order.phone_number
                    })

            url = 'https://api.novaposhta.ua/v2.0/json/'
            headers = {
                'Content-Type': 'application/json'
            }
            r = requests.Request("POST", url, headers=headers, json=request_body)
            prep = r.prepare()
            s = requests.Session()
            resp = s.send(prep)
            r.encoding = 'utf-8'
            res = resp.json()
            print(res)
            if res['success']:
                print('data', res['data'])
                for data in res['data']:
                    try:
                        user_order = OrderSelf.objects\
                            .get(ttn=data['Number'], user=user)
                        if user_order:
                            NovaPoshtaDeliveryHistoryItemOrderSelf.objects.update_or_create(
                                contractor_order=user_order,
                                status=data['Status'],
                                status_code=data['StatusCode']
                            )
                    except Exception as e:
                        print(type(e) , e , 'error !!!!!')

# @app.task
# def new_order_from_shop(data, source):
#     store = MyStore.objects.get(domain_name=source)
#     order = Order.objects.create(
#         created=data['created'],
#         user=store.user,
#         status=OrderStatuses.NEW_ORDER,
#         status_group=OrderStatusGroups.IN_PROCESSING,
#         user_phone=data['phone'],
#         amount=data['amount'],
#         amount_with_discount=data['amount'],
#         cost=data['amount'],
#         cost_with_discount=data['amount'],
#         created_type=OrderCreateTypes.SHOP
#     )
#     quantity = 0
#     for item in data['items']:
#         try:
#             product = Product.objects.get(id=item['product_id'])
#             order.products.add(product)
#             quantity += item['quantity']
#         except:
#             pass
#     order.total_quantity = quantity
#     order.save()
