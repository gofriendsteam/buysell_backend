import requests
from django.db import models, transaction
from django.template.loader import render_to_string
from django.utils.translation import ugettext as _
from django.conf import settings

from news.models import TimeStampedModel
from orders.constants import OrderCreateTypes, OrderStatusGroups, OrderStatuses, CounterpartyProperties
from payments.constants import TransactionTypes, TransactionSources
from payments.models import PaymentTransaction
from users.tasks import send_email_task


class Order(models.Model):
    rozetka_id = models.PositiveIntegerField(null=True, blank=True)
    prom_id = models.PositiveIntegerField(null=True, blank=True)
    market_id = models.PositiveIntegerField(null=True, blank=True)
    created = models.DateTimeField()
    amount = models.DecimalField(max_digits=12, decimal_places=2, null=True)
    amount_with_discount = models.DecimalField(max_digits=12, decimal_places=2, null=True)
    cost = models.DecimalField(max_digits=12, decimal_places=2, null=True)
    cost_with_discount = models.DecimalField(max_digits=12, decimal_places=2, null=True)
    status = models.PositiveSmallIntegerField(choices=OrderStatuses.ORDER_STATUSES, null=True)
    status_group = models.PositiveSmallIntegerField(choices=OrderStatusGroups.STATUS_GROUPS, null=True)
    status_prom = models.CharField(max_length=32, null=True)
    seller_comment_created = models.DateTimeField(null=True, blank=True)
    current_seller_comment = models.TextField(null=True, blank=True)
    comment = models.TextField(null=True, blank=True)
    user_phone = models.CharField(max_length=32)
    from_warehouse = models.PositiveSmallIntegerField(null=True, blank=True)
    ttn = models.CharField(max_length=32, null=True, blank=True)
    total_quantity = models.PositiveSmallIntegerField(null=True, blank=True)
    can_copy = models.BooleanField(default=False)
    created_type = models.PositiveSmallIntegerField(choices=OrderCreateTypes.CREATE_TYPES, null=True)

    last_update = models.DateTimeField(auto_now=True)
    system_comment = models.TextField(blank=True)

    user = models.ForeignKey('users.CustomUser', on_delete=models.CASCADE, null=True)

    products = models.ManyToManyField('catalog.Product', blank=True)

    def __str__(self):
        return '{}, {}, {}'.format(
            self.created,
            dict(OrderCreateTypes.CREATE_TYPES)[self.created_type],
            dict(OrderStatuses.ORDER_STATUSES)[self.status]
        )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.__pk = self.pk

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)
        if not self.__pk and self.status_group in [OrderStatusGroups.IN_PROCESSING, OrderStatusGroups.UNSUCCESSFUL]:
            mail_subject = ''
            message = ''
            if self.status_group == OrderStatusGroups.IN_PROCESSING:
                mail_subject = 'Новый заказ на rozetka.ua'
                message = render_to_string('new_order_email.html', {
                    'domain': settings.HOST_NAME,
                    'order_id': self.id
                })
            elif self.status_group == OrderStatusGroups.UNSUCCESSFUL:
                mail_subject = 'Отмена заказа на rozetka.ua'
                message = render_to_string('canceled_order_email.html', {
                    'domain': settings.HOST_NAME,
                    'order_id': self.id
                })
            data = {
                'to_emails': [self.user.email, ],
                'subject': mail_subject,
                'html_content': message
            }
            send_email_task.delay(**data)

    class Meta:
        verbose_name = _('Заказ продавца')
        verbose_name_plural = _('Заказы продавцов')


class ContractorOrder(models.Model):
    order = models.ForeignKey(Order, on_delete=models.CASCADE)
    contractor = models.ForeignKey('users.CustomUser', on_delete=models.CASCADE)
    status = models.PositiveSmallIntegerField(choices=OrderStatuses.ORDER_STATUSES)
    products = models.ManyToManyField('catalog.Product', blank=True)
    payer_type = models.CharField(
        null=True,
        blank=True,
        max_length=64,
        default='Recipient'
    )  # Значение из справочника Тип плательщика
    payment_method = models.CharField(
        null=True,
        blank=True,
        max_length=64,
        default='Cash'
    )  # Значение из справочника Форма оплаты
    date = models.DateField(auto_now=True, null=True, blank=True)
    cargo_type = models.CharField(null=True, blank=True, max_length=64)  # Значение из справочника Тип груза
    volume_general = models.DecimalField(null=True, blank=True, max_digits=10, decimal_places=3)
    weight = models.DecimalField(null=True, blank=True, max_digits=10, decimal_places=3)
    service_type = models.CharField(null=True, blank=True, max_length=64)  # Значение из справочника Технология доставки
    seats_amount = models.PositiveSmallIntegerField(null=True, blank=True)
    description = models.TextField(null=True, blank=True)
    cost = models.PositiveSmallIntegerField(null=True, blank=True)

    sender = models.UUIDField(null=True, blank=True)
    city_sender = models.UUIDField(null=True, blank=True)
    sender_address = models.UUIDField(null=True, blank=True)
    contact_sender = models.UUIDField(null=True, blank=True)
    senders_phone = models.CharField(null=True, blank=True, max_length=64)

    recipient = models.UUIDField(null=True, blank=True)
    city_recipient = models.UUIDField(null=True, blank=True)
    recipient_address = models.UUIDField(null=True, blank=True)
    contact_recipient = models.UUIDField(null=True, blank=True)
    recipients_phone = models.CharField(null=True, blank=True, max_length=64)

    ttn = models.CharField(null=True, blank=True, max_length=64)

    class Meta:
        verbose_name = _('Заказ поставщика')
        verbose_name_plural = _('Заказы поставщиков')

    def __str__(self):
        return '{}'.format(self.order)

    def save(self, *args, **kwargs):
        return_data = super().save(*args, **kwargs)
        # self.create_transaction()
        return return_data

    def create_transaction(self):
        partner_products = self.products.all().values_list('id', flat=True)
        contractor_percent = self.contractor.percent_for_partners or 5
        total_order_sum = 0
        marketplace_percent = 0
        total_contractor_product_sum = 0
        for item in self.order.order_items.filter(system_product__in=partner_products):
            total_order_sum += item.price * item.quantity
            marketplace_percent += item.quantity * item.system_product.price * contractor_percent / 100
            total_contractor_product_sum += item.system_product.contractor_product.price * item.quantity
        if self.status == OrderStatuses.PACKAGE_RECEIVED:
            with transaction.atomic():
                # снятие денег с поставщика
                PaymentTransaction.objects.update_or_create(
                    user=self.contractor,
                    system_order_id=self.pk,
                    source=TransactionSources.SUCCESSFUL_ORDER,
                    defaults={
                        'amount': total_order_sum - total_contractor_product_sum,
                        'trans_type': TransactionTypes.INVOICE,
                        'total_sum': int(total_order_sum or 0),
                        'partner_and_marketplace_part': int(total_contractor_product_sum or 0)
                    }
                )

                # начисление денег продавцу
                PaymentTransaction.objects.update_or_create(
                    user=self.order.user,
                    system_order_id=self.pk,
                    source=TransactionSources.SUCCESSFUL_ORDER,
                    defaults={
                        'amount': total_order_sum - total_contractor_product_sum - marketplace_percent,
                        'trans_type': TransactionTypes.INVOICE,
                        'source': TransactionSources.SUCCESSFUL_ORDER,
                        'contractor_and_marketplace_part': int(total_contractor_product_sum or 0),
                        'total_sum': int(total_order_sum or 0),
                    },
                )
        elif self.status == OrderStatuses.RETURN:
            # возврат денег поставщику
            PaymentTransaction.objects.update_or_create(
                user=self.contractor,
                system_order_id=self.pk,
                source=TransactionSources.RETURN_PRODUCT,
                defaults={
                    'amount': total_order_sum - total_contractor_product_sum,
                    'trans_type': TransactionTypes.INVOICE,
                }
            )
            # снятие денег с продавца
            PaymentTransaction.objects.update_or_create(
                user=self.order.user,
                system_order_id=self.pk,
                source=TransactionSources.RETURN_PRODUCT,
                defaults={
                    'amount': total_order_sum - total_contractor_product_sum - marketplace_percent,
                    'trans_type': TransactionTypes.INVOICE,
                },
            )



class NovaPoshtaDeliveryHistoryItem(TimeStampedModel):
    contractor_order = models.ForeignKey(ContractorOrder, on_delete=models.CASCADE)
    status = models.CharField(max_length=512)
    status_code = models.PositiveSmallIntegerField()

    def __str__(self):
        return '{}'.format(self.contractor_order)


class OrderUser(models.Model):
    order = models.OneToOneField(Order, on_delete=models.CASCADE)
    email = models.EmailField(null=True)
    login = models.CharField(max_length=64, null=True)
    contact_fio = models.CharField(max_length=256)
    rozetka_id = models.PositiveIntegerField(null=True)
    prom_id = models.PositiveIntegerField(null=True)

    def __str__(self):
        return '{}, {}'.format(self.order.id, self.email)

    class Meta:
        verbose_name = _('Покупатель')
        verbose_name_plural = _('Покупатели')


class OrderDelivery(models.Model):
    order = models.OneToOneField(Order, on_delete=models.CASCADE)
    delivery_service_id = models.PositiveIntegerField()
    delivery_service_name = models.CharField(null=True, max_length=256)
    recipient_title = models.CharField(null=True, max_length=256)
    place_id = models.PositiveIntegerField(null=True, blank=True)
    place_street = models.CharField(max_length=1024, null=True)
    place_number = models.CharField(max_length=32, null=True, blank=True)
    place_house = models.CharField(max_length=32, null=True)
    place_flat = models.CharField(null=True, blank=True, max_length=64)
    cost = models.CharField(null=True, blank=True, max_length=64)
    city = models.CharField(max_length=256, null=True)
    delivery_method_id = models.PositiveIntegerField(null=True)
    ref_id = models.UUIDField(null=True, blank=True)
    name_logo = models.CharField(null=True, max_length=32)
    address = models.CharField(null=True, max_length=100)
    email = models.CharField(null=True, max_length=100)

    def __str__(self):
        return '{}, {}'.format(self.order.id, self.delivery_service_name)

    class Meta:
        verbose_name = _('Доставка')
        verbose_name_plural = _('Доставки')


class OrderItem(models.Model):
    order = models.ForeignKey(Order, on_delete=models.CASCADE, related_name='order_items')

    product_id = models.PositiveIntegerField()
    image_url = models.URLField(null=True, blank=True)
    quantity = models.PositiveSmallIntegerField()
    name = models.CharField(max_length=512)
    price = models.DecimalField(max_digits=12, decimal_places=2)

    system_product = models.ForeignKey('catalog.Product', on_delete=models.CASCADE, null=True, blank=True)

    def __str__(self):
        return 'ID заказа: {}, ID продукта: {}'.format(self.order.id, self.product_id)

    class Meta:
        verbose_name = _('Товар заказа')
        verbose_name_plural = _('Товары заказа')


class OrderSellerComment(models.Model):
    order = models.ForeignKey(Order, on_delete=models.CASCADE)
    comment = models.TextField()
    created = models.DateTimeField()

    def __str__(self):
        return '{}, {}'.format(self.order.id, self.created)

    class Meta:
        verbose_name = _('Комментарий продавца')
        verbose_name_plural = _('Комментарии продавца')


class OrderStatusHistoryItem(models.Model):
    order = models.ForeignKey(Order, on_delete=models.CASCADE)
    status_id = models.PositiveIntegerField(choices=OrderStatuses.ORDER_STATUSES)
    created = models.DateTimeField()

    def __str__(self):
        return '{}, {}'.format(self.order.id, dict(OrderStatuses.ORDER_STATUSES)[self.status_id])

    class Meta:
        verbose_name = _('Элемент истории заказа')
        verbose_name_plural = _('История заказа')


# САМОВІКУП
class OrderSelf(models.Model):
    product = models.ForeignKey('catalog.Product' , on_delete=models.CASCADE)
    user = models.ForeignKey('users.CustomUser', on_delete=models.CASCADE, null=True , verbose_name='Продавец')
    contractor_user_id  = models.PositiveIntegerField(verbose_name='Поставщик продукта' , default=0 , blank=True , null=True)
    full_name =  models.CharField(max_length=300 , verbose_name='FIO')
    count  = models.PositiveIntegerField()
    phone_number  = models.CharField(max_length=15 , verbose_name='Phone number')
    total_price = models.DecimalField(verbose_name='Total price' , max_digits=12, decimal_places=5)
    ref_id_poshta = models.CharField(null=True, max_length=500)
    city_name  = models.CharField(max_length=255 , verbose_name='Город доставки')
    post_office_name = models.CharField(max_length=255 , verbose_name='Адрес отделения доставки')
    ttn = models.CharField(max_length=64, null=True, blank=True)
    date_create  = models.DateTimeField(auto_now=True)
    cargo_type = models.CharField(null=True, blank=True, max_length=64)  # Значение из справочника Тип груза
    weight = models.DecimalField(null=True, blank=True, max_digits=10, decimal_places=3)
    service_type = models.CharField(null=True, blank=True, max_length=64)  # Значение из справочника Технология доставки
    seats_amount = models.PositiveSmallIntegerField(null=True, blank=True)
    description = models.TextField(null=True, blank=True)

    def __str__(self):
        return 'Заказ для самовыкупа {} {}'.format(self.id , self.user)



class OrderSelfStatusHistoryItem(models.Model):
    order = models.ForeignKey(OrderSelf, on_delete=models.CASCADE, related_name="order_self_status_history_item")
    status_id = models.PositiveIntegerField(choices=OrderStatuses.ORDER_STATUSES , default=1, blank=True)
    created = models.DateTimeField(auto_now=True)

    def __str__(self):
        return '{}, {}'.format(self.order.id, dict(OrderStatuses.ORDER_STATUSES)[self.status_id])

    class Meta:
        verbose_name = _('Элемент истории заказа')
        verbose_name_plural = _('История заказа')


class NovaPoshtaDeliveryHistoryItemOrderSelf(models.Model):
    contractor_order = models.ForeignKey(OrderSelf, on_delete=models.CASCADE, related_name="order_nova_posta")
    status = models.CharField(max_length=512)
    status_code = models.PositiveSmallIntegerField()
    create_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return '{}'.format(self.contractor_order)
