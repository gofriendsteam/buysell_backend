from django.urls import path
from rest_framework.routers import DefaultRouter

from orders.views import DestroyAllRozetkaOrders, SendToContractor, SetStatusRozetka, \
    GetStatusDictRozetka, GetStatusDictProm, SetStatusProm
from . import views

app_name = 'orders'

router = DefaultRouter()
router.register(r'orders', views.OrderViewSet, base_name='orders')
router.register(r'orders_prom', views.OrderPromViewSet, base_name='orders_prom')
router.register(r'orders_contractor', views.ContractorOrderViewSet, base_name='orders_contractor')
router.register(r'order_self', views.OrderSelfiewSet, base_name='order_self')

urlpatterns = router.urls

urlpatterns += [
    path('destroy_orders/', DestroyAllRozetkaOrders.as_view(), name='destroy_orders'),
    path('order/send_to_contractor/', SendToContractor.as_view(), name='send_to_contractor'),
    path('order/set_status_rozetka/', SetStatusRozetka.as_view(), name='set_status_rozetka'),
    path('order/set_status_prom/', SetStatusProm.as_view(), name='set_status_prom'),
    path('order/get_statuses_rozetka/', GetStatusDictRozetka.as_view(), name='get_statuses_rozetka'),
    path('order/get_statuses_prom/', GetStatusDictProm.as_view(), name='get_statuses_prom'),
]
