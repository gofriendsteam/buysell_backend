import requests
from rest_framework import serializers

from catalog.models import Product
from catalog.serializers import ProductImageSerializer, ProductImageURLSerializer, ProductSerializer
from catalog.utils import get_rozetka_auth_token
from orders.constants import NovaPoshtaCargoTypes, ServiceTypes, OrderStatusGroups, ORDER_STATUS_HIERARCHY
from users.serializers import UserSerializer
from .models import Order, OrderUser, OrderDelivery, OrderItem, OrderSellerComment, OrderStatusHistoryItem, \
    ContractorOrder, NovaPoshtaDeliveryHistoryItem, OrderSelf, OrderSelfStatusHistoryItem, \
    NovaPoshtaDeliveryHistoryItemOrderSelf


class OrderUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = OrderUser
        fields = (
            'email',
            'login',
            'contact_fio',
        )


class OrderDeliverySerializer(serializers.ModelSerializer):
    class Meta:
        model = OrderDelivery
        fields = (
            'delivery_service_id',
            'delivery_service_name',
            'recipient_title',
            'place_id',
            'place_street',
            'place_number',
            'place_house',
            'place_flat',
            'cost',
            'city',
            'delivery_method_id',
            'ref_id',
            'name_logo',
        )


class OrderItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = OrderItem
        fields = (
            'product_id',
            'image_url',
            'quantity',
            'name',
            'price',
            'system_product',
        )


class OrderSellerCommentSerializer(serializers.ModelSerializer):
    class Meta:
        model = OrderSellerComment
        fields = (
            'comment',
            'created',
        )


class OrderStatusHistoryItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = OrderStatusHistoryItem
        fields = (
            'status_id',
            'created',
        )


class OrderSelfStatusHistoryItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = OrderSelfStatusHistoryItem
        fields = (
            'status_id',
            'created',
        )


class OrderProductSerializer(serializers.ModelSerializer):
    cover_images = ProductImageSerializer(many=True, source='product_images', required=False)
    image_urls = ProductImageURLSerializer(many=True, source='product_image_urls', required=False)

    class Meta:
        model = Product
        fields = (
            'id',
            'cover_images',
            'image_urls',
            'name'
        )



class ContractorNovaPoshtaDeliveryHistoryItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = NovaPoshtaDeliveryHistoryItem
        fields = (
            'status',
            'status_code',
            'created',
            'updated',
        )


class NovaPoshtaDeliveryHistoryItemOrderSelfSerializer(serializers.ModelSerializer):
    class Meta:
        model = NovaPoshtaDeliveryHistoryItemOrderSelf
        fields = [
            'status',
            'status_code',
            'create_at'
        ]


class NovaPoshtaDeliveryHistoryItemSerializer(serializers.ModelSerializer):
    nova_poshta_delivery_history = ContractorNovaPoshtaDeliveryHistoryItemSerializer(
        source='novaposhtadeliveryhistoryitem_set',
        many=True
    )

    class Meta:
        model = ContractorOrder
        fields = (
            'id',
            'nova_poshta_delivery_history',
        )


class OrderSerializer(serializers.ModelSerializer):
    user = OrderUserSerializer(source='orderuser')
    delivery = OrderDeliverySerializer(source='orderdelivery')
    items = OrderItemSerializer(source='order_items', many=True)
    seller_comments = OrderSellerCommentSerializer(source='ordersellercomment_set', many=True)
    status_history = OrderStatusHistoryItemSerializer(source='orderstatushistoryitem_set', many=True)
    passed_to_contractor = serializers.SerializerMethodField(read_only=True)
    contractor_nova_poshta_delivery_history = NovaPoshtaDeliveryHistoryItemSerializer(
        source='contractororder_set',
        many=True
    )

    def get_passed_to_contractor(self, obj):
        return ContractorOrder.objects.filter(order=obj).exists()

    def get_status_children(self, obj):
        print(66666)
        if obj.contractororder_set.exists():
            return []
        if obj.status_group == OrderStatusGroups.IN_PROCESSING:
            return ORDER_STATUS_HIERARCHY.get(obj.get_status_display())
        return []

    class Meta:
        model = Order
        fields = (
            'id',
            'rozetka_id',
            'market_id',
            'created',
            'amount',
            'amount_with_discount',
            'cost',
            'cost_with_discount',
            'status',
            'status_prom',
            'status_group',
            'seller_comment_created',
            'current_seller_comment',
            'comment',
            'user_phone',
            'from_warehouse',
            'ttn',
            'total_quantity',
            'can_copy',
            'created_type',
            'products',
            'last_update',
            'passed_to_contractor',
            'items',
            'contractor_nova_poshta_delivery_history',
            'user',
            'delivery',
            'seller_comments',
            'status_history',
        )


class OrderUpdateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Order
        fields = (
            'rozetka_id',
            'system_comment',
            'status',
            'ttn',
        )

        def update(self, instance, validated_data):
            instance = super().update(instance, validated_data)
            url = 'https://api.seller.rozetka.com.ua/orders/{}'.format(instance.order.rozetka_id)
            token_rozetka = get_rozetka_auth_token(instance.contractor)
            data = {'ttn': instance.ttn}
            headers = {
                'Authorization': "Bearer {}".format(token_rozetka),
                'cache-control': "no-cache",
            }
            r = requests.Request("PUT", url, json=data, headers=headers)
            prep = r.prepare()
            s = requests.Session()
            resp = s.send(prep)
            r.encoding = 'utf-8'
            data = resp.json()
            if data['success']:
                instance.order.ttn = instance.ttn
            else:
                instance.order.ttn = '{} {}'.format(data['errors']['code'], data['errors']['message'])
            instance.order.save()
            print("added")
            return instance


class ContractorOrderSerializer(serializers.ModelSerializer):
    base_order = OrderSerializer(source='order')
    item_products = OrderProductSerializer(source='products', many=True)
    nova_poshta_delivery_history = ContractorNovaPoshtaDeliveryHistoryItemSerializer(
        source='novaposhtadeliveryhistoryitem_set',
        many=True
    )

    class Meta:
        model = ContractorOrder
        fields = (
            'contractor',
            'status',
            'city_sender',
            'sender_address',
            'sender_address',
            'contact_sender',
            'city_recipient',
            'base_order',
            'nova_poshta_delivery_history',
            'item_products',
        )


class ContractorOrderUpdateSerializer(serializers.ModelSerializer):
    class Meta:
        model = ContractorOrder
        fields = (
            'status',
            'ttn',
        )

    def update(self, instance, validated_data):
        instance = super().update(instance, validated_data)
        url = 'https://api.seller.rozetka.com.ua/orders/{}'.format(instance.order.rozetka_id)
        token_rozetka = get_rozetka_auth_token(instance.contractor)
        data = {'ttn': instance.ttn}
        headers = {
            'Authorization': "Bearer {}".format(token_rozetka),
            'cache-control': "no-cache",
        }
        r = requests.Request("PUT", url, json=data, headers=headers)
        prep = r.prepare()
        s = requests.Session()
        resp = s.send(prep)
        r.encoding = 'utf-8'
        data = resp.json()
        if data['success']:
            instance.order.ttn = instance.ttn
        else:
            instance.order.ttn = '{} {}'.format(data['errors']['code'], data['errors']['message'])
        instance.order.save()
        print("added")
        return instance


class GenerateTTNSerializer(serializers.Serializer):
    weight = serializers.DecimalField(max_digits=12, decimal_places=3)
    seats_amount = serializers.IntegerField(default=1)
    description = serializers.CharField(max_length=2048)
    cargo_type = serializers.ChoiceField(
        choices=NovaPoshtaCargoTypes.NOVA_POSHTA_CARGO_TYPES,
        default=NovaPoshtaCargoTypes.CARGO
    )
    service_type = serializers.ChoiceField(
        choices=ServiceTypes.SERVICE_TYPES,
        default=ServiceTypes.WAREHOUSE_WAREHOUSE
    )


class OrderSelfContactor(serializers.ModelSerializer):
    user = UserSerializer(read_only=True)
    status_history = OrderSelfStatusHistoryItemSerializer(source='order_self_status_history_item', many=True,
                                                          required=False)

    product = ProductSerializer()
    nova_poshta_delivery_history_order_self = NovaPoshtaDeliveryHistoryItemOrderSelfSerializer(
        source='order_nova_posta',
        many=True
    )

    # x = serializers.FloatField()

    class Meta:
        model = OrderSelf
        fields = [
            # 'x',
            'id',
            'user',
            'full_name',
            'count',
            'product',
            'phone_number',
            'total_price',
            'ref_id_poshta',
            'city_name',
            'post_office_name',
            'status_history',
            'date_create',
            'nova_poshta_delivery_history_order_self',
            'ttn',
            'service_type'
        ]

    def create(self, validated_data):
        validated_data['user'] = self.context['request'].user
        print('asdasdsad')
        try:
            product_id = validated_data['product'].id
            target_product = Product.objects.get(id=product_id)
            if not target_product.contractor_product:
                validated_data['contractor_user_id'] = target_product.user.id
            else:
                validated_data['contractor_user_id'] = target_product.contractor_product.user.id
        except Exception as e:
            print(type(e), e)
        instance = super().create(validated_data)
        import datetime
        OrderSelfStatusHistoryItem.objects.create(order=instance, status_id=1, created=datetime.datetime.now())
        return instance


class OrderSelfCreate(serializers.ModelSerializer):
    class Meta:
        model = OrderSelf
        fields = [
            'full_name',
            'count',
            'product',
            'phone_number',
            'total_price',
            'ref_id_poshta',
            'city_name',
            'post_office_name',
            'service_type'
        ]

    def create(self, validated_data):
        validated_data['user'] = self.context['request'].user
        # print('asdasdsad')
        try:
            product_id = validated_data['product'].id
            target_product = Product.objects.get(id=product_id)
            if not target_product.contractor_product:
                validated_data['contractor_user_id'] = target_product.user.id
            else:
                validated_data['contractor_user_id'] = target_product.contractor_product.user.id
        except Exception as e:
            print(type(e), e)
        instance = super().create(validated_data)
        import datetime
        OrderSelfStatusHistoryItem.objects.create(order=instance, status_id=1, created=datetime.datetime.now())
        return instance


class OrderSelfUpdateSerializer(serializers.ModelSerializer):
    class Meta:
        model = OrderSelf
        fields = (
            'ttn',
        )

        def update(self, instance, validated_data):
            print('update !!!!!!!!!')
            super().update(instance, validated_data)
            return instance



class SimpleContractorOrderSerializer(serializers.Serializer):
    order_id = serializers.IntegerField()

    class Meta:
        fields = (
            'order_id'
        )


class SimpleStatusSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    status = serializers.IntegerField()

    class Meta:
        fields = (
            'id',
            'status'
        )

class SimpleStatusPromSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    status = serializers.CharField(max_length=50)

    class Meta:
        fields = (
            'id',
            'status'
        )