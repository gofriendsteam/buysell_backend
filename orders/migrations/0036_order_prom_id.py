# Generated by Django 2.1.7 on 2019-11-29 11:07

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('orders', '0035_auto_20191122_1829'),
    ]

    operations = [
        migrations.AddField(
            model_name='order',
            name='prom_id',
            field=models.PositiveIntegerField(blank=True, null=True),
        ),
    ]
