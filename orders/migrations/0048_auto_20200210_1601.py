# Generated by Django 2.1.7 on 2020-02-10 16:01

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('orders', '0047_auto_20200206_1509'),
    ]

    operations = [
        migrations.AlterField(
            model_name='orderself',
            name='total_price',
            field=models.DecimalField(decimal_places=5, max_digits=12, verbose_name='Total price'),
        ),
    ]
