# Generated by Django 2.1.7 on 2020-01-31 15:56

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0088_auto_20200129_1237'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('orders', '0043_auto_20191221_1314'),
    ]

    operations = [
        migrations.CreateModel(
            name='NovaPoshtaDeliveryHistoryItemOrderSelf',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('status', models.CharField(max_length=512)),
                ('status_code', models.PositiveSmallIntegerField()),
                ('create_at', models.DateTimeField(auto_now=True)),
            ],
        ),
        migrations.CreateModel(
            name='OrderSelf',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('contractor_user_id', models.PositiveIntegerField(blank=True, default=0, null=True, verbose_name='Поставщик продукта')),
                ('full_name', models.CharField(max_length=300, verbose_name='FIO')),
                ('count', models.PositiveIntegerField()),
                ('phone_number', models.CharField(max_length=15, verbose_name='Phone number')),
                ('total_price', models.FloatField(verbose_name='Total price')),
                ('ref_id_poshta', models.CharField(max_length=500, null=True)),
                ('city_name', models.CharField(max_length=255, verbose_name='Город доставки')),
                ('post_office_name', models.CharField(max_length=255, verbose_name='Адрес отделения доставки')),
                ('ttn', models.CharField(blank=True, max_length=64, null=True)),
                ('date_create', models.DateTimeField(auto_now=True)),
                ('cargo_type', models.CharField(blank=True, max_length=64, null=True)),
                ('weight', models.DecimalField(blank=True, decimal_places=3, max_digits=10, null=True)),
                ('service_type', models.CharField(blank=True, max_length=64, null=True)),
                ('seats_amount', models.PositiveSmallIntegerField(blank=True, null=True)),
                ('description', models.TextField(blank=True, null=True)),
                ('product', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='catalog.Product')),
                ('user', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL, verbose_name='Продавец')),
            ],
        ),
        migrations.CreateModel(
            name='OrderSelfStatusHistoryItem',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('status_id', models.PositiveIntegerField(blank=True, choices=[(1, 'Новый заказ'), (2, 'Данные подтверждены. Ожидает отправки'), (3, 'Передан в службу доставки'), (4, 'Доставляется'), (5, 'Ожидает в пункте самовывоза'), (6, 'Посылка получена'), (7, 'Не обработан продавцом'), (10, 'Отправка просрочена'), (11, 'Не забрал посылку'), (12, 'Отказался от товара'), (13, 'Отменен Администратором'), (15, 'Некорректный ТТН'), (16, 'Нет в наличии/брак'), (17, 'Отмена. Не устраивает оплата'), (18, 'Не удалось связаться с покупателем'), (19, 'Возврат'), (20, 'Отмена. Не устраивает товар'), (24, 'Отмена. Не устраивает доставка'), (25, 'Тестовый заказ'), (26, 'Обрабатывается менеджером'), (27, 'Требует доукомплектации'), (28, 'Некорректные контактные данные'), (29, 'Отмена. Некорректная цена на сайте'), (30, 'Истек срок резерва'), (31, 'Отмена. Заказ восстановлен'), (32, 'Отмена. Не устраивает разгруппировка заказа'), (33, 'Отмена. Не устраивает стоимость доставки'), (34, 'Отмена. Не устраивает перевозчик, способ доставки'), (35, 'Отмена. Не устраивают сроки доставки'), (36, 'Отмена. Клиент хочет оплату по безналу. У продавца нет такой возможности'), (37, 'Отмена. Не устраивает предоплата'), (38, 'Отмена. Не устраивает качество товара'), (39, 'Отмена. Не подошли характеристики товара (цвет,размер)'), (40, 'Отмена. Клиент передумал'), (41, 'Отмена. Купил на другом сайте'), (42, 'Нет в наличии'), (43, 'Брак'), (44, 'Отмена. Фейковый заказ'), (45, 'Отменен покупателем'), (46, 'Восстановлен при прозвоне'), (47, 'Обрабатывается менеджером (не удалось связаться 1-ый раз)'), (48, 'Обрабатывается менеджером (не удалось связаться 2-ой раз)')], default=1)),
                ('created', models.DateTimeField()),
                ('order', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='orders.OrderSelf')),
            ],
            options={
                'verbose_name': 'Элемент истории заказа',
                'verbose_name_plural': 'История заказа',
            },
        ),
        migrations.AddField(
            model_name='novaposhtadeliveryhistoryitemorderself',
            name='contractor_order',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='orders.OrderSelf'),
        ),
    ]
