from django.contrib import admin

from .models import Order, OrderUser, OrderDelivery, OrderItem, OrderSellerComment, OrderStatusHistoryItem, \
    ContractorOrder, NovaPoshtaDeliveryHistoryItem, OrderSelf, OrderSelfStatusHistoryItem


class NovaPoshtaDeliveryHistoryItemTabular(admin.TabularInline):
    model = NovaPoshtaDeliveryHistoryItem
    fields = (
        'status',
        'status_code',
    )
    readonly_fields = (
        'created',
        'updated',
    )
    extra = 0


@admin.register(ContractorOrder)
class ContractorOrderAdmin(admin.ModelAdmin):
    inlines = (
        NovaPoshtaDeliveryHistoryItemTabular,
    )
    ordering = ('-order__created',)

    list_filter = ('order__status_group',)


class OrderUserTabular(admin.TabularInline):
    model = OrderUser
    fields = (
        'email',
        'login',
        'contact_fio',
        'rozetka_id',
    )
    extra = 0


class OrderDeliveryTabular(admin.TabularInline):
    model = OrderDelivery
    fields = (
        'delivery_service_id',
        'delivery_service_name',
        'recipient_title',
        'place_id',
        'place_street',
        'place_number',
        'place_house',
        'place_flat',
        'cost',
        'city',
        'delivery_method_id',
        'ref_id',
        'name_logo',
    )
    extra = 0


class OrderItemTabular(admin.TabularInline):
    model = OrderItem
    fields = (
        'product_id',
        'image_url',
        'quantity',
        'name',
        'price',
        'system_product',
    )
    extra = 0


class OrderSellerCommentTabular(admin.TabularInline):
    model = OrderSellerComment
    fields = (
        'comment',
        'created',
    )
    extra = 0


class OrderStatusHistoryItemTabular(admin.TabularInline):
    model = OrderStatusHistoryItem
    fields = (
        'status_id',
        'created',
    )
    extra = 0


@admin.register(Order)
class OrderAdmin(admin.ModelAdmin):
    inlines = (
        OrderUserTabular,
        OrderDeliveryTabular,
        OrderItemTabular,
        OrderSellerCommentTabular,
        OrderStatusHistoryItemTabular,
    )
    ordering = ('-created',)

    fields = (
        'rozetka_id',
        'market_id',
        'user',
        'created',
        'amount',
        'amount_with_discount',
        'cost',
        'cost_with_discount',
        'status',
        'status_group',
        'seller_comment_created',
        'current_seller_comment',
        'comment',
        'user_phone',
        'from_warehouse',
        'ttn',
        'total_quantity',
        'can_copy',
        'created_type',
        'products',
        'last_update',
    )

    list_display = (
        'rozetka_id',
        'market_id',
        'user',
        'created',
        'amount',
        'amount_with_discount',
        'cost',
        'cost_with_discount',
        'status',
        'ttn',
        'total_quantity',
    )

    readonly_fields = (
        'last_update',
    )

    list_filter = ('status_group',)


class OrderSelfStatusHistoryItemInline(admin.StackedInline):
    model = OrderSelfStatusHistoryItem

@admin.register(OrderSelfStatusHistoryItem)
class OrderSelfStatusHistoryItemAdmin(admin.ModelAdmin):
    pass

@admin.register(OrderSelf)
class OrderSelfAdmin(admin.ModelAdmin):
    inlines = [OrderSelfStatusHistoryItemInline]
