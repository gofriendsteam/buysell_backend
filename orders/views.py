import datetime
from collections import defaultdict

import requests
from django.contrib.auth.models import User
from django.db.models import Sum
from rest_framework import viewsets, status
from rest_framework.decorators import action
from rest_framework.generics import CreateAPIView
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from django_filters import rest_framework as filters
from rest_framework.views import APIView

from catalog.models import Product
from catalog.utils import get_rozetka_auth_token
from orders.constants import OrderStatuses, ORDER_STATUS_HIERARCHY, ORDER_STATUS_HIERARCHY_PROM
from users.models import CustomUser
from users.permissions import IsPartner, IsContractor
from .models import Order, ContractorOrder, OrderSelf, OrderItem
from .serializers import OrderSerializer, OrderUpdateSerializer, ContractorOrderSerializer, \
    ContractorOrderUpdateSerializer, GenerateTTNSerializer, OrderSelfContactor, OrderSelfUpdateSerializer, \
    OrderSelfCreate, SimpleContractorOrderSerializer, SimpleStatusSerializer, SimpleStatusPromSerializer


class OrderFilter(filters.FilterSet):
    id = filters.NumberFilter(field_name="id", lookup_expr='contains')
    min_date = filters.DateTimeFilter(field_name="created", lookup_expr='gte')
    max_date = filters.DateTimeFilter(field_name="created", lookup_expr='lte')
    status = filters.ChoiceFilter(field_name="status", choices=OrderStatuses.ORDER_STATUSES)
    user_fio = filters.CharFilter(field_name="orderuser__contact_fio", lookup_expr='icontains')
    user_phone = filters.CharFilter(field_name="user_phone", lookup_expr='icontains')
    status_group = filters.NumberFilter(field_name='status_group', lookup_expr='exact')

    class Meta:
        model = Order
        fields = (
            'id',
            "rozetka_id",
            'min_date',
            'max_date',
            'status',
            'user_fio',
            'user_phone',
            'status_group',
        )


class ContractorOrderFilter(OrderFilter):
    id = filters.NumberFilter(field_name="id", lookup_expr='contains')
    min_date = filters.DateTimeFilter(field_name="order__created", lookup_expr='gte')
    max_date = filters.DateTimeFilter(field_name="order__created", lookup_expr='lte')
    status = filters.ChoiceFilter(field_name="order__status", choices=OrderStatuses.ORDER_STATUSES)
    user_fio = filters.CharFilter(field_name="order__orderuser__contact_fio", lookup_expr='icontains')
    user_phone = filters.CharFilter(field_name="order__user_phone", lookup_expr='icontains')
    status_group = filters.NumberFilter(field_name='order__status_group', lookup_expr='exact')

    class Meta:
        model = ContractorOrder
        fields = (
            'id',
            'min_date',
            'max_date',
            'status',
            'user_fio',
            'user_phone',
            'status_group',
        )


class OrderViewSet(viewsets.ModelViewSet):
    permission_classes = (IsPartner,)
    http_method_names = ('get', 'patch', 'post')
    filter_backends = (filters.DjangoFilterBackend,)
    filterset_class = OrderFilter

    def get_serializer_class(self):
        if self.action == 'partial_update':
            return OrderUpdateSerializer
        return OrderSerializer

    def get_queryset(self):
        # print(self.request.user)
        # print(get_rozetka_auth_token(self.request.user))
        # return Order.objects.filter(status_prom__isnull=True)  # изменит на низ
        return Order.objects.filter(user=self.request.user).filter(status_prom__isnull=True)  # може не виводити закази
        # return Order.objects.all()

    def update(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)
        serializer.save()
        ################
        url = 'https://api.seller.rozetka.com.ua/orders/{}'.format(
            serializer.data.get('rozetka_id'))  # redect rozetka_id
        token_rozetka = get_rozetka_auth_token(self.request.user)
        data = request.data
        headers = {
            'Authorization': "Bearer {}".format(token_rozetka),
            'cache-control': "no-cache",
            'Content-type': "application/json",
        }
        r = requests.Request("PUT", url, json=data, headers=headers)
        prep = r.prepare()
        s = requests.Session()
        resp = s.send(prep)
        r.encoding = 'utf-8'
        data = resp.json()
        if data['success']:
            pass
            # print(data)      #print
        ############
        # print(k.data.order.rozetka_id)

        return Response(serializer.data)

    @action(detail=True, methods=['GET'])
    def pass_to_contractor(self, request, *args, **kwargs):
        order = self.get_object()
        contractor_to_products_dict = defaultdict(list)
        for product in order.products.all():
            if product.contractor_product:
                contractor_to_products_dict[product.contractor_product.user.pk].append(product.id)

        statuses = []
        for key, value in contractor_to_products_dict.items():
            try:
                contractor_order = ContractorOrder.objects.get(
                    order=order
                )
                contractor_order.status = order.status
                contractor_order.contractor_id = key
                contractor_order.senders_phone = User.objects.get(id=key).phone
                contractor_order.recipients_phone = order.user_phone
                contractor_order.save()
            except ContractorOrder.DoesNotExist:
                contractor_order = ContractorOrder.objects.create(
                    order=order,
                    status=order.status,
                    contractor_id=key,
                    senders_phone=User.objects.get(id=key).phone,
                    recipients_phone=order.user_phone
                )
            contractor_order.products.add(*value)
            statuses.append(contractor_order.status)

        return Response({'statuses': statuses})


class OrderSelfiewSet(viewsets.ModelViewSet):
    permission_classes = (AllowAny , )
    http_method_names = ('get', 'patch', 'post')
    filter_backends = (filters.DjangoFilterBackend,)

    def get_serializer_class(self):
        if self.action == 'list':
            return OrderSelfContactor
        elif self.action == 'generate_ttn':
            return GenerateTTNSerializer
        elif self.action == 'partial_update':
            return OrderSelfUpdateSerializer
        return OrderSelfCreate

    def get_queryset(self):
        user = self.request.user
        if user.role == 'PARTNER':
            qs = OrderSelf.objects.filter(user=user).order_by('-id')
            return qs
        elif user.role == 'CONTRACTOR':
            qs = OrderSelf.objects.filter(contractor_user_id=user.id).order_by('-id')
            return qs

    @action(detail=True, methods=['POST', 'PATCH'])
    def generate_ttn(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid()
        order = self.get_object()
        order.cargo_type = serializer.data['cargo_type']
        order.weight = serializer.data['weight']
        order.service_type = serializer.data['service_type']
        order.seats_amount = serializer.data['seats_amount']
        order.description = serializer.data['description']
        order.save()

        return Response({'ttn': ''}, status=status.HTTP_200_OK)


class OrderPromViewSet(viewsets.ModelViewSet):
    permission_classes = (IsPartner,)
    http_method_names = ('get', 'patch',)
    filter_backends = (filters.DjangoFilterBackend,)
    filterset_class = OrderFilter

    def get_serializer_class(self):
        if self.action == 'partial_update':
            return OrderUpdateSerializer
        return OrderSerializer

    def get_queryset(self):
        print(33)
        return Order.objects.filter(user=self.request.user).filter(status_prom__isnull=False)

    def update(self, request, *args, **kwargs):
        print('go')
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data)

        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)
        serializer.save()
        ############
        token_prom = self.request.user.token_prom
        if token_prom:
            url = "https://my.prom.ua/api/v1/orders/set_status"
            headers = {
                # 'Authorization': "Bearer {}".format("595447eaaab555bcdb57a86f513be0209831a1f9"),
                'Authorization': "Bearer {}".format(token_prom),
                'Content-Type': "application/json",
            }
            body = {
                "status": "delivered",
                "ids": [
                    serializer.data.get('prom_id')  # redact id_prom
                ]
            }
            r = requests.Request("POST", url, json=body, headers=headers)
            prep = r.prepare()
            s = requests.Session()
            resp = s.send(prep)
            r.encoding = 'utf-8'

            data = resp.json()
            if resp.status_code == 200:
                print(data)  # sended to prom
                # print(resp.status_code)

            # # r = requests.Request("GET", url, headers=headers)
            # r = requests.post(url, data=body, headers=headers)
            # print(r.status_code)
            # if r.status_code == 200:
            #     print("ok")
        ###########
        print('updated')
        return Response(serializer.data)

    @action(detail=True, methods=['GET'])
    def pass_to_contractor(self, request, *args, **kwargs):
        order = self.get_object()
        contractor_to_products_dict = defaultdict(list)
        for product in order.products.all():
            if product.contractor_product:
                contractor_to_products_dict[product.contractor_product.user.pk].append(product.id)

        statuses = []
        for key, value in contractor_to_products_dict.items():
            try:
                contractor_order = ContractorOrder.objects.get(
                    order=order
                )
                contractor_order.status = order.status
                contractor_order.contractor_id = key
                contractor_order.senders_phone = User.objects.get(id=key).phone
                contractor_order.recipients_phone = order.user_phone
                contractor_order.save()
            except ContractorOrder.DoesNotExist:
                contractor_order = ContractorOrder.objects.create(
                    order=order,
                    status=order.status,
                    contractor_id=key,
                    senders_phone=User.objects.get(id=key).phone,
                    recipients_phone=order.user_phone
                )
            contractor_order.products.add(*value)
            statuses.append(contractor_order.status)

        return Response({'statuses': statuses})


class ContractorOrderViewSet(viewsets.ModelViewSet):
    permission_classes = (IsContractor,)
    http_method_names = ('get', 'patch', 'post',)
    filter_backends = (filters.DjangoFilterBackend,)
    filterset_class = ContractorOrderFilter

    def get_serializer_class(self):
        if self.action == 'partial_update':
            return ContractorOrderUpdateSerializer
        elif self.action == 'generate_ttn':
            return GenerateTTNSerializer
        # elif self.action == 'partial_uppdate'
        return ContractorOrderSerializer

    def get_queryset(self):
        return ContractorOrder.objects.filter(contractor=self.request.user)

    @action(detail=True, methods=['POST', 'PATCH'])
    def generate_ttn(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid()

        order = self.get_object()
        order.date = datetime.datetime.now()
        order.cargo_type = serializer.data['cargo_type']
        order.weight = serializer.data['weight']
        order.service_type = serializer.data['service_type']
        order.seats_amount = serializer.data['seats_amount']
        order.description = serializer.data['description']
        order.cost = order.products.all().aggregate(order_price=Sum('price'))['order_price']

        order.save()

        return Response({'ttn': ''}, status=status.HTTP_200_OK)


class DestroyAllRozetkaOrders(APIView):
    permission_classes = (AllowAny,)

    def get(self, *args, **kwargs):
        Order.objects.all().delete()
        ContractorOrder.objects.all().delete()
        return Response(status=200)


class SendToContractor(CreateAPIView):
    permission_classes = (AllowAny,)
    serializer_class = SimpleContractorOrderSerializer


    def post(self, request, *args, **kwargs):
        print(request.data)
        try:
            order = Order.objects.get(id=request.data['order_id'])

            # print(order.products)
            # product = Product.objects.get(id=request.data['product_id'])
            # contractor = product.contractor_product.user

            # city_sender = order.orderdelivery.city
            # sender_address = order.orderdelivery.place_street + order.orderdelivery.place_number
            # senders_phone = order.user_phone

            order_item = OrderItem.objects.filter(order=order)
            for item in order_item:
                # print(item.product_id)
                # print(item.price)
                # print(item.quantity)
                cost = item.price
                if item.system_product_id is not None:
                    product = Product.objects.get(id=item.system_product_id)
                    print(product)
                    contractor = product.contractor_product.user


                    contractor_order = ContractorOrder(order=order,
                                                       contractor=contractor,
                                                       status=order.status,
                                                       cost=cost
                                                       )
                    contractor_order.save()

                    contractor_order.products.add(product.id)
                    contractor_order.save()
                else:
                    return Response({'error': str("Product does not exist")}, status=200)

        except Exception as e:
            print(e)
            return Response({'error': str(e)}, status=200)
            # quantity = order_item.quantity

            # product = Product.objects.get(id=request.data['product_id'])
        return Response({'status': 'ok'}, status=200)


class SetStatusRozetka(CreateAPIView):
    permission_classes = (AllowAny,)
    serializer_class = SimpleStatusSerializer


    def post(self, request, *args, **kwargs):

        # print(request.data['id'])
        # print(request.data['status'])

        # print(ORDER_STATUS_HIERARCHY["Данные подтверждены. Ожидает отправки"][1])
        url = 'https://api.seller.rozetka.com.ua/orders/{}'.format(request.data['id'])
        # user = CustomUser.objects.filter(id=0)
        # print(self.request.user)
        # print(self.request.user)
        token_rozetka = get_rozetka_auth_token(self.request.user)
        # token_rozetka = get_rozetka_auth_token(None)
        data = {'status': request.data['status']}
        headers = {
            'Authorization': "Bearer {}".format(token_rozetka),
            'cache-control': "no-cache",
        }
        r = requests.Request("PUT", url, json=data, headers=headers)
        prep = r.prepare()
        s = requests.Session()
        resp = s.send(prep)
        r.encoding = 'utf-8'
        data = resp.json()
        print(data)
        return Response(data, status=200)


class SetStatusProm(CreateAPIView): # error need testing
    permission_classes = (AllowAny,)
    serializer_class = SimpleStatusPromSerializer


    def post(self, request, *args, **kwargs):

        token_prom = self.request.user.token_prom
        try:
            if token_prom:
                url = "https://my.prom.ua/api/v1/orders/set_status"
                headers = {
                    # 'Authorization': "Bearer {}".format("595447eaaab555bcdb57a86f513be0209831a1f9"),
                    'Authorization': "Bearer {}".format(token_prom),
                    'Content-Type': "application/json",
                }
                body = {
                    "status": request.data['status'],
                    "ids": [
                        request.data['id']  # redact id_prom
                    ]
                }
                r = requests.Request("POST", url, json=body, headers=headers)
                prep = r.prepare()
                s = requests.Session()
                resp = s.send(prep)
                r.encoding = 'utf-8'

                data = resp.json()
                if resp.status_code == 200:
                    print(data)  # sended to prom

                return Response(data, status=200)
        except Exception as e:
            print(e)
        return Response(status=200)


class GetStatusDictRozetka(APIView):

    def get(self, request, *args, **kwargs):
        # print(ORDER_STATUS_HIERARCHY)
        return Response(ORDER_STATUS_HIERARCHY, status=200)


class GetStatusDictProm(APIView):

    def get(self, request, *args, **kwargs):
        # print(ORDER_STATUS_HIERARCHY_PROM)
        return Response(ORDER_STATUS_HIERARCHY_PROM, status=200)