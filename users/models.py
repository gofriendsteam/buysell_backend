from datetime import datetime, timedelta

from django.conf import settings
from django.contrib.postgres.fields import JSONField
from django.core.validators import RegexValidator
from django.db.models import Sum
from django.template.loader import render_to_string
from django.utils.translation import ugettext as _
from django.db import models
from django.contrib.auth.validators import UnicodeUsernameValidator
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin
from sendgrid import SendGridAPIClient
from sendgrid.helpers.mail import Mail

from abc_profiles.models import MainContent
from payments.constants import TransactionSources, TransactionTypes, LiqPayConstant
from users.constants import DOMEN, USER_ROLE, DeliveryChoices
from .managers import CustomUserManager


class CustomUser(AbstractBaseUser, PermissionsMixin):

    manager = models.ForeignKey(
        'Company',
        on_delete=models.SET_NULL,
        verbose_name=_('Менеджер'),
        null=True, blank=True,
    )

    role = models.CharField(
        max_length=10,
        choices=USER_ROLE,
    )

    user_pocket = models.ManyToManyField(
        'academy.Package',
        verbose_name=_('Пакет услуг')
    )

    first_name = models.CharField(
        max_length=256,
        null=True,
        blank=True,
        verbose_name=_('Имя')
    )

    last_name = models.CharField(
        max_length=256,
        null=True,
        blank=True,
        verbose_name=_('Фамилия')
    )

    markup = models.DecimalField(
        max_digits=10,
        decimal_places=2,
        verbose_name=_('Наценка'),
        default=5,
        null=True,
        blank=True,
    )

    fixed_recommended_price = models.BooleanField(
        default=False,
        verbose_name='Fix РРЦ',)

    patronymic = models.CharField(
        max_length=256,
        null=True,
        blank=True,
        verbose_name=_('Отчество')
    )

    email = models.EmailField(unique=True, null=True, verbose_name=_('Емейл'))
    phone = models.CharField(
        max_length=50,
        null=True, blank=True,
        verbose_name=_('Телефон')
    )
    web_site = models.URLField(
        null=True, blank=True,
        verbose_name=_('Веб сайт (url)'),
    )
    date_joined = models.DateTimeField(auto_now_add=True, verbose_name=_('Дата регистрации'))
    is_staff = models.BooleanField(
        'staff status',
        default=False,
        help_text='Is the user allowed to have access to the admin',
    )
    is_active = models.BooleanField(
        'active',
        default=False,
        help_text='Is the user account currently active',
    )
    username_validator = UnicodeUsernameValidator()
    username = models.CharField(
        'username',
        max_length=150,
        unique=True,
        help_text='150 characters or fewer. Letters, digits and @/./+/-/_ only.',
        validators=[username_validator],
        error_messages={
            'unique': "A user with that username already exists.",
        },
        null=True,
        blank=True
    )
    avatar = models.ImageField(blank=True, null=True, upload_to='user_profiles/avatars')

    verified = models.BooleanField(default=False)

    percent_for_partners = models.PositiveSmallIntegerField(
        null=True,
        blank=True,
        verbose_name=_("Процент для продавцов")
    )

    rozetka_username = models.CharField(max_length=128, null=True, blank=True)
    rozetka_password = models.CharField(max_length=512, null=True, blank=True)
    token_prom = models.CharField(max_length=512, null=True, blank=True)

    rozetka_old_orders_imported = models.BooleanField(default=False)
    rozetka_orders_last_update = models.DateTimeField(
        null=True, blank=True,
        verbose_name=_('Последнее обновление заказов')
    )

    # Для доставки Новою Поштою (створення ттн)
    nova_poshta_api_key = models.CharField(max_length=64, null=True, blank=True)
    sender_ref = models.CharField(max_length=64, null=True, blank=True)

    # Поля для ФОП
    organizational_legal_form_of_the_company = models.CharField(
        max_length=512,
        null=True,
        blank=True,
        verbose_name=_('Организационно-правовая форма предприятия')
    )
    organization = models.CharField(
        max_length=512,
        null=True,
        blank=True,
        verbose_name=_('Организация')
    )
    edpnou = models.CharField(
        max_length=512,
        null=True,
        blank=True,
        verbose_name=_('ЕДРПОУ')
    )
    vat_payer_certificate = models.CharField(
        max_length=512,
        null=True,
        blank=True,
        verbose_name=_('Свидетельства плательщика НДС')
    )
    bank_name = models.CharField(
        max_length=512,
        null=True,
        blank=True,
        verbose_name=_('Название банка')
    )
    mfi = models.CharField(
        max_length=512,
        null=True,
        blank=True,
        verbose_name=_('МФО')
    )
    checking_account = models.CharField(
        max_length=512,
        null=True,
        blank=True,
        verbose_name=_('Рассчетный счет')
    )
    available_products_count = models.PositiveIntegerField(
        default=200
    )

    # ABC profile
    buy_abc_modules = models.ManyToManyField(
        MainContent,
        verbose_name=_('Модули ABC')
    )

    USERNAME_FIELD = 'email'
    objects = CustomUserManager()

    def __str__(self):
        return '{} {} {}'.format(self.id,
                                 self.first_name if self.first_name else '',  self.email)

    def get_full_name(self):
        return '{} {} {}'.format(self.last_name, self.first_name, self.patronymic)

    def get_short_name(self):
        return '{}'.format(self.first_name)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.__pk = self.pk
        self.__verified = self.verified

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)
        if not self.__pk and self.role == 'PARTNER':
            Company.objects.get_or_create(user_id=self.pk)
            MyStore.objects.get_or_create(user_id=self.pk)

        if not self.__verified and self.verified:
            from_email = settings.DEFAULT_FROM_EMAIL
            message = render_to_string('account_verification_email.html', {
                'domain': settings.HOST_NAME,
            })
            data = {
                'to_emails': [self.email, ],
                'subject': "Ваш аккаунт верифицирован",
                'html_content': message,
            }

            message = Mail(
                from_email=from_email,
                **data
            )

            try:
                sg = SendGridAPIClient(settings.SENDGRID_API_KEY)
                sg.send(message)
            except Exception as e:
                print(e.args)

    @property
    def products_count(self):
        return self.products.count()

    @property
    def user_balance(self):
        balance = 0
        if self.role == 'CONTRACTOR':
            positive_liqpay_payments = self.payment_transactions.filter(
                status=LiqPayConstant.SUCCESS,
                trans_type=TransactionTypes.LIQPAY,
                source__in=[
                    TransactionSources.RECHARGE_BALANCE,
                    TransactionSources.RETURN_PRODUCT
                ]
            ).aggregate(Sum('amount'))
            positive_invoice_payments_return_order= self.payment_transactions.filter(
                trans_type=TransactionTypes.INVOICE,
                source__in=[
                    TransactionSources.RETURN_PRODUCT,
                ]
            ).aggregate(Sum('amount'))
            positive_invoice_payments_approved_recharge = self.payment_transactions.filter(
                trans_type=TransactionTypes.INVOICE,
                source__in=[
                    TransactionSources.RECHARGE_BALANCE,
                ],
                is_approved=True,
            ).aggregate(Sum('amount'))

            negative_payments = self.payment_transactions.filter(
                source__in=[
                    TransactionSources.SUCCESSFUL_ORDER,
                ]
            ).aggregate(Sum('amount'))

            balance = (positive_invoice_payments_return_order['amount__sum'] or 0) \
                      + (positive_invoice_payments_approved_recharge['amount__sum'] or 0) \
                      + (positive_liqpay_payments['amount__sum'] or 0) \
                      - (negative_payments['amount__sum'] or 0) \
                      - self.frozen_balance
        elif self.role == 'PARTNER':
            positive_payments = self.payment_transactions.filter(
                source__in=[
                    TransactionSources.RECHARGE_BALANCE,
                    TransactionSources.SUCCESSFUL_ORDER,
                ]
            ).aggregate(Sum('amount'))
            negative_payments = self.payment_transactions.filter(
                source__in=[
                    TransactionSources.RETURN_PRODUCT,
                ]
            ).aggregate(Sum('amount'))
            balance = (positive_payments['amount__sum'] or 0)\
                      - (negative_payments['amount__sum'] or 0)\
                      - self.frozen_balance
        return balance

    @property
    def frozen_balance(self):
        amount = 0
        frozen_time = datetime.now() - timedelta(days=14)
        if self.role == 'PARTNER':
            amount = self.payment_transactions.filter(
                source__in=[
                    TransactionSources.SUCCESSFUL_ORDER,
                ],
                created__gte=frozen_time
            ).aggregate(Sum('amount'))
            amount = amount['amount__sum'] or 0
        return amount

    class Meta:
        verbose_name = _('Пользователь')
        verbose_name_plural = _('Пользователи')


class UserNotificationEmail(models.Model):
    user = models.OneToOneField(CustomUser, on_delete=models.CASCADE, related_name='email_notifications')
    new_order = models.BooleanField(default=False, verbose_name=_('Новый заказ (email)'))
    ttn_change = models.BooleanField(default=False, verbose_name=_('Смена ТТН заказа'))
    order_paid = models.BooleanField(default=False, verbose_name=_('Получение счета на оплату'))
    sales_report = models.BooleanField(default=False, verbose_name=_('Уведомление о продажах'))
    new_message = models.BooleanField(default=False, verbose_name=_('Новое сообщение во внутреннем почтовом уведомлении'))
    cancel_order = models.BooleanField(default=False, verbose_name=_('Уведомление об отмене заказа'))

    def __str__(self):
        return '{}'.format(self.user.get_full_name())

    class Meta:
        verbose_name = _('Уведомление пользователя(email)')
        verbose_name_plural = _('Увидемления пользователя(email)')


class UserNotificationPhone(models.Model):
    user = models.OneToOneField(CustomUser, on_delete=models.CASCADE, related_name='phone_notifications')
    new_order = models.BooleanField(default=False, verbose_name=_('Новый заказ (смс)'))

    def __str__(self):
        return '{}'.format(self.user.get_full_name())

    class Meta:
        verbose_name = _('Уведомление пользователя(тел)')
        verbose_name_plural = _('Увидемления пользователя(тел)')


class Company(models.Model):
    user = models.OneToOneField(
        CustomUser, on_delete=models.CASCADE,
        verbose_name=_('Владецел компании'),
        related_name='user_company'
    )

    name = models.CharField(
        max_length=255,
        null=True, blank=True,
        verbose_name=_('Название компании')
    )

    town = models.TextField(
        max_length=30,
        null=True, blank=True,
        verbose_name=_('Город')
    )

    address = models.TextField(
        max_length=40,
        null=True, blank=True,
        verbose_name=_('Адресс')
    )

    url = models.URLField(
        max_length=200,
        null=True, blank=True,
        verbose_name=_('URL-путь')
    )

    working_conditions = models.TextField(
        max_length=100,
        null=True, blank=True,
        verbose_name=_('Условия работы')
    )

    logo = models.ImageField(
        upload_to='users/company/logos',
        null=True, blank=True,
        verbose_name=_('Лого компании'),
    )

    web_site = models.URLField(
        max_length=200,
        null=True, blank=True,
        verbose_name=_('Веб-сайт')
    )

    phone = models.CharField(
        max_length=50,
        null=True, blank=True,
        verbose_name=_('Телефон')
    )

    email = models.EmailField(
        max_length=200,
        null=True, blank=True,
        verbose_name=_('Емейл')
    )

    who_see_contact = models.CharField(
        max_length=200,
        null=True, blank=True,
        verbose_name=_('Кому видны контактные данные?')
    )

    # Тип деятельности для розничной торговли

    is_internet_shop = models.BooleanField(
        verbose_name=_('Интернет магазин'),
        default=False
    )

    is_offline_shop = models.BooleanField(
        verbose_name=_('Оффлайн-магазин'),
        default=False
    )

    retail_network = models.BooleanField(
        verbose_name=_('Розничная сеть'),
        default=False
    )

    # Тип деятельности для оптовой торговли

    distributor = models.BooleanField(
        verbose_name=_('Дистрибьютор'),
        default=False
    )

    manufacturer = models.BooleanField(
        verbose_name=_('Производитель'),
        default=False
    )

    importer = models.BooleanField(
        verbose_name=_('Импортер'),
        default=False
    )

    dealer = models.BooleanField(
        verbose_name=_('Дилер'),
        default=False
    )

    sub_dealer = models.BooleanField(
        verbose_name=_('Субдилер'),
        default=False
    )

    exporter = models.BooleanField(
        verbose_name=_('Експортер'),
        default=False
    )

    official_representative = models.BooleanField(
        verbose_name=_('Официальный представитель'),
        default=False
    )

    # Страница компании

    about_company = models.TextField(
        max_length=500,
        null=True, blank=True,
        verbose_name=_('Информация')
    )

    activity_area = models.ForeignKey(
        'ActivityAreas',
        null=True, blank=True,
        related_name='activity_areas',
        on_delete=models.SET_NULL

        )

    service_industry = models.ForeignKey(
        'ServiceIndustry',
        null=True, blank=True,
        related_name='service_industries',
        on_delete=models.SET_NULL
    )

    company_type = models.ForeignKey(
        'CompanyType',
        null=True, blank=True,
        related_name='company_types',
        on_delete=models.SET_NULL
    )

    class Meta:
        verbose_name = _('Компания продавца')
        verbose_name_plural = _('Компании продавцов')

    def __str__(self):
        return '{} {}'.format(self.id, self.name)


class NovaPoshtaDelivery(models.Model):
    user = models.OneToOneField(CustomUser, on_delete=models.CASCADE)
    addresses = JSONField()
    senders = JSONField()
    sender_ref = models.CharField

    class Meta:
        verbose_name = _('Доставка Новой Почтой')
        verbose_name_plural = _('Доставки Новой Почтой')

    def __str__(self):
        return '{} - доставка Новой Почтой'.format(self.user)


class ActivityAreas(models.Model):

    name = models.TextField(
        max_length=1095,
        verbose_name=_('Имя сферы деятельности')
    )

    def __str__(self):
        return '{}'.format(self.name)

    class Meta:
        verbose_name = _('Сфера деятельности')
        verbose_name_plural = _('Сферы деятельности')


class ServiceIndustry(models.Model):

    name = models.TextField(
        max_length=1095,
        verbose_name=_('Имя сферы услуг')
    )

    def __str__(self):
        return '{}'.format(self.name)

    class Meta:
        verbose_name = _('Сфера услуг')
        verbose_name_plural = _('Сферы услуг')


class CompanyType(models.Model):

    name = models.TextField(
        max_length=1095,
        verbose_name=_('Тип компании')
    )

    class Meta:
        verbose_name = _('Тип компании')
        verbose_name_plural = _('Типы компаний')


# Документы
class Passport(models.Model):
    company = models.ForeignKey(
        'Company',
        related_name='passports',
        on_delete=models.CASCADE,
        null=True, blank=True
    )
    pass_doc = models.ImageField(
        upload_to='companies/documents/passports',
        null=True, blank=True,
        verbose_name=_('Паспорт')
    )

    class Meta:
        verbose_name = _('Паспорт')
        verbose_name_plural = _('Паспорта')


class UkraineStatistic(models.Model):
    company = models.ForeignKey(
        'Company',
        related_name='ukraine_statistics',
        on_delete=models.CASCADE,
        null=True, blank=True,
        verbose_name=_('Компания')
        )
    uk_doc = models.FileField(
        upload_to='companies/documents/uk_statistics',
        null=True, blank=True,
        verbose_name=_('Справка Государственного комитета статистики Украины')
    )

    class Meta:
        verbose_name = _('Статистика (комитет статистики Украины)')
        verbose_name_plural = _('Статистика (комитет статистики Украины)')


class Certificate(models.Model):
    company = models.ForeignKey(
        'Company',
        related_name='certificates',
        on_delete=models.CASCADE,
        null=True, blank=True,
        verbose_name=_('Компания')
        )
    cert_doc = models.FileField(
        upload_to='companies/documents/certificates',
        null=True, blank=True,
        verbose_name=_('Свидетельство о регистрации или выписка с ЕГРПОУ')
    )

    class Meta:
        verbose_name = _('Свидетельсво')
        verbose_name_plural = _('Свидетельсва')


class TaxPayer(models.Model):
    company = models.ForeignKey(
        'Company',
        related_name='tax_payers',
        on_delete=models.CASCADE,
        null=True, blank=True,
        verbose_name=_('Компания')
        )
    tax_doc = models.FileField(
        upload_to='companies/documents/tax_payers',
        null=True, blank=True,
        verbose_name=_('Справка 4 Учета плательщика налогов')
    )

    class Meta:
        verbose_name = _('Плательщик налогов')
        verbose_name_plural = _('Плательщики налогов')


class PayerRegister(models.Model):
    company = models.ForeignKey(
        'Company',
        related_name='payer_registers',
        on_delete=models.CASCADE,
        null=True, blank=True,
        verbose_name=_('Компания')
        )
    payer_reg_doc = models.FileField(
        upload_to='companies/documents/payer_registers',
        null=True, blank=True,
        verbose_name=_('Выписка из реестра плательщиков НДС')
    )

    class Meta:
        verbose_name = _('Регистрация плательщика')
        verbose_name_plural = _('Регистрации плательщиков')


class PayerCertificate(models.Model):
    company = models.ForeignKey(
        'Company',
        related_name='payer_certificates',
        on_delete=models.CASCADE,
        null=True, blank=True,
        verbose_name=_('Компания')

    )
    payer_cert_doc = models.FileField(
        upload_to='companies/documents/payer_certificates',
        null=True, blank=True,
        verbose_name=_('Cвидетельство плательщика единого налога')
    )

    class Meta:
        verbose_name = _('Свидетельство плательщика ЕН')
        verbose_name_plural = _('Свидетельства плательщиков ЕН')

# Питч


class CompanyPitch(models.Model):
    company = models.OneToOneField(
        'Company',
        on_delete=models.CASCADE,
        null=True, blank=True,
        verbose_name=_('Компания')

    )
    who_are_you = models.TextField(
        max_length=30,
        null=True, blank=True,
        verbose_name=_('Кто вы?')
    )

    guru = models.TextField(
        max_length=100,
        null=True, blank=True,
        verbose_name=_('В чем вы Гуру?')
    )

    for_whom = models.TextField(
        max_length=50,
        null=True, blank=True,
        verbose_name=_('Для кого работает ваша компания?')
    )

    difference = models.TextField(
        max_length=100,
        null=True, blank=True,
        verbose_name=_('Чем отличаетесь от конкурентов?')
    )

    good_partner = models.TextField(
        max_length=100,
        null=True, blank=True,
        verbose_name=_('Мы классные продавцы, потому что:')
    )

    future = models.TextField(
        max_length=100,
        null=True, blank=True,
        verbose_name=_('Какой будет Ваша компания через 5 лет?')
    )

    class Meta:
        verbose_name = _('Питч компании')
        verbose_name_plural = _('Питчи компаний')


# Мой магазин

class MyStore(models.Model):

    user = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, null=True, blank=True)

    logo = models.ImageField(upload_to='users/company_logo', null=True, blank=True, verbose_name=_('Логотип'))
    shop_name = models.CharField(max_length=40, null=True, blank=True, verbose_name=_('Название магазина'))
    first_phone = models.CharField(max_length=20, null=True, blank=True, verbose_name=_('Контакт'))
    second_phone = models.CharField(max_length=20, null=True, blank=True, verbose_name=_('Контакт'))

    domain_subdomain = models.CharField(max_length=3, choices=DOMEN, blank=True, null=True, verbose_name=_('Домен/поддомен'))
    domain_name = models.CharField(max_length=200, null=True, blank=True, verbose_name=_('Имя домена'), unique=True)

    facebook = models.URLField(max_length=100, null=True, blank=True)
    instagram = models.URLField(max_length=100, null=True, blank=True)
    twitter = models.URLField(max_length=100, null=True, blank=True)
    google = models.URLField(max_length=100, null=True, blank=True)

    contacts_text = models.TextField(null=True, blank=True)

    # top_sales = models.BooleanField(default=False, verbose_name='Топ продаж')
    # no_items = models.BooleanField(default=False, verbose_name='Без товара')

    products = models.ManyToManyField('catalog.Product', blank=True)

    @property
    def get_url(self):
        if self.domain_subdomain == 'SDM':
            return 'https://{}.buysell.com.ua/'.format(self.domain_name)
        return 'https://{}/'.format(self.domain_name)

    class Meta:
        verbose_name = _('Магазин продавца')
        verbose_name_plural = _('Магазины продавцов')

    def __str__(self):
        return '{} {}'.format(self.id, self.user)


# General info for transaction to my site


class DeliveryAndPayment(models.Model):
    store = models.ForeignKey(MyStore, on_delete=models.CASCADE, related_name='delivery_and_payments')
    type = models.CharField(choices=DeliveryChoices.CHOICES, verbose_name=_('Тип'), max_length=50)
    title = models.CharField(max_length=50, null=True, blank=True, verbose_name=_('Заголовок'))
    text = models.TextField(null=True, blank=True, verbose_name=_('Текст'))


class ExchangeAndReturn(models.Model):
    store = models.OneToOneField(MyStore, on_delete=models.CASCADE, related_name='exchange_and_return')
    title = models.CharField(max_length=50, null=True, blank=True, verbose_name=_('Заголовок'))
    text = models.TextField(null=True, blank=True, verbose_name=_('Текст'))


class HowToUse(models.Model):
    store = models.ForeignKey(MyStore, on_delete=models.CASCADE, related_name='how_to_uses')
    title = models.CharField(max_length=50, null=True, blank=True, verbose_name=_('Заголовок'))
    text = models.TextField(null=True, blank=True, verbose_name=_('Текст'))


class AboutUs(models.Model):
    store = models.OneToOneField(MyStore, on_delete=models.CASCADE, related_name='about_us')
    title = models.CharField(max_length=40, null=True, blank=True, verbose_name=_('Заголовок'))
    image = models.URLField(null=True, blank=True)
    text = models.TextField(null=True, blank=True, verbose_name=_('Текст'))
    description = models.TextField(null=True, blank=True, verbose_name=_('Описание'))


class StoreSliderImage(models.Model):
    store = models.ForeignKey(MyStore, on_delete=models.CASCADE, related_name='slider_images')
    image = models.ImageField(upload_to='users/company/store/slider', verbose_name='Картинка для слайдера')


class StoreSliderImageURL(models.Model):
    store = models.ForeignKey(MyStore, on_delete=models.CASCADE, related_name='slider_image_urls')
    url = models.URLField(max_length=5000)


# Info from my site


class CallBack(models.Model):
    store = models.ForeignKey(MyStore, on_delete=models.CASCADE, related_name='store_call_backs')
    date = models.DateTimeField(auto_now_add=True)
    phone_regex = RegexValidator(regex=r'^\+?1?\d{9,15}$',
                                 message='Phone number must be entered in the format: "+380123456789". '
                                         'Up to 15 digits allowed.', code='invalid_phone', )

    phone_number = models.CharField(validators=[phone_regex, ], max_length=17, blank=True, null=True)
    name = models.CharField(max_length=70)
    is_checked = models.BooleanField(default=False)

    class Meta:
        verbose_name = _('Обратная связь с мого магазина')
        verbose_name_plural = _('Обратная связь с мого магазина')


class Help(models.Model):
    store = models.ForeignKey(MyStore, on_delete=models.CASCADE, related_name='helps')
    name = models.CharField(max_length=20, null=True, blank=True, verbose_name=_('Имя'))
    email = models.EmailField(max_length=40, null=True, blank=True, verbose_name=_('Емейл'))
    message = models.TextField(null=True, blank=True, verbose_name=_('Сообщение'))
    date = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name = _('Помощь с моего магазина')
        verbose_name_plural = _('Помощь с моего магазина')


class EmailSubscriber(models.Model):
    store = models.ForeignKey(MyStore, on_delete=models.CASCADE, related_name='email_subscribers')
    email = models.EmailField()
    date = models.DateTimeField()

    class Meta:
        verbose_name = _('Подписка на емейл с магазина')
        verbose_name_plural = _('Подписки на емейл с магазинов')
