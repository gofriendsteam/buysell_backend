# Generated by Django 2.1.7 on 2019-08-12 11:54

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0049_auto_20190808_1732'),
    ]

    operations = [
        migrations.CreateModel(
            name='EmailSubscriver',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('email', models.EmailField(max_length=254)),
                ('date', models.DateTimeField()),
                ('store', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='email_subscribers', to='users.MyStore')),
            ],
        ),
    ]
