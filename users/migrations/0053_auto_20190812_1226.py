# Generated by Django 2.1.7 on 2019-08-12 12:26

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0052_auto_20190812_1205'),
    ]

    operations = [
        migrations.AlterField(
            model_name='help',
            name='store',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='helps', to='users.MyStore'),
        ),
    ]
