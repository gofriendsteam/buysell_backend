# Generated by Django 2.1.7 on 2019-08-06 17:40

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0043_auto_20190806_1704'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='mystore',
            name='call_back',
        ),
        migrations.AlterField(
            model_name='help',
            name='store',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='help', to='users.MyStore'),
        ),
    ]
