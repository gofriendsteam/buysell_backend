import psycopg2


class MyDatabase:
    def __init__(self, db="market_gen_dev", user="market_gen_user", password='pass12pass'):
        self.conn = psycopg2.connect(database=db, user=user, password=password)
        self.cur = self.conn.cursor()

    def query(self, query):
        self.cur.execute(query)

    def fetchall(self):
        return self.cur.fetchall()

    def close(self):
        self.cur.close()
        self.conn.close()

