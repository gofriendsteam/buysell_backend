from pprint import pprint

from celery import shared_task
from django.contrib.auth import get_user_model
from django.template.loader import render_to_string
from sendgrid import SendGridAPIClient
from sendgrid.helpers.mail import Mail, Header
from django.conf import settings
from consul_kv import Connection
import logging
import os
import sys

from messages_client.constants import MessageSubjectType, MAIN_SERVICE_NAME
from messages_client.publisher import generate_pub_args, send_nats_message
from buy_sell.celery import app
from users.connections import MyDatabase
from users.models import CustomUser, MyStore

User = get_user_model()
logging.basicConfig(stream=sys.stdout, level=logging.INFO)

@shared_task
def send_email_task(*args, **kwargs):
    from_email = settings.DEFAULT_FROM_EMAIL
    message = Mail(
        from_email=from_email,
        **kwargs
    )
    message.add_header(Header('reply_to', 'uchetsmartlead@gmail.com'))

    try:
        sg = SendGridAPIClient(settings.SENDGRID_API_KEY)
        sg.send(message)
    except Exception as e:
        print(e.args)


@app.task
def generate_store(sub_domain):
    consul = os.getenv('CONSUL', 'localhost')
    front_srv = os.getenv('FRONT_SRV', 'http://gen')
    endpoint = f'http://{consul}:8500/v1'
    sub_domain = sub_domain.lower()
    conn = Connection(endpoint=endpoint)

    record = {
        f'traefik/frontends/{sub_domain}/backend': 'gen',
        f'traefik/frontends/{sub_domain}/routes/root/rule': f'Host:{sub_domain}.smartlead.top',
        'traefik/backends/gen/servers/server1/url': f'{front_srv}'
    }

    try:
        conn.put_mapping(record)
        db = MyDatabase(
            db=os.getenv('GENERATE_SITE_DATABASE_NAME'),
            user=os.getenv('GENERATE_SITE_DATABASE_USER'),
            password=os.getenv('GENERATE_SITE_DATABASE_PASSWORD'),

        )
        query = "insert into django_site (id, domain, name) values (default, '{sub_domain}.buysell.com.ua', '{sub_domain}')".format(
            sub_domain=sub_domain
        )
        db.query(query=query)
        db.conn.commit()
        db.close()
        logging.info('Reccord added {}'.format(record))
    except Exception as e:
        raise e


def send_user_email():

    users = CustomUser.objects.all().filter(id__gte=10)
    message = render_to_string('send_email_enter_phone.html')

    data = {
        'to_emails': list(users.values_list('email', flat=True)),
        'subject': "Регистрация на платформе “Smartlead 2.0”",
        'html_content': message,
    }

    send_email_task(**data)


def mystore_general_info_generator(content):
    data = dict()
    data['type'] = MessageSubjectType.UPDATE_MAIN_INFO
    data['source'] = MAIN_SERVICE_NAME
    data['content'] = content

    return data


def update_mystore_general_info_generator(data, subdomain):
    data = mystore_general_info_generator(data)
    args = generate_pub_args(subdomain, str(data))
    send_nats_message(args)

    logging.info('Updated general info in subdomain  {}'.format(subdomain))


@shared_task
def new_subscriber_email(data, source):
    store = MyStore.objects.get(domain_name=source)
    store.email_subscribers.create(
        email=data['email'],
        date=data['date'],
    )
    logging.info('Receive new subscriber email {}'.format(source))


def receive_subscribe_email(data, source):
    new_subscriber_email.delay(data, source)


@shared_task
def new_call_back(data, source):
    store = MyStore.objects.get(domain_name=source)
    store.store_call_backs.create(
        date=data['date'],
        phone_number=data['phone_number'],
        name=data['name'],
    )
    logging.info('Receive new call back {}'.format(source))


def receive_call_back(data, source):
    new_call_back.delay(data, source)


@shared_task
def new_help(data, source):
    store = MyStore.objects.get(domain_name=source)
    store.helps.create(
        email=data['email'],
        date=data['date'],
        name=data['name'],
        message=data['message'],
    )
    logging.info('Receive new help {}'.format(source))


def receive_help(data, source):
    new_help.delay(data, source)
