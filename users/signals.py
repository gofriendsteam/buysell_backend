from django.db.models.signals import post_save
from django.dispatch import receiver

from users.admin import ContractorProxy, PartnerUserProxy, UserAdmin
from academy.tasks import check_finish_lessons, check_finish_module

@receiver(post_save, sender=PartnerUserProxy)
def update_stock2(sender, instance, created, **kwargs):
    if instance:
        check_finish_lessons.delay()
        check_finish_module.delay()