from django.utils.translation import ugettext as _

DOMEN = (
    ('DM', _("Домен")),
    ('SDM', _('Поддомен'))
)

CALL_BACK = (
    ('YES', _('Включена')),
    ('NO', _('Выключена'))
)

USER_ROLE = (
    ('CONTRACTOR', _('Поставщик')),
    ('PARTNER', _('Продавец')),
)

class DeliveryChoices:
    REGIONS = 'regions'
    COURIER_DELIVERY = 'courier_delivery'
    PICKUP = 'pickup'

    CHOICES = (
        (REGIONS, 'Регионы доставки'),
        (COURIER_DELIVERY, 'Курьерская адресная доставка'),
        (PICKUP, 'Самовывоз из филиалов интернет-магазина'),

    )