from django.core.management.base import BaseCommand
from catalog.models import Category2


class Command(BaseCommand):
    help = 'Load categories'

    def handle(self, *args, **options):
        Category2.load_categories()

        self.stdout.write(self.style.SUCCESS('Categories loaded.'))
