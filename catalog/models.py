# encoding=utf8
import sys

from django.core.files.base import ContentFile
from requests import Request

from users.models import CustomUser

sys.getdefaultencoding()
import os
from decimal import Decimal

from django.conf import settings
from django.core.files.storage import FileSystemStorage
from django.db import models
from django.utils.translation import ugettext as _
from mptt.models import MPTTModel, TreeForeignKey

from news.models import TimeStampedModel
from django.contrib.auth import get_user_model
import catalog.constants as constants
from django.db import transaction
from .utils import get_category_data, get_directory_path_product_imgs
from catalog.managers import ContractorProductManager, PartnerProductManager

User = get_user_model()


class Category_prom_x(MPTTModel):
    id = models.PositiveIntegerField(
        primary_key=True,
        verbose_name='id - primary key',
    )
    name = models.CharField(
        max_length=256,
        verbose_name=_('Название категории'),
    )
    parent = TreeForeignKey(
        'self',
        null=True,
        blank=True,
        related_name='children',
        on_delete=models.SET_NULL,
        db_index=True,
    )

    class MPTTMeta:
        order_insertion_by = ['name']

    class Meta:
        verbose_name = _('Категория promX')
        verbose_name_plural = _('Категории promX')

    @property
    def get_category_level(self):
        return self.get_level()

    @property
    def is_have_children(self):
        if self.get_descendant_count() > 0:
            return True
        return False

    def __str__(self):
        return '{}'.format(self.name)

    # @staticmethod
    # def load_categories_prom():
    #     """
    #
    #     python manage.py load_categories
    #
    #     :return: None
    #     """
    #     data = get_category_data()
    #     with transaction.atomic():
    #         with Category_prom_x.objects.disable_mptt_updates():
    #             for obj in data['content']['marketCategorys']:
    #                 if int(obj['parent_id']) > 0:
    #                     parent, created_parent = Category_prom_x.objects.get_or_create(
    #                         id=int(obj['parent_id'])
    #                     )
    #                     if created_parent:
    #                         parent.name = 'instance'
    #                         parent.save()
    #                 else:
    #                     parent = None
    #                 instance, _ = Category_prom_x.objects.get_or_create(
    #                     id=int(obj['category_id']),
    #                 )
    #                 instance.name = obj['name']
    #                 instance.parent = parent
    #                 instance.save()
    #         Category_prom_x.objects.rebuild()


class Category2(MPTTModel):
    id = models.PositiveIntegerField(
        primary_key=True,
        verbose_name='id - primary key',
    )
    name = models.CharField(
        max_length=256,
        verbose_name=_('Название категории'),
    )
    parent = TreeForeignKey(
        'self',
        null=True,
        blank=True,
        related_name='children',
        on_delete=models.SET_NULL,
        db_index=True,
    )

    class MPTTMeta:
        order_insertion_by = ['name']

    class Meta:
        verbose_name = _('Категория rozetka')
        verbose_name_plural = _('Категории rozetka')

    def to_json(self):
        return {
            'id': self.id,
            'name': self.name,
            # 'parent': self.parent
        }

    @property
    def get_category_level(self):
        return self.get_level()

    @property
    def is_have_children(self):
        if self.get_descendant_count() > 0:
            return True
        return False

    def __str__(self):
        return '{}'.format(self.name)

    @staticmethod
    def load_categories():
        """

        python manage.py load_categories

        :return: None
        """
        data = get_category_data()
        with transaction.atomic():
            with Category2.objects.disable_mptt_updates():
                for obj in data['content']['marketCategorys']:
                    if int(obj['parent_id']) > 0:
                        parent, created_parent = Category2.objects.get_or_create(
                            id=int(obj['parent_id'])
                        )
                        if created_parent:
                            parent.name = 'instance'
                            parent.save()
                    else:
                        parent = None
                    instance, _ = Category2.objects.get_or_create(
                        id=int(obj['category_id']),
                    )
                    instance.name = obj['name']
                    # print(obj['name'])
                    instance.parent = parent
                    instance.save()
            Category2.objects.rebuild()


class PriceStatus(models.Model):
    status = models.SmallIntegerField(choices=constants.StatusPriceNumbers.REVIEW_TYPES, null=True)
    created = models.DateTimeField(auto_now_add=True, verbose_name=_('Дата создания'))

    class Meta:
        verbose_name = _('Статус цены')
        verbose_name_plural = _('Статус цены')


class Product(TimeStampedModel):
    """
    Если поле contractor=NULL - товар добавлен поставщиком.
    В противном случае это товар, добавленный продавцом от поставщика
    """

    category = TreeForeignKey(
        Category2,
        null=True,
        verbose_name=_('Категория товара rozetka'),
        on_delete=models.SET_NULL,
        blank=True,
    )

    category_prom_x = TreeForeignKey(
        Category_prom_x,
        null=True,
        verbose_name=_('Категория товара prom'),
        on_delete=models.SET_NULL,
        related_name='category_prom_x',
        blank=True,
    )

    user = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        verbose_name=_('Пользователь'),
        related_name='products',
        null=True,
        blank=True,
    )

    avatar_url = models.URLField(
        verbose_name=_('Аватарка товара'),
        null=True, blank=True,
        max_length=2000
    )

    # product full name fields
    product_type = models.CharField(
        max_length=256,
        verbose_name=_('Модель'),
        null=True, blank=True
    )
    brand = models.CharField(
        max_length=255,
        verbose_name=_('Бренд'),
        null=True, blank=True
    )
    name = models.CharField(
        max_length=255,
        verbose_name=_('Имя продукта'),
        null=True,
    )
    created_type = models.CharField(
        max_length=255,
        verbose_name=_('Тип товара'),
        null=True, blank=True
    )
    variety_type = models.CharField(
        max_length=256,
        verbose_name=_('Название разновидности'),
        null=True, blank=True
    )
    vendor_code = models.CharField(
        max_length=300,
        verbose_name=_('Артикул'),
        null=True,
        blank=True
    )

    # required product specs
    warranty_duration = models.PositiveIntegerField(default=0)  # warranty duration in days
    vendor_country = models.CharField(
        max_length=256,
        null=True, blank=True
    )
    box_size = models.CharField(
        max_length=256,
        null=True, blank=True
    )
    count = models.PositiveIntegerField(
        default=0,
        verbose_name=_('Наличие'),
        null=True
    )
    price = models.DecimalField(
        max_digits=10,
        decimal_places=2,
        verbose_name=_('Розниная цена товара'),
    )
    clean_price = models.DecimalField(
        max_digits=10,
        decimal_places=2,
        verbose_name=_('Цена товара'),
        null=True, blank=True
    )
    recommended_price = models.DecimalField(
        max_digits=10,
        decimal_places=2,
        null=True,
        blank=True,
        verbose_name=_('Рекомендованная цена товара'),
    )

    fixed_recommended_price = models.BooleanField(
        default=False,
        verbose_name='Fix РРЦ', )

    weight = models.DecimalField(
        max_digits=10,
        decimal_places=2,
        null=True,
        blank=True,
        verbose_name=_('Вес товара, кг'),
    )
    volume = models.DecimalField(
        max_digits=10,
        decimal_places=2,
        null=True,
        blank=True,
        verbose_name=_('Объем товара, куб. м'),
    )
    description = models.TextField(
        max_length=5000,
        verbose_name=_('Описание'),
        null=True
    )  # html tags allowed

    # not required product specs
    extra_description = models.TextField(
        null=True,
        blank=True,
        verbose_name=_('Дополнительные характеристики')
    )  # html tags allowed
    age_group = models.CharField(
        max_length=128,
        null=True,
        blank=True,
        verbose_name=_('Возрастная группа')
    )
    material = models.CharField(
        max_length=128,
        null=True,
        blank=True,
        verbose_name=_('Материал товара')
    )

    contractor_product = models.ForeignKey(
        'self',
        null=True,
        blank=True,
        on_delete=models.CASCADE,
        verbose_name=_('Связь с поставщиком продукта'),
        related_name='contractor_products',
    )

    partner_percent = models.IntegerField(
        null=True, blank=True,
        default=10,
        verbose_name=_('Процент продавца')
    )

    rozetka_id = models.IntegerField(
        null=True,
        blank=True
    )

    prom_id = models.IntegerField(
        null=True,
        blank=True
    )
    yml_id = models.CharField(
        max_length=256,
        null=True,
        blank=True
    )
    color = models.CharField(
        max_length=256,
        null=True,
        blank=True
    )
    price_status = models.ForeignKey(
        PriceStatus,
        on_delete=models.CASCADE,
        verbose_name=_('Статус цены'),
        related_name='price_status',
        null=True,
        blank=True,
    )

    # managers
    objects = models.Manager()
    products_by_contractors = ContractorProductManager()
    products_by_partners = PartnerProductManager()

    #
    # def __str__(self):
    #     return '{}'.format(str(self.id))

    def __str__(self):
        return '{} - {}'.format(self.id, str(self.name)[:20])

    @property
    def price_percent(self):
        if self.fixed_recommended_price:
            return self.recommended_price
        else:

            markup_price = float((self.contractor_product.price if self.contractor_product else 0) * (
                        1 + self.contractor_product.user.markup / 100) if self.contractor_product else 0)
            markup_price *= 1 + (self.partner_percent or 0) / 100
            return round(markup_price, 2)

    @property
    def is_in_stock(self):
        return self.count == 0

    @property
    def partner_price(self):
        if self.contractor_product:
            return int(self.contractor_product.price * \
                       (1 + Decimal(0.01) * (self.contractor_product.user.percent_for_partners or 0)) * \
                       (1 + Decimal(0.01) * self.partner_percent))
        return int(self.price) if self.price else 0

    @property
    def contractor_price_for_partner(self):
        return int(self.contractor_product.price *
                   (1 + Decimal(0.01) * (self.contractor_product.user.percent_for_partners or 0)))

    @property
    def is_added_to_yml_rozetka(self):
        return self.user.ymltemplate_set.filter(products=self).exists()

    @property
    def type_yml(self, *args, **kwargs):
        return self

    class Meta:
        verbose_name = _('Товар')
        verbose_name_plural = _('Товары')
        unique_together = (
            ('user', 'product_type', 'brand', 'name', 'variety_type', 'vendor_code', 'contractor_product'),
        )
        ordering = ['-created']


# ============ Options "choices" from rozetka ================ #

class OptionGroup(models.Model):
    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=500, verbose_name=_('Название параметра'))
    group_type = models.SmallIntegerField(
        verbose_name=_('Тип группы'),
        choices=constants.ProductOptionTypes.TYPES,
        default=constants.ProductOptionTypes.CHOICE,
    )

    class Meta:
        verbose_name = _('Option Group')
        verbose_name_plural = _('Option Groups')

    def __str__(self):
        return self.name


class CategoryOptionGroup(models.Model):
    group = models.ForeignKey(OptionGroup, on_delete=models.CASCADE, related_name='category_option_groups')
    category = models.ForeignKey(Category2, related_name='optiongroups', on_delete=models.CASCADE)

    class Meta:
        verbose_name = _('Category options group')
        verbose_name_plural = _('Category option groups')

    def __str__(self):
        return '{} - {}'.format(self.category.name, self.group.name)


class CategoryPromOptionGroup(models.Model):
    group = models.ForeignKey(OptionGroup, on_delete=models.CASCADE, related_name='category_prom_option_groups')
    category = models.ForeignKey(Category_prom_x, related_name='optiongroupsprom', on_delete=models.CASCADE)

    class Meta:
        verbose_name = _('Category options group')
        verbose_name_plural = _('Category option groups')

    def __str__(self):
        return '{} - {}'.format(self.category.name, self.group.name)


class OptionValue(models.Model):
    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=500, verbose_name=_('Значение параметра'))

    def __str__(self):
        return self.name


class CategoryOptionGroupValue(models.Model):
    value = models.ForeignKey(
        OptionValue,
        on_delete=models.CASCADE,
        related_name='optionvalues',
    )
    category_group = models.ForeignKey(CategoryOptionGroup, related_name='optiongroupvalues', on_delete=models.CASCADE)

    class Meta:
        verbose_name = _('Category Option Group value')
        verbose_name_plural = _('Category Option Groups values')

    def __str__(self):
        return f'{self.value.name} ' + '--' + f' {self.category_group.group.name}'


class CategoryPromOptionGroupValue(models.Model):
    value = models.ForeignKey(
        OptionValue,
        on_delete=models.CASCADE,
        related_name='optionvaluesprom',
    )
    category_group = models.ForeignKey(CategoryPromOptionGroup, related_name='optiongroupvaluesprom',
                                       on_delete=models.CASCADE)

    class Meta:
        verbose_name = _('Category Option Group value')
        verbose_name_plural = _('Category Option Groups values')

    def __str__(self):
        return f'{self.value.name} ' + '--' + f' {self.category_group.group.name}'


class ProductOption(models.Model):
    product = models.ForeignKey(Product, related_name='product_options', on_delete=models.CASCADE)
    option = models.ForeignKey(CategoryOptionGroupValue, on_delete=models.CASCADE)

    class Meta:
        verbose_name = _('Category option')
        verbose_name_plural = _('Category options')


# ============= OPTIONS by Rozetka with textarea ================ #

class ProductOptionValueText(models.Model):
    product = models.ForeignKey(Product, related_name='product_by_textarea_options', on_delete=models.CASCADE)
    group = models.ForeignKey(CategoryOptionGroup, on_delete=models.CASCADE)
    value = models.CharField(max_length=500, verbose_name=_('Значение параметра'))

    class Meta:
        verbose_name = _('Category option by textarea')
        verbose_name_plural = _('Category options by textarea')

    def to_json(self):
        return {
            'id': self.id,
            'group': self.group.id,
            'value': self.value
        }


# ============= Mproduct_optionsANUAL OPTIONS ================ #


class ProductOptionManualInput(models.Model):
    product = models.ForeignKey(Product, related_name='product_manual_options', on_delete=models.CASCADE, null=True)
    group = models.CharField(max_length=2000, verbose_name=_('Название группы'), null=True)
    value = models.CharField(max_length=2000, verbose_name=_('Значение параметра'), null=True)

    class Meta:
        verbose_name = _('Category manual option')
        verbose_name_plural = _('Category manual options')

    def to_json(self):
        return {
            'id': self.id,
            'group': self.group,
            'value': self.value,
        }


# ==================================== #


class ProductImageURL(models.Model):
    product = models.ForeignKey(
        Product,
        on_delete=models.CASCADE,
        related_name='product_image_urls'
    )
    url = models.URLField(
        verbose_name=_('Ссылка на изображение товара'),
        max_length=100000,
    )


class ProductImage(models.Model):
    product = models.ForeignKey(
        Product,
        on_delete=models.CASCADE,
        related_name='product_images'
    )
    image = models.ImageField(
        # upload_to='catalog/products/images',
        upload_to=get_directory_path_product_imgs,
        verbose_name=_('Изображение товара'),
    )


class OverwriteStorage(FileSystemStorage):

    def get_available_name(self, name):
        """Returns a filename that's free on the target storage system, and
        available for new content to be written to.

        Found at http://djangosnippets.org/snippets/976/

        This file storage solves overwrite on upload problem. Another
        proposed solution was to override the save method on the model
        like so (from https://code.djangoproject.com/ticket/11663):

        def save(self, *args, **kwargs):
            try:
                this = MyModelName.objects.get(id=self.id)
                if this.MyImageFieldName != self.MyImageFieldName:
                    this.MyImageFieldName.delete()
            except: pass
            super(MyModelName, self).save(*args, **kwargs)
        """
        # If the filename already exists, remove it as if it was a true file system
        if self.exists(name):
            os.remove(os.path.join(settings.MEDIA_ROOT, name))
        return name


# image = models.ImageField(max_length=SOME_CONST, storage=OverwriteStorage(), upload_to=image_path)


class ProductUploadHistory(models.Model):
    user = models.ForeignKey(
        User,
        related_name='product_import_files',
        verbose_name=_('Пользователь'),
        on_delete=models.CASCADE,
    )
    created = models.DateTimeField(auto_now_add=True, verbose_name=_('Дата создания'))
    is_uploaded = models.BooleanField(
        verbose_name=_('Загрузка прошла успешно'),
        default=False,
    )
    errors = models.TextField(
        verbose_name=_('Ошибки'),
        null=True, blank=True,
    )
    xls_file = models.FileField(
        upload_to='catalog/product/uploads',
        verbose_name=_('XLS файл'),
        null=True
    )
    file_type = models.CharField(
        max_length=7,
        choices=constants.ProductUploadFileTypes.PRODUCT_UPLOAD_FILE_TYPES
    )
    total_products_count = models.PositiveIntegerField(default=0)
    imported_products_count = models.PositiveIntegerField(default=0)

    def __str__(self):
        return '{}'.format(self.user)

    class Meta:
        verbose_name = _('Файл для импорта товаров')
        verbose_name_plural = _('Файлы для импорта товаров')


class YMLTemplate(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    template = models.FileField(
        upload_to='yml_templates',
    )
    yml_type = models.CharField(
        max_length=10,
        choices=constants.YMLFileTypes.YML_TYPES
    )
    products = models.ManyToManyField(Product, related_name='products')

    #
    # def __str__(self):
    #     return self.yml_type

    # def save(self ,*args, **kwargs):
    #     print('lwargs  in save' , kwargs , args)
    #     try:
    #         print(kwargs , kwargs['filename'])
    #     except Exception as e:
    #         print(type(e) , e)
    #     self.template.save(kwargs['filename'] , ContentFile(kwargs['content']))
    #     super().save()

    def __str__(self):
        return '{} {}'.format(self.user.id, self.template.name)

    class Meta:
        verbose_name = _('YML шаблон')
        verbose_name_plural = _('YML шаблоны')
        unique_together = ('user', 'yml_type',)


class YmlPriceInfo(models.Model):
    TYPE_YML = (
        ('rozetka', 'rozetka'),
        ('prom', 'prom'),
    )
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    product_id = models.PositiveIntegerField()
    type_yml = models.CharField(max_length=255, choices=TYPE_YML)
    percent = models.FloatField()

    def __str__(self):
        return '{} {} {}'.format(self.product_id, self.type_yml, self.percent)
