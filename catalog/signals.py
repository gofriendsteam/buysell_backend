from django.db.models.signals import post_save, pre_delete, pre_save
from django.dispatch import receiver


from catalog.models import ProductUploadHistory, YMLTemplate, Product , ProductImage
from catalog.tasks import load_products_from_xls
from users.models import CustomUser


@receiver(post_save, sender=ProductUploadHistory)
def upload_product_from_xls(sender, instance, created, **kwargs):
    # print('upload_product_from_xls')
    if created:
        instance.save()
        data = {
            'instance_id': instance.id,
        }
        # print(data, 'id!!!!!!!!!!!!!!!!!!!!')
        load_products_from_xls.delay(**data)


@receiver(pre_delete, sender=YMLTemplate)
def mymodel_delete(sender, instance, **kwargs):
    # Pass false so FileField doesn't save the model.
    instance.template.delete(False)


@receiver(post_save, sender=Product)
def upload_product_from_xls6(sender, instance, created, **kwargs):
    # print('66666666666666666666')
    if created:
        # print(instance.user)
    #     print(Product.objects.get(instance.id).user)
    #     print(CustomUser.objects.get(pk=instance.user.id).fixed_recommended_price)
        if CustomUser.objects.get(pk=instance.user.id).fixed_recommended_price:
            instance.fixed_recommended_price = True
        instance.save()

        # print(instance.id)
    #     instance.save()
        # data = {
        #     'instance_id': instance.id,
        # }
        # print(data, 'id!!!!!!!!!!!!!!!!!!!!')
        # load_products_from_xls.delay(**data)


#
# @receiver(pre_delete, sender=ProductImage)
# def delete_image(sender, instance, **kwargs):
#     print('signal pre delete!!!!!')
#     instance.image.delete(False)
#     print('signal pre delete!!!!!')