from django.contrib.auth import get_user_model
from django_filters import rest_framework as filters
from .models import Product, Category2, YMLTemplate

User = get_user_model()



class FilterYMLProduct(filters.FilterSet):
    type_yml =  filters.CharFilter(field_name='type_yml' , method='filter_yml')


    def filter_yml(self , queryset , value , *args):
        print('filter yml method')
        print(self)
        print(queryset)
        print(value)
        print(args)
        return queryset

from .yml_handler_view import  YmlHandler


class ProductFilter(filters.FilterSet):
    min_price = filters.NumberFilter(field_name="price", lookup_expr='gte')
    max_price = filters.NumberFilter(field_name="price", lookup_expr='lte')
    category_id = filters.ModelChoiceFilter(
        field_name='category',
        queryset=Category2.objects.all()
    )
    in_stock = filters.BooleanFilter(
        method='filter_in_stock'
    )
    brand = filters.CharFilter(field_name='brand', lookup_expr='icontains')
    name = filters.CharFilter(field_name='name', lookup_expr='icontains')
    vendor_code = filters.CharFilter(field_name='vendor_code', lookup_expr='icontains')
    user_id = filters.ModelChoiceFilter(
        field_name='user',
        queryset=User.objects.filter(role='CONTRACTOR')
    )
    contractor_product = filters.ModelChoiceFilter(
        field_name='contractor_product__user_id',
        queryset=User.objects.all()
    )
    type_yml = filters.CharFilter(field_name='type_yml', method='filter_by_type_yml')

    class Meta:
        model = Product
        fields = [
            'category_id',
            'name',
            'vendor_code',
            'min_price',
            'max_price',
            'in_stock',
            'user_id',
            'contractor_product',
            'type_yml',
            'price'

        ]

    def filter_in_stock(self, queryset, name, value):
        if value:
            return queryset.filter(count__gte=1)
        if not value:
            return queryset.filter(count=0)
        return queryset

    def filter_by_type_yml(self, queryset, value, *args):
        #
        type_yml = args[0]
        # print('id' , self.request.user.id , 'type_yml' , type_yml)
        filename = '{type_yml}_products-{id}.xml'.format(type_yml=type_yml, id=self.request.user.id)
        file_yml = YmlHandler(filename)
        res = file_yml.convert_from_file_to_response()
        # print('filter_by_type_yml' , res  , len(res) )
        if value:
            products_by_type_yml = YMLTemplate.objects.filter(user=self.request.user)
            for yml in products_by_type_yml:
                if yml.yml_type == args[0]:
                    return yml.products.all()
            return queryset
        else:
            return queryset
