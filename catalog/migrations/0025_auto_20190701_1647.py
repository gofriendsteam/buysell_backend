# Generated by Django 2.1.7 on 2019-07-01 16:47

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0024_auto_20190701_1626'),
    ]

    operations = [
        migrations.CreateModel(
            name='OptionValue',
            fields=[
                ('id', models.IntegerField(primary_key=True, serialize=False)),
                ('name', models.CharField(max_length=100, verbose_name='Значение параметра')),
            ],
        ),
        migrations.AlterField(
            model_name='categoryoptiongroupvalue',
            name='category_group',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='optiongroupvalues', to='catalog.CategoryOptionGroup'),
        ),
        migrations.AlterField(
            model_name='categoryoptiongroupvalue',
            name='id',
            field=models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID'),
        ),
        migrations.AlterUniqueTogether(
            name='categoryoptiongroupvalue',
            unique_together=set(),
        ),
        migrations.RemoveField(
            model_name='categoryoptiongroupvalue',
            name='name',
        ),
        migrations.AddField(
            model_name='categoryoptiongroupvalue',
            name='value',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, to='catalog.OptionValue'),
            preserve_default=False,
        ),
    ]
