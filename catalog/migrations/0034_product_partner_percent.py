# Generated by Django 2.1.7 on 2019-08-13 12:31

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0033_auto_20190729_1537'),
    ]

    operations = [
        migrations.AddField(
            model_name='product',
            name='partner_percent',
            field=models.IntegerField(blank=True, default=0, null=True, verbose_name='Процент продавца'),
        ),
    ]
