# Generated by Django 2.1.7 on 2019-11-20 15:16

from django.db import migrations
import django.db.models.deletion
import mptt.fields


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0055_auto_20191120_1511'),
    ]

    operations = [
        migrations.AddField(
            model_name='product',
            name='category_prom',
            field=mptt.fields.TreeForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='category_prom', to='catalog.Category_prom', verbose_name='Категория товара prom'),
        ),
        migrations.AlterField(
            model_name='product',
            name='category',
            field=mptt.fields.TreeForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='catalog.Category', verbose_name='Категория товара rozetka'),
        ),
    ]
