# Generated by Django 2.1.7 on 2019-11-14 10:40

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0047_auto_20191113_1824'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='categoryrozetka',
            name='category_ptr',
        ),
        migrations.AlterModelOptions(
            name='category',
            options={'verbose_name': 'Категория rozetka', 'verbose_name_plural': 'Категории rozetka'},
        ),
        migrations.DeleteModel(
            name='CategoryRozetka',
        ),
    ]
