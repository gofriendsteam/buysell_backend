# Generated by Django 2.1.7 on 2019-11-15 11:50

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0048_auto_20191114_1040'),
    ]

    operations = [
        migrations.AddField(
            model_name='product',
            name='type_product',
            field=models.CharField(max_length=255, null=True, verbose_name='Тип товара'),
        ),
        migrations.AlterField(
            model_name='productuploadhistory',
            name='file_type',
            field=models.CharField(choices=[('inner', 'Inner'), ('rozetka', 'Rozetka'), ('yml', 'YML'), ('promYml', 'promYml')], max_length=7),
        ),
    ]
