import base64
import json
import subprocess
import uuid
import reprlib
import time

import requests
from django.conf import settings
from django.core.cache import cache


def get_category_data():
    with open('catalog/categories.json' , encoding='utf-8') as f:
        data = json.load(f)
        return data


def get_rozetka_auth_token(user, invalidate_cache=False):
    if invalidate_cache:
        token_rozetka = None
    else:
        token_rozetka = cache.get('user_id_{}'.format(user.pk))
    try:
        login = user.rozetka_username
        password = base64.b64encode(bytes(user.rozetka_password, 'utf-8')).decode('utf-8')
    except Exception as e:
        # print(e)
        return

    if not token_rozetka:
        if login and password:
            url = "https://api.seller.rozetka.com.ua/sites"
            headers = {
                'Content-Type': 'application/json',
            }
            payload = {
                'username': login,
                'password': password,
            }
            data = send_request(url, method='POST', json=payload, headers=headers)
            if data['success']:
                token_rozetka = data['content']['access_token']
                cache.set('user_id_{}'.format(user.pk), token_rozetka, 60 * 60 * 24)

    return token_rozetka



def send_request(url, method='GET', **kwargs):
    r = requests.Request(method, url, **kwargs)
    prep = r.prepare()
    s = requests.Session()
    resp = s.send(prep)
    r.encoding = 'utf-8'
    return resp.json()

def _filter(_d):
  return {a:b for a, b in _d.items() if a != 'parent'}


def group_vals(_d, _start=None):
  return list(
      _filter({**i, 'subcategories': group_vals(_d, i['id'])})
      for i in _d if i['parent'] == _start
  )


def remove_prefix(text, prefix):
    if text.startswith(prefix):
        return text[len(prefix):]
    return text


def get_host_name():
    if settings.HOST_NAME == 'dev.buy-sell.com.ua':
        return 'https://api-' + settings.HOST_NAME
    elif settings.HOST_NAME == 'buy-sell.com.ua':
        return 'https://api.' + settings.HOST_NAME
    else:
        return settings.HOST_NAME


def get_storage_host_name():
    return f'https://api.buy-sell.com.ua/'



def get_file_name_without_domain_and_signature(url):
    try:
        return url.split('?')[0].split('/')[-1]
    except:
        return


def get_directory_path_product_imgs(instance, filename):
    file_format = filename.split('.')[-1]
    return '{}/{}.{}'.format('catalog/products/images', str(uuid.uuid4()), file_format)



def write_to_errors_file(error):
    import os
    import datetime
    file_error = os.path.join('.', 'error.txt')
    with open(file_error, 'a') as f:
        print(error + str(datetime.datetime.now()), file=f)
        print()



def process_result(result_gen, func, **kwargs):
    """
    :param result_gen: generator, that give us data per page
    :param func: function, that process the data
    :param kwargs: all needed data for func
    """

    for data in result_gen:
        if data is None:
            return
        try:
            func(data, **kwargs)
        except Exception as e:
            print("Exept in utils")
            print(e)


def get_rozetka_data_per_page(user, url_template, token_rozetka=None):
    """
    Send request to rozetka, then yield response per page
    :param user: CustomUser
    :param marketplace: UserMarketplaces
    :param url_template: url with query for 'next_page'
    :param token_rozetka: Bearer token for Rozetka
    :return: Generator with result
    """
    if token_rozetka is None:
        token_rozetka = get_rozetka_auth_token(user)
    if token_rozetka:
        next_page = 1
        retry = False
        processing = True
        while processing:
            url = url_template.format(next_page=next_page)
            data = send_request_to_rozetka(url, token_rozetka)
            print('DATA\n', reprlib.repr(data))
            if not data['success']:
                break
            try:
                yield data
            except Exception:
                if not retry:
                    retry = True
                    time.sleep(0.5)
                    continue
                break

            if next_page == data['content']['_meta']['pageCount'] or data['content']['_meta']['pageCount'] == 0:
                processing = False
            else:
                next_page += 1

            # to prevent DOS attack :-)
            time.sleep(0.4)
    yield None


def send_request_to_rozetka(url, token, **kwargs):
    headers = {
        'Authorization': f"Bearer {token}",
        'cache-control': "no-cache",
    }
    return send_request(url, headers=headers, **kwargs)