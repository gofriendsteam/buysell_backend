import datetime
import traceback
from decimal import Decimal

from django.core.exceptions import ObjectDoesNotExist
from django.core.files.base import ContentFile
from django.shortcuts import render_to_response
from django.db import transaction
from requests import Response
from rest_framework import serializers
from rest_framework.exceptions import ValidationError
from django.utils.translation import gettext as _
from rest_framework.serializers import raise_errors_on_nested_writes
from rest_framework.utils import model_meta

from catalog.constants import YMLFileTypes
from catalog.models import Product, ProductImage, ProductImageURL, YMLTemplate, ProductUploadHistory, \
    CategoryOptionGroupValue, CategoryOptionGroup, ProductOption, OptionValue, OptionGroup, ProductOptionValueText, \
    ProductOptionManualInput, Category_prom_x, CategoryPromOptionGroup, CategoryPromOptionGroupValue, Category2, User, \
    PriceStatus , YmlPriceInfo
from catalog.utils import remove_prefix, get_host_name, get_storage_host_name, \
    get_file_name_without_domain_and_signature
from catalog.yml_handler_view import YmlHandler
from users.models import Company, MyStore, CustomUser
from users.utils import CustomBase64Field



class RecursiveField(serializers.BaseSerializer):

    def to_representation(self, instance):
        ParentSerializer = self.parent.parent.__class__
        serializer = ParentSerializer(instance, context=self.context)
        return serializer.data

    def to_internal_value(self, data):
        ParentSerializer = self.parent.parent.__class__
        Model = ParentSerializer.Meta.model
        try:
            instance = Model.objects.get(pk=data)
        except ObjectDoesNotExist:
            raise serializers.ValidationError(
                'Object {} does not exist'.format(Model().__class__.__name__)
            )
        return instance


class CategoryListSerializer(serializers.ModelSerializer):
    is_have_children = serializers.ReadOnlyField()

    class Meta:
        model = Category2
        fields = (
            'id',
            'name',
            'parent',
            'is_have_children',
        )


class PriceStatusSerializer(serializers.ModelSerializer):
    class Meta:
        model = PriceStatus
        fields = (
            'status',
            'created',
        )


class CategoryPromListSerializer(serializers.ModelSerializer):
    is_have_children = serializers.ReadOnlyField()

    class Meta:
        model = Category_prom_x
        fields = (
            'id',
            'name',
            'parent',
            'is_have_children',
        )


class CategoryListWithParentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Category2
        fields = (
            'id',
            'name',
            'parent',
        )


class CategorySmallSerializer(serializers.ModelSerializer):
    class Meta:
        model = Category2
        fields = (
            'id',
            'name',
        )


class CategoryPromSmallSerializer(serializers.ModelSerializer):
    class Meta:
        model = Category_prom_x
        fields = (
            'id',
            'name',
        )


class Category2Serializer(serializers.ModelSerializer):
    name = serializers.CharField(required=False)
    subcategories = RecursiveField(
        source='children',
        many=True, required=False,
    )

    class Meta:
        model = Category2
        fields = (
            'id',
            'name',
            'subcategories',
        )

    def validate(self, attrs):
        name = attrs.get('name', None)
        subcategories = attrs.get('children', None)

        if not name and not subcategories:
            raise serializers.ValidationError(
                'Enter subcategory for association.'
            )
        return attrs


class CategoryPromSerializer(serializers.ModelSerializer):
    name = serializers.CharField(required=False)
    subcategories = RecursiveField(
        source='children',
        many=True, required=False,
    )

    class Meta:
        model = Category_prom_x
        fields = (
            'id',
            'name',
            'subcategories',
        )

    def validate(self, attrs):
        name = attrs.get('name', None)
        subcategories = attrs.get('children', None)

        if not name and not subcategories:
            raise serializers.ValidationError(
                'Enter subcategory for association.'
            )
        return attrs


class ProductImageSerializer(serializers.ModelSerializer):
    image_decoded = CustomBase64Field(source='image', required=False)

    class Meta:
        model = ProductImage
        fields = (
            'id',
            'image_decoded',
        )

    # def to_representation(self, instance):
    #     print('___________________')
    #     # print(self.context['request'].data)
    #     ret = super().to_representation(instance)
    #     print('to_representation to_representation to_representation to_representation')
    #     print(ret['image_decoded'])
    #     # print('image_decoded')
    #     # ProductImage.objects.update_or_create(product=ret['product'],image=ret['image_decoded'])
    #     return ret['image_decoded']
    #     # print(ret['product_id'])
    #     # ProductImageURL.objects.update_or_create(product_id=ret['product_id'], url=ret['image_decoded'])


class ProductImageURLSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProductImageURL
        fields = (
            'id',
            'url',
        )


class ProductIdCountSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    count = serializers.IntegerField()


class ProductsCountSerializer(serializers.Serializer):
    products = ProductIdCountSerializer(many=True)


class ProductListIdSerializer(serializers.ModelSerializer):
    product_list_ids = serializers.PrimaryKeyRelatedField(
        many=True,
        read_only=False,
        queryset=Product.products_by_contractors.all(),
    )

    class Meta:
        model = Product
        fields = (
            'product_list_ids',
        )


class ProductOptionsSerializer(serializers.ModelSerializer):
    group = serializers.ReadOnlyField(source='option.category_group.id')
    value = serializers.IntegerField(source='option.id')

    class Meta:
        model = ProductOption
        fields = (
            'group',
            'value',
        )


class ProductOptionsByTextAreaSerializer(serializers.ModelSerializer):
    group = serializers.ReadOnlyField(source='group.id')

    class Meta:
        model = ProductOptionValueText
        fields = (
            'group',
            'value',
        )


class ProductOptionsByTextAreaEditableSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProductOptionValueText
        fields = (
            'group',
            'value',
        )


class ProductOptionManualInputSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProductOptionManualInput
        fields = (
            'group',
            'value',
        )


class ProductCategoryObjectSerializer(serializers.ModelSerializer):
    cover_images = ProductImageSerializer(many=True, source='product_images', required=False)
    image_urls = ProductImageURLSerializer(many=True, source='product_image_urls', required=False)
    category = CategorySmallSerializer(many=False)
    category_prom_x = CategoryPromSmallSerializer(many=False)
    options = ProductOptionsSerializer(many=True, source='product_options')
    option_by_text_areas = ProductOptionsByTextAreaSerializer(many=True, source='product_by_textarea_options')
    manual_options = ProductOptionManualInputSerializer(many=True, source='product_manual_options')
    price_status = PriceStatusSerializer(many=False, required=False, allow_null=True)

    def to_representation(self, instance):
        ret = super().to_representation(instance)
        # print("hhh")
        # print(ret['user'])
        # print(ret['price'])
        # print(CustomUser.objects.get(pk=ret['user']).markup)
        # print(User.objects.all())

        try:
            category_id = ret.get('category').get('id', None)
        except Exception:
            category_id = None

        if category_id:
            category = Category2.objects.get(id=category_id)
            ret['selectedCategories'] = category.get_ancestors(
                ascending=False, include_self=True
            ).values_list('id', flat=True)
        else:
            ret['selectedCategories'] = None

        if self.context['request'].user.role == 'PARTNER':
            ret['price'] = Decimal(instance.price * Decimal(
                instance.user.percent_for_partners * 0.01 + 1 if instance.user.percent_for_partners else 1
            )) if instance.price else None
            ret['recommended_price'] = Decimal(instance.price * Decimal(
                instance.user.percent_for_partners * 0.01 + 1 if instance.user.percent_for_partners else 1
            )) if instance.price else None
        else:
            ret['price'] = instance.price
            ret['recommended_price'] = instance.recommended_price

        try:
            ret['price'] = round(ret['price'] + (ret['price'] * CustomUser.objects.get(pk=ret['user']).markup / 100), 2)
        except Exception as e:
            print(type(e), e, 'Error if price is NONE!')

        ret['recommended_price'] = instance.recommended_price
        if instance.clean_price:
            ret['price'] = round(
                instance.clean_price + (instance.clean_price * CustomUser.objects.get(pk=ret['user']).markup / 100), 2)
            ret['clean_price'] = instance.clean_price
        else:
            ret['clean_price'] = float(instance.price)
        return ret

    class Meta:
        model = Product
        fields = (
            'id',
            'category',
            'user',
            'category_prom_x',
            'name',
            'avatar_url',
            'vendor_code',
            'contractor_product',
            'brand',
            'product_type',
            'count',
            'description',
            'price',
            'recommended_price',
            'weight',
            'volume',
            'cover_images',
            # 'id_contractor',
            'image_urls',
            'options',
            'option_by_text_areas',
            'manual_options',
            'created_type',
            'price_status',
            'clean_price',
        )


class ProductCategoryObjectPartnerSerializer(serializers.ModelSerializer):
    cover_images = ProductImageSerializer(many=True, source='product_images', required=False)
    image_urls = ProductImageURLSerializer(many=True, source='product_image_urls', required=False)
    category = CategorySmallSerializer(many=False)
    category_prom_x = CategoryPromSmallSerializer(many=False)
    options = ProductOptionsSerializer(many=True, source='product_options')
    option_by_text_areas = ProductOptionsByTextAreaSerializer(many=True, source='product_by_textarea_options')
    manual_options = ProductOptionManualInputSerializer(many=True, source='product_manual_options')
    # price = serializers.ReadOnlyField(source='partner_price')
    contractor_product = ProductCategoryObjectSerializer(many=False)
    price_status = PriceStatusSerializer(many=False, required=False)

    class Meta:
        model = Product
        fields = (
            'id',
            'category',
            'user',
            'category_prom_x',
            'name',
            'avatar_url',
            'vendor_code',
            'contractor_product',
            'brand',
            'product_type',
            'price',
            'count',
            'description',
            'contractor_price_for_partner',
            'partner_percent',
            'recommended_price',
            'fixed_recommended_price',
            'cover_images',
            'image_urls',
            'options',
            'volume',
            'box_size',
            'weight',
            'option_by_text_areas',
            'manual_options',
            'is_added_to_yml_rozetka',
            'created_type',
            'price_status',
            # 'marketplace'
        )

    def to_representation(self, instance):
        ret = super().to_representation(instance)
        ret['marketplace'] = {}
        ret['marketplace']['rozetka'] = False
        ret['marketplace']['prom'] = False

        try:
            filename_rozetka = '{type_yml}_products-{id}.xml'.format(type_yml='rozetka',
                                                                     id=ret.get('user'))

            current_yml_roz = YmlHandler(filepath=filename_rozetka)

            if current_yml_roz.exist_product(id=ret.get('id')):
                print('yes')
                ret['marketplace']['rozetka'] = True
            else:
                print('no')
                # pass
        except Exception as e:
            print(e)
        try:
            filename_prom = '{type_yml}_products-{id}.xml'.format(type_yml='prom',
                                                                  id=ret.get('user'))
            current_yml_prom = YmlHandler(filepath=filename_prom)
            if current_yml_prom.exist_product(id=ret.get('id')):
                print('yes')
                ret['marketplace']['prom'] = True
            else:
                print('no')
        except Exception as e:
            print(e)
        # ret['marketplace'] = 10
        try:
            category_id = ret.get('category').get('id', None)
        except Exception:
            category_id = None
            # user = Product.objects.get(id)

        if category_id:
            category = Category2.objects.get(id=category_id)
            ret['selectedCategories'] = category.get_ancestors(
                ascending=False, include_self=True
            ).values_list('id', flat=True)
        else:
            ret['selectedCategories'] = None

        try:
            category_id2 = ret.get('category_prom_x').get('id', None)
        except Exception:
            category_id2 = None
        if category_id2:
            category_prom_x = Category_prom_x.objects.get(id=category_id2)
            ret['selectedCategoriesProm'] = category_prom_x.get_ancestors(
                ascending=False, include_self=True
            ).values_list('id', flat=True)
        else:
            ret['selectedCategoriesProm'] = None
        print('______________________________________')
        try:
            # if CustomUser.objects.get(pk=ret['id']).fixed_recommended_price:
            if CustomUser.objects.get(pk=ret['contractor_product']['user']).fixed_recommended_price:
                print(1)
                ret['selling_price'] = ret['contractor_product']['recommended_price']
            else:
                print(2)
                if ret['contractor_product']:
                    # pass
                    ret['selling_price'] = round(float(ret['contractor_product']['price']) + (
                            float(ret['contractor_product']['price']) * ret['partner_percent'] / 100), 2)
        except Exception as e:
            print(e, '++++++++++')
            print(3)
            if ret['contractor_product']:
                ret['selling_price'] = ret['contractor_product']['recommended_price']
        return ret


class ProductContractorPercentSerializer(serializers.ModelSerializer):
    cover_images = ProductImageSerializer(many=True, source='product_images', required=False)
    image_urls = ProductImageURLSerializer(many=True, source='product_image_urls', required=False)
    product_percent = serializers.Field(source='user.percent_for_partners')
    price_status = PriceStatusSerializer(many=False, required=False)

    class Meta:
        model = Product
        fields = (
            'id',
            'category',
            'user',
            'category_prom_x',
            'name',
            'avatar_url',
            'vendor_code',
            'contractor_product',
            'brand',
            'count',
            'description',
            'price',
            'recommended_price',
            'cover_images',
            'image_urls',
            'product_percent',
            'created_type',
            'price_status',
        )


class ProductSerializerMixin:

    def set_avatar_url(self, product, avatar_url, cover_images_data):
        print('product', product)
        print('avatar_url', avatar_url)
        print('cover_images_data', cover_images_data)
        print('isinstance(avatar_url, ContentFile)', isinstance(avatar_url, ContentFile))
        # print('product' , product)
        if isinstance(avatar_url, ContentFile):
            for img in cover_images_data:
                if isinstance(img['image'], ContentFile) and avatar_url.read() == img['image'].read():
                    new_image = ProductImage.objects.create(product=product, image=img['image'])
                    img['image'] = None
                    product.avatar_url = new_image.image.url
                    return new_image.id
        else:
            product.avatar_url = avatar_url

    def validate(self, attrs):
        # if self.context['request'].user.role == 'CONTRACTOR' \
        #         and self.context['request'].user.available_products_count == 0:
        #     raise ValidationError([_('У вас закончились свободные места для товаров.')])

        return super().validate(attrs)

    def create(self, validated_data):
        cover_images_data = validated_data.pop('product_images', None)
        avatar_url = validated_data.pop('avatar_url', None)
        image_urls = validated_data.pop('product_image_urls', None)
        options = validated_data.pop('product_options', None)
        option_by_text_areas = validated_data.pop('product_by_textarea_options', None)
        manual_options = validated_data.pop('product_manual_options', None)
        with transaction.atomic():
            product = Product.objects.create(**validated_data, user=self.context['request'].user)
            self.set_avatar_url(product, avatar_url, cover_images_data)
            product.save(update_fields=('avatar_url',))
            if cover_images_data:
                for img in cover_images_data:
                    if img['image'] is not None:
                        try:
                            filepath = img['image']
                            media_abs_path_index = filepath.find('/media')
                            media_path = filepath[media_abs_path_index + len('/media'):]
                            new_product_image = ProductImage()
                            new_product_image.product = product
                            new_product_image.image.name = media_path
                            new_product_image.save()
                        except AttributeError as er:
                            new_product_image = ProductImage()
                            new_product_image.product = product
                            new_product_image.image = img['image']
                            new_product_image.save()
            if image_urls:
                ProductImageURL.objects.bulk_create([
                    ProductImageURL(product=product, **url_data)
                    for url_data in image_urls
                ])
            ProductOption.objects.filter(product=product).delete()
            if options:
                ProductOption.objects.bulk_create(
                    ProductOption(product=product, option_id=option['option']['id'])
                    for option in options
                )
            if option_by_text_areas:
                ProductOptionValueText.objects.bulk_create([
                    ProductOptionValueText(product=product, **option)
                    for option in option_by_text_areas
                ])
            if manual_options:
                ProductOptionManualInput.objects.bulk_create([
                    ProductOptionManualInput(product=product, **option)
                    for option in manual_options
                ])

            self.context['request'].user.available_products_count -= 1
            self.context['request'].user.save()
            return product

    def update(self, instance, validated_data):

        cover_images_data = validated_data.pop('product_images', None)
        avatar_url = validated_data.pop('avatar_url', None)
        image_urls = validated_data.pop('product_image_urls', None)
        options = validated_data.pop('product_options', None)
        option_by_text_areas = validated_data.pop('product_by_textarea_options', None)
        manual_options = validated_data.pop('product_manual_options', None)
        partner_percent = validated_data.pop('partner_percent', None)  # partnerPercent

        print('9999' * 50)
        print(partner_percent)
        with transaction.atomic():
            image_valid_url = set()
            image_valid_instance = set()
            for attr, value in validated_data.items():
                setattr(instance, attr, value)
            img_id = self.set_avatar_url(instance, avatar_url, cover_images_data)
            print('img_id is ', img_id)
            if img_id is not None:
                image_valid_instance.add(img_id)

            if cover_images_data:
                print('cover_images_data in mixins')
                for cover_data in cover_images_data:
                    image_data = cover_data.get('image', None)
                    if image_data is None:
                        continue
                    elif isinstance(image_data, ContentFile):
                        print('isinstanceisinstanceisinstanceisinstanceisinstanceisinstanceisinstanceisinstance')
                        new_image = ProductImage.objects.create(product=instance, image=image_data)
                        image_valid_instance.add(new_image.id)
                    else:
                        image_valid_url.add(image_data.split('?')[0])
                image_valid_url = [
                    remove_prefix(image_data, get_storage_host_name())
                    for image_data in image_valid_url
                ]
                ProductImage.objects.filter(
                    product=instance
                ).exclude(
                    id__in=image_valid_instance
                ).exclude(
                    image__in=image_valid_url
                )
            else:
                ProductImage.objects.filter(product=instance).delete()

            instance.product_image_urls.all().delete()
            if image_urls:
                ProductImageURL.objects.bulk_create([
                    ProductImageURL(product=instance, **image_url)
                    for image_url in image_urls
                ])

            ProductOption.objects.filter(product=instance).delete()
            if options:
                ProductOption.objects.bulk_create(
                    ProductOption(product=instance, option_id=option['option']['id'])
                    for option in options
                )
            instance.product_by_textarea_options.all().delete()
            if option_by_text_areas:
                ProductOptionValueText.objects.bulk_create([
                    ProductOptionValueText(product=instance, **option)
                    for option in option_by_text_areas
                ])
            instance.product_manual_options.all().delete()
            if manual_options:
                ProductOptionManualInput.objects.bulk_create([
                    ProductOptionManualInput(product=instance, **option)
                    for option in manual_options
                ])
            # cover_images_data_path = [get_file_name_without_domain_and_signature(img.get('image')) for img in
            #                           cover_images_data]
            #
            # image_urls_list_checker = [get_file_name_without_domain_and_signature(img.get('url')) for img in image_urls]

            # if (get_file_name_without_domain_and_signature(
            #         instance.avatar_url) not in cover_images_data_path + image_urls_list_checker) and (
            #         instance.avatar_url not in image_urls):
            #     instance.avatar_url = None
            instance.partner_percent = partner_percent
            instance.save()
            return instance


def decimal_validation(value):
    print('decimal_validation', value)


class ProductSerializer(ProductSerializerMixin, serializers.ModelSerializer):
    cover_images = ProductImageSerializer(many=True, source='product_images', required=False)
    image_urls = ProductImageURLSerializer(many=True, source='product_image_urls', required=False)
    options = ProductOptionsSerializer(many=True, source='product_options', required=False)
    option_by_text_areas = ProductOptionsByTextAreaEditableSerializer(
        many=True, source='product_by_textarea_options', required=False
    )
    manual_options = ProductOptionManualInputSerializer(
        many=True, source='product_manual_options', required=False
    )

    price = serializers.DecimalField(validators=[decimal_validation], max_digits=10, decimal_places=2)

    def validate(self, attrs, **kwargs):
        print('validate partial update ProductSerializer')
        print('kwargs ', kwargs)
        print('attrs ', attrs)
        # return serializers.ModelSerializer.validate(attrs , kwargs)
        # return serializers.ModelSerializer.validate(attrs , kwargs)
        return super().validate(attrs)

    # def validate_price(self , value):
    #

    class Meta:
        model = Product
        fields = (
            'id',
            'category',
            # 'user',
            'category_prom_x',
            'name',
            'avatar_url',
            'vendor_code',
            'contractor_product',
            'brand',
            'count',
            'description',
            'partner_percent',
            'price',
            'recommended_price',
            'weight',
            'volume',
            'cover_images',
            'image_urls',
            'options',
            'option_by_text_areas',
            'manual_options',
            'created_type',
            'clean_price'
        )


class ProductYmlSerializer(serializers.ModelSerializer):
    cover_images = ProductImageSerializer(many=True, source='product_images', required=False)
    image_urls = ProductImageURLSerializer(many=True, source='product_image_urls', required=False)
    options = ProductOptionsSerializer(many=True, source='product_options', required=False)
    option_by_text_areas = ProductOptionsByTextAreaEditableSerializer(
        many=True, source='product_by_textarea_options', required=False
    )
    manual_options = ProductOptionManualInputSerializer(
        many=True, source='product_manual_options', required=False
    )

    class Meta:
        model = Product
        fields = (
            'id',
            'category',
            # 'user',
            'category_prom_x',
            'name',
            'avatar_url',
            'vendor_code',
            'contractor_product_id',
            'brand',
            'count',
            'description',
            'price',
            'recommended_price',
            'weight',
            'volume',
            'cover_images',
            'image_urls',
            'options',
            'option_by_text_areas',
            'manual_options',
            'created_type'
        )


class ProductPartnerSerializer(ProductSerializerMixin, serializers.ModelSerializer):
    cover_images = ProductImageSerializer(many=True, source='product_images', required=False)
    image_urls = ProductImageURLSerializer(many=True, source='product_image_urls', required=False)
    options = ProductOptionsSerializer(many=True, source='product_options', required=False)
    option_by_text_areas = ProductOptionsByTextAreaEditableSerializer(
        many=True, source='product_by_textarea_options', required=False
    )
    manual_options = ProductOptionManualInputSerializer(
        many=True, source='product_manual_options', required=False
    )
    price_status = PriceStatusSerializer(many=False, required=False)

    # price = serializers.ReadOnlyField(source='partner_price')

    class Meta:
        model = Product
        fields = (
            'id',
            'category',
            'user',
            'category_prom_x',
            'name',
            'avatar_url',
            'vendor_code',
            'contractor_product',
            'brand',
            'count',
            'description',
            'contractor_price_for_partner',
            'recommended_price',
            'partner_percent',
            'cover_images',
            'image_urls',
            'options',
            'option_by_text_areas',
            'manual_options',
            'is_added_to_yml_rozetka',
            'created_type',
            'price_status',
            'price'
        )


from django.core.exceptions import ObjectDoesNotExist


class YMLHandlerSerializer(serializers.ModelSerializer):
    product_ids = serializers.ListField(child=serializers.IntegerField(), required=True, write_only=True)

    class Meta:
        model = YMLTemplate
        fields = (
            'template',
            'yml_type',
            'product_ids',
        )
        read_only_fields = ('template',)

    def validate_product_ids(self, val):
        existing_product_ids = set(Product.objects.filter(pk__in=val).values_list('pk', flat=True))
        non_existing_product_ids = set(val) - existing_product_ids
        if non_existing_product_ids:
            raise ValidationError(
                _('Продукты с этими id не существуют: ') + ', '.join([str(item) for item in non_existing_product_ids]))
        return val

    def create(self, validated_data):
        print('Create method!!!!!!!!! test', )
        product_ids = validated_data.pop('product_ids')
        products_to_add = Product.objects.filter(id__in=product_ids)
        type_yml  = validated_data['yml_type']
        for id_ in product_ids:
            product = Product.objects.get(id=id_)
            price_info ,  created = YmlPriceInfo.objects.update_or_create(
                user = validated_data['user'],
                product_id = id_,
                type_yml = type_yml,
                defaults = {
                    'percent': product.partner_percent or 0
                }
            )
        categories = products_to_add.values('category__pk', 'category__name')
        filename = '{type_yml}_products-{id}.xml'.format(type_yml=validated_data['yml_type'],
                                                         id=validated_data['user'].id)
        try:
            yml_template = YMLTemplate.objects.get(
                yml_type=validated_data['yml_type'],
                user=validated_data['user']
            )
            print('yml_template ', yml_template)
            current_yml = YmlHandler(filepath=filename)
            list_new_product = []
            new_str = ''
            for id_ in product_ids:
                if current_yml.exist_product(id=id_):
                    print('exists!!!!!!!')
                else:
                    product = Product.objects.get(id=id_)
                    product_str = current_yml.create_offers_str(product)
                    new_str += product_str
            print('write_new_product_to_file')
            current_yml.write_new_product_to_file(new_str, categories)
            return yml_template
        except ObjectDoesNotExist as error:
            print('ObjectDoesNotExist ObjectDoesNotExist')
            yml_template = YMLTemplate(
                yml_type=validated_data['yml_type'],
                user=validated_data['user'],
            )
            yml_template.save()
            try:
                company = Company.objects.get(user_id=validated_data['user'].id)
            except Exception as e:
                pass
            base_url = 'https://buysell.com.ua'
            categoriest_product = [Category2.objects.get(id=cat_id[0]) for cat_id in set(list(Product.objects.
                filter(id__in=product_ids,
                       category__isnull=False)
                .values_list(
                'category__pk')))]
            data = {
                'categories': categoriest_product,
                'products': Product.objects.filter(id__in=product_ids),
                'current_datetime': datetime.datetime.now().strftime('%Y-%m-%d %H:%M'),
                'company': 'company',
                'base_url': base_url,
                'domain': get_host_name()
            }
            new_yml_str = render_to_response('{type_yml}.xml'.format(type_yml=validated_data['yml_type']),
                                             data).content.decode('utf-8')
            yml_template.template.save(filename, ContentFile(new_yml_str))
            yml_template.save()
            return yml_template
        except FileNotFoundError as e:
            print('error in create yml FileNotFoundError', type(e), e)
            print('ObjectDoesNotExist ObjectDoesNotExist')
            yml_template = YMLTemplate.objects.get(
                yml_type=validated_data['yml_type'],
                user=validated_data['user'],
            )
            # yml_template.save()
            try:
                company = Company.objects.get(user_id=validated_data['user'].id)
            except Exception as e:
                pass
            base_url = 'https://buysell.com.ua'
            categoriest_product = [Category2.objects.get(id=cat_id[0]) for cat_id in set(list(Product.objects.
                filter(id__in=product_ids,
                       category__isnull=False)
                .values_list(
                'category__pk')))]
            data = {
                'categories': categoriest_product,
                'products': Product.objects.filter(id__in=product_ids),
                'current_datetime': datetime.datetime.now().strftime('%Y-%m-%d %H:%M'),
                'company': 'company',
                'base_url': base_url,
                'domain': get_host_name()
            }
            new_yml_str = render_to_response('{type_yml}.xml'.format(type_yml=validated_data['yml_type']),
                                             data).content.decode('utf-8')
            yml_template.template.save(filename, ContentFile(new_yml_str))
            yml_template.save()
            return yml_template
            # return YMLTemplate.objects.get(
            #     yml_type=validated_data['yml_type'],
            #     user=validated_data['user']
            # )

    @staticmethod
    def render_yml_file(yml_template, company, products):
        category_dict = products.values('category__pk', 'category__name')
        # base_url = 'http://buysell.com.ua'
        invalid_product = []
        list_articles = []
        same_articles = []
        print('render_yml_filerender_yml_filerender_yml_filerender_yml_file')

        for product in products:
            if product.vendor_code in list_articles:
                same_articles.append(product)
            else:
                list_articles.append(product.vendor_code)
            if same_articles:
                raise ValidationError(
                    _("You are trying to add products with the same article: ") +
                    ', '.join([str(product) for product in same_articles]))
        for product in products:
            if not product.brand or not product.category:
                invalid_product.append(product)
        if invalid_product:
            raise ValidationError(
                _('Эти продукты невалидны (нет категории/бренда): ') +
                ', '.join([str(product) for product in invalid_product]))
        base_url = 'https://buysell.com.ua'
        if MyStore.objects.filter(user=company.user).exists():
            if company.user.mystore.get_url:
                base_url = company.user.mystore.get_url
        try:
            if yml_template.yml_type == YMLFileTypes.PROM:
                file_name = 'prom_products-{}.xml'
                context = {
                    'categories': category_dict.distinct('category__pk').order_by('category__pk'),
                    'products': products,
                    'current_datetime': datetime.datetime.now().strftime('%Y-%m-%d %H:%M'),
                    'company': company,
                    'base_url': base_url,
                    'domain': get_host_name(),
                }
                content = render_to_response('prom.xml', context).content
            else:
                context = {
                    'categories': category_dict.distinct('category__pk').order_by('category__pk'),
                    'products': products,
                    'current_datetime': datetime.datetime.now().strftime('%Y-%m-%d %H:%M'),
                    'company': company,
                    'base_url': base_url,
                    'domain': get_host_name(),
                }
                content = render_to_response('rozetka.xml', context).content
                file_name = 'rozetka_products-{}.xml'
            yml_template.template.delete(save=True)
            yml_template.template.save(file_name.format(company.user_id), ContentFile(content), save=True)
        except Exception as e:
            print(e)
            raise ValidationError
        return yml_template


class ProductUploadHistorySerializer(serializers.ModelSerializer):
    class Meta:
        model = ProductUploadHistory
        fields = (
            'xls_file',
            'created',
            'is_uploaded',
            'errors',
            'file_type',
            'total_products_count',
            'imported_products_count',
        )
        read_only_fields = (
            'created',
            'is_uploaded',
            'errors',
        )

    def validate_xls_file(self, val):
        if val.content_type not in (
                'application/vnd.ms-excel',
                'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                'application/octet-stream',
                'text/xml',
        ):
            raise ValidationError(_('Unsupported Media Type'))
        return val


class ProductChangeBrandSerializer(serializers.Serializer):
    product_ids = serializers.ListField(required=True)
    brand = serializers.CharField(required=True)

    class Meta:
        fields = (
            'product_ids',
            'brand',
        )


class OptionValueSerializer(serializers.ModelSerializer):
    class Meta:
        model = OptionValue
        fields = (
            'name',
        )


class CategoryOptionGroupValueSerializer(serializers.ModelSerializer):
    value = serializers.ReadOnlyField(source='value.name')  # OptionValueSerializer(many=False)

    class Meta:
        model = CategoryOptionGroupValue
        fields = (
            'id',
            'value',
        )


class CategoryPromOptionGroupValueSerializer(serializers.ModelSerializer):
    value = serializers.ReadOnlyField(source='value.name')  # OptionValueSerializer(many=False)

    class Meta:
        model = CategoryPromOptionGroupValue
        fields = (
            'id',
            'value',
        )


class OptionGroupSerializer(serializers.ModelSerializer):
    class Meta:
        model = OptionGroup
        fields = (
            'id',
            'name',
        )


class CategoryOptionGroupSerializer(serializers.ModelSerializer):
    values = CategoryOptionGroupValueSerializer(many=True, source='optiongroupvalues', read_only=True)
    group = serializers.ReadOnlyField(source='group.name')  # OptionGroupSerializer(many=False)

    class Meta:
        model = CategoryOptionGroup
        fields = (
            'id',
            'group',
            'values',
        )


class CategoryPromOptionGroupSerializer(serializers.ModelSerializer):
    values = CategoryPromOptionGroupValueSerializer(many=True, source='optiongroupvalues', read_only=True)
    group = serializers.ReadOnlyField(source='group.name')  # OptionGroupSerializer(many=False)

    class Meta:
        model = CategoryPromOptionGroup
        fields = (
            'id',
            'group',
            'values',
        )


class RozetkaTokenSerializer(serializers.Serializer):
    token = serializers.CharField()


class CategoryOptionTextAreaSerializer(serializers.ModelSerializer):
    group = serializers.ReadOnlyField(source='group.name')

    class Meta:
        model = CategoryOptionGroup
        fields = (
            'id',
            'group',
        )


class CategoryPromOptionTextAreaSerializer(serializers.ModelSerializer):
    group = serializers.ReadOnlyField(source='group.name')

    class Meta:
        model = CategoryPromOptionGroup
        fields = (
            'id',
            'group',
        )


# ============== For MySite ================== #

class ProductOptionsToMySiteSerializer(serializers.ModelSerializer):
    group = serializers.ReadOnlyField(source='option.category_group.group.name')
    value = serializers.ReadOnlyField(source='option.value.name')

    class Meta:
        model = ProductOption
        fields = (
            'group',
            'value',
        )


class ProductOptionsByTextAreaToMySiteSerializer(serializers.ModelSerializer):
    group = serializers.ReadOnlyField(source='group.group.name')

    class Meta:
        model = ProductOptionValueText
        fields = (
            'group',
            'value',
        )


class ProductToMySiteSerializer(serializers.ModelSerializer):
    cover_images = ProductImageSerializer(many=True, source='product_images', required=False)
    image_urls = ProductImageURLSerializer(many=True, source='product_image_urls', required=False)
    options = ProductOptionsToMySiteSerializer(many=True, source='product_options', required=False)
    option_by_text_areas = ProductOptionsByTextAreaToMySiteSerializer(
        many=True, source='product_by_textarea_options', required=False
    )
    manual_options = ProductOptionManualInputSerializer(
        many=True, source='product_manual_options', required=False
    )

    class Meta:
        model = Product
        fields = (
            'id',
            'category',
            'name',
            'avatar_url',
            'vendor_code',
            'created_type',
            # 'contractor_product',
            'brand',
            'product_type',
            'count',
            'description',
            'price',
            # 'recommended_price',
            'cover_images',
            'image_urls',
            'options',
            'option_by_text_areas',
            'manual_options',
        )

    def to_representation(self, instance):
        ret = super().to_representation(instance)
        ret['cover_images'] = [item['image_decoded'] for item in ret['cover_images']]
        ret['image_urls'] = [item['url'] for item in ret['image_urls']]
        ret['options'] = [dict(item) for item in ret['options']]
        ret['option_by_text_areas'] = [dict(item) for item in ret['option_by_text_areas']]
        ret['manual_options'] = [dict(item) for item in ret['manual_options']]
        return dict(ret)


class DeleteImageSerializer(serializers.Serializer):
    image_urls = serializers.ListField()
    image_cover = serializers.ListField()

    class Meta:
        fields = (
            'image_urls',
            'image_cover'
        )