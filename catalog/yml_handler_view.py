import itertools
import os
from xml import etree
from xml.etree import ElementTree

from django.conf import settings
import xml.etree.ElementTree as ET
from pprint import pprint

from django.core.files.base import ContentFile
from django.shortcuts import render_to_response
from sendgrid import Category

from catalog.models import Category2, ProductOption, ProductOptionValueText, \
    ProductOptionManualInput, Product, YmlPriceInfo
from catalog.utils import write_to_errors_file, get_host_name


class YmlHandler:
    OPEN_TAG_OFFERS = '<offers>'
    CLOSE_TAG_OFFERS = '</offers>'
    OPEN_TAG_CATEGORIES = '<categories>'
    CLOSE_TAG_CATEGORIES = '</categories>'
    OPEN_TAG_PARAM = '<param>'
    CLOSE_TAG_PARAM = '</param>'
    def __init__(self, filepath):
        self.abs_path = os.path.join(settings.MEDIA_ROOT, 'yml_templates', filepath)

    def get_root(self):
        tree = ET.parse(self.abs_path)
        root = tree.getroot()
        return root

    def get_tree(self):
        return ET.parse(self.abs_path)

    def exist_product(self, id):
        try:
            root_element = self.get_root()
            target_elem = root_element.find("./shop/offers/offer[@id='{id}']".format(id=id))
            return bool(target_elem)
        except Exception as e:
            return False

    def insert_data_to_file(self, data, id, user):
        if 'rozetka' in self.abs_path:
            return self.render_rozetka_item(data, id, user)
        elif 'prom' in self.abs_path:
            return self.render_prom_item(data, id)
        else:
            raise ValueError('invalid platform')

    def render_rozetka_item(self, data, id, user):
        product = data
        offer = self.get_current_offer()
        product_id = offer.attrib['id']
        new_offer_obj = {}
        all_params = offer.findall('./param')
        all_pictures = offer.findall('./picture')

        # new_offer_obj['coverImages'] = [{"imageDecoded": picture.text} for picture in all_pictures
        #                                 if '/media/catalog/products/' in picture.text]
        # new_offer_obj['imageUrls'] = [{"url": picture.text} for picture in all_pictures
        #                               if '/media/catalog/products/' not in picture.text]

        if data.get('product_images'):
            new_offer_obj['coverImages'] = [{"imageDecoded": picture.get('image')} for picture in data['product_images']
                                            if '/media/catalog/products/' in picture.get('image')]

        if data.get('product_image_urls'):
            new_offer_obj['imageUrls'] = [{"url": picture.get('url')} for picture in data['product_image_urls']
                                          if '/media/catalog/products/' not in picture.get('url')]

        all_group_name = [param_group_name.attrib['name']
                          for param_group_name in all_params]

        all_group_value = [param_group_name.text
                           for param_group_name in all_params]

        try:
            current_product_option = ProductOption.objects.filter(product_id=product_id,
                                                                  option__value__name__in=all_group_value)

            product_by_textarea_options = ProductOptionValueText.objects.filter(product_id=product_id,
                                                                                group__group__name__in=all_group_name,
                                                                                value__in=all_group_value)

            product_manual_options = ProductOptionManualInput.objects.filter(product_id=product_id,
                                                                             group__in=all_group_name,
                                                                             value__in=all_group_value)
            new_offer_obj['manualOptions'] = [prod.to_json() for prod in product_manual_options]
            new_offer_obj['optionByTextAreas'] = [prod.to_json() for prod in product_by_textarea_options]
        except Exception as e:
            print(e, type(e))
        for sub_elem in self.get_current_offer():
            if sub_elem.tag == 'categoryId':
                new_offer_obj['category'] = sub_elem.text
                try:
                    new_offer_obj['selectedCategories'] = [elem.id
                                                           for elem in Category2.objects.get(id=int(sub_elem.text))
                                                               .get_descendants(include_self=True)
                                                           ]
                except Exception as e:
                    print('selectedCategories error', e)

            elif sub_elem.tag == 'price':
                product_of_change = Product.objects.get(id=product_id)
                contractor_price = product_of_change.contractor_product.price * (
                        1 + product_of_change.contractor_product.user.markup / 100)
                price_info = YmlPriceInfo.objects.get(user=user, product_id=product_id, type_yml='rozetka')
                new_offer_obj['price'] = round(float(contractor_price) * (1 + price_info.percent / 100), 2)
            elif sub_elem.tag == 'name':
                new_offer_obj['name'] = sub_elem.text
            elif sub_elem.tag == 'vendor':
                new_offer_obj['brand'] = sub_elem.text
            elif sub_elem.tag == 'stock_quantity':
                new_offer_obj['count'] = sub_elem.text
            elif sub_elem.tag == 'description':
                new_offer_obj['description'] = sub_elem.text
        for key in new_offer_obj:
            if key in product:
                try:
                    if key == 'category':
                        new_offer_obj[key] = product[key].id
                    elif key != 'price':
                        new_offer_obj[key] = product[key]
                except Exception as e:
                    print(type(e), e, 'keyerorro!!!!')
        # end updated fields simple
        new_offer_obj['id'] = id
        context = {
            'product': new_offer_obj
        }

        new_str = render_to_response('offer_rozetka.xml', context).content
        return new_str

    def render_prom_item(self, data, id):
        product = data
        offer = self.get_current_offer()
        product_id = offer.attrib['id']
        new_offer_obj = {}
        all_params = offer.findall('./param')
        all_pictures = offer.findall('./picture')
        new_offer_obj['coverImages'] = [{"imageDecoded": picture.text} for picture in all_pictures
                                        if '/media/catalog/products/' in picture.text]
        new_offer_obj['imageUrls'] = [{"url": picture.text} for picture in all_pictures
                                      if '/media/catalog/products/' not in picture.text]

        all_group_name = [param_group_name.attrib['name']
                          for param_group_name in all_params]

        all_group_value = [param_group_name.text
                           for param_group_name in all_params]

        try:
            current_product_option = ProductOption.objects.filter(product_id=product_id,
                                                                  option__value__name__in=all_group_value)

            product_by_textarea_options = ProductOptionValueText.objects.filter(product_id=product_id,
                                                                                group__group__name__in=all_group_name,
                                                                                value__in=all_group_value)

            product_manual_options = ProductOptionManualInput.objects.filter(product_id=product_id,
                                                                             group__in=all_group_name,
                                                                             value__in=all_group_value)
            new_offer_obj['manualOptions'] = [prod.to_json() for prod in product_manual_options]
            new_offer_obj['optionByTextAreas'] = [prod.to_json() for prod in product_by_textarea_options]
        except Exception as e:
            print(e, type(e))
        for sub_elem in offer:
            if sub_elem.tag == 'categoryId':
                new_offer_obj['category'] = sub_elem.text
                new_offer_obj['selectedCategories'] = [elem.id for elem in
                                                       Category2.objects.get(id=int(sub_elem.text)).get_descendants(
                                                           include_self=True)]
            elif sub_elem.tag == 'price':
                new_offer_obj['price'] = sub_elem.text
                print()
            elif sub_elem.tag == 'name':
                new_offer_obj['name'] = sub_elem.text
            elif sub_elem.tag == 'vendor':
                new_offer_obj['brand'] = sub_elem.text
            elif sub_elem.tag == 'stock_quantity':
                new_offer_obj['count'] = sub_elem.text
            elif sub_elem.tag == 'description':
                new_offer_obj['description'] = sub_elem.text

        for key in new_offer_obj:
            if key in product:
                # print('-' * 10)
                # print(product[key])
                # print('-' * 10)
                new_offer_obj[key] = product[key]
        new_offer_obj['id'] = id
        # print('current_offer ', new_offer_obj)

        context = {
            'product': new_offer_obj
        }
        new_str = render_to_response('offer_prom.xml', context).content
        # root_element = self.get_root()
        # offers = root_element.findall('./shop/offers/')
        # with open(self.abs_path, 'r') as f:
        #     str_from_file = f.read()
        #     from_index = str_from_file.find(self.OPEN_TAG_OFFERS) + len(self.OPEN_TAG_OFFERS)
        #     to_index = str_from_file.rfind(self.CLOSE_TAG_OFFERS) - len(self.CLOSE_TAG_OFFERS)  # find last index
        # result = ''
        # for off in offers:
        #     result += ET.tostring(off).decode('utf-8')
        # result = result + new_str.decode('utf-8')
        # new_str1 = str_from_file[:from_index] + result + str_from_file[to_index:]
        #
        # with open(self.abs_path, 'w') as f:
        #     f.write(new_str1)
        return new_str

    def create_offers_str(self, product):
        context = {
            'product': product,
            'host': get_host_name()
        }
        new_str = ''
        if 'rozetka' in self.abs_path:
            new_str = render_to_response('offer_add_rozetka.xml', context).content.decode('utf-8')
        elif 'prom' in self.abs_path:
            new_str = render_to_response('offer_prom.xml', context).content.decode('utf-8')
        return new_str

    def write_new_product_to_file(self, new_str_offers, categories):
        root_element = self.get_root()
        offers = root_element.findall('./shop/offers/')
        with open(self.abs_path, 'r') as f:
            str_from_file = f.read()
            from_index = str_from_file.find(self.OPEN_TAG_OFFERS) + len(self.OPEN_TAG_OFFERS)
            to_index = str_from_file.rfind(self.CLOSE_TAG_OFFERS) - len(self.CLOSE_TAG_OFFERS)  # find last index
        result = ''
        for off in offers:
            result += ET.tostring(off).decode('utf-8')
        result = result + new_str_offers
        new_str1 = str_from_file[:from_index] + result + str_from_file[to_index:]

        with open(self.abs_path, 'w') as f:
            f.write(new_str1)

        with open(self.abs_path, 'r') as f:
            str_from_file = f.read()
            from_index_cat = str_from_file.find(self.OPEN_TAG_CATEGORIES) + len(self.OPEN_TAG_CATEGORIES)
            to_index_cat = str_from_file.rfind(self.CLOSE_TAG_CATEGORIES) - len(
                self.CLOSE_TAG_CATEGORIES)  # find last index

        new_categories_str = ''
        template_category = """
        <category id="{id}">{name}</category>
        """
        all_categories = root_element.findall('./shop/categories/')
        category_id_list = [{'id': str(cat.attrib['id']), 'name': cat.text} for cat in all_categories]
        categories_1 = [{'id': str(cat.get('category__pk')), 'name': cat.get('category__name')}
                        for cat in categories] + category_id_list
        unique_categories = list({v['id']: v for v in categories_1}.values())

        print('unique_categories!!!!! ', unique_categories)
        #
        # print('category_id_list' , category_id_list)
        for cat in unique_categories:
            if cat.get('id') != 'None':
                new_categories_str += template_category.format(id=cat['id'], name=cat['name'])
            else:
                continue
        new_str1 = str_from_file[:from_index_cat] + new_categories_str + str_from_file[to_index_cat:]
        with open(self.abs_path, 'w') as f:
            f.write(new_str1)
        print('new_categories_str ', new_categories_str)

    def update_options(self, data, id, user):
        root_element = self.get_root()

        offers = root_element.findall('./shop/offers/')

        target_elem = root_element.find("./shop/offers/offer[@id='{id}']".format(id=id))
        print(target_elem)

        #######################################################
        root_element = self.get_root()
        target_elem = root_element.find("./shop/offers/offer[@id='{id}']".format(id=id))
        self.set_current_offer(offer=target_elem)
        if target_elem is not None:
            offers_dict = {}
            # for child in target_elem:
            #     target_elem_dict[child.tag] = child.text
            params = target_elem.findall('param')
            # print(params)
            for param in params:
                value = param.text
                group = param.attrib['name']

                offers_dict[group] = value
            print(offers_dict)

            for opt in data:
                # print(opt)
                offers_dict[opt['group']] = opt['value']

            # print(type(target_elem))

            #######################################################
            for child in list(target_elem):
                # print(child)
                if child.tag == "param":  ### need
                    target_elem.remove(child)
            #######################################################

            i = len(target_elem)
            for key, value in offers_dict.items():
                tag = ElementTree.Element("param")
                tag.text = value
                # tag.attrib = key.encode('utf-8')
                tag.attrib['name'] = key
                target_elem.insert(i, tag)
                i = i+1

            with open(self.abs_path, 'r') as f:
                str_from_file = f.read()
                from_index = str_from_file.find(self.OPEN_TAG_OFFERS) + len(self.OPEN_TAG_OFFERS)
                to_index = str_from_file.rfind(self.CLOSE_TAG_OFFERS) - len(self.CLOSE_TAG_OFFERS)
            result = ''
            for off in offers:
                if int(off.attrib['id']) == id:
                    # result += self.insert_data_to_file(data, id, user).decode('utf-8')
                    result += ET.tostring(target_elem).decode('utf-8')
                    pass
                else:
                    result += ET.tostring(off).decode('utf-8')
            new_str = str_from_file[:from_index] + result + str_from_file[to_index:]
            with open(self.abs_path, 'w') as f:
                f.write(new_str)
        else:
            raise ValueError('element not found')

    def update_categories(self):
        root_element = self.get_root()
        offers = root_element.findall('./shop/offers/')
        with open(self.abs_path, 'r') as f:
            str_from_file = f.read()
            from_index_cat = str_from_file.find(self.OPEN_TAG_CATEGORIES) + len(self.OPEN_TAG_CATEGORIES)
            to_index_cat = str_from_file.rfind(self.CLOSE_TAG_CATEGORIES) - len(
                self.CLOSE_TAG_CATEGORIES)  # find last index
        all_categories = root_element.findall('./shop/categories/')
        x = all_categories if all_categories else None
        categories_ids = []

        print('categories_ids')
        new_categories_str = ''
        template_category = """
                       <category id="{id}">{name}</category>
                       """
        try:
            for offer in offers:
                categoryId = int(offer.find('./categoryId').text)
                categories_ids.append(categoryId)

                # if categoryId not in categories_ids:
                #     new_categories_str += template_category.format(id=categoryId,
                #                                                    name=Category2.objects.get(id=categoryId).name)

            unique_categories = list(set(categories_ids))
            for cat in unique_categories:
                new_categories_str += template_category.format(id=cat,
                                                               name=Category2.objects.get(id=cat).name)
            new_str1 = str_from_file[:from_index_cat] + new_categories_str + str_from_file[to_index_cat:]
            with open(self.abs_path, 'w') as f:
                f.write(new_str1)
        except Exception as e:
            print(type(e), e)

    def set_current_offer(self, offer):
        self.__current_offer = offer

    def get_current_offer(self):
        return self.__current_offer

    def update_element_by_id(self, id, data, user):
        root_element = self.get_root()
        target_elem = root_element.find("./shop/offers/offer[@id='{id}']".format(id=id))
        self.set_current_offer(offer=target_elem)
        if target_elem is not None:
            target_elem_dict = {}
            for child in target_elem:
                target_elem_dict[child.tag] = child.text
            offers = root_element.findall('./shop/offers/')
            with open(self.abs_path, 'r') as f:
                str_from_file = f.read()
                from_index = str_from_file.find(self.OPEN_TAG_OFFERS) + len(self.OPEN_TAG_OFFERS)
                to_index = str_from_file.rfind(self.CLOSE_TAG_OFFERS) - len(self.CLOSE_TAG_OFFERS)
            result = ''
            for off in offers:
                if int(off.attrib['id']) == id:
                    result += self.insert_data_to_file(data, id, user).decode('utf-8')
                else:
                    result += ET.tostring(off).decode('utf-8')
            new_str = str_from_file[:from_index] + result + str_from_file[to_index:]
            with open(self.abs_path, 'w') as f:
                f.write(new_str)
        else:
            raise ValueError('element not found')

    def delete_product(self, offer_ids):
        root_element = self.get_root()
        result = ''
        offers = root_element.findall('./shop/offers/')
        with open(self.abs_path, 'r') as f:
            str_from_file = f.read()
            from_index = str_from_file.find(self.OPEN_TAG_OFFERS) + len(self.OPEN_TAG_OFFERS)
            to_index = str_from_file.rfind(self.CLOSE_TAG_OFFERS) - len(self.CLOSE_TAG_OFFERS)
        for off in offers:
            if off.attrib['id'] not in offer_ids:
                result += ET.tostring(off).decode('utf-8')
        new_str = str_from_file[:from_index] + result + str_from_file[to_index:]
        with open(self.abs_path, 'w') as f:
            f.write(new_str)
        # check unique categories
        self.update_categories()

    def convert_from_file_to_response(self, user, from_index, to_index):
        root = self.get_root()
        print('convert_from_file_to_response')
        print('!!!!' * 15)
        all_offers_origin = root.findall('./shop/offers/')
        all_offers = all_offers_origin[from_index: to_index]
        list_offers = []
        for offer in all_offers:
            new_offer_obj = {}
            product_id = offer.attrib['id']
            try:
                current_product = Product.objects.get(id=int(product_id))
            except Exception as e:
                write_to_errors_file(error=str(e) + 'yml handler view in 270 line')
                print('Errro !!!!', e)
                continue
            new_offer_obj['id'] = product_id
            all_params = offer.findall('./param')
            all_pictures = offer.findall('./picture')
            if all_pictures:
                new_offer_obj['coverImages'] = [{"imageDecoded": picture.text} for picture in all_pictures
                                                if '/media/catalog/products/' in picture.text]
                new_offer_obj['imageUrls'] = [{"url": picture.text} for picture in all_pictures
                                              if '/media/catalog/products/' not in picture.text]
            try:
                all_group_name = [param_group_name.attrib['name']
                                  for param_group_name in all_params]

                all_group_value = [param_group_name.text
                                   for param_group_name in all_params]
                current_product_option = ProductOption.objects.filter(product_id=product_id,
                                                                      option__value__name__in=all_group_value)

                product_by_textarea_options = ProductOptionValueText.objects.filter(product_id=product_id,
                                                                                    group__group__name__in=all_group_name,
                                                                                    value__in=all_group_value)

                product_manual_options = ProductOptionManualInput.objects.filter(product_id=product_id,
                                                                                 group__in=all_group_name,
                                                                                 value__in=all_group_value)
                new_offer_obj['manualOptions'] = [prod.to_json() for prod in product_manual_options]
                new_offer_obj['optionByTextAreas'] = [prod.to_json() for prod in product_by_textarea_options]
            except Exception as e:
                write_to_errors_file(error=str(e) + 'yml handler view in 299 line')
                print(e, type(e))
            for sub_elem in offer:
                if sub_elem.tag == 'categoryId':
                    try:
                        new_offer_obj['category'] = {
                            'id': sub_elem.text,
                            'name': Category2.objects.get(id=int(sub_elem.text)).name
                        }
                        all_categories = Category2.objects.get(id=int(sub_elem.text)).get_descendants(include_self=True)
                    except Exception as e:
                        print('error category does not exists', e)
                        write_to_errors_file(error=str(e) + 'yml handler view in 310 line')
                        product_category = current_product.category
                        new_offer_obj['category'] = {
                            'id': product_category.id,
                            'name': Category2.objects.get(id=int(product_category.id)).name
                        }
                        all_categories = Category2.objects.get(id=int(product_category.id)).get_descendants(
                            include_self=True)
                    new_offer_obj['selectedCategories'] = [sub_category.id for sub_category in all_categories]
                elif sub_elem.tag == 'price':
                    try:
                        new_offer_obj['sellingPrice'] = sub_elem.text.replace(',', '.')
                    except Exception as e:
                        new_offer_obj['sellingPrice'] = current_product.price_percent
                        print(type(e), e, 'erorr' * 10)
                    try:
                        contractor_price = current_product.contractor_product.price * (
                                1 + current_product.contractor_product.user.markup / 100)
                        new_offer_obj['contractorProduct'] = {
                            'price': contractor_price
                            # 'price': str(
                            #     round(float(current_price) / (1 + current_product.partner_percent / 100), 2)).replace(
                            #     ',', '.')
                        }
                    except Exception as e:
                        print(type(e), e, 'Error in convert response YML (price)')
                        new_offer_obj['contractorProduct'] = {
                            'price': sub_elem.text
                        }
                elif sub_elem.tag == 'name':
                    new_offer_obj['name'] = sub_elem.text
                elif sub_elem.tag == 'vendor':
                    new_offer_obj['brand'] = sub_elem.text
                elif sub_elem.tag == 'stock_quantity':
                    new_offer_obj['count'] = sub_elem.text
                elif sub_elem.tag == 'description':
                    new_offer_obj['description'] = sub_elem.text
                new_offer_obj['vendorCode'] = current_product.vendor_code
                new_offer_obj['createdType'] = current_product.created_type
                new_offer_obj['user'] = current_product.user.id
                new_offer_obj['recommendedPrice'] = current_product.recommended_price
                type_yml = None
                if 'rozetka' in self.abs_path:
                    type_yml = 'rozetka'
                elif 'prom' in self.abs_path:
                    type_yml = 'prom'
                try:
                    new_offer_obj['partnerPercent'] = YmlPriceInfo.objects.get(user=user, product_id=product_id,
                                                                               type_yml=type_yml).percent
                except Exception as e:
                    print(type(e), e)
                    new_offer_obj['partnerPercent'] = current_product.partner_percent
                new_offer_obj['fixedRecommendedPrice'] = current_product.fixed_recommended_price
                try:
                    # product_of_change = Product.objects.get(id=product_id)
                    contractor_price = current_product.contractor_product.price * (
                            1 + current_product.contractor_product.user.markup / 100)
                    price_info, created = YmlPriceInfo.objects.get_or_create(user=user, product_id=product_id,
                                                                             type_yml='rozetka',
                                                                             defaults={
                                                                                 'percent': current_product.partner_percent
                                                                             })
                    new_offer_obj['contractorPriceForPartner'] = round(
                        float(contractor_price) * (1 + price_info.percent / 100), 2)
                except Exception as e:
                    print(e, 'Error in convert_from_file_to_response')
                    write_to_errors_file(error=str(e) + 'yml handler view in 342 line')
            list_offers.append(new_offer_obj)
        return {
            'len': len(all_offers_origin),
            'results': list_offers[::-1]
        }

    def update_price_info(self):
        root = self.get_root()
        offers = root.findall('./shop/offers/')
        name_of_file = self.abs_path[self.abs_path.rfind('/') + 1:]
        type_yml = name_of_file.split('_')[0]
        user_id = name_of_file.replace('.xml', '').split('-')[-1]
        for offer in offers:
            product_id = offer.attrib['id']
            try:
                product = Product.objects.get(id=product_id)
                print(product.partner_percent)
            except Exception as e:
                print(type(e), e)
        return

    def __str__(self):
        return 'Yml file {}'.format(self.abs_path)
