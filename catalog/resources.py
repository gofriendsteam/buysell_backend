import tablib
from import_export.fields import Field
from import_export.widgets import ManyToManyWidget

from catalog.models import Product, Category2, ProductImageURL
from import_export import resources, fields, widgets


class ProductResource(resources.ModelResource):
    category_id = fields.Field(
        column_name='category_id',
        attribute='category',
        widget=widgets.ForeignKeyWidget(Category2, )
    )
    # data = None
    class Meta:
        model = Product
        fields = (
            'id',
            'name',
            'category_id',
            'vendor_code',
            'brand',
            'variety_type',
            'count',
            'description',
            'price',
            'recommended_price',
            'extra_description',
            'age_group',
            'material',
            'avatar_url',
            'product_image_urls',
        )

    def import_data(self, **kwargs):

        self.data = kwargs.get('dataset').dict
        return super(ProductResource, self).import_data(**kwargs)


    def after_save_instance(self, instance, using_transactions, dry_run):
        vendor_code = str(instance.vendor_code)
        for urls in self.data:
            # print(urls.get('product_image_urls'))
            id = urls.get('id')
            # print(id)
            url = urls.get('product_image_urls')
            url = url.split(', ')
            for item in url:
                # print(item)
                ProductImageURL.objects.update_or_create(product_id=id, url=item)
                # c = ProductImageURL(product_id=id, url=item)
                # c.save()


        Product.objects.filter(id=instance.id).update(vendor_code=vendor_code.split('.0', 1)[0])
        super().after_save_instance(instance, using_transactions, dry_run)

    def after_import_instance(self, instance, new, **kwargs):
        instance.user_id = kwargs.get('user_id')
        super().after_import_instance(instance, new, **kwargs)
