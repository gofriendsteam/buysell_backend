class YMLFileTypes:
    ROZETKA = 'rozetka'
    PROM = 'prom'

    YML_TYPES = (
        (ROZETKA, 'Rozetka'),
        (PROM, 'Prom'),
    )


class ProductUploadFileTypes:
    INNER = 'inner'
    ROZETKA = 'rozetka'
    YML = 'yml'
    YML_PROM = 'promYml'
    PROM = 'prom'

    PRODUCT_UPLOAD_FILE_TYPES = (
        (INNER, 'Inner'),
        (ROZETKA, 'Rozetka'),
        (YML, 'YML'),
        (YML_PROM, 'promYml'),
        (PROM, 'prom')
    )


class ProductOptionTypes:
    CHOICE = 1
    TEXT_AREA = 2

    TYPES = (
        (CHOICE, 'Выбор из нескольки (select)'),
        (TEXT_AREA, 'Ввод значение вручную'),
    )


class StatusPriceNumbers:
    UP = 1
    DOWN = -1
    NOT_CHANGES = 0

    REVIEW_TYPES = (
        (UP, 'Поднялась'),
        (DOWN, 'Опустилась'),
        (NOT_CHANGES, 'Без изменений')
    )