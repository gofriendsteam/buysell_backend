# -*- coding: utf-8 -*-
import base64
import binascii
import datetime
import os
import uuid
from copy import deepcopy
from decimal import Decimal
from django.conf import settings
from django.core.files.storage import default_storage
from django.db.models import Prefetch
from django.shortcuts import render_to_response
from django.views import View
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import viewsets, permissions, status, generics
from django.db import transaction
from django.db import IntegrityError
from rest_framework.exceptions import ValidationError
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.reverse import reverse_lazy
from rest_framework.views import APIView

from catalog.constants import ProductOptionTypes, YMLFileTypes
from catalog.tasks import upload_category_options, update_products_with_categories
from catalog.utils import group_vals, get_file_name_without_domain_and_signature, get_host_name
from catalog.serializers import ProductSerializer, YMLHandlerSerializer, \
    ProductUploadHistorySerializer, ProductListIdSerializer, CategoryListSerializer, ProductCategoryObjectSerializer, \
    ProductChangeBrandSerializer, CategoryOptionGroupSerializer, RozetkaTokenSerializer, \
    CategoryOptionTextAreaSerializer, ProductCategoryObjectPartnerSerializer, ProductPartnerSerializer, \
    CategoryPromSerializer, CategoryPromListSerializer, CategoryPromOptionGroupSerializer, \
    CategoryPromOptionTextAreaSerializer, Category2Serializer, ProductYmlSerializer, ProductsCountSerializer, \
    DeleteImageSerializer
from catalog.models import Product, YMLTemplate, ProductUploadHistory, ProductImageURL, ProductImage, \
    CategoryOptionGroup, Category_prom_x, CategoryPromOptionGroup, Category2, PriceStatus, ProductOptionManualInput, \
    YmlPriceInfo
from users.permissions import IsPartner, IsContractor
from rest_framework.decorators import action
from django_filters import rest_framework as filters
from catalog.filters import ProductFilter, FilterYMLProduct
from djangorestframework_camel_case.parser import CamelCaseJSONParser
from rest_framework.parsers import MultiPartParser
from rest_framework.generics import get_object_or_404, ListAPIView, UpdateAPIView, CreateAPIView
from rest_framework.response import Response
from rest_framework import parsers
from django.core.files.base import ContentFile
from users.models import Company, MyStore, CustomUser
from rest_framework.pagination import PageNumberPagination
from django.core.cache import cache

from django.contrib.auth import get_user_model
from .yml_handler_view import YmlHandler

# from ..orders.serializers import DeleteImageSerializer

User = get_user_model()


class ClientAccessPermission(permissions.BasePermission):
    message = 'Check if both Company and MyStore added to user.'

    def has_permission(self, request, view):
        return (request.user.is_authenticated
                and Company.objects.filter(user=request.user).exists()
                and MyStore.objects.filter(user=request.user).exists())


class CategoryViewSet(viewsets.ModelViewSet):
    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = Category2Serializer
    http_method_names = ['get', ]
    queryset = Category2.objects.root_nodes()
    pagination_class = None

    @action(detail=False, methods=['get'], serializer_class=CategoryListSerializer)
    def first_level(self, request, *args, **kwargs):
        queryset = Category2.objects.filter(
            level=0
        )
        serializer = self.serializer_class(queryset, many=True)
        return Response(
            status=status.HTTP_200_OK,
            data=serializer.data
        )

    @action(detail=True, methods=['get'], serializer_class=CategoryListSerializer)
    def children(self, request, *args, **kwargs):
        category = get_object_or_404(Category2, pk=kwargs.get('pk'))
        queryset = category.get_children()
        serializer = self.serializer_class(queryset, many=True)
        return Response(
            status=status.HTTP_200_OK,
            data=serializer.data
        )

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        if ('categories_data' in cache) and len(cache.get('categories_data')) > 0:
            data = cache.get('categories_data')
            # print(data)
            return Response(data)
        else:
            data = serializer.data
            cache.set('categories_data', data)
            return Response(data)


class CategoryPromViewSet(viewsets.ModelViewSet):
    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = CategoryPromSerializer
    http_method_names = ['get', ]
    queryset = Category_prom_x.objects.root_nodes()
    pagination_class = None

    @action(detail=False, methods=['get'], serializer_class=CategoryPromListSerializer)
    def first_level(self, request, *args, **kwargs):
        queryset = Category_prom_x.objects.filter(
            level=0
        )
        serializer = self.serializer_class(queryset, many=True)
        return Response(
            status=status.HTTP_200_OK,
            data=serializer.data
        )

    @action(detail=True, methods=['get'], serializer_class=CategoryPromListSerializer)
    def children(self, request, *args, **kwargs):
        category = get_object_or_404(Category_prom_x, pk=kwargs.get('pk'))
        queryset = category.get_children()
        serializer = self.serializer_class(queryset, many=True)
        return Response(
            status=status.HTTP_200_OK,
            data=serializer.data
        )

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())
        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)
        serializer = self.get_serializer(queryset, many=True)
        if ('prom_categories_data' in cache) and len(cache.get('prom_categories_data')) > 0:
            data = cache.get('prom_categories_data')
            # return Response(data)
            return Response(None)
        else:
            data = serializer.data
            cache.set('prom_categories_data', data)
            # return Response(data)
            return Response(None)


class PagePagination(PageNumberPagination):
    page_size_query_param = 'page_size'


class ProductContractorViewSet(viewsets.ModelViewSet):
    """
    Продукты поставщика
    """
    parser_classes = (MultiPartParser, CamelCaseJSONParser,)
    permission_classes = (IsAuthenticated,)
    # permission_classes = (AllowAny, )
    serializer_class = ProductSerializer
    http_method_names = ['get', 'post', 'put', 'patch', 'delete', ]
    filter_backends = (filters.DjangoFilterBackend,)
    filterset_class = ProductFilter
    pagination_class = PagePagination

    def update(self, request, *args, **kwargs):
        print('8888888888888888888888889')
        print(request.data)

        print(kwargs['pk'])
        current_product = Product.objects.get(id=kwargs['pk'])
        if current_product.price != request.data['price']:
            current_price = current_product.price
            if current_price > Decimal(request.data['price']):
                print('xxxxxxxxxxxxxxxxxxx')
                status = PriceStatus(status=1)
                status.save()
                current_product.price_status = status
                # print(Product.objects.get(id=kwargs['pk']).price_status.created)
                # request.data['price_status']['id'] = status.id
            elif current_price < Decimal(request.data['price']):
                print('yyyyyyyyyyyyyyyyyyy')
                status = PriceStatus(status=-1)
                status.save()
                print('---------')
                current_product.price_status = status
                # print(Product.objects.get(id=kwargs['pk']).price_status)
                # request.data['price_status']['id'] = status.id
        else:
            status = PriceStatus(status=0)
            status.save()

        current_product.save()
        return super().update(self.request, *args, **kwargs)

    def create(self, request, *args, **kwargs):
        # request.data['user'] = 55  # виконується при методі PATCH
        # print(self.request.user.id)
        # print(request.data['user'])
        self.request.data['price'] = self.request.data['clean_price']
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    def get_queryset(self):
        return Product.products_by_contractors.filter(
            user=self.request.user
        ).prefetch_related(
            Prefetch('product_image_urls', queryset=ProductImageURL.objects.order_by('-id')),
            Prefetch('product_images', queryset=ProductImage.objects.order_by('-id'))

        )

    def get_serializer_class(self):
        if self.action in ('list', 'retrieve'):
            return ProductCategoryObjectSerializer
        return ProductSerializer

    @action(detail=False, methods=['get'], serializer_class=CategoryListSerializer)
    def contractor_categories(self, request, *args, **kwargs):
        queryset = Category2.objects.filter(
            product__in=self.get_queryset()
        ).get_ancestors(include_self=True)
        serializer = self.serializer_class(queryset, many=True)
        data = group_vals(serializer.data)
        return Response(
            status=status.HTTP_200_OK,
            data=data
        )

    @action(detail=False, methods=['get'], serializer_class=ProductUploadHistorySerializer, filterset_class=None)
    def upload_history(self, request, *args, **kwargs):
        queryset = ProductUploadHistory.objects.filter(
            user=self.request.user
        )
        serializer = self.serializer_class(queryset, many=True)
        try:
            serializer.save()
        except AssertionError:
            print("Except AssertionError")
        return Response(
            status=status.HTTP_200_OK,
            data=serializer.data
        )

    @action(detail=False, methods=['post'], serializer_class=ProductListIdSerializer)
    def delete_list_of_products(self, request, *args, **kwargs):
        product_list_id = request.data.get('product_list_ids', None)
        self.get_queryset().filter(id__in=product_list_id).delete()
        self.request.user.available_products_count += len(product_list_id)
        self.request.user.save()
        return Response(
            status=status.HTTP_200_OK,
        )

    def destroy(self, request, *args, **kwargs):
        self.request.user.available_products_count += 1
        self.request.user.save()
        return super().destroy(request, *args, **kwargs)


from rest_framework import mixins, permissions
from rest_framework.viewsets import GenericViewSet


class ProductPartnerViewSet(viewsets.ModelViewSet):
    """
    Продукты продавца
    """
    parser_classes = (MultiPartParser, CamelCaseJSONParser,)
    permission_classes = (IsAuthenticated,)
    serializer_class = ProductSerializer
    http_method_names = ['get', 'post', 'put', 'patch', 'delete', ]
    filter_backends = (filters.DjangoFilterBackend,)
    filterset_class = ProductFilter
    pagination_class = PagePagination

    def get_queryset(self):
        print(97)
        if self.action == 'products_by_contractors':
            partner_products = Product.products_by_partners.filter(
                user=self.request.user,
                contractor_product__isnull=False,
            ).values_list('contractor_product__id', flat=True)

            # return Product.products_by_contractors.exclude(user__verified=False).exclude(id__in=[id_ for id_ in partner_products if id_])
            return Product.products_by_contractors.exclude(user__verified=False).exclude(
                id__in=partner_products
            ).exclude(price=0)

        elif self.action in ['my_site_products', 'update_mysite_products']:
            print(91)
            mysite = get_object_or_404(MyStore, user=self.request.user)
            return mysite.products.all()
        # print("nameeeeeeeee")
        # k = Product.products_by_partners.filter(user=self.request.user)
        #
        # for l in k.last().product_images.all():
        #     print(l.image)
        # print(f.image)
        # pass
        return Product.products_by_partners.filter(
            user=self.request.user,
        )

    def update(self, request, *args, **kwargs):
        try:
            request.data['contractor_product'] = request.data['contractor_product'][
                'id']  # виконується при методі PATCH
            # images  = request.data['cover_images']

            # def filter_images(img):
            #     try:
            #         img['image_decoded']
            #         return img
            #     except Exception as e:
            #
            #         pass
            # without_urls  = [x for x in filter( filter_images , images)]
            # just_urls = [url for url in images if not url.get('image_decoded' , None)]
            # new_d = {}
            # print(request.data)
            # for elem in without_urls:
            #     new_d['image_decoded'] = elem
            # request.data['cover_images']= without_urls

            # print('***********************************************************')
            # print("len(request.data['cover_images])")
            # print("len(request.data['cover_images])")
        except Exception as e:
            print(type(e), e)
            print('NoneType exept')
        # kwargs.update(partial=True)
        return super().update(self.request, *args, **kwargs)
        # print('returned' , returned)
        # # current_yml = YMLTemplate.objects.filter(user=request.user)
        # #
        # # for yml in current_yml:
        # #     if yml.yml_type == 'rozetka':
        # #         products_in_yml = yml.products.all()
        # #         # self.update_yml_rozetka(request=request, target_yml=yml, products=products_in_yml)
        # return returned

    @staticmethod
    def update_yml_rozetka(request, target_yml, products):
        category_dict = products.values('category__pk', 'category__name')
        context = {
            'categories': category_dict.distinct('category__pk').order_by('category__pk'),
            'products': products,
            'current_datetime': datetime.datetime.now().strftime('%Y-%m-%d %H:%M'),
            'company': Company.objects.get(user=request.user),
            'base_url': 'example.com',
            'domain': get_host_name(),
        }
        content = render_to_response('rozetka.xml', context).content
        file_name = 'rozetka_products-{}.xml'
        target_yml.template.delete(save=True)
        target_yml.template.save(file_name.format(Company.objects.get(user=request.user).user_id),
                                 ContentFile(content),
                                 save=True)

    def get_serializer_class(self):
        print(555556)
        if self.action in ('list', 'retrieve',):
            print(2)
            print("self.action in ('list', 'retrieve', )")
            return ProductCategoryObjectPartnerSerializer
        elif self.action in ('products_by_contractors',):
            return ProductCategoryObjectSerializer
        elif self.action in ('set_brand_to_products',):
            return ProductChangeBrandSerializer
        elif self.action in ('delete_list_of_products', 'add_to_mysite'):
            return ProductListIdSerializer
        elif self.action == 'copy_to_my_products':
            return ProductsCountSerializer
        elif self.action in ('partial_update',):
            self.request.data['clean_price'] = 0
            print(self.action, 'elif!!!!!!!!!!!' * 10)
            return ProductSerializer
        # self.request.data['contractor_product'] = self.request.data['contractor_product']['id']
        return ProductPartnerSerializer

    @action(detail=False, methods=['get'], serializer_class=CategoryListSerializer)
    def partner_categories(self, request, *args, **kwargs):
        print(555556)
        queryset = Category2.objects.filter(
            product__in=self.get_queryset()
        ).get_ancestors(include_self=True)
        serializer = self.serializer_class(queryset, many=True)
        data = group_vals(serializer.data)
        # print(696969)
        # print(data)
        return Response(
            status=status.HTTP_200_OK,
            data=data
        )

    @action(detail=False, methods=['post'], serializer_class=ProductListIdSerializer)
    def copy_to_my_products(self, request, **kwargs):
        print(request.data)
        print(555557)
        with transaction.atomic():
            for product in request.data['products']:
                prod_id = product.get('id')
                new_partner_product = get_object_or_404(Product, pk=prod_id)
                new_partner_product.id = None
                new_partner_product.count = product['count']
                # new_partner_product.price_status = new_partner_product.price_status
                contractor_percent_for_partners = new_partner_product.user.percent_for_partners
                new_partner_product.user = self.request.user  # сюди вхуярити наценку
                new_partner_product.price = new_partner_product.price * Decimal(
                    contractor_percent_for_partners * 0.01 + 1 if contractor_percent_for_partners else 1
                ) if new_partner_product.price else new_partner_product.price

                new_partner_product.recommended_price = new_partner_product.recommended_price * Decimal(
                    contractor_percent_for_partners * 0.01 + 1 if contractor_percent_for_partners else 1
                ) if new_partner_product.recommended_price else new_partner_product.recommended_price
                new_partner_product.contractor_product_id = prod_id

                contractor_prod = Product.objects.get(pk=prod_id)
                contractor_imgs = contractor_prod.product_images.all()
                new_partner_product.price = new_partner_product.price + (
                        new_partner_product.price * CustomUser.objects.get(
                    pk=new_partner_product.user.id).markup / 100)
                try:
                    new_partner_product.save()
                except IntegrityError as e:
                    if 'unique constraint' in e.args[0]:
                        return Response(
                            status=status.HTTP_409_CONFLICT,
                        )

                if contractor_imgs:
                    for img in contractor_imgs:
                        old_img = deepcopy(img)
                        img.id = None
                        img.product_id = new_partner_product.id
                        try:
                            picture_copy = ContentFile(img.image.read())
                            new_picture_name = str(uuid.uuid4()) + '.' + img.image.name.split('/')[-1].split('.')[1]
                            img.image.save(new_picture_name, picture_copy)
                            img.save()
                        except Exception as e:
                            print(type(e), e)
                        if old_img.image.name.split('/')[-1] == get_file_name_without_domain_and_signature(
                                contractor_prod.avatar_url):
                            new_partner_product.avatar_url = img.image.url

                contractor_options = contractor_prod.product_options.all()
                if contractor_options:
                    for option in contractor_options:
                        option.id = None
                        option.product = new_partner_product
                        option.save()

                contractor_text_area_options = contractor_prod.product_by_textarea_options.all()
                if contractor_text_area_options:
                    for option in contractor_text_area_options:
                        option.id = None
                        option.product = new_partner_product
                        option.save()

                contractor_manual_options = contractor_prod.product_manual_options.all()
                if contractor_manual_options:
                    for option in contractor_manual_options:
                        option.id = None
                        option.product = new_partner_product
                        option.save()

                contractor_urls = contractor_prod.product_image_urls.all()
                if contractor_urls:
                    for url in contractor_urls:
                        url.id = None
                        url.product_id = new_partner_product.id
                        url.save()

                new_partner_product.save()
        return Response(
            status=status.HTTP_201_CREATED,
        )

    @action(detail=False, methods=['get'], filterset_class=ProductFilter)
    def products_by_contractors(self, request, *args, **kwargs):
        print(8)
        return self.list(request, *args, **kwargs)

    @action(detail=False, methods=['post'], serializer_class=ProductListIdSerializer)
    def delete_list_of_products(self, request, *args, **kwargs):
        print(9)
        product_list_id = request.data.get('product_list_ids', None)
        enabled_marketplace = ['rozetka', 'prom']
        filename = '{type_yml}_products-{id}.xml'
        try:
            files = [YmlHandler(filename.format(type_yml=market, id=self.request.user.id))
                     for market in enabled_marketplace]

        except Exception as e:
            print(type(e), e)
        delete_product_count = 0
        for product_id in product_list_id:
            # check product exists in all ymltemplate
            for file in files:
                if file.exist_product(int(product_id)):
                    # if file.exist_product(product_id):
                    file.delete_product([str(product_id)])
                    file.update_categories()
            # # if True in [file.exist_product(product_id) for file in files]:
            # #
            # #     continue
            #     else:
            delete_product_count += 1
            self.get_queryset().get(id=product_id).delete()
        self.request.user.available_products_count += len(product_list_id) - delete_product_count
        self.request.user.save()
        # product_list_id = request.data.get('product_list_ids', None)
        # self.get_queryset().filter(id__in=product_list_id).delete()
        return Response(
            status=status.HTTP_200_OK,
        )

    @action(detail=False, methods=['post'])
    def set_brand_to_products(self, request, *args, **kwargs):
        print(10)
        Product.objects.filter(
            user=request.user,
            id__in=request.data.get('product_ids', None)
        ).update(brand=request.data.get('brand', None))
        return Response(status=200)

    @action(detail=False, methods=['get'], filterset_class=ProductFilter,
            serializer_class=ProductCategoryObjectSerializer)
    def my_site_products(self, request, *args, **kwargs):
        print(11)
        return self.list(request, *args, **kwargs)

    @action(detail=False, methods=['post'], serializer_class=ProductListIdSerializer)
    def add_to_mysite(self, request, *args, **kwargs):
        print(12)
        mysite = get_object_or_404(MyStore, user=self.request.user)
        mysite_product_ids = mysite.products.all().values_list('id', flat=True)
        product_list_ids = self.request.data.get('product_list_ids', [])
        products = self.get_queryset().filter(id__in=product_list_ids).exclude(id__in=mysite_product_ids)
        try:
            mysite.products.add(*products)
        except:
            raise ValueError
        return Response(status=status.HTTP_200_OK)

    @action(detail=False, methods=['post'], serializer_class=ProductListIdSerializer)
    def remove_from_mysite(self, *args, **kwargs):
        print(13)
        product_list_ids = self.request.data.get('product_list_ids', [])
        removed_proudcts = self.get_queryset().filter(id__in=product_list_ids)
        mysite = get_object_or_404(MyStore, user=self.request.user)
        mysite.products.remove(*removed_proudcts)
        return Response(status=status.HTTP_200_OK)

    @action(detail=False, methods=['get'])
    def update_mysite_products(self, request, *args, **kwargs):
        print(14)
        product_ids = list(self.get_queryset().values_list('id', flat=True))
        update_products_with_categories(product_ids, 'test')
        return Response(status=status.HTTP_200_OK)


class ProductsInYMLView(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated,)
    serializer_class = ProductSerializer
    # filter_backends = (filters.DjangoFilterBackend,)
    # filterset_class = ProductFilter
    http_method_names = ['get', 'post', 'put', 'patch', 'delete', ]
    pagination_class = PagePagination
    filter_backends = (filters.DjangoFilterBackend,)
    filterset_class = ProductFilter

    # queryset = None
    # #
    def get_queryset(self):
        print(self.request.GET.get('type_yml'))
        products_by_type_yml = YMLTemplate.objects.filter(user=self.request.user)
        for yml in products_by_type_yml:
            if yml.yml_type == self.request.GET.get('type_yml'):
                return yml.products.all()

    def list(self, request, *args, **kwargs):
        products_by_type_yml = YMLTemplate.objects.filter(user=self.request.user)
        for yml in products_by_type_yml:
            if yml.yml_type == request.GET.get('type_yml'):
                serializer = self.get_serializer(self.get_queryset(), many=True)
                return Response(status=status.HTTP_200_OK, data=serializer.data)
        else:
            return Response(status=status.HTTP_404_NOT_FOUND, data={
                'detail': 'Product in selected type does not exists'
            })


class ProductUpdateInYml(UpdateAPIView):
    serializer_class = ProductYmlSerializer
    permission_classes = (permissions.IsAuthenticated,)
    queryset = Product.objects.all()

    def update(self, request, *args, **kwargs):
        instance = self.get_object()
        for i, img in enumerate(request.data['cover_images']):
            try:
                # base64.b64decode(img['image_decoded'], validate=True)
                format, imgstr = img['image_decoded'].split(';base64,')
                ext = format.split('/')[-1]
                data = ContentFile(base64.b64decode(imgstr), name='temp33.' + ext)
                img_save_path = 'catalog/products/images/cover_image.' + ext
                path = default_storage.save(img_save_path, data)
                # print(default_storage.exists(path))
                # print(path)
                request.data['cover_images'][i] \
                    ['image_decoded'] = get_host_name() + '/media/{}'.format(
                    path)
            except Exception as e:
                print("no correct base64")

        type_yml = kwargs.get('type_yml')
        partner_price = request.data.get('partner_percent', None)
        product_id = request.data.get('id', None)
        price_info, created = YmlPriceInfo.objects.update_or_create(
            user=self.request.user,
            product_id=product_id,
            type_yml=type_yml,
            defaults={
                'percent': partner_price
            }
        )
        d = request.data
        for key in d:
            if key == 'price':
                try:
                    request.data[key] = float(d[key].replace(',', '.'))
                except Exception as e:
                    print(type(e), e)
        copy_data = deepcopy(request.data)
        copy_data['price'] = request.data['selling_price']
        try:
            # request.data['price'] = request.data['selling_price']
            serializer = ProductYmlSerializer(instance=instance, data=copy_data)
            filename = '{type_yml}_products-{id}.xml'.format(type_yml=type_yml, id=self.request.user.id)
            file = YmlHandler(filepath=filename)

            instance = self.get_object()
            serializer.is_valid(raise_exception=True)
            if file.exist_product(instance.id):
                file.update_element_by_id(id=instance.id, data=serializer.validated_data, user=self.request.user)
                file.update_categories()
                file.update_options(request.data['manual_options'], instance.id, self.request.user) # дядь якщо в тебе появились ще якісь параметри крім
                                                                                                    # мануальних можеш їх добавити нижче
            else:
                return Response(data={
                    'error': 'not found in file'
                })
            return Response(status=status.HTTP_200_OK, data={
                'host': get_host_name()
            })
        except Exception as e:
            print(type(e), e)
            return Response(data={
                'error': str(e)
            })


from django.urls import reverse


class ProductGetFromYml(ListAPIView):

    def build_paginate_url(self, page, page_size, req, base_paginate_url, check_next_page):
        next_page = ''
        prev_page = ''
        if page:
            next_page = base_paginate_url + '?page={}'.format(page + 1)
            prev_page = base_paginate_url + '?page={}'.format(page - 1) if page != 1 else None
        if page_size:
            next_page += '&page_size={}'.format(page_size)
            if prev_page:
                prev_page += '&page_size={}'.format(page_size)

        protocol = 'https://' if req.is_secure() else 'http://'
        host = req.META['HTTP_HOST']
        next_page = protocol + host + next_page if next_page else None
        prev_page = protocol + host + prev_page if prev_page else None
        # print(check_next_page)
        return {
            'next': next_page if check_next_page else None,
            'previous': prev_page,
        }

    def get(self, request, token=None, format=None, *args, **kwargs):

        # print('args ProductGetFromYml', args)
        # print('kwargs ProductGetFromYml', kwargs)
        page = request.query_params.get('page', 1)
        page_size = request.query_params.get('page_size', 10)
        type_yml = kwargs.get('type_yml', None)
        base_paginate_url = reverse('catalog:get_from_yml', kwargs={
            'type_yml': type_yml,
            'version': 'v1'
        })

        print('page', page)
        page = int(page)
        page_size = int(page_size)

        if type_yml:
            filename = '{type_yml}_products-{id}.xml'.format(type_yml=type_yml, id=self.request.user.id)
            try:
                file = YmlHandler(filepath=filename)

                if page == 1:
                    from_index = 0
                    to_index = int(page_size)
                else:
                    to_index = page * page_size
                    from_index = to_index - page_size
                products = file.convert_from_file_to_response(self.request.user, from_index, to_index)
                # print("from_index , to_index ", from_index, to_index)
                # results = products[from_index:to_index]
                results = products
                paginate_url = self.build_paginate_url(page=page, page_size=page_size, req=request,
                                                       base_paginate_url=base_paginate_url, check_next_page=results)
                data = {'count': products['len'],
                        'results': products['results'],
                        }
                data.update(**paginate_url)
                return Response(data=data)
            except Exception as e:
                return Response(data={
                    'error': str(e),
                    'results': []
                })


class YMLHandlerViewSet(viewsets.ModelViewSet):
    serializer_class = YMLHandlerSerializer
    permission_classes = (IsPartner,)
    lookup_field = 'yml_type'
    http_method_names = ('get', 'post', 'delete',)

    def get_queryset(self):
        if self.action == 'my_rozetka_yml_products':
            print('if!!!!!!')
            yml = get_object_or_404(YMLTemplate, user=self.request.user)
            return yml.products.all()
        print('else!!!!!')
        return YMLTemplate.objects.filter(user=self.request.user)

    def perform_create(self, serializer):
        print('sdaasd')
        serializer.save(user=self.request.user)

    @action(methods=['POST', ], detail=False)
    def delete_products_from_yml(self, request, *args, **kwargs):
        print('smth call!!!')
        serializer = self.serializer_class(data=request.data)

        user = self.request.user
        data = self.request.data
        type_yml = data.get('yml_type', None)
        print('type_yml ' , type_yml)
        products_ids = data.get('selected_ids', None)
        filename = '{type_yml}_products-{id}.xml'.format(type_yml=type_yml, id=user.id)
        file = YmlHandler(filepath=filename)
        file.delete_product(offer_ids=products_ids)
        products = file.convert_from_file_to_response(self.request.user, 0, 10)
        print(user, data, products_ids)
        # products_list = Product.products_by_partners.filter(
        #     user=user,
        #     id__in=data.get('product_ids', [])
        # )
        # yml_template, _ = user.ymltemplate_set.get_or_create(
        #     yml_type=data['yml_type'],
        # )
        # yml_template.products.remove(*products_list)
        #
        # serializer.render_yml_file(
        #     yml_template,
        #     user.user_company,
        #     yml_template.products.all()
        # )
        # return_serializer = self.serializer_class(yml_template)
        # return Response(status=status.HTTP_200_OK, data=return_serializer.data)

        return Response(status=status.HTTP_206_PARTIAL_CONTENT, data={
            'count': products['len'],
            'results': products['results']
        })

    @action(methods=['get', ], detail=False, serializer_class=ProductCategoryObjectPartnerSerializer)
    def my_rozetka_yml_products(self, request, *args, **kwargs):
        return super().list(request, *args, **kwargs)


class ProductImportViewSet(viewsets.ModelViewSet):
    parser_classes = (parsers.MultiPartParser, parsers.FormParser,)
    queryset = ProductUploadHistory.objects.all()
    serializer_class = ProductUploadHistorySerializer
    http_method_names = ('post',)
    permission_classes = (permissions.IsAuthenticated,)

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class OptionsByCategoryView(ListAPIView):
    serializer_class = CategoryOptionGroupSerializer
    permission_classes = (AllowAny,)
    pagination_class = None

    def get_queryset(self):
        return CategoryOptionGroup.objects.filter(
            category_id=self.kwargs.get('category_id'),
            group__group_type=ProductOptionTypes.CHOICE
        )


class OptionsByCategoryPromView(ListAPIView):
    serializer_class = CategoryPromOptionGroupSerializer
    permission_classes = (AllowAny,)
    pagination_class = None

    def get_queryset(self):
        return CategoryPromOptionGroup.objects.filter(
            category_id=self.kwargs.get('category_id'),
            group__group_type=ProductOptionTypes.CHOICE
        )


class OptionByCategoryTextAreaView(ListAPIView):
    serializer_class = CategoryOptionTextAreaSerializer
    permission_classes = (AllowAny,)
    pagination_class = None

    def get_queryset(self):
        return CategoryOptionGroup.objects.filter(
            category_id=self.kwargs.get('category_id'),
            group__group_type=ProductOptionTypes.TEXT_AREA
        )


class OptionByCategoryPromTextAreaView(ListAPIView):
    serializer_class = CategoryPromOptionTextAreaSerializer
    permission_classes = (AllowAny,)
    pagination_class = None

    def get_queryset(self):
        return CategoryPromOptionGroup.objects.filter(
            category_id=self.kwargs.get('category_id'),
            group__group_type=ProductOptionTypes.TEXT_AREA
        )


class LoadOptionView(APIView):
    serializer_class = RozetkaTokenSerializer
    permission_classes = (permissions.AllowAny,)

    def post(self, *args, **kwargs):
        rozetka_data = self.request.data
        data = {
            "token": rozetka_data['token']
        }
        upload_category_options.delay(**data)
        return Response(status=status.HTTP_200_OK)


class HostNameView(APIView):
    def get(self, *args, **kwargs):
        return Response(data={'host': settings.HOST_NAME})


import json
from django.core import serializers


class Mass_update(generics.UpdateAPIView):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer

    def update(self, request, *args, **kwargs):
        product_ids = [elem['id'] for elem in request.data]
        data = request.data
        manual_options = None
        new_data = None
        # print(data)
        for el in data:
            print(el)

            ######################## це стрелочки #####################
            current_product = Product.objects.get(id=el['id'])
            # print(55)
            print(el['price'])
            print(current_product.price)
            if current_product.price != el['price']:
                current_price = current_product.price
                if current_price > Decimal(el['price']):
                    print('xxxxxxxxxxxxxxxxxxx')
                    status2 = PriceStatus(status=1)
                    status2.save()
                    current_product.price_status = status2
                    # print(Product.objects.get(id=kwargs['pk']).price_status.created)
                    # request.data['price_status']['id'] = status.id
                elif current_price < Decimal(el['price']):
                    print('yyyyyyyyyyyyyyyyyyy')
                    status2 = PriceStatus(status=-1)
                    status2.save()
                    print('---------')
                    current_product.price_status = status2
                    # print(Product.objects.get(id=kwargs['pk']).price_status)
                    # request.data['price_status']['id'] = status.id
            # else:
            #     status = PriceStatus(status=0)
            #     status.save()
            current_product.save()
            # ######################################


        for elem in data:

            # print(elem)
            elem.pop('id')
            try:
                manual_options = elem.pop('manual_options')
            except Exception as e:
                print(type(e), e)
            new_data = elem
            break
        try:
            category = Category2.objects.get(id=new_data['category'])
            new_data['category'] = category
        except Exception as e:
            print(type(e), e)
        all_products_for_update = Product.objects.filter(id__in=product_ids)
        all_products_for_update.update(**new_data)

        ### start update manual option

        if manual_options:
            for product in all_products_for_update:
                for manual in manual_options:
                    if manual.get('id'):
                        product_manual_options = ProductOptionManualInput.objects.get(pk=manual['id'])
                        product_manual_options.value = manual['value']
                        product_manual_options.group = manual['group']
                        product_manual_options.save()
                    else:
                        ProductOptionManualInput.objects.create(
                            product=product, value=manual['value'], group=manual["group"]
                        )

        ### end update manual option

        products = Product.objects.filter(id__in=product_ids)
        response = json.loads(serializers.serialize('json', products))
        response_to_json = []
        for elem in response:


            # print(elem)
            product_to_json = {}
            product_to_json['id'] = elem['pk']
            manual_options = ProductOptionManualInput.objects.filter(product_id=int(elem['pk']))
            for field in elem['fields']:
                try:
                    if field == 'category':
                        product_to_json['category'] = {
                            'id': elem['fields'][field],
                            'name': Category2.objects.get(pk=elem['fields'][field]).name
                        }
                    else:
                        product_to_json[field] = elem['fields'][field]
                except Exception as e:
                    print(e)
                    product_to_json[field] = elem['fields'][field]

            product_to_json['manualOptions'] = [{'id': manual.id,
                                                 'group': manual.group,
                                                 'value': manual.value}
                                                for manual in manual_options]
            try:
                product_to_json['selling_price'] = float(product_to_json['price']) + (
                        float(product_to_json['price']) * product_to_json['partner_percent'] / 100)
            except Exception as e:
                print(e)

            response_to_json.append(product_to_json)
        return Response(status=status.HTTP_200_OK, data={
            'results': response_to_json
        })


class DeleteImageProductView(viewsets.ModelViewSet):
    permission_classes = (AllowAny,)
    http_method_names = ('post', 'delete')
    filter_backends = (filters.DjangoFilterBackend,)

    def get_queryset(self):
        return ProductImage.objects.all()

    def get_serializer_class(self):
        if self.action == 'delete':
            return None
        if self.action == 'delete_image_from_product':
            return DeleteImageSerializer

    @action(methods=['POST'], detail=True)
    def delete_image_from_product(self, *args, **kwargs):
        try:
            list_ids_urls = self.request.data.pop('image_urls')
            list_ids_cover = self.request.data.pop('image_cover')
            product_id = kwargs.pop('pk')
            target_product = Product.objects.get(pk=product_id)
            cover_image_for_product = ProductImage.objects.filter(product=target_product, id__in=list_ids_cover)
            image_urls_for_product = ProductImageURL.objects.filter(product=target_product, id__in=list_ids_urls)
            if cover_image_for_product:
                cover_image_for_product.delete()
            if image_urls_for_product:
                image_urls_for_product.delete()
        except Exception as e:
            print(type(e), e)
        return Response(status=status.HTTP_200_OK)

    def destroy(self, request, *args, **kwargs):
        return Response(status=status.HTTP_200_OK)

    def post(self, request, *args, **kwargs):
        serializer = ProductYmlSerializer(data=request.data)
        return Response(status=status.HTTP_200_OK)


class CoptProductView(View):

    def get(self, request, *args, **kwargs):
        # print(kwargs['pk'])
        product = Product.objects.get(pk=kwargs['pk'])

        print(product.id)
        product.id = None
        product.save()
        # print(product.id)
        images = ProductImageURL.objects.filter(product_id=kwargs['pk'])

        for img in images:
            img.id = None
            img.product_id = product.id
            img.save()
        images2 = ProductImage.objects.filter(product_id=kwargs['pk'])
        for img in images2:
            img.id = None
            img.product_id = product.id
            img.save()

        man_opt = ProductOptionManualInput.objects.filter(product_id=kwargs['pk'])
        for opt in man_opt:
            opt.id = None
            opt.product_id = product.id
            opt.save()

        return Response(status=status.HTTP_200_OK)
