import json
import logging
import os
from django.conf import settings
import re
import sys
import urllib
from urllib.request import urlopen, Request

import requests
from django.core.cache import cache
from django.core.paginator import Paginator
from django.db import transaction
from celery import shared_task
from celery.utils.log import get_task_logger
from import_export.results import RowResult
from rest_framework import request

from buy_sell.settings import BASE_URL
from catalog.constants import ProductUploadFileTypes, ProductOptionTypes
from catalog.resources import ProductResource
from tablib import Dataset

from catalog.serializers import CategoryListWithParentSerializer, \
    ProductToMySiteSerializer, CategoryPromSerializer, Category2Serializer
from messages_client.constants import MessageSubjectType, MAIN_SERVICE_NAME
from messages_client.publisher import generate_pub_args, send_nats_message
from buy_sell.celery import app
from .models import ProductUploadHistory, Product, ProductImageURL, CategoryOptionGroup, \
    CategoryOptionGroupValue, OptionGroup, OptionValue, ProductOptionManualInput, Category_prom_x, Category2
from rest_framework.exceptions import ValidationError
from catalog.utils import get_rozetka_auth_token
import xlrd
from django.core.files.storage import default_storage
try:
    from lxml import etree
except ImportError:
    import xml.etree.ElementTree as etree

logger = get_task_logger(__name__)


# celery worker -A buy_sell --concurrency=2 --loglevel=INFO --beat -E

# @shared_task
# def update_database():
#     # print('v')
#     queryset = Product.objects.all()
#     # queryset = Product.objects.filter(product_manual_options__group__contains="Вес")
#     # queryset = Product.objects.filter(category__name="Спортивные товары")
#     for product in queryset:
#         print('v')
#         newstr = product.name
#         query_color = product.product_manual_options.all().filter(group="Цвет")
#         query_size = product.product_manual_options.all().filter(group="Размеры")
#         query_size2 = product.product_manual_options.all().filter(group="Размер")
#         for q in query_color:
#             # print(q.value)
#             color = q.value
#             if color:
#                 newstr = newstr.replace(color, "")
#         for q in query_size:
#             # print(q.value)
#             size = q.value
#             if size:
#                 newstr = newstr.replace(size, "")
#         for q in query_size2:
#             # print(q.value)
#             size = q.value
#             if size:
#                 newstr = newstr.replace(size, "")
#
#         if product.brand:
#             newstr = newstr.replace(product.brand, "")
#         product.created_type = newstr
#         product.save()


# @shared_task
# def update_cat():
#     with transaction.atomic():
#         # print(1) це робочий код
#         # cache.clear()
#         print('go')
#         queryset = Category_prom_x.objects.root_nodes()
#         serializer = CategoryPromSerializer(queryset, many=True)
#         cache.set('prom_categories_data', serializer.data)
#         print(1)
#         # Category2.load_categories()
#         # queryset = Category2.objects.root_nodes()
#         # serializer = Category2Serializer(queryset, many=True)
#         # cache.set('categories_data', serializer.data)




# @shared_task
# def update_database():
#     # print('v')
#     workbook = xlrd.open_workbook('catalog/xls.xls')
#     # print(workbook)
#     worksheet = workbook.sheet_by_index(0)
#     # c = CategoryProm(id=1, name='ll')
#     # c.save()
#     def readRows():
#         i = 0;
#         for rownum in range(worksheet.nrows):
#             i = i + 1
#             if i!=1:
#                 rows = worksheet.row_values(rownum)
#                 # Password = rows[2]
#                 # supID = rows[3]
#                 if rows[0]:
#                     name = rows[0]
#                     if rows[1]:
#                         name = rows[1]
#                         if rows[2]:
#                             name = rows[2]
#                             if rows[3]:
#                                 name = rows[3]
#                 value = int(rows[5])
#                 # print(name)
#                 # print(value)
#                 # break
#                 c = Category_prom_x(id=value, name=name)
#                 c.save()
#     readRows()
# print(worksheet.cell(1, 6).value)


@shared_task
def load_products_from_xls(**kwargs):
    # print(kwargs)
    dataset = Dataset()
    with transaction.atomic():
        prod_hist = ProductUploadHistory.objects.get(id=int(kwargs.get('instance_id')))
    try:
        file_path = default_storage.open(prod_hist.xls_file.name)
    except Exception as e:
        print(e)
        print("dont open file")
    if prod_hist.file_type == ProductUploadFileTypes.INNER:
        dataset.load(file_path.read(), 'xls')

        product_resource = ProductResource()
        # print(dataset.dict[1].get('product_image_urls'))
        result = product_resource.import_data(
            dataset=dataset,
            dry_run=True,
            user_id=prod_hist.user.id,
            # product_image_urls=
        )
        if len(dataset) > prod_hist.user.available_products_count:
            prod_hist.errors = _('У вас доступно {} свободных мест для товаров, но файл содержит {} товаров.'
                                 .format(prod_hist.user.available_products_count, len(dataset)))
            prod_hist.is_uploaded = True
            prod_hist.save()
        else:
            if not result.has_errors() and not result.has_validation_errors():
                with transaction.atomic():
                    product_resource.import_data(
                        dataset=dataset,
                        dry_run=False,
                        user_id=prod_hist.user.id,
                        use_transactions=True,
                        collect_failed_rows=True
                    )
                    prod_hist.total_products_count = len(dataset)
                    prod_hist.user.available_products_count -= prod_hist.total_products_count
                    prod_hist.user.save()
                    prod_hist.imported_products_count = result.totals.get(RowResult.IMPORT_TYPE_NEW)
                    prod_hist.is_uploaded = True
                    prod_hist.errors = 'No errors'
                    prod_hist.save()
            else:
                error = ''
                for i, row in result.row_errors():
                    for err in row:
                        error += '{} {}\n'.format(i, err.error)
                prod_hist.errors = error
                prod_hist.save()
    elif prod_hist.file_type == ProductUploadFileTypes.PROM:
        print(5555)
        upload_products_from_prom(**kwargs)
    elif prod_hist.file_type == ProductUploadFileTypes.ROZETKA:
        try:
            file_name, _ = urllib.request.urlretrieve(default_storage.url(prod_hist.xls_file.name))
            workbook = xlrd.open_workbook(file_name)
            sheet = workbook.sheet_by_index(0)
            if sheet.cell_value(rowx=0, colx=0) == 'ID товара в розетке':
                product_id_list = sheet.col_slice(0, start_rowx=1)
                list_with_zero = map(lambda x: int(x.value), product_id_list)
                result_tuple = tuple(filter(lambda x: x if x > 0 else None, list_with_zero))
            else:
                raise ValidationError(_('Invalid headers in file'))

            token_rozetka = get_rozetka_auth_token(prod_hist.user)
            if token_rozetka:
                if len(dataset) > prod_hist.user.available_products_count:
                    prod_hist.errors = _('У вас доступно {} свободных мест для товаров, но файл содержит {} товаров.'
                                         .format(prod_hist.user.available_products_count, len(dataset)))
                    prod_hist.is_uploaded = True
                    prod_hist.save()
                else:
                    count = 0
                    for product_id in result_tuple:
                        url = "https://api.seller.rozetka.com.ua/items/{product_id}" \
                              "?expand=sell_status,sold,status,description,description_ua" \
                              ",details,parent_category,status_available,group_item" \
                            .format(product_id=product_id)
                        headers = {
                            'Authorization': "Bearer {}".format(token_rozetka),
                            'cache-control': "no-cache"
                        }
                        r = requests.Request("GET", url, headers=headers)
                        prep = r.prepare()
                        s = requests.Session()
                        resp = s.send(prep)
                        r.encoding = 'utf-8'
                        data = resp.json()
                        if data['success']:
                            product = data['content']
                            try:
                                product_instance, created = Product.objects.update_or_create(
                                    rozetka_id=product.get('id'),
                                    user=prod_hist.user,
                                    defaults={
                                        'rozetka_id': product.get('id'),
                                        'user': prod_hist.user,
                                        'name': product.get('name') if product.get('name') else product.get('name_ua'),
                                        'vendor_code': int(product['article']) if product.get('article') else 0,
                                        'price': product.get('price'),
                                        'category_id': product.get('catalog_id'),
                                        'description': product.get('description')
                                        if product.get('description') else product.get('description_ua'),
                                    }
                                )
                                if created:
                                    count += 1

                                for photo in product['photo']:
                                    ProductImageURL.objects.update_or_create(
                                        product=product_instance,
                                        url=photo
                                    )
                            except Exception as e:
                                prod_hist.errors = str(e)
                        # time.sleep(0.7)
                    prod_hist.total_products_count = len(result_tuple)
                    prod_hist.user.available_products_count -= prod_hist.total_products_count
                    prod_hist.user.save()
                    prod_hist.imported_products_count = count
                    prod_hist.is_uploaded = True
                    prod_hist.save()
            else:
                prod_hist.errors = 'Неправильный логин и пароль с сервиса розетки.'
                prod_hist.is_uploaded = True
                prod_hist.save()
        except:
            prod_hist.errors = 'Неправильный формат файла. После импорта с розетки, пересохраните файл!'
            prod_hist.is_uploaded = True
            prod_hist.save()
    elif prod_hist.file_type == ProductUploadFileTypes.YML:
        upload_products_from_yml_task(kwargs.get('instance_id', None))
    elif prod_hist.file_type == ProductUploadFileTypes.YML_PROM:
        upload_products_from_yml_prom_task(kwargs.get('instance_id', None))


def upload_products_from_prom(**kwargs):
    prod_hist = ProductUploadHistory.objects.get(id=int(kwargs.get('instance_id')))
    token = prod_hist.user.token_prom
    # token = 'bc79040c68d221dccdcc996b470205acfeded9f0'
    headers = {'Authorization': 'Bearer {}'.format(token),
               'Content-type': 'application/json'}
    r = requests.get('https://my.prom.ua/api/v1/products/list', headers=headers)
    if r.status_code == 200:
        data = r.json()
        # print(data)
        count = 0
        for product in data['products']:
            # print(product['id'])
            # print(product.get('description'))
            try:
                product_instance, created = Product.objects.update_or_create(
                    prom_id=product['id'],
                    user=prod_hist.user,
                    defaults={
                        'prom_id': product.get('id'),
                        'user': prod_hist.user,
                        'name': product['name'],
                        'vendor_code': int(product['sku']) if product['sku'] else 0,
                        'price': product['price'],
                        # 'category_id': product['category']['id'],
                        'description': product['description'],
                        'avatar_url': product['main_image'],
                    }
                )
                if created:
                    count += 1

                for photo in product['images']:
                    ProductImageURL.objects.update_or_create(
                        product=product_instance,
                        url=photo
                    )
            except Exception as e:
                print(str(e))
            # prod_hist.total_products_count = len(result_tuple)
            prod_hist.user.available_products_count -= prod_hist.total_products_count
            prod_hist.user.save()
            prod_hist.imported_products_count = count
            prod_hist.is_uploaded = True
            prod_hist.save()


@app.task
def load_categories():
    with transaction.atomic():
        # Category2.load_categories()
        # print('load categories')
        # queryset = Category2.objects.root_nodes()
        # serializer = Category2Serializer(queryset, many=True)
        # cache.set('categories_data', serializer.data)
        pass


@app.task
def load_categories_prom():
    with transaction.atomic():
        queryset = Category_prom_x.objects.root_nodes()
        serializer = CategoryPromSerializer(queryset, many=True)
        cache.set('prom_categories_data', serializer.data)


@app.task
def upload_category_options(*args, **kwargs):
    logging.basicConfig(stream=sys.stdout, level=logging.INFO)
    category_ids = Category2.objects.all().values_list('id', flat=True)
    i = 1
    token = kwargs.get('token', None)
    for category_id in category_ids:
        logging.info('Category id = {}, number of iter = {}'.format(category_id, i))
        try:
            url = 'https://api.seller.rozetka.com.ua/market-categories/category-options?category_id={}'.format(category_id)
            headers = {
                'Authorization': "Bearer {}".format(token),
                'cache-control': "no-cache",
            }

            r = requests.Request("GET", url, headers=headers)
            prep = r.prepare()
            s = requests.Session()
            resp = s.send(prep)
            r.encoding = 'utf-8'
            data = resp.json()
            content = json.loads(data['content'])
            for option_dict in content:
                if option_dict.get('id', None) and option_dict.get('name', None) \
                        and option_dict.get('value_id', None) and option_dict.get('value_name', None) \
                        and option_dict.get('attr_type', None) in ['CheckBoxGroupValues', 'List', 'ListValues', 'ComboBox']:
                    option_group, _ = OptionGroup.objects.update_or_create(
                        id=option_dict.get('id', None),
                        defaults={
                            'name': option_dict.get('name', None),
                            # 'group_type': ProductOptionTypes.CHOICE
                        },
                    )
                    category_option_group, _ = CategoryOptionGroup.objects.update_or_create(
                        group=option_group,
                        category_id=category_id,
                    )

                    option_value, _ = OptionValue.objects.update_or_create(
                        id=option_dict.get('value_id', None),
                        defaults={
                            'name': re.sub("<.*?>", "", option_dict.get('value_name', None)),
                        },
                    )

                    CategoryOptionGroupValue.objects.update_or_create(
                        value=option_value,
                        category_group=category_option_group,
                    )
                elif option_dict.get('id', None) and option_dict.get('name', None) \
                        and option_dict.get('attr_type', None) in ['TextArea', ]:

                    option_group, _ = OptionGroup.objects.update_or_create(
                        id=option_dict.get('id', None),
                        defaults={
                            'name': option_dict.get('name', None),
                            'group_type': ProductOptionTypes.TEXT_AREA
                        },
                    )

                    category_option_group, _ = CategoryOptionGroup.objects.update_or_create(
                        group=option_group,
                        category_id=category_id,
                    )
            i += 1
        except Exception as e:
            print(e.args[0])


def generate_categories_tree_by_products(product_ids, subdomain):
    data = dict()
    category_ids = Product.objects.filter(id__in=product_ids).values_list('category_id', flat=True)
    categories = Category2.objects.filter(id__in=category_ids)
    categories_tree = Category2.objects.get_queryset_ancestors(queryset=categories, include_self=True)
    serializer = CategoryListWithParentSerializer(categories_tree, many=True)
    data['type'] = MessageSubjectType.UPDATE_CATEGORIES
    data['source'] = MAIN_SERVICE_NAME
    data['content'] = [dict(item) for item in serializer.data]
    args = generate_pub_args(subdomain, str(data))
    send_nats_message(args)
    return data['type'], data['source'], len(serializer.data)


def products_generator(products_ids, per_page):
    products = Product.objects.filter(id__in=products_ids)
    paginator = Paginator(products, per_page)
    for i in paginator.page_range:
        data = iter(paginator.get_page(i))
        serializer = ProductToMySiteSerializer(data, many=True)
        yield serializer.data


def send_product_to_my_site(content, subdomain):
    data = dict()
    data['type'] = MessageSubjectType.UPDATE_PRODUCTS
    data['source'] = MAIN_SERVICE_NAME
    data['content'] = content
    args = generate_pub_args(subdomain, str(data))
    send_nats_message(args)
    return data['type'], data['source'], len(data)


@app.task
def update_products_with_categories(product_ids, subdomain):
    generate_categories_tree_by_products(product_ids, subdomain)

    for part_content in products_generator(product_ids, 1):
        send_product_to_my_site(part_content, subdomain)


def upload_products_from_yml(upload_hist_id):
    update_products_with_categories(upload_hist_id)


def upload_products_from_yml_task(upload_hist_id):

            # print(data)
    if upload_hist_id:
        prod_hist = ProductUploadHistory.objects.get(id=upload_hist_id)
        try:
            with open(os.path.join(settings.MEDIA_ROOT, prod_hist.xls_file.name), 'r') as file:
                data = file.read()
            xml_categories = dict()
            root = etree.fromstring(data)
        except Exception as e:
            root = None
            prod_hist.errors = e.args
            prod_hist.is_uploaded = True
            prod_hist.errors = str(e)
            prod_hist.save()
            return prod_hist.errors

    try:
        for shop in root.getchildren():
            for elem in shop.getchildren():
                if elem.tag == 'categories':
                    for category in elem.getchildren():
                        xml_categories[category.get('id')] = str(category.text).strip()
                if elem.tag == 'offers':

                    with transaction.atomic():
                        product_count = 0
                        for offer in elem.getchildren():
                            name = xml_categories[offer.findtext('categoryId')]

                            category = Category2.objects.filter(name=name).first()

                            # print(name)
                            product_dict = dict()
                            product_dict['id'] = offer.get('id')
                            product_dict['price'] = offer.findtext('price')
                            product_dict['name'] = offer.findtext('name')
                            product_dict['brand'] = offer.findtext('vendor')
                            product_dict['count'] = offer.findtext('stock_quantity')
                            product_dict['category'] = category
                            product_dict['variety_type'] = offer.findtext('typePrefix')
                            product_dict['vendor_code'] = offer.findtext('vendorCode', default=offer.get('id'))
                            product_dict['description'] = offer.findtext('description')
                            product_dict['vendor_country'] = offer.findtext('vendor_of_origin')

                            product_params = [(item.get('name'), item.text) for item in offer.findall('param')] if offer.findall('param') else None
                            product_image_urls = [item.text for item in offer.findall('picture')]

                            if(len(product_dict['vendor_code'])) > 63:
                                product_dict['vendor_code'] = offer.get('id')
                                # print(offer.get('id'))
                                # return
                            newstr = product_dict['name']
                            if product_params:
                                for option in product_params:
                                    if option[0] == "Цвет":
                                        product_dict['color'] = option[1]
                                    if option[0] == "Размеры":
                                        product_dict['box_size'] = option[1]

                                for option in product_params:
                                    if option[1]:
                                        newstr = newstr.replace(option[1], "")
                            if product_dict['brand']:
                                # print(product_dict['brand'])
                                newstr = newstr.replace(product_dict['brand'], "")
                            # print(product_dict['brand'])
                            product_dict['created_type'] = newstr
                            instance, created = Product.objects.update_or_create(
                                user=prod_hist.user,
                                yml_id=product_dict.pop('id'),
                                defaults=product_dict
                            )
                            if not created:
                                instance.product_image_urls.all().delete()
                                instance.product_manual_options.all().delete()
                            if product_params:
                                instance.product_manual_options.bulk_create(
                                    ProductOptionManualInput(
                                        product=instance,
                                        group=option[0],
                                        value=option[1]
                                    ) for option in product_params
                                )
                            if product_image_urls:
                                instance.product_image_urls.bulk_create(
                                    ProductImageURL(
                                        product=instance,
                                        url=url
                                    ) for url in product_image_urls
                                )
                            product_count += 1
                        prod_hist.total_products_count = product_count
                        prod_hist.imported_products_count = product_count

    except Exception as e:
        if e.args[0]:
            prod_hist.errors = str(e) + BASE_URL + prod_hist.xls_file.url
        # return
    prod_hist.is_uploaded = True
    prod_hist.save()
    return prod_hist.errors or 'Complete!'


def upload_products_from_yml_prom_task(upload_hist_id):
    if upload_hist_id:
        prod_hist = ProductUploadHistory.objects.get(id=upload_hist_id)

        try:
            with open(os.path.join(settings.MEDIA_ROOT, prod_hist.xls_file.name), 'r') as file:
                data = file.read()
            xml_categories = dict()
            root = etree.fromstring(data)
        except Exception as e:
            root = None
            prod_hist.errors = e.args
            prod_hist.is_uploaded = True
            prod_hist.errors = str(e)
            prod_hist.save()
            return prod_hist.errors
        try:
            for shop in root.getchildren():
                for elem in shop.getchildren():
                    if elem.tag == 'categories':
                        for category in elem.getchildren():
                            xml_categories[category.get('id')] = str(category.text).strip()
                    if elem.tag == 'offers':
                        with transaction.atomic():
                            product_count = 0
                            for offer in elem.getchildren():
                                # name = xml_categories[offer.findtext('categoryId')]
                                name = offer.findtext('categoryId')
                                category = Category_prom_x.objects.filter(id=name).first()
                                # print(name)
                                product_dict = dict()
                                product_dict['id'] = offer.get('id')
                                product_dict['price'] = offer.findtext('price')
                                product_dict['name'] = offer.findtext('name')
                                product_dict['brand'] = offer.findtext('vendor')
                                product_dict['count'] = offer.findtext('stock_quantity')
                                product_dict['category_prom_x'] = category
                                # product_dict['category'] = offer.findtext('categoryId')
                                product_dict['variety_type'] = offer.findtext('typePrefix')
                                product_dict['vendor_code'] = offer.findtext('vendorCode', default=offer.get('id'))
                                product_dict['description'] = offer.findtext('description')
                                product_dict['vendor_country'] = offer.findtext('country_of_origin')
                                product_params = [(item.get('name'), item.text) for item in
                                                  offer.findall('param')] if offer.findall('param') else None
                                product_image_urls = [item.text for item in offer.findall('picture')]
                                instance, created = Product.objects.update_or_create(
                                    user=prod_hist.user,
                                    yml_id=product_dict.pop('id'),
                                    defaults=product_dict
                                )
                                if not created:
                                    instance.product_image_urls.all().delete()
                                    instance.product_manual_options.all().delete()

                                if product_params:
                                    instance.product_manual_options.bulk_create(
                                        ProductOptionManualInput(
                                            product=instance,
                                            group=option[0],
                                            value=option[1]
                                        ) for option in product_params
                                    )
                                if product_image_urls:
                                    instance.product_image_urls.bulk_create(
                                        ProductImageURL(
                                            product=instance,
                                            url=url
                                        ) for url in product_image_urls
                                    )
                                product_count += 1
                            prod_hist.total_products_count = product_count
                            prod_hist.imported_products_count = product_count
        except Exception as e:
            if e.args[0]:
                prod_hist.errors = str(e) + BASE_URL + prod_hist.xls_file.url
        prod_hist.is_uploaded = True
        prod_hist.save()
    return prod_hist.errors or 'Complete!'