from django.db import models


class ContractorProductManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().filter(user__role='CONTRACTOR')


class PartnerProductManager(models.Manager):
    def get_queryset(self):
        return super(PartnerProductManager, self).get_queryset().filter(user__role='PARTNER')
