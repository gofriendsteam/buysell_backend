from django.contrib import admin

from payments.models import DataFromWidget
from payments.serializers import WayForPaySerializer
from .models import Product, YMLTemplate, ProductUploadHistory, ProductImage, ProductImageURL, \
    OptionGroup, \
    ProductOption, ProductOptionValueText, ProductOptionManualInput, OptionValue, CategoryOptionGroup, \
    CategoryOptionGroupValue, Category_prom_x, Category2, PriceStatus
from import_export.admin import ImportExportActionModelAdmin
from catalog.resources import ProductResource

admin.site.register(Category_prom_x)
admin.site.register(Category2)
# admin.site.register(YMLTemplate)
admin.site.register(ProductUploadHistory)
admin.site.register(OptionGroup)
admin.site.register(CategoryOptionGroup)
admin.site.register(CategoryOptionGroupValue)
admin.site.register(ProductImage)
admin.site.register(OptionValue)
admin.site.register(ProductOptionValueText)
admin.site.register(ProductOption)
admin.site.register(PriceStatus)
admin.site.register(DataFromWidget)

# admin.site.register(ProductUploadHistory)

@admin.register(YMLTemplate)
class YMLTemplateAdmin(admin.ModelAdmin):
    # fields =
    exclude = [
        'products'
    ]
class ProductPartner(Product):
    class Meta:
        proxy = True
        verbose_name = 'Товар продавца'
        verbose_name_plural = 'Товары продавца'


class ProductImageTabular(admin.TabularInline):
    model = ProductImage
    exclude = (
        'id',
    )
    extra = 0


class ProductImageURLTabular(admin.TabularInline):
    model = ProductImageURL
    exclude = (
        'id',
    )
    extra = 0


class ProductOptionInline(admin.TabularInline):
    model = ProductOption
    exclude = (
        'id',
    )
    extra = 0

    def has_add_permission(self, request, obj=None):
        return False

    def has_change_permission(self, request, obj=None):
        return False


class ProductOptionValueTextInline(admin.TabularInline):
    model = ProductOptionValueText
    exclude = (
        'id',
    )
    extra = 0

    def has_add_permission(self, request, obj=None):
        return False

    def has_change_permission(self, request, obj=None):
        return False


class ProductOptionManualInputInline(admin.TabularInline):
    model = ProductOptionManualInput
    exclude = (
        'id',
    )
    extra = 0

    def has_add_permission(self, request, obj=None):
        return False

    def has_change_permission(self, request, obj=None):
        return False


@admin.register(Product)
class ProductAdmin(ImportExportActionModelAdmin):
    resource_class = ProductResource
    fields = (
        # 'id',
        'fixed_recommended_price',
        'category',
        'category_prom_x',
        'user',
        'price',
        'price_status',
        'weight',
        'volume',
        'avatar_url',
        'product_type',
        'brand',
        'name',
        'variety_type',
        'vendor_code',
        'warranty_duration',
        'vendor_country',
        'box_size',
        'count',
        'description',
        'extra_description',
        'age_group',
        'material',
        'created_type',
        'contractor_product',

    )
    inlines = (
        ProductImageTabular,
        ProductImageURLTabular,
        ProductOptionInline,
        ProductOptionValueTextInline,
        ProductOptionManualInputInline
    )


    list_display = (
        'id',
        'category',
        'category_prom_x',
        'name',
        'user',
        'vendor_code',
        'count',
        'price',
        'rozetka_id',
        'created',
        'updated',
    )

    def get_queryset(self, request):
        return self.model.objects.filter(contractor_product__isnull=True)


@admin.register(ProductPartner)
class ProductPartnerAdmin(ImportExportActionModelAdmin):
    resource_class = ProductResource
    inlines = (
        ProductImageTabular,
        ProductImageURLTabular,
        ProductOptionInline,
        ProductOptionValueTextInline,
        ProductOptionManualInputInline
    )

    fields = (
        'fixed_recommended_price',
        'category',
        'category_prom_x',
        'user',
        'contractor_product',
        'partner_price',
        'price_status',
        'avatar_url',
        'product_type',
        'brand',
        'name',
        'variety_type',
        'vendor_code',
        'warranty_duration',
        'vendor_country',
        'box_size',
        'count',
        'description',
        'extra_description',
        'age_group',
        'material',
    )

    list_display = (
        'id',
        'fixed_recommended_price',
        'category',
        'category_prom_x',
        'name',
        'user',
        'contractor_product',
        'vendor_code',
        'count',
        'price',
        'rozetka_id',
        'created',
        'updated',
    )

    readonly_fields = (
        'partner_price',
    )

    def get_queryset(self, request):
        return self.model.objects.filter(contractor_product__isnull=False)


