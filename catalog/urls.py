from django.urls import path

from catalog import views as catalog_view
from rest_framework.routers import DefaultRouter

from catalog.views import OptionsByCategoryView, LoadOptionView, HostNameView, OptionByCategoryTextAreaView, \
    OptionsByCategoryPromView, OptionByCategoryPromTextAreaView, ProductsInYMLView, Mass_update, DeleteImageProductView, \
    CoptProductView
from payments.views import DeleteAllRozetkaTrans

app_name = 'catalog'

router = DefaultRouter()
router.register(r'contractor_products', catalog_view.ProductContractorViewSet, base_name='contractor_products')
router.register(r'products_upload', catalog_view.ProductImportViewSet, base_name='product_uploads')
router.register(r'yml-handler', catalog_view.YMLHandlerViewSet, base_name='yml_handler')
router.register(r'partner_products', catalog_view.ProductPartnerViewSet, base_name='partner_products')
router.register(r'categories', catalog_view.CategoryViewSet, base_name='categories')
router.register(r'categories_prom', catalog_view.CategoryPromViewSet, base_name='categories_prom')
# router.register(r'update_product/<str:type_yml>', catalog_view.ProductUpdateInYml, base_name='update_product')
router.register(r'products_in_yml', catalog_view.ProductsInYMLView, base_name='products_in_yml')
router.register(r'delete_image' , DeleteImageProductView , base_name='delete_image')

urlpatterns = router.urls

urlpatterns += [
    path('options_by_category/<int:category_id>', OptionsByCategoryView.as_view(), name='options_by_category'),
    path('options_by_category_prom/<int:category_id>', OptionsByCategoryPromView.as_view(),
         name='options_by_category_prom'),
    path('options_by_category_with_text_area/<int:category_id>',
         OptionByCategoryTextAreaView.as_view(),
         name='options_with_text_area_by_category'),
    path('options_by_category_prom_with_text_area/<int:category_id>',
         OptionByCategoryPromTextAreaView.as_view(),
         name='options_with_text_area_by_category_prom'),
    path('load_options', LoadOptionView.as_view(), name='load_options'),
    path('host_name', HostNameView.as_view(), name='host_name'),
    path('destroy_rozetka_trans/', DeleteAllRozetkaTrans.as_view(), name='destroy'),
    path('update_product/<str:type_yml>/<int:pk>' , catalog_view.ProductUpdateInYml.as_view() , name='update_in_yml'),
    path('get_product_from_yml/<str:type_yml>' , catalog_view.ProductGetFromYml.as_view() , name='get_from_yml'),
    path('update_partner_product_mass/', Mass_update.as_view(), name='mass_update'),
    path('copy_prod/<int:pk>/', CoptProductView.as_view(), name='copy_prod'),
]
