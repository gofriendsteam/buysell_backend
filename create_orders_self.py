# start script -> python create_orders_self.py rozetka_order # для генерации

import os
import django
import random
import uuid
import sys
import datetime

# django setup
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "buy_sell.settings")
SECRET_KEY = os.environ.get('SECRET_KEY')
django.setup()

from orders.models import OrderSelf, OrderSelfStatusHistoryItem, Order, ContractorOrder
from orders.constants import NovaPoshtaCargoTypes, OrderStatuses
from catalog.models import Product
from users.models import CustomUser
from payments.models import PaymentTransaction

PARTNER = 'PARTNER'
CONTRACTOR = 'CONTRACTOR'


def create_order():
    all_users = CustomUser.objects.all()
    products_random = random.sample(list(Product.objects.all().filter(price__isnull=False)), 400)
    partners = all_users.filter(role='PARTNER')
    list_ids_contactor = [elem['id'] for elem in all_users.filter(role='CONTRACTOR').values('id')]
    new_order_list = []
    print('start generate order')
    for product in products_random:
        print('Working ...')
        data = {}
        data['product'] = product
        data['user'] = random.choice(partners)
        data['contractor_user_id'] = random.choice([list_ids_contactor[0]])
        data['full_name'] = random.choice(['Test account', 'Dima Belyaev', 'Anton', 'Ruslan'])
        count = random.randint(1, 3)
        total_price = round(count * product.price, 2)
        data['count'] = count
        data['total_price'] = total_price
        data['phone_number'] = '+380978458457'
        data['ref_id_poshta'] = str(uuid.uuid4())
        data['city_name'] = random.choice([
            'Киев',
            'Одесса',
            'Львов',
            'Днепр',
            'Чернигов',
            'Винница',
            'Ялта'
        ])
        data['post_office_name'] = 'Улица мира 165'
        data['ttn'] = None
        # data['ttn'] = ''.join(
        #     random
        #         .sample('asdasd-asdasdasfkjd-asdasd asdasd as d as d as d asjdkhasj dhjhas djjahsjdh ajsdhjh ajdh asd'
        #                 , 32)
        # )
        data['cargo_type'] = dict(NovaPoshtaCargoTypes.NOVA_POSHTA_CARGO_TYPES)[random.choice(['parcel',
                                                                                               'cargo',
                                                                                               'documents',
                                                                                               'tires_wheels',
                                                                                               'pallet'])]
        new_order = OrderSelf(**data)
        new_order.save()
        new_order_list.append(new_order)
    print('End generate order!')
    return new_order_list


def create_history(orders):
    print('Create history .....')
    for order in orders:
        for i in range(10):
            data_history = {}
            data_history['order'] = order
            data_history['status_id'] = random.randint(1, 5)
            if i == 9:
                data_history['status_id'] = 6
            history_item = OrderSelfStatusHistoryItem(**data_history)
            history_item.save()
    print('END.')


def generate_order_rozetka():
    print('Generate order for rozetka')
    new_orders = []
    contractor_orders = []
    all_users = CustomUser.objects.filter(role=PARTNER)
    all_users_contractor = CustomUser.objects.filter(role=CONTRACTOR)
    for i in range(200):

        random_par = random.choice(all_users)
        random_contractor = random.choice(all_users_contractor)
        all_product = Product.objects.all()[:10]
        for product in all_product:
            product.contractor_product = product
            product.partner_percent = random.randint(10, 30)
            product.save()
        products_qs = Product.objects.filter(contractor_product__isnull=False)
        if products_qs:
            all_products_contractor = list(products_qs)
            count_products = random.randrange(1, 7)
            products = random.sample(all_products_contractor, count_products)
            random_products_ids = [product.id for product in products]
            random_sum = sum([product.price for product in products])
            print(random_products_ids, random_sum)
            order_rozetka = Order(user=random_par,
                                  rozetka_id=int(''.join(random.sample('123456789', 6))),
                                  market_id=int(''.join(random.sample('123456789', 6))),
                                  created=datetime.datetime.now(),
                                  amount=random_sum,
                                  amount_with_discount=round(float(random_sum) * random.random(), 2),
                                  cost=random_sum,
                                  cost_with_discount=round(float(random_sum) * random.random(), 2),
                                  status=6,  # Доставлено
                                  status_group=2,  # Группа - успешные
                                  seller_comment_created=datetime.datetime.now(),
                                  current_seller_comment='current_seller_comment some comment',
                                  comment='some comment',
                                  user_phone='+38978548754',
                                  from_warehouse=1,
                                  ttn=str(uuid.uuid4())[:32],
                                  total_quantity=count_products,
                                  can_copy=False,
                                  created_type=3,  # OrderCreateTypes
                                  )

            order_rozetka.save()
            for id_ in products:
                order_rozetka.products.add(id_)
            order_rozetka.save()
            print('end add product to contractor order')
    return {
        'orders': new_orders,
        'contractor_orders': contractor_orders
    }


def payment_generate(orders):
    all_users = CustomUser.objects.filter(role=CONTRACTOR)
    random_contractor = random.choice(all_users)
    for order in orders:
        print('payment_generate')
        trans = PaymentTransaction(
            user=random_contractor,
            amount=order.order.amount,
            trans_type=1,
            source=random.randrange(1, 3),
            system_order=order,
            date_status=random.choice([True, False]),
            system_order_self=None
        )
        # trans.save()
        print(trans)
        pass
        #
        # print(random_contractor)
        # payment = PaymentTransaction(user=random_contractor)


def main():
    orders = create_order()
    create_history(orders=orders)


if __name__ == '__main__':
    try:

        if sys.argv[1] == 'payment':
            print('payment')
            payment_generate()
        elif sys.argv[1] == 'rozetka_order':
            rozetka_contactor_orders = generate_order_rozetka()
            # payment_generate(rozetka_contactor_orders['contractor_orders'])
        else:
            main()
    except Exception as e:
        print(type(e), e)